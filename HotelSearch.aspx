﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HotelSearch.aspx.cs" Inherits="CUTUK.HotelSearch" %>

<!DOCTYPE html>
<!--[if IE 8]>          <html class="ie ie8"> <![endif]-->
<!--[if IE 9]>          <html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->
<head>
    <!-- Page Title -->
    <title>Hotel Search</title>

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Travelo - Travel, Tour Booking HTML5 Template">
    <meta name="author" content="SoapTheme">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    <!-- Theme Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/animate.min.css">

    <!-- Current Page Styles -->
    <link rel="stylesheet" type="text/css" href="components/revolution_slider/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/revolution_slider/css/style.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/jquery.bxslider/jquery.bxslider.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/flexslider/flexslider.css" media="screen" />

    <!-- Main Style -->
    <link id="main-style" rel="stylesheet" href="css/style.css">

    <!-- Updated Styles -->
    <link rel="stylesheet" href="css/updates.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="css/custom.css">

    <!-- Responsive Styles -->
    <link rel="stylesheet" href="css/responsive.css">
    <link href="css/jquery-ui.css" rel="stylesheet" />
    <!-- CSS for IE -->
    <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie.css" />
    <![endif]-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->
    <script src="js/jquery-2.1.3.min.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/modal.js"></script>
    <script src="scripts/HotelSearch.js"></script>
    <script src="scripts/CountryCityCode.js?v=1.0"></script>
    <script src="scripts/login.js?v=1.0"></script>
    <script src="scripts/B2CCustomerLogin.js"></script>
    <script type="text/javascript">

        var chkinDate;
        var chkinMonth;
        var chkoutDate;
        var chkoutMonth;
        //tpj.noConflict();

        $(document).ready(function () {

            //.....................................................................................................//
            $("#txtCity").autocomplete({

                source: function (request, response) {

                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "../HotelHandler.asmx/GetDestinationCode",
                        data: "{'name':'" + document.getElementById('txtCity').value + "'}",
                        dataType: "json",
                        success: function (data) {

                            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            response(result);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    debugger;
                    $('#hdnDCode').val(ui.item.id);
                }
            });
            $("#txt_HotelName").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "../HotelHandler.asmx/GetHotel",
                        data: "{'name':'" + $('#txt_HotelName').val() + "','destination':'" + $('#hdnDCode').val() + "'}",
                        dataType: "json",
                        success: function (data) {
                            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            response(result);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    $('#hdnHCode').val(ui.item.id);
                }
            });
        });
    </script>




    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800,700,600' rel='stylesheet' type='text/css'>
    <style>
        section#content {
            min-height: 1000px;
            padding: 0;
            position: relative;
            overflow: hidden;
        }

        #main {
            padding-top: 200px;
        }

        .page-title, .page-description {
            color: #fff;
        }

        .page-title {
            font-size: 4.1667em;
            font-weight: bold;
        }

        .page-description {
            font-size: 2.5em;
            margin-bottom: 50px;
        }

        .featured {
            position: absolute;
            right: 50px;
            bottom: 50px;
            z-index: 9;
            margin-bottom: 0;
            text-align: right;
        }

            .featured figure a {
                border: 2px solid #fff;
            }

            .featured .details {
                margin-right: 10px;
            }

                .featured .details > * {
                    color: #fff;
                    line-height: 1.25em;
                    margin: 0;
                    font-weight: bold;
                    text-shadow: 2px -2px rgba(0, 0, 0, 0.2);
                }
    </style>
</head>
<body>
    <div id="page-wrapper">
        <header id="header" class="navbar-static-top style4">
            <div class="container">
                <h1 class="logo navbar-brand">
                    <a href="Default.aspx" title="Hamara Trip - home">
                        <img src="images/logo.png" alt="Hamara Trip logo" style="width: 115px; height: 100px;" />
                    </a>
                </h1>
                <div class="pull-right hidden-mobile">
                    <ul class="social-icons clearfix pull-right hidden-mobile">
                        <li class="twitter"><a title="twitter" href="#" data-toggle="tooltip"><i class="soap-icon-twitter"></i></a></li>
                        <li class="facebook"><a title="facebook" href="#" data-toggle="tooltip"><i class="soap-icon-facebook"></i></a></li>
                        <li class="linkedin"><a title="linkedin" href="#" data-toggle="tooltip"><i class="soap-icon-linkedin"></i></a></li>
                        <li class="vimeo"><a title="vimeo" href="#" data-toggle="tooltip"><i class="soap-icon-vimeo"></i></a></li>
                    </ul>
                </div>
                <h2 style="margin-top: 3%">Unisafe Tours</h2>
            </div>
            <a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle">Mobile Menu Toggle
            </a>

            <div class="main-navigation">
                <div class="container">
                    <nav id="main-menu" role="navigation">
                        <ul class="menu">
                            <li class="menu-item-has-children">
                                <a href="Default.aspx">Home</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="AboutUs.aspx">About Us</a>
                            </li>

                            <li class="menu-item-has-children">
                                <a href="HotelSearch.aspx">Hotels</a>
                            </li>

                            <li class="menu-item-has-children">
                                <a href="Packages.aspx">Packages</a>
                            </li>

                            <%--<li class="menu-item-has-children">
                                <a href="TourPackages.aspx">Tour Packages</a>
                            </li>--%>
                            <li class="menu-item-has-children">
                                <a href="Gallery.aspx">Gallery</a>
                            </li>

                            <li class="menu-item-has-children megamenu-menu">
                                <a href="CompanyCertifications.aspx">Company Certifications</a>
                            </li>

                            <li class="menu-item-has-children">
                                <a href="Payment.aspx">Payment</a>
                            </li>

                            <li class="menu-item-has-children">
                                <a href="CarBooking.aspx">Car Booking</a>
                            </li>

                            <li class="menu-item-has-children">
                                <a href="Contact.aspx">Contact Us</a>
                            </li>

                        </ul>
                        <ul class="menu pull-right">
                            <li><a href="#" onclick="SignUpModal()">login / Register</a></li>
                            <li><a href="http://b2b.Vacaaay.com/">B2B Login</a></li>
                        </ul>
                    </nav>
                </div>
            </div>

            <nav id="mobile-menu-01" class="mobile-menu collapse">
                <ul id="mobile-primary-menu" class="menu">
                    <li class="menu-item-has-children">
                        <a href="Default.aspx">Home</a>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="AboutUs.aspx">About Us</a>
                    </li>

                    <li class="menu-item-has-children">
                        <a href="HotelSearch.aspx">Hotels</a>
                    </li>

                    <li class="menu-item-has-children">
                        <a href="Packages.aspx">Packages</a>
                    </li>

                    <%--<li class="menu-item-has-children">
                        <a href="TourPackages.aspx">Tour Packages</a>
                    </li>--%>
                    <li class="menu-item-has-children">
                        <a href="Gallery.aspx">Gallery</a>
                    </li>

                    <li class="menu-item-has-children">
                        <a href="CompanyCertifications.aspx">Company Certifications</a>
                    </li>

                    <li class="menu-item-has-children">
                        <a href="Payment.aspx">Payment</a>
                    </li>

                    <li class="menu-item-has-children">
                        <a href="CarBooking.aspx">Car Booking</a>
                    </li>

                    <li class="menu-item-has-children">
                        <a href="Contact.aspx">Contact Us</a>
                    </li>

                </ul>
                <ul class="menu pull-right">
                    <li><a href="#" onclick="SignUpModal()">login / Register</a></li>
                    <li><a href="http://b2b.Vacaaay.com/">B2B Login</a></li>
                </ul>
            </nav>

        </header>

        <div class="container">
            <div id="main" style="padding-top: 50px">
                <div class="search-box-wrapper style2">
                    <div class="search-box">
                        <div class="search-tab-content">
                            <div class="tab-pane fade active in" id="hotels-tab">
                                <form action="hotel-list-view.html" method="post">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <span class="text-left" style="color: #838383">Where do you want to go?</span>
                                            <br />
                                            <div class="input-group" style="width: 100%">
                                                <input type="text" style="background-color: none" placeholder="Enter City Here" id="txtCity" class="form-control" />
                                            </div>
                                            <input type="hidden" id="hdnDCode" />
                                        </div>
                                        <div class="col-md-3">
                                            <span class="text-left" style="color: #2d3e52">Check-In:</span>
                                            <div class="datepicker-wrap">
                                                <input type="text" name="date_from" id="datepicker_HotelCheckin" readonly="readonly" style="cursor: pointer" class="input-text full-width" placeholder="Check In" />
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <span class="text-left" style="color: #2d3e52">Check-Out:</span>
                                            <div class="datepicker-wrap">
                                                <input type="text" name="date_to" id="datepicker_HotelCheckout" readonly="readonly" style="cursor: pointer" class="input-text full-width" placeholder="Check Out" />
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <span class="text-left" style="color: #838383">Total Nights:</span>
                                            <br />
                                            <select id="Select_TotalNights" class="form-control">
                                                <option selected="selected" value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                            </select>
                                        </div>
                                    </div>
                                    <br />

                                    <br id="br1" style="display: none" />
                                    <div class="row">
                                        <div class="col-md-4">
                                            <span class="text-left" style="color: #838383">Nationality:</span>
                                            <br />
                                            <select id="Select_Nationality" class="form-control">
                                            </select>
                                        </div>

                                        <div class="col-md-3" style="display:none">
                                            <span class="text-left" style="color: #838383">Star Rating:</span>
                                            <br />
                                            <select id="Select_StarRating" class="form-control"></select>
                                        </div>

                                        <div class="col-md-5">
                                            <span class="text-left" style="color: #838383">Hotel Name:</span>
                                            <br />
                                            <input class="form-control" type="text" placeholder="Enter Hotel Here" id="txt_HotelName" />
                                            <input type="hidden" id="hdnHCode" />
                                        </div>

                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="col-md-2">
                                            <span class="text-left" style="color: #838383">Rooms:</span>
                                            <select id="Select_Rooms" class="form-control">
                                                <option selected="selected" value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <span class="text-left" style="color: #838383">Room Type:</span>
                                            <input type="text" id="spn_RoomType1" style="cursor: default;" disabled="disabled" class="form-control" value="Room 1" />
                                        </div>
                                        <div class="col-md-2">
                                            <span class="text-left" style="color: #838383">Adults:</span>
                                            <select id="Select_Adults1" class="form-control">
                                                <option value="1">1</option>
                                                <option value="2" selected="selected">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <span class="text-left" style="color: #838383">Child:</span>
                                            <select id="Select_Children1" class="form-control">
                                                <option selected="selected" value="0">0</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <span id="Span_AgeChildFirst1" class="text-left" style="display: none; color: #838383">Age of Child 1:</span>
                                            <select id="Select_AgeChildFirst1" class="form-control" style="display: none">
                                                <option selected="selected" value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <span id="Span_AgeChildSecond1" class="text-left" style="display: none; color: #838383">Age of Child 2:</span>
                                            <select id="Select_AgeChildSecond1" class="form-control" style="display: none;">
                                                <option selected="selected" value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                            </select>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="col-md-2">
                                        </div>
                                        <div class="col-md-2">
                                            <span id="Span_RoomType2" class="text-left" style="display: none; color: #838383">Room Type:</span>
                                            <input type="text" id="spn_RoomType2" class="form-control" disabled="disabled" value="Room 2" style="display: none; cursor: default;" />
                                        </div>
                                        <div class="col-md-2">
                                            <span id="Span_Adults2" class="text-left" style="display: none; color: #838383">Adults:</span>
                                            <select id="Select_Adults2" class="form-control" style="display: none">
                                                <option value="1">1</option>
                                                <option value="2" selected="selected">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <span id="Span_Children2" class="text-left" style="display: none; color: #838383">Child:</span>
                                            <select id="Select_Children2" class="form-control" style="display: none">
                                                <option selected="selected" value="0">0</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <span id="Span_AgeChildFirst2" class="text-left" style="display: none; color: #838383">Age of Child 1:</span>
                                            <select id="Select_AgeChildFirst2" class="form-control" style="display: none">
                                                <option selected="selected" value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <span id="Span_AgeChildSecond2" class="text-left" style="display: none; color: #838383">Age of Child 2:</span>
                                            <select id="Select_AgeChildSecond2" class="form-control" style="display: none;">
                                                <option selected="selected" value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                            </select>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="col-md-2">
                                        </div>
                                        <div class="col-md-2">
                                            <span id="Span_RoomType3" class="text-left" style="display: none; color: #838383">Room Type:</span>
                                            <input type="text" id="spn_RoomType3" class="form-control" disabled="disabled" value="Room 3" style="display: none; cursor: default;" />
                                        </div>
                                        <div class="col-md-2">
                                            <span id="Span_Adults3" class="text-left" style="display: none; color: #838383">Adults:</span>
                                            <select id="Select_Adults3" class="form-control" style="display: none">
                                                <option value="1">1</option>
                                                <option value="2" selected="selected">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <span id="Span_Children3" class="text-left" style="display: none; color: #838383">Child:</span>
                                            <select id="Select_Children3" class="form-control" style="display: none">
                                                <option selected="selected" value="0">0</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <span id="Span_AgeChildFirst3" class="text-left" style="display: none; color: #838383">Age of Child 1:</span>
                                            <select id="Select_AgeChildFirst3" class="form-control" style="display: none">
                                                <option selected="selected" value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <span id="Span_AgeChildSecond3" class="text-left" style="display: none; color: #838383">Age of Child 2:</span>
                                            <select id="Select_AgeChildSecond3" class="form-control" style="display: none;">
                                                <option selected="selected" value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                            </select>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="col-md-2">
                                        </div>
                                        <div class="col-md-2">
                                            <span id="Span_RoomType4" class="text-left" style="display: none; color: #838383">Room Type:</span>
                                            <input type="text" id="spn_RoomType4" class="form-control" value="Room 4" disabled="disabled" style="display: none; cursor: default;" />
                                        </div>
                                        <div class="col-md-2">
                                            <span id="Span_Adults4" class="text-left" style="display: none; color: #838383">Adults:</span>
                                            <select id="Select_Adults4" class="form-control" style="display: none">
                                                <option value="1">1</option>
                                                <option value="2" selected="selected">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <span id="Span_Children4" class="text-left" style="display: none; color: #838383">Child:</span>
                                            <select id="Select_Children4" class="form-control" style="display: none">
                                                <option selected="selected" value="0">0</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <span id="Span_AgeChildFirst4" class="text-left" style="display: none; color: #838383">Age of Child 1:</span>
                                            <select id="Select_AgeChildFirst4" class="form-control" style="display: none">
                                                <option selected="selected" value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <span id="Span_AgeChildSecond4" class="text-left" style="display: none; color: #838383">Age of Child 2:</span>
                                            <select id="Select_AgeChildSecond4" class="form-control" style="display: none;">
                                                <option selected="selected" value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2" style="float: right">
                                        <button type="button" class="button btn-medium sky-blue1" id="btn_Search" onclick="Redirect();">SEARCH</button>
                                    </div>
                                    <br />
                                    <br />

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer id="footer">
            <div class="footer-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2>About Unisafe Tours</h2>
                            <p style="text-align: justify">Unisafe Tours is a leading Tour & Travel management company with the specialization of various kinds of tour packages developed by the group of professionals which make it unique and one of the most-sought after tour and travel organization in India located in Agra Utter Pradesh (India).</p>


                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2>Contact Us</h2>
                            <ul class="travel-news">
                                <li>

                                    <address class="contact-details">
                                        <span class="contact-phone"><i class="soap-icon-phone"></i>+91 9910084810</span>
                                        <br />
                                        <br />
                                        <span class="contact-phone"><i class="soap-icon-phone"></i>0562 -22334810</span>
                                        <br />
                                        <br />
                                        <h2><span class="contact-message"><i class="soap-icon-message"></i><a href="mailto:info@Vacaaay.com ">info@Vacaaay.com </a></span></h2>

                                    </address>
                                </li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2>Mailing List</h2>
                            <p>Sign up for our mailing list to get latest updates and offers.</p>

                            <div class="icon-check">
                                <input type="text" class="input-text full-width" placeholder="your email" />
                            </div>
                            <br />
                            <span>We respect your privacy</span>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2>Discover</h2>
                            <ul class="discover triangle hover row">
                                <li class="col-xs-6"><a href="Default.aspx">HOME</a></li>
                                <li class="col-xs-6"><a href="AboutUs.aspx">ABOUT US</a></li>
                                <li class="col-xs-6"><a href="TourPackages.aspx">PACKAGES</a></li>
                                <li class="col-xs-6"><a href="Gallery.aspx">GALLERY</a></li>
                                <li class=" col-xs-6"><a href="CompanyCertifications.aspx">CERTIFICATIONS</a></li>
                                <li class="col-xs-6"><a href="Payment.aspx">PAYMENT</a></li>
                                <li class="col-xs-6"><a href="CarBooking.aspx">CAR BOOKING</a></li>
                                <li class="col-xs-6"><a href="Contact.aspx">CONTACT US</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom gray-area">
                <div class="container">
                    <div class="pull-left">
                        <p>Developed by <a href="http://nasstechnologies.com" target="_blank">Nass Technologies</a></p>
                    </div>
                    <div class="pull-right">
                        <a id="back-to-top" href="#" class="animated" data-animation-type="bounce"><i class="soap-icon-longarrow-up circle"></i></a>
                    </div>
                    <div class="copyright pull-right">
                        <p>Copyright © 2016, www.Vacaaay.com   </p>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <div class="modal fade" id="AlertModal" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="width: 40%; padding-top: 15%">

            <div class="modal-content">
                <div class="modal-header" style="border-bottom: none">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>

                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="frow2">
                            <div>
                                <center><span id="AlertMessage"></span></center>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="ModelMessege" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="width: 40%; padding-top: 15%">

            <div class="modal-content">
                <div class="modal-header" style="border-bottom: none">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>

                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="frow2">
                            <div>
                                <center><span id="SpnMessege"></span></center>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="ConformModal" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="alert">
                            <p class="size17" id="ConformMessage">
                            </p>
                            <table align="right" style="margin-left: -50%;">
                                <tbody>
                                    <tr>
                                        <td style="border-top-width: 1px; border-top-style: solid; border-top-color: rgb(255, 255, 255);">
                                            <input type="button" id="Cancel" value="Cancel" onclick="Cancel()" class="btn-search4 margtop20" style="float: none;">
                                        </td>
                                        <td style="border-top-width: 1px; border-top-style: solid; border-top-color: rgb(255, 255, 255);">
                                            <input type="button" value="Ok" id="btnOk" class="btn-search4 margtop20" style="float: none;">
                                        </td>

                                    </tr>
                                </tbody>
                            </table>
                            <br />

                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade bs-example-modal-lg" id="PreviewModalRegisteration" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="border-bottom: 1px solid #fff">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel3" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="tab-container style1">
                            <ul class="tabs">
                                <li class="active"><a href="#Login" data-toggle="tab" aria-expanded="true">Login</a></li>
                                <li class=""><a href="#Register" data-toggle="tab" aria-expanded="false">Register</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="Login">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <span class="size13 dark">Username / Email : </span>
                                            <input type="text" id="txtUserName" data-trigger="focus" data-placement="top" placeholder="Username" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your B2C_Id" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_txtUserName">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-6">
                                            <span class="size13 dark">Password : </span>
                                            <input type="password" id="txtPassword" data-trigger="focus" data-placement="top" placeholder="Password" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your B2C_Id" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_txtPassword">
                                                <b>* This field is required</b></label>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="block" style="float: right">
                                        <input type="button" onclick="LoginAgent()" class="button btn-small sky-blue1" value="Login" />
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="Register">

                                    <div class="row">
                                        <div class="col-md-4">
                                            <span class="size13 dark">User Name : </span>
                                            <input type="text" id="txt_UserName" data-trigger="focus" data-placement="top" placeholder="User Name" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your User Name" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_UserName">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-4">
                                            <span class="size13 dark">Password: </span>
                                            <input type="password" id="txt_Password" data-trigger="focus" data-placement="top" placeholder="Password" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your Password" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_Password">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-4">
                                            <span class="size13 dark">Confirm Password: </span>
                                            <input type="password" id="txt_ConfirmPassword" data-trigger="focus" data-placement="top" placeholder="Confirm Password" class="form-control" data-content="This field is mandatory" data-original-title="Here you need to confirm your Password" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_ConfirmPassword">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-4">
                                            <br />
                                            <span class="size13 dark">Mobile : </span>
                                            <input type="text" id="txt_Mobile" data-trigger="focus" data-placement="top" placeholder="Mobile" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your mobile number" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_Mobile">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-4">
                                            <br />
                                            <span class="size13 dark">Phone : </span>
                                            <input type="text" id="txt_Phone" data-trigger="focus" data-toggle="popover" data-placement="top" placeholder="Phone" class="form-control" data-content="This field is mandatory" data-original-title="Here you can create your Phone" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_Phone">
                                                <b>* This field is required</b></label>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <span class="size13 dark">Address : </span>
                                            <input type="text" id="txt_Address" data-trigger="focus" data-placement="top" placeholder="Address" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit address" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_Address">
                                                <b>* This field is required</b></label>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <span class="size13 dark">City : </span>
                                            <select id="selCity" class="form-control">
                                                <option selected="selected" value="-">Select Any City</option>
                                            </select>
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_City">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-4">
                                            <span class="size13 dark">Pin Code : </span>
                                            <input type="text" id="txt_PinCode" data-trigger="focus" data-placement="top" placeholder="Pin Code" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit pin code" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_PinCode">
                                                <b>* This field is required</b></label>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="block" style="float: right">
                                        <input type="button" id="btn_RegiterAgent" value="Register" class="button btn-small sky-blue1" onclick="return ValidateRegistration()" />
                                        <input type="button" id="btn_Cancel" value="Cancel" class="button btn-small orange" onclick="ClearRegistration();" />
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>
        </div>
    </div>

    <!-- Javascript -->
    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.noconflict.js"></script>
    <script type="text/javascript" src="js/modernizr.2.7.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="js/jquery-ui.1.10.4.min.js"></script>
    <script src="js/moment.js"></script>
    <!-- Twitter Bootstrap -->
    <script type="text/javascript" src="js/bootstrap.js"></script>

    <!-- load revolution slider scripts -->
    <script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>

    <!-- Flex Slider -->
    <script type="text/javascript" src="components/flexslider/jquery.flexslider-min.js"></script>

    <!-- load BXSlider scripts -->
    <script type="text/javascript" src="components/jquery.bxslider/jquery.bxslider.min.js"></script>

    <!-- parallax -->
    <script type="text/javascript" src="js/jquery.stellar.min.js"></script>

    <!-- waypoint -->
    <script type="text/javascript" src="js/waypoints.min.js"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="js/theme-scripts.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>

    <script type="text/javascript">

        var tpj = jQuery;
        var chkinDate;
        var chkinMonth;
        var chkoutDate;
        var chkoutMonth;
        tpj.noConflict();

        tpj(document).ready(function () {

            if (tpj.fn.cssOriginal != undefined)
                tpj.fn.css = tpj.fn.cssOriginal;

            tpj('.fullscreenbanner').revolution(
                {
                    delay: 9000,
                    startwidth: 1170,
                    startheight: 600,

                    onHoverStop: "on",						// Stop Banner Timet at Hover on Slide on/off

                    thumbWidth: 100,							// Thumb With and Height and Amount (only if navigation Tyope set to thumb !)
                    thumbHeight: 50,
                    thumbAmount: 3,

                    hideThumbs: 0,
                    navigationType: "bullet",				// bullet, thumb, none
                    navigationArrows: "solo",				// nexttobullets, solo (old name verticalcentered), none

                    navigationStyle: false,				// round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom


                    navigationHAlign: "left",				// Vertical Align top,center,bottom
                    navigationVAlign: "bottom",					// Horizontal Align left,center,right
                    navigationHOffset: 30,
                    navigationVOffset: 30,

                    soloArrowLeftHalign: "left",
                    soloArrowLeftValign: "center",
                    soloArrowLeftHOffset: 20,
                    soloArrowLeftVOffset: 0,

                    soloArrowRightHalign: "right",
                    soloArrowRightValign: "center",
                    soloArrowRightHOffset: 20,
                    soloArrowRightVOffset: 0,

                    touchenabled: "on",						// Enable Swipe Function : on/off


                    stopAtSlide: -1,							// Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
                    stopAfterLoops: -1,						// Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic

                    hideCaptionAtLimit: 0,					// It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
                    hideAllCaptionAtLilmit: 0,				// Hide all The Captions if Width of Browser is less then this value
                    hideSliderAtLimit: 0,					// Hide the whole slider, and stop also functions if Width of Browser is less than this value


                    fullWidth: "on",							// Same time only Enable FullScreen of FullWidth !!
                    fullScreen: "off",						// Same time only Enable FullScreen of FullWidth !!


                    shadow: 0								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows -  (No Shadow in Fullwidth Version !)

                });

            tpj("#datepicker_Departure").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "d-m-yy",


                minDate: "dateToday",

            });
            tpj("#datepicker_Return").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "d-m-yy",
            });






            tpj("#datepicker_HotelCheckin").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "d-m-yy",
                onSelect: insertDepartureDate,
                //dateFormat: "yy-m-d",
                minDate: "dateToday",
                //maxDate: "+3M +10D"


            });

            tpj("#datepicker_HotelCheckin").datepicker("setDate", new Date());
            var dateObject = tpj("#datepicker_HotelCheckin").datepicker('getDate', '+1d');
            dateObject.setDate(dateObject.getDate() + 1);
            tpj("#datepicker_HotelCheckout").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "d-m-yy",
                beforeShow: insertDepartureDate,
                onSelect: insertDays,
                //dateFormat: "yy-m-d",
                minDate: "+1D",
                //maxDate: "+3M +20D"
            });
            tpj("#datepicker_HotelCheckout").datepicker("setDate", dateObject);
            Addyear();
            tpj('#datepicker_HotelCheckin').change(function () {
                var date2 = tpj('#datepicker_HotelCheckin').datepicker('getDate', '+1d');
                chkinDate = date2.getDate();
                //chkinMonth = date2.getMonth();
                date2.setDate(date2.getDate() + 1);
                tpj('#datepicker_HotelCheckout').datepicker('setDate', date2);
                tpj('#Select_TotalNights').val("1");
            });
            tpj('#Select_TotalNights').change(function () {
                manage_date();
                //    tpj('#datepicker_HotelCheckout').datepicker('setDate', '+' + Number(Number(chkinDate) + Number(tpj('#Select_TotalNights').val())) + 'd')
            });
            //....................................................................................................//

            function insertDays() {
                var arrival_date = new Date(tpj('#datepicker_HotelCheckin').datepicker("getDate"));
                var departure_date = new Date(tpj('#datepicker_HotelCheckout').datepicker("getDate"));
                days = (departure_date.getTime() - arrival_date.getTime()) / (1000 * 60 * 60 * 24);
                if (days > 30) {
                    alert("You are allowed to book for a maximum 30 Nights");
                    days = tpj('#Select_TotalNights').val();
                    days = parseInt(days);
                    var departure_date = new Date(arrival_date.getFullYear(), arrival_date.getMonth(), arrival_date.getDate() + days);
                    tpj('#datepicker_HotelCheckout').datepicker('setDate', departure_date);
                    tpj('#Select_TotalNights').val(days);
                    //document.getElementById("sel_days").value = days;
                    return false;
                }
                tpj('#Select_TotalNights').val(days);
                //document.getElementById("sel_days").value = days;
            }

            function manage_date() {
                days = tpj('#Select_TotalNights').val();
                days = parseInt(days);
                var firstDate = new Date(tpj('#datepicker_HotelCheckin').datepicker("getDate"));
                var secondDate = new Date(firstDate.getFullYear(), firstDate.getMonth(), firstDate.getDate() + days);
                tpj('#datepicker_HotelCheckout').datepicker('setDate', secondDate);
                insertDepartureDate('', '', '');

            }

            function insertDepartureDate(value, date, inst) {
                debugger;
                var firstDate = new Date(tpj('#datepicker_HotelCheckin').datepicker("getDate"));
                var dateAdjust = tpj('#Select_TotalNights').val();
                var current_date = new Date();

                current_time = current_date.getTime();
                days = (firstDate.getTime() - current_time) / (1000 * 60 * 60 * 24);
                if (days < 0) {
                    var add_day = 1;
                } else {
                    var add_day = 2;
                }
                days = parseInt(days);
                tpj('#datepicker_HotelCheckout').datepicker("option", "minDate", days + add_day);
                tpj('#datepicker_HotelCheckout').datepicker("option", "maxDate", days + 365);
                dateAdjust = parseInt(dateAdjust);
                var secondDate = new Date(firstDate.getFullYear(), firstDate.getMonth(), firstDate.getDate() + dateAdjust);
                tpj('#datepicker_HotelCheckout').datepicker('setDate', secondDate);

            }

            function Addyear() {
                var current_date = new Date();
                var firstDate = new Date(tpj('#datepicker_HotelCheckin').datepicker("getDate"));
                current_time = current_date.getTime();
                days = (firstDate.getTime() - current_time) / (1000 * 60 * 60 * 24);
                days = parseInt(days);
                var i = 0
                tpj('#datepicker_HotelCheckin').datepicker("option", "minDate", days + i);
                tpj('#datepicker_HotelCheckin').datepicker("option", "maxDate", days + 365);
            }
        });
    </script>

    <script type="text/javascript">
        tjq(".flexslider").flexslider({
            animation: "fade",
            controlNav: false,
            animationLoop: true,
            directionNav: false,
            slideshow: true,
            slideshowSpeed: 5000
        });
    </script>
</body>
</html>
