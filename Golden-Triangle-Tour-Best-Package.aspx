﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Golden-Triangle-Tour-Best-Package.aspx.cs" Inherits="CUTUK.Golden_Triangle_Tour_Best_Package1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-title-container">
        <div class="container">
            <div class="page-title pull-left">
                <h2 class="entry-title">INDIA GOLDEN TRIANGLE TOURS</h2>
            </div>
            <ul class="breadcrumbs pull-right">
                <li><a href="#">HOME</a></li>
                <li class="active">INDIA GOLDEN TRIANGLE TOURS</li>
            </ul>
        </div>
    </div>
    <section id="content">
        <div class="container tour-detail-page">
            <div class="row">
                <div id="main" class="col-md-12">
                    <div class="featured-image">
                        <img src="images/Golden-Triangel-Tour-1.jpg" alt="" />
                    </div>

                    <div id="tour-details" class="travelo-box">
                        <h2>General Information </h2>
                        <p>
                            3 Star Hotels based
                                Packages price based on double sharing :<br />

                            04 Star Hotels based
                                Packages price based on double sharing  :<br />
                            <b>Meal Plan </b>Nights Delhi Meridian PlazaHans Plaza Bed + Breakfast<br />

                            <b>Agra</b> 02 Nights The Retreat  Samovar premier / Similar Bed + Breakfast

                                <b>Jaipur</b> 01 Night  Regenta Central Sahapura house Bed + Breakfast

                                <b>Delhi</b>  02 Nights Meridian Plaza	Hans Plaza	Bed + Breakfast	01 Night
                        </p>
                        <h2>Beginning of seven days agra city tour </h2>
                        <h2>Day 01</h2>
                        <p>Delhi (Please let us know arrival details) Arrive at Delhi airport. Upon arrival met with our representative who will assist you for check into Hotel.</p>

                        <h2>Day 02</h2>
                        <p>Delhi: Morning after Breakfast visit Old Delhi (Jama Masjid, Red fort, chandini Chock with Rickshaw ride). Afternoon visit Raj Ghat, New Delhi (India Gate, Parliament street, Qutub Minar, Human Tomb, Lotus temple. </p>
                        <h2>Day 03</h2>
                        <p>
                            Delhi – Agra (Approx. drive 03 hours) Morning after Breakfast drive to Agra. Enroute visit Sikandra, Itmad Daula (Baby Taj). Evening enjoy visit of Agra fort, Taj Mahal during Sunset back side
                        </p>
                        <h2>Full day agra tour begins</h2>
                        <h2>Day 04</h2>
                        <p>
                            Agra – Jaipur via Fatehpur Sikri (Approx drive 04 hours) Early Morning visit Taj Mahal during Sunrise from Inside. After Breakfast drive to Jaipur. Enroute visit Fatehpur SIkri, Baoli, and Continue drive to Jaipur. Upon arrival check into Hotel in Jaipur.
                        </p>
                        <h2>Day 05</h2>
                        <p>
                            Jaipur: Morning after Breakfast visit Hawa Mahal, Amber fort with Elephant ride. Afternoon visit City Palace, Jantar Mantar. Evening enjoy visit water palace (Jal Mahal).
                        </p>
                        <h2>Day 06</h2>
                        <p>
                            Jaipur – Delhi (Approx. drive 05 hours)  Morning after Breakfast drive to Delhi. Upon arrival at Delhi check into Hotel in Delhi. Evening departure transfer to airport to board flight to your next destination..

                        </p>
                        <h2>Day 07</h2>
                        <p>
                            Delhi - Dwarika (Morning flight) Morning after Breakfast we will pick you from hotel and drop you at Airport for Dwarika.
                        </p>


                        <div class="intro small-box table-wrapper full-width hidden-table-sms">

                            <div class="col-sm-8 table-cell">
                                <div class="detailed-features">

                                    <div class="container">
                                        <div class="row">
                                            <div id="main" class="col-md-9">
                                                <div class="travelo-box travel-ideas">
                                                    <h2 class="idea-title box"><i><span class="icon soap-icon-plane-right take-off"></span></i><u>Tour Inclusion:</u></h2>

                                                    <div class="suggested-places">
                                                        <div class="overflow-hidden">
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <ul class="check-square box">
                                                                        <li>
                                                                            <h5 class="box-title">Accommodation with Breakfast as per program.</h5>

                                                                        </li>
                                                                        <li>
                                                                            <h5 class="box-title">AC Cars as per passenger as per program.</h5>

                                                                        </li>
                                                                        <li>
                                                                            <h5 class="box-title">Local English speaking guide in Each city.</h5>

                                                                        </li>


                                                                    </ul>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <ul class="check-square box">
                                                                        <li>

                                                                            <h5 class="box-title">Ricksaw ride at Old Delhi.</h5>

                                                                        </li>
                                                                        <li>

                                                                            <h5 class="box-title">Tonga ride at Taj Mahal, Agra.</h5>

                                                                        </li>
                                                                        <li>

                                                                            <h5 class="box-title">Driver allowance, Toll Tax, Parking.</h5>

                                                                        </li>


                                                                    </ul>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <ul class="check-square box">
                                                                        <li>

                                                                            <h5 class="box-title">Elephant ride at Amber fort, Jaipur.</h5>

                                                                        </li>
                                                                        <li>

                                                                            <h5 class="box-title">Assistance on arrival / Departure as per program.</h5>

                                                                        </li>
                                                                        <li>

                                                                            <h5 class="box-title">WIFI Internet services available in Hotels as well in the our cars.</h5>

                                                                        </li>


                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                    </div>

                </div>


            </div>

        </div>

        <div class="container">
            <div id="main">

                <div class="row add-clearfix image-box style1 tour-locations">
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <article class="box">
                            <figure>
                                <a href="#" class="hover-effect">
                                    <img src="images/MeridianPlaza.jpg" alt="MeridianPlaza" />
                                </a>
                            </figure>
                            <div class="details">
                                <span class="day">2 Night</span>
                                <h3 class="box-title">Delhi</h3>
                                <hr />
                                <ul class="features check">
                                    <li>
                                        <h6><strong>3 Star Hotels</strong> - Meridian Plaza</h6>
                                    </li>
                                    <li>
                                        <h6><strong>4 Star Hotels </strong>- Hans Plaza</h6>
                                    </li>
                                    <li>
                                        <h6><strong>Meal Plan</strong> - Bed + Breakfast</h6>
                                    </li>

                                </ul>


                            </div>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <article class="box">
                            <figure>
                                <a href="#" class="hover-effect">
                                    <img src="images/sarovarAgra.jpg" alt="Hotel image" />
                                </a>
                            </figure>
                            <div class="details">
                                <span class="day">1 Night</span>
                                <h3 class="box-title">Agra</h3>
                                <hr />
                                <ul class="features check">
                                    <li>
                                        <h6><strong>3 Star Hotels</strong> - The Retreat</h6>
                                    </li>

                                    <li>
                                        <h6><strong>4 Star Hotels </strong>- Samovar premier/Similar</h6>
                                    </li>

                                    <li>
                                        <h6><strong>Meal Plan</strong> - Bed + Breakfast</h6>
                                    </li>

                                </ul>


                            </div>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <article class="box">
                            <figure>
                                <a href="#" class="hover-effect">
                                    <img src="images/shahpuraHousejaipur.jpg" alt="shahpuraHousejaipur">
                                </a>
                            </figure>
                            <div class="details">
                                <span class="day">2 Night</span>
                                <h3 class="box-title">Jaipur</h3>
                                <hr />
                                <ul class="features check">
                                    <li>
                                        <h6><strong>3 Star Hotels</strong> - Regenta Central</h6>
                                    </li>
                                    <li>
                                        <h6><strong>4 Star Hotels </strong>- Shahpura house</h6>
                                    </li>
                                    <li>
                                        <h6><strong>Meal Plan</strong> - Bed + Breakfast</h6>
                                    </li>

                                </ul>



                            </div>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <article class="box">
                            <figure>
                                <a href="#" class="hover-effect">
                                    <img src="images/HansPlaza.jpg" alt="HansPlaza" />
                                </a>
                            </figure>
                            <div class="details">
                                <span class="day">1 Night</span>
                                <h3 class="box-title">Delhi</h3>
                                <hr />
                                <ul class="features check">
                                    <li>
                                        <h6><strong>3 Star Hotels</strong> - Meridian Plaza</h6>
                                    </li>
                                    <li>
                                        <h6><strong>4 Star Hotels </strong>- Hans Plaza</h6>
                                    </li>
                                    <li>
                                        <h6><strong>Meal Plan</strong> - Bed + Breakfast</h6>
                                    </li>

                                </ul>
                            </div>
                        </article>
                    </div>
                </div>

            </div>
        </div>
    </section>
</asp:Content>
