﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="AboutUs.aspx.cs" Inherits="CUTUK.AboutUs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="page-title-container">
        <div class="container">
            <div class="page-title pull-left">
                <h2 class="entry-title">About Us</h2>
            </div>
            <ul class="breadcrumbs pull-right">
                <li><a href="#">HOME</a></li>
                <li class="active">About Us</li>
            </ul>
        </div>
    </div>

    <section id="content">
     <div class="container" style="margin-top: -30px;">
            <div class="row">
                <div id="main" class="col-md-12">
                    <div class="tab-container style1" >
                        <div class="tab-content">
                            <div class="tab-pane fade active in" >

                                <div class="main-content">

                                    <div class="long-description">
                                        <h2>General Information About Vacaaay</h2>
                                        <p>Vacaaay.com is Malaysia’s very 1st online travel agency. We provide services for flights and hotels under 1 roof. We have been in the travel business since 1985 and now taking the legacy for travel forward with wave of fast moving technology.</p>
                                        <p>Our HQ is based in Ipoh, Perak Malaysia. We believe in fulfilling travel experiences for our each and every customer. We want them to see the way they travel, experience in it, the joy & excitement and most important the memory they bring or take back.</p>
                                        <p>We want to empower travellers to travel more and non-travellers to travel and feel the experience through us. We offer best range of Air Fares & Hotel deals. Soon In future we plan to expand with great other facilities to make people’s life easy by sitting at home and booking their travel. As we believe in our motto “Make Travel Easy as you can”</p>
                                        <p>We are committed and dedicated to know your wants and needs for travel there for we have 24X7 customer service member who will be ready to serve you around the clock via emails or phone.Info@vacaaay.com</p>
                                        <p>For the past 33 Years we have built strong and good reputation with other travel players around the world and ensuring the travel business remains strong in every countries economy.</p>
                                    </div>
                                    <br />
                                    <br />
                                    <h1 style="text-align: center">Why Book with us?</h1>
                                    <hr />
                                    <div class="row">
                                        <div class="col-lg-4" style="text-align: center;">
                                            <div>
                                                <img src="images/5.png" class="_2HNyG">
                                                 <br />
                                                <br />
                                                <h3 class="_2fJEB">Travel Without the Hassle</h3>
                                                <p class="U91ar">A stress-free booking experience - secure payments, no hidden costs, pay online and receive your itinerary via email.</p>

                                            </div>

                                        </div>
                                        <div class="col-lg-4" style="text-align: center;">
                                            <div>
                                                <img src="images/2.png" class="_2HNyG">
                                                 <br />
                                                 <br />
                                                <h3 class="_2fJEB">All You Need to Know</h3>
                                                <p class="U91ar">Make well-informed travel decisions - view schedules, reviews, amenities, and other useful information before booking.</p>
                                            </div>
                                        </div>
                                        <div class="col-lg-4" style="text-align: center;">
                                            <div>
                                                <img src="images/4.png" class="_2HNyG">
                                                 <br />
                                                 <br />
                                                <h3 class="_2fJEB">We’ve Got You Covered</h3>
                                                <p class="U91ar">Our travel experts are passionate about supporting you with 24/7  instant responses via chat, email, and phone.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>


                    </div>
                </div>



                <div id="main" class="col-md-12">
                    <div class="tab-container style1" id="travel-guide">
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="travel-guide-info">

                                <div class="main-content">

                                    <div class="long-description">
                                    </div>
                                </div>
                            </div>


                        </div>


                    </div>
                </div>


            </div>
        </div>
    </section>
</asp:Content>
