﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="TourPackages.aspx.cs" Inherits="CUTUK.TourPackages" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-title-container">
        <div class="container">
            <div class="page-title pull-left">
                <h2 class="entry-title">Tour Packages</h2>
            </div>
            <ul class="breadcrumbs pull-right">
                <li><a href="#">HOME</a></li>
                <li class="active">Tour Packages</li>
            </ul>
        </div>
    </div>

    <section id="content">
        <div class="container">
            <div id="main">
                <div class="row">
                    <div class="col-sm-12 col-md-12">

                        <div class="hotel-list listing-style3 hotel">
                            <article class="box">
                                <figure class="col-sm-5 col-md-4">
                                    <a title="" href="#" class="hover-effect popup-gallery">
                                        <img width="270" height="300" alt="" src="images/imgpackages/p1northindia.jpg"></a>

                                </figure>
                                <div class="details col-sm-7 col-md-8">
                                    <div>
                                        <div>
                                            <h4 class="box-title"><i class="soap-icon-departure yellow-color"></i>North India Tour Packages</h4>

                                        </div>
                                    </div>
                                    <div>
                                        <ul class="discover triangle hover row" style="text-align: justify">

                                            <li class="col-xs-6"><a href="Golden-Triangle-Tour-Best-Package.aspx">Golden Triangle Tour Best Package</a></li>
                                            <li class="col-xs-6"><a href="#.">Golden Triangle Shimla Tours</a> </li>
                                            <li class="col-xs-6"><a href="Overnight-Tour-To-Agra.aspx">Over Night Tour To Agra</a> </li>
                                            <li class="col-xs-6"><a href="#.">Tour Plan of A Jungle Journey in India</a></li>
                                            <li class="col-xs-6"><a href="#.">Heritage with Jungle Safari Tour</a></li>
                                            <li class="col-xs-6"><a href="Same-Day-Agra-Tour.aspx">Same Day Agra Tour</a></li>
                                            <li class="col-xs-6"><a href="#.">Same Day Agra Tour with Train Ride</a></li>
                                            <li class="col-xs-6"><a href="#.">Delhi with Rajasthan Tour- Heritage of North India</a></li>
                                            <li class="col-xs-6"><a href="#.">01 Night / 02 Days Golden Triangle tour with Train ride</a></li>
                                            <li class="col-xs-6"><a href="#.">11 Days North India With Kasmir And Mumbai</a></li>
                                            <li class="col-xs-6"><a href="#.">15 Days Hindu Pilgrimage North India Tour</a> </li>
                                            <li class="col-xs-6"><a href="#.">01 Night / 02 Days Golden Triangle tour with Train ride.</li>
                                            <li class="col-xs-6"><a href="#.">Golden Triangle Tour 04 Nights / 05 Days</a></li>
                                            <li class="col-xs-6"><a href="#.">Goldern Triangle Tour with Udaipur And Varanasi</li>
                                            <li class="col-xs-6"><a href="#.">The Golden Triangle Tour with Halal Meal</a></li>
                                            <li class="col-xs-6"><a href="#.">Palace on Wheels Train Tour</a></li>
                                            <li class="col-xs-6"><a href="#.">Delhi and Agra with Bharatpur Tour</a></li>
                                            <li class="col-xs-6"><a href="#.">North india with Dargah Tour</a> </li>

                                        </ul>
                                    </div>
                                </div>
                            </article>

                            <article class="box">

                                <div class="details col-sm-7 col-md-8">
                                    <div>
                                        <div>
                                            <h4 class="box-title"><i class="soap-icon-departure yellow-color"></i>Rajasthan Tour Packages</h4>

                                        </div>
                                    </div>
                                    <div>
                                        <ul class="discover triangle hover row" style="text-align: justify">
                                            <li class="col-xs-6">
                                                <a href="#.">Overnight Jaipur Tour -The Pink City</a>
                                            </li>
                                            <li class="col-xs-6"><a href="#.">Rajasthan Cultural Tour</a></li>
                                            <li class="col-xs-6"><a href="#.">Rajasthan Desert Safari Tour</a></li>
                                            <li class="col-xs-6"><a href="#.">Weekend Tour in Rajasthan Jaipur City 03 Nights 04 Days </a>
                                            </li>
                                            <li class="col-xs-6"><a href="#.">Tour Packages Rajasthan With Kerala&nbsp;Long Holidays in India</a>
                                            </li>
                                            <li class="col-xs-6"><a href="#.">Golden Triangle&nbsp;Tour 04 Nights 05 days</a>
                                            </li>
                                            <li class="col-xs-6"><a href="#.">Regal Rajasthan Tours</a></li>
                                            <li class="col-xs-6"><a href="#.">Mumbai Slum tour Rajasthan Cultural</a></li>
                                            <li class="col-xs-6">
                                                <a href="#.">Mahrashtra Tour Package</a></li>

                                        </ul>
                                    </div>
                                </div>
                                <figure class="col-sm-5 col-md-4">
                                    <a title="" href="#" class="hover-effect popup-gallery">
                                        <img width="270" height="160" alt="" src="images/imgpackages/p2Rajasthan.jpeg"></a>
                                </figure>
                            </article>

                            <article class="box">
                                <figure class="col-sm-5 col-md-4">
                                    <a title="" href="# " class="hover-effect popup-gallery">
                                        <img width="270" height="160" alt="" src="images/imgpackages/p3SouthIndia.jpg"></a>

                                </figure>
                                <div class="details col-sm-7 col-md-8">
                                    <div>
                                        <div>
                                            <h4 class="box-title"><i class="soap-icon-departure yellow-color"></i>South India Tour Packages</h4>

                                        </div>
                                    </div>
                                    <div>
                                        <ul class="discover triangle hover row" style="text-align: justify">
                                            <li class="col-xs-6"><a href="#">Golden Chariot Pride of the South</a></li>
                                            <li class="col-xs-6"><a href="#.">India Heart Soul tour</a></li>
                                            <li class="col-xs-6"><a href="#.">Spectacular India Tours</a></li>
                                            <li class="col-xs-6"><a href="#.">Indian Temple Tours</a></li>
                                            <li class="col-xs-6"><a href="#.">Tamilnadu Tour Package</a></li>
                                            <li class="col-xs-6"><a href="#">God’s Own Country – Kerala</a></li>
                                            <li class="col-xs-6"><a href="#.">Kerala &amp; Tamilnadu Tour</a></li>
                                            <li class="col-xs-6"><a href="#">Spirit &amp; Spice of India 2014</a></li>
                                            <li class="col-xs-6"><a href="#.">Golden Triangle &amp; Backwaters</a></li>

                                        </ul>
                                    </div>
                                </div>
                            </article>

                            <article class="box">

                                <div class="details col-sm-7 col-md-8">
                                    <div>
                                        <div>
                                            <h4 class="box-title"><i class="soap-icon-departure yellow-color"></i>East India Tour Packages</h4>

                                        </div>
                                    </div>
                                    <div>
                                        <ul class="discover triangle hover row" style="text-align: justify">

                                            <li class="col-xs-6"><a href="#.">Andaman and Nicobar Islands</a></li>
                                            <li class="col-xs-6"><a href="#.">Assam Tour</a></li>
                                            <li class="col-xs-6"><a href="#.">Meghalaya Tour</a></li>
                                            <li class="col-xs-6"><a href="#.">Palace on Wheels Train Tour</a></li>
                                            <li class="col-xs-6"><a href="#.">Classical Tour India</a></li>
                                            <li class="col-xs-6"><a href="#.">A King’s Journey</a></li>
                                            <li class="col-xs-6"><a href="#.">Monuments Tour India</a></li>
                                            <li class="col-xs-6"><a href="#.">Buddha Tours</a></li>

                                        </ul>
                                    </div>
                                </div>
                                <figure class="col-sm-5 col-md-4">
                                    <a title="" href="#" class="hover-effect popup-gallery">
                                        <img width="270" height="160" alt="" src="images/imgpackages/p4EastIndia.jpg"></a>

                                </figure>
                            </article>

                            <article class="box">
                                <figure class="col-sm-5 col-md-4">
                                    <a title="" href="#" class="hover-effect popup-gallery">
                                        <img width="270" height="160" alt="" src="images/imgpackages/p5NorthEastIndia.jpg"></a>
                                </figure>
                                <div class="details col-sm-7 col-md-8">
                                    <div>
                                        <div>
                                            <h4 class="box-title"><i class="soap-icon-departure yellow-color"></i>North East India Tour Packages</h4>

                                        </div>
                                    </div>
                                    <div>
                                        <ul class="discover triangle hover row" style="text-align: justify">
                                            <li class="col-xs-6"><a href="#.">Himalayan Wonder – 01</a></li>
                                            <li class="col-xs-6"><a href="#.">Himalayan Wonder – 02</a></li>
                                            <li class="col-xs-6"><a href="#.">North East Tour 01</a></li>
                                            <li class="col-xs-6"><a href="#.">North East Tour 02</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </article>

                            <article class="box">

                                <div class="details col-sm-7 col-md-8">
                                    <div>
                                        <div>
                                            <h4 class="box-title"><i class="soap-icon-departure yellow-color"></i>Special Tour Packages</h4>

                                        </div>
                                    </div>
                                    <div>
                                        <ul class="discover triangle hover row" style="text-align: justify">
                                            <li class="col-xs-6"><a href="#.">Andaman and Nicobar Islands</a></li>
                                            <li class="col-xs-6"><a href="#.">Assam Tour</a></li>
                                            <li class="col-xs-6"><a href="#.">Meghalaya Tour</a></li>
                                            <li class="col-xs-6"><a href="#.">Palace on Wheels Train Tour</a></li>
                                            <li class="col-xs-6"><a href="#.">Classical Tour India</a></li>
                                            <li class="col-xs-6"><a href="#.">A King’s Journey</a></li>
                                            <li class="col-xs-6"><a href="#.">Monuments Tour India</a></li>
                                            <li class="col-xs-6"><a href="#.">Buddha Tours</a></li>

                                        </ul>
                                    </div>
                                </div>
                                <figure class="col-sm-5 col-md-4">
                                    <a title="" href="#" class="hover-effect popup-gallery">
                                        <img width="270" height="160" alt="" src="images/imgpackages/p6SpecialTourPackages.jpg"></a>
                                </figure>
                            </article>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
