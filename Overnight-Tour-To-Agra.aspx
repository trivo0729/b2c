﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Overnight-Tour-To-Agra.aspx.cs" Inherits="CUTUK.Overnight_Tour_To_Agra" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="page-wrapper">


        <div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">OVER NIGHT TOUR TO AGRA</h2>
                </div>
                <ul class="breadcrumbs pull-right">
                    <li><a href="#">HOME</a></li>
                    <li class="active">OVER NIGHT TOUR TO AGRA</li>
                </ul>
            </div>
        </div>

        <section id="content">
            <div class="container tour-detail-page">
                <div class="row">
                    <div id="main" class="col-md-12">
                        <div class="featured-image">
                            <img src="images/OVERNIGHTTOURTOAGRA1.jpg" alt="" />
                        </div>

                        <div id="tour-details" class="travelo-box">
                            <div class="intro small-box table-wrapper full-width hidden-table-sms">
                                <div class="col-sm-4 table-cell travelo-box">
                                    <dl class="term-description">
                                        <dt>3/4 Star Hotels based </dt>
                                        <dd>AGRA – DELHI</dd>
                                        <dt>Duration:</dt>
                                        <dd>2 Day</dd>
                                    </dl>
                                </div>
                                <div class="col-sm-8 table-cell">
                                    <div class="detailed-features">
                                        <div class="price-section clearfix">
                                            <div class="details">
                                                <h4 class="box-title">OVERNIGHTTOURTOAGRA<small>2 days tour</small></h4>
                                            </div>
                                            <div class="details">
                                                <!--<span class="price">$534</span>-->
                                                <a href="#" class="button green btn-small uppercase">Book Tour</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h2>Day 01</h2>
                            <p><strong>AGRA (Approx drive 03 hours)</strong> Morning after Breakfast pick up from Delhi airport or Hotels. Drive to Agra. Upon arrival met with our representative who will assist you for check into Hotel. Afternoon after Lunch visit Taj Mahal, Agra fort. Evening enjoy Live Perfomance show mohabbat e taj show in the Evening at 6:15 PM every day.</p>
                            <h2>Day 02</h2>
                            <p><strong>AGRA – DELHI (Approx drive 03 hours)</strong> Early Morning visit Taj Mahal During Sunrise. After Breakfast visit Itmad Daula (Baby Taj). Afternoon drive to Delhi for your transfer to drop you at Hotel or Airport.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    </div>
</asp:Content>
