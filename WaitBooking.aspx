﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WaitBooking.aspx.cs" Inherits="CUTUK.WaitBooking" %>

<!DOCTYPE html>
<!--[if IE 8]>          <html class="ie ie8"> <![endif]-->
<!--[if IE 9]>          <html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="">
<!--<![endif]-->
<head>
    <!-- Page Title -->
    <title>Hotel Booking</title>

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Travelo - Travel, Tour Booking HTML5 Template">
    <meta name="author" content="SoapTheme">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    <!-- Theme Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,200,300,500' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/animate.min.css">
    <script src="js/jquery-2.1.3.min.js"></script>

    <!-- Main Style -->
    <link id="main-style" rel="stylesheet" href="css/style.css">

    <!-- Updated Styles -->
    <link rel="stylesheet" href="css/updates.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="css/custom.css">

    <!-- Responsive Styles -->
    <link rel="stylesheet" href="css/responsive.css">

    <!-- CSS for IE -->
    <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie.css" />
    <![endif]-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->
    <script src="js/modal.js"></script>
</head>
<body class="soap-login-page style3 body-blank">

    <div id="page-wrapper" class="wrapper-blank">
        <header id="header" class="navbar-static-top">
            <div class="container">
                <h1 class="logo"></h1>
            </div>
        </header>
        <section id="content">
            <div class="container">
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <div id="main">
                    <h4 class="logo block">
                        <a href="Default.aspx" title="">
                            <img src="images/vacaaay1.png" alt="logo" />
                        </a>
                    </h4>
                    <br />
                    <div class="welcome-text box" style="font-size: 1.7em;">Please wait! Your request is being processed...</div>
                    <div class="welcome-text box" style="font-size: 1.2em;">In a few moments, you'll see your booking status and other details!</div>
                    <img src="images/loading.gif" style="height: 50px" />
                    <br />
                </div>
            </div>
        </section>
        <div class="modal fade" id="AlertModal" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

            <div class="modal-dialog" style="width: 40%; padding-top: 15%">

                <div class="modal-content">
                    <div class="modal-header" style="border-bottom: none">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>

                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="frow2">
                                <div>
                                    <center><span id="AlertMessage"></span></center>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="ModelMessege" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

            <div class="modal-dialog" style="width: 40%; padding-top: 15%">

                <div class="modal-content">
                    <div class="modal-header" style="border-bottom: none">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>

                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="frow2">
                                <div>
                                    <center><span id="SpnMessege"></span></center>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="ConformModal" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="alert" style="color: #ff6000">
                                <p class="size17" id="ConformMessage">
                                </p>
                                <table align="right" style="margin-left: -50%;">
                                    <tbody>
                                        <tr>
                                            <td style="border-top-width: 1px; border-top-style: solid; border-top-color: rgb(255, 255, 255);">
                                                <input type="button" id="Cancel" value="Cancel" onclick="Cancel()" class="button btn-mini red" style="float: none;">
                                                <br />
                                            </td>
                                            <br />
                                            <td style="border-top-width: 1px; border-top-style: solid; border-top-color: rgb(255, 255, 255);">
                                                <input type="button" value="Ok" id="btnOk" class="button btn-mini sky-blue1" style="float: none;">
                                            </td>

                                        </tr>
                                    </tbody>
                                </table>
                                <br />
                            </div>

                        </div>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>

    <!-- Javascript -->
    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.noconflict.js"></script>
    <script type="text/javascript" src="js/modernizr.2.7.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="js/jquery-ui.1.10.4.min.js"></script>

    <!-- Twitter Bootstrap -->
    <script type="text/javascript" src="js/bootstrap.js"></script>

    <!-- parallax -->
    <script type="text/javascript" src="js/jquery.stellar.min.js"></script>

    <!-- waypoint -->
    <script type="text/javascript" src="js/waypoints.min.js"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="js/theme-scripts.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>
    <script src="scripts/Alerts.js"></script>

    <script src="scripts/HotelRoom.js"></script>
    <script type="text/javascript">
        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }
        $(document).ready(function () {

            debugger;
            //var ConfirmId = getParameterByName('ConfirmationId');
            var ConfirmId = getParameterByName('Id');
            if (ConfirmId) {
                Waitforbooking(ConfirmId);
            }
            else {

                OnlinePayment();
            }
        });
    </script>

    <script>
        $('.btn-close').click(function (e) {
            e.preventDefault();
            $(this).parent().parent().parent().fadeOut();
        });
        $('.btn-minimize').click(function (e) {
            e.preventDefault();
            var $target = $(this).parent().parent().next('.box-content');
            if ($target.is(':visible')) $('i', $(this)).removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
            else $('i', $(this)).removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
            $target.slideToggle();
        });
        $('.btn-setting').click(function (e) {
            e.preventDefault();
            $('#myModal').modal('show');
        });
        function Cancel() {
            $("#ConformModal").modal("hide")
        }
        function close() {

            $("#AlertModal").modal("hide")
        }
        function Ok(Message, Method, arg) {
            debugger;
            $("#btnOk").css("display", "")
            var id = [];
            if (arg != null) {
                for (var i = 0; i < arg.length; i++) {
                    id.push('"' + arg[i] + '"')
                }
            }

            $("#ConformMessage").html(Message);
            if (arg == null)
                document.getElementById("btnOk").setAttribute("onclick", Method + "()")
            else
                document.getElementById("btnOk").setAttribute("onclick", Method + "(" + id + ")")
            $("#ConformModal").modal("show")
        }
        function Success(Message) {
            $("#AlertMessage").html(Message);
            $("#AlertModal").modal("show")
        }
    </script>

    <script type="text/javascript">
        enableChaser = 0; //disable chaser
    </script>
</body>
</html>
