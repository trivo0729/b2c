﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="PackageDetails.aspx.cs" Inherits="CUTUK.PackageDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="scripts/PackageDetail.js"></script>
    <script src="scripts/Package.js"></script>
    <script>
        window.onload = function () {
            var d = new Date().getTime();
            document.getElementById("tid").value = d;
        };

        //$("#txt_date").datepicker({
        //    changeMonth: true,
        //    changeYear: true,
        //    dateFormat: "dd-mm-yy",
        //    minDate: new Date(),
        //})
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="content">
        <div class="container">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-8">
                        <div class="tab-container">
                            <div class="tab-content">
                                <div class="tab-pane fade in active">
                                    <div class="intro table-wrapper full-width hidden-table-sms">
                                        <div class="tab-content" jstcache="0" id="PackgImg">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div id="cruise-features" class="tab-container" style="margin-top: 0px">

                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="cruise-description">
                                    <div class="intro table-wrapper full-width hidden-table-sms">

                                        <div class="col-sm-7 col-lg-8 table-cell cruise-itinerary" id="PackageRight">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="tab-container style1">
                            <ul class="tabs full-width" id="PkgTab">
                               
                            </ul>
                        </div>

                        <div class="travelo-box">
                            <button type="button" class="btn-medium icon-check uppercase full-width" onclick="Booking()">Book now</button>
                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div id="PackgDescription" class="tab-container style1">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade bs-example-modal-lg" id="BookingModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <form method="post" name="customerData" action="ccavRequestHandler.aspx">
                <input type="hidden" name="tid" id="tid" />
                <input type="hidden" name="amount" id="Amount" />
                <input type="hidden" name="order_id" id="order_id" />
                <input type="hidden" name="merchant_id" id="merchant_id" value="174467" />
                <input type="hidden" name="currency" id="currency" value="INR" />
                <input type="hidden" name="redirect_url" value="Packages.aspx" id="redirect_url" />
                <input type="hidden" name="cancel_url" value="ccavResponseHandler.aspx" id="cancel_url" />
                <div class="modal-content">
                    <div class="modal-header" style="border-bottom: 1px solid #fff">

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true">&times;</span></button>
                        <%--<h4 class="modal-title" id="gridSystemModalLabel3" style="text-align: left">Booking Details</h4>--%>
                    </div>

                    <div class="modal-body">
                        <div class="row">
                            <div style="background: rgba(255, 153, 0, 0.4); color: #fff; padding: 3px 10px 3px 10px;"><b>Booking Details</b></div>
                            <div class="">

                                <table>
                                    <tbody>

                                        <tr>
                                            <td style="border-top: 1px solid rgb(255, 255, 255); padding: 10px"><span class="dark bold">Package Name: </span></td>
                                            <td style="padding-left: 15px; border-top: 1px solid rgb(255, 255, 255);"><span class="dark" id="dspPackageName"></span></td>
                                            <td style="padding-left: 20px; border-top: 1px solid rgb(255, 255, 255);"><span class="dark bold">City: </span></td>
                                            <td style="padding-left: 15px; border-top: 1px solid rgb(255, 255, 255);"><span class="dark" id="dspCitys"></span></td>
                                            <td style="padding-left: 20px; border-top: 1px solid rgb(255, 255, 255);"><span class="dark bold">Days: </span></td>
                                            <td style="padding-left: 15px; border-top: 1px solid rgb(255, 255, 255);"><span class="dark" id="dspNDays"></span></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid rgb(255, 255, 255); padding: 10px"><span class="dark bold">Starts From: </span></td>
                                            <td style="padding-left: 15px; border-top: 1px solid rgb(255, 255, 255);"><span class="dark" id="dspCheckIn"></span></td>
                                            <td style="padding-left: 20px; border-top: 1px solid rgb(255, 255, 255);"><span class="dark bold">Up To: </span></td>
                                            <td style="padding-left: 15px; border-top: 1px solid rgb(255, 255, 255);"><span class="dark" id="dspCheckOut"></span></td>

                                        </tr>
                                    </tbody>
                                </table>
                                <div class="clearfix"></div>
                                <br>
                            </div>
                        </div>
                        <div class="row">
                            <div style="background: rgba(255, 153, 0, 0.4); color: #fff; padding: 3px 10px 3px 10px;"><b>Passenger Details</b></div>
                            <div class="frow3">
                                <div class="col-md-6">

                                    <label class="margtop10">Name</label>
                                    <input type="text" id="txt_name" name="billing_name" placeholder="Name" class="form-control logpadding" />
                                    <label class="margtop10">Phone</label>
                                    <input type="text" name="billing_tel" placeholder="Phone" id="txt_phone" class="form-control logpadding" />
                                    <label class="margtop10">E-mail</label>
                                    <input type="text" name="billing_email" placeholder="E-mail" id="txt_email" class="form-control logpadding" />
                                    <label class="margtop20">Adults</label>
                                    <select id="Select_Adults" class="form-control logpadding">
                                        <option value="1">1</option>
                                        <option value="2" selected="selected">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                    </select>


                                    <div class="row">
                                        <div class="col-md-4" id="Childs">
                                            <label class="margtop20">Childs</label>
                                            <select id="Select_CHILD" class="form-control logpadding " onchange="NoChild(this.value)">
                                                <option value="0" selected="selected">0</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>

                                            </select>
                                        </div>
                                        <div class="col-md-4" id="div_FirstChild" style="display: none">
                                            <label class="margtop20">Ages</label>
                                            <select id="Select_AgeChildSecond1" class="form-control logpadding ">
                                                <option selected="selected" value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4" id="div_SecondChild" style="display: none">
                                            <label class="margtop20">
                                                <br />
                                            </label>
                                            <select id="Select_AgeChildSecond2" class="form-control logpadding ">
                                                <option selected="selected" value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="margtop10">Travel date</label>
                                    <input type="text" id="txt_date" placeholder="dd-mm-yy" class="form-control logpadding" />
                                    <%-- <div class="datepicker-wrap">
                                        <input type="text" name="date_from" id="txt_date" style="cursor: pointer" class="form-control logpadding" placeholder="Check In" />
                                    </div>--%>
                                    <label class="margtop10">Add on required</label>
                                    <textarea rows="13" id="txt_Remark" class="form-control"></textarea>
                                    <br />
                                    <button id="Btn_CCAvenue" type="submit" class="button btn-small orange">Book</button>
                                    <br />
                                </div>
                            </div>
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                        </div>

                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
            <br />
            <br />
            <br />
            <br />
        </div>
    </div>
    <div class="modal fade" id="AlertModal" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="border-bottom: 1px solid #fff">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" title="Close">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <%-- <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Incomplete Document</b><label id="No"></label></div>--%>
                        <div>
                            <p class="size17" id="AlertMessage">
                            </p>
                            <br />

                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</asp:Content>
