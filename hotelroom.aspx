﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="hotelroom.aspx.cs" Inherits="CUTUK.hotelroom" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Travo: Room Details
    </title>
    <!-- Bootstrap -->
    <link href="../dist/css/bootstrap.css" rel="stylesheet" media="screen" />
    <link href="../assets/css/custom.css" rel="stylesheet" media="screen" />
    <link href="../examples/carousel/carousel.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js"></script>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->
    <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:700,400,300,300italic' rel='stylesheet' type='text/css' />
    <!-- Font-Awesome -->
    <link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.css" media="screen" />
    <!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="assets/css/font-awesome-ie7.css" media="screen" /><![endif]-->
    <!-- REVOLUTION BANNER CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="../css/fullscreen.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../rs-plugin/css/settings.css" media="screen" />
    <!-- Picker UI-->
    <link rel="stylesheet" href="../assets/css/jquery-ui.css" />
    <!-- bin/jquery.slider.min.css -->
    <link rel="stylesheet" href="../plugins/jslider/css/jslider.css" type="text/css" />
    <link rel="stylesheet" href="../plugins/jslider/css/jslider.round-blue.css" type="text/css" />
    <!-- jQuery-->
    <script src="../assets/js/jquery.v2.0.3.js"></script>
    <!-- jQuery Hotel Image Slider-->
    <link href="../SliderFiles/css/lightGallery.css" rel="stylesheet" />
    <script src="../SliderFiles/js/lightGallery.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            debugger;
            var param1 = getParameterByName('hotelcode');
            var param2 = getParameterByName('roomcode');
            var param3 = getParameterByName('Supplier');
            var param4 = getParameterByName('roomdescriptionId');
            var param5 = getParameterByName('RoomP');
            var param6 = getParameterByName('_noRooms');
            var param7 = getParameterByName('CctCode');
            param5 = hasWhiteSpace(param5);
            HotelRoom(param1, param2, param3, param4, param5, param6, param7);
            //HotelRoom(param1, param2, param3);
        });
        function OpenSlider() {
            $("#light-gallery").lightGallery();
        }
    </script>
    <script src="../assets/js/jquery-ui.js"></script>
    <!-- bin/jquery.slider.min.js -->
    <script type="text/javascript" src="../plugins/jslider/js/jshashtable-2.1_src.js"></script>
    <script type="text/javascript" src="../plugins/jslider/js/jquery.numberformatter-1.2.3.js"></script>
    <script type="text/javascript" src="../plugins/jslider/js/tmpl.js"></script>
    <script type="text/javascript" src="../plugins/jslider/js/jquery.dependClass-0.1.js"></script>
    <script type="text/javascript" src="../plugins/jslider/js/draggable-0.1.js"></script>
    <script type="text/javascript" src="../plugins/jslider/js/jquery.slider.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
      <br />
    <br />
    <br />
    <div class="container breadcrub">
        <div>
            <a class="homebtn left" href="#"></a>
            <div class="left">
                <ul class="bcrumbs">
                    <li>/</li>
                    <li><a href="b2chotel.aspx?success=1">Back</a></li>
                </ul>
            </div>
            <a class="backbtn right" href="#"></a>
        </div>
        <div class="clearfix"></div>
        <div class="brlines"></div>
    </div>
    <div class="container mt10">
        <div class="Hoteldetails"></div>

        <div class="container mt25 offset-0">


            <!-- LEFT CONTENT -->
            <div class="col-md-8 offset-0 ">

                <div class="padding10 opensans">
                    <div class="pagecontainer2">
                        <ul class="nav nav-tabs" id="myTab">
                            <li onclick="mySelectUpdate()" class="active"><a data-toggle="tab" href="#roominfo"><span class="rates\"></span><span class="hidetext">Room Info</span>&nbsp;</a></li>
                            <li onclick="mySelectUpdate()"><a data-toggle="tab" href="#summary"><span class="summary"></span><span class="hidetext">Hotel Info</span>&nbsp;</a></li>
                            <li onclick="mySelectUpdate()"><a data-toggle="tab" href="#roomrates"><span class="rates"></span><span class="hidetext">Room Options</span>&nbsp;</a></li>
                            <li onclick="loadScript()"><a data-toggle="tab" href="#maps"><span class="maps"></span><span class="hidetext">Maps</span>&nbsp;</a></li>
                        </ul>
                        <div class="tab-content4">
                            <div id="roominfo" class="tab-pane fade active in">
  

                            </div>
                            <div id="summary" class="tab-pane fade">

                            </div>
                            <div id="roomrates" class="tab-pane fade">
                                
                            </div>
                            <div id="maps" class="tab-pane fade">
                            </div>

                        </div>

                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>
            <!-- END OF LEFT CONTENT -->

            <!-- RIGHT CONTENT -->
            <div class="col-md-4">
                <div class="" id="HotelDetailRight">
                </div>

            </div>
            <!-- END OF RIGHT CONTENT -->
        </div>


    </div>
    <div id="divhotelroom" class="" style="height: 700px;">
    </div>
    <div class="modal fade bs-example-modal-lg" id="TermsModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel2" style="text-align: left">Cancellation Policy</h4>
                </div>
                <div class="modal-body">
                    <p id="Policy" class="MsoNormal" style="text-align: justify; color: #ff9966"></p>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bs-example-modal-lg" id="PromotionModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel4" style="text-align: left">Promotion</h4>
                </div>
                <div class="modal-body">
                    <p id="Promotion" class="MsoNormal" style="text-align: justify; color: #ff9966"></p>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bs-example-modal-lg" id="FacilitiesModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel3" style="text-align: left">Cancellation Policy</h4>
                </div>
                <div class="modal-body">
                    <p id="Facilities" class="MsoNormal" style="text-align: justify"></p>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bs-example-modal-lg" id="CMinStay" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel6" style="text-align: left">Minimum Stay Policy</h4>
                </div>
                <div class="modal-body">
                    <p id="MinStay" class="MsoNormal" style="text-align: justify; color: #ff9966"></p>
                </div>
            </div>
        </div>
    </div>
    <!-- Javascript -->
    <script src="../assets/js/js-details.js"></script>
    <!-- Googlemap -->
    <script src="../assets/js/initialize-google-map.js"></script>
    <!-- Custom Select -->
    <script type='text/javascript' src='../assets/js/jquery.customSelect.js'></script>
    <!-- Custom functions -->
    <script src="../assets/js/functions.js"></script>
    <!-- Nicescroll  -->
    <script src="../assets/js/jquery.nicescroll.min.js"></script>
    <!-- jQuery KenBurn Slider  -->
    <script type="text/javascript" src="../rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
    <!-- CarouFredSel -->
    <script src="../assets/js/jquery.carouFredSel-6.2.1-packed.js"></script>
    <script src="../assets/js/helper-plugins/jquery.touchSwipe.min.js"></script>
    <script type="text/javascript" src="../assets/js/helper-plugins/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="../assets/js/helper-plugins/jquery.transit.min.js"></script>
    <script type="text/javascript" src="../assets/js/helper-plugins/jquery.ba-throttle-debounce.min.js"></script>
    <!-- Counter -->
    <script src="../assets/js/counter.js"></script>
    <!-- Carousel-->

    <!-- Js Easing-->
    <script src="../assets/js/jquery.easing.js"></script>
    <!-- Bootstrap-->
    <script src="../dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../scripts/HotelRoom.js?v=1.1"></script>
    <script src="scripts/B2CCustomerLogin.js"></script>
    <%--  <link rel="stylesheet" href="../others/light-gallery/css/lightGallery.css" />
    <script src="../others/light-gallery/js/lightGallery.js"></script>--%>
    <script type="text/javascript">
        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }

        function hasWhiteSpace(s) {
            if (s.indexOf(' ') >= 0)
                return s.replace(/ /g, '+');
            else
                return s;
        }
        //$(document).ready(function () {
        //    $("#light-gallery").lightGallery();
        //    var param1 = getParameterByName('hotelcode');
        //    var param2 = getParameterByName('roomcode');
        //    HotelRoom(param1, param2);
        //});
        //function CancellationPolicy(obj, amount, fdate, tdate, bdate) {
        //    var data = 'Charges - ' + amount + ' INR applicable if cancelled between ' + fdate + ' and ' + tdate + '.No Charges before ' + bdate + ' hrs';
        //    $('#Policy').html(data);
        //    $(obj).attr('data-toggle', 'modal');
        //}
        function CancellationPolicy(obj, msg) {
            //msg = msg.replace("fa fa-inr", "\"fa fa-inr\"");
            $('#Policy').html(msg);
            $(obj).attr('data-toggle', 'modal');
        }
        function Promotion(obj, msg) {
            $('#Promotion').html(msg);
            $(obj).attr('data-toggle', 'modal');
        }
        function RoomFacilities(obj, msg) {
            $('#Facilities').html(msg);
            $(obj).attr('data-toggle', 'modal');
        }
        function MinStay(obj, msg) {
            $('#MinStay').html(msg);
            $(obj).attr('data-toggle', 'modal');
        }
    </script>
</asp:Content>
