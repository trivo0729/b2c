﻿using CUT.BL;
using CUT.DataLayer;
using CUTUK.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CUTUK
{
    /// <summary>
    /// Summary description for DashBoardHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class DashBoardHandler : System.Web.Services.WebService
    {

        string json = "";
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

        private string escapejosndata(string data)
        {
            return data.Replace("\\", "\\\\").Replace("\"", "\\\"");
        }

        [WebMethod(EnableSession = true)]
        public string GetProfile()
        {

            DataTable dtResult = new DataTable();


            Int64 sid;
            string UserName = "";

            string B2C_Id = "";
            string Mobile = "";
            string Email = "";
            string Phone = "";
            string City = "";
            string Address = "";


            if (HttpContext.Current.Session["LoginUser"] != null)
            {
                GlobalDefault objGlobalDefaultCustomer = (GlobalDefault)HttpContext.Current.Session["LoginCustomer"];
                sid = objGlobalDefaultCustomer.sid;
                UserName = objGlobalDefaultCustomer.UserName;

                B2C_Id = objGlobalDefaultCustomer.B2C_Id;
                Mobile = objGlobalDefaultCustomer.Mobile;
                Email = objGlobalDefaultCustomer.Email;
                Phone = objGlobalDefaultCustomer.Phone;
                City = objGlobalDefaultCustomer.City;
                Address = objGlobalDefaultCustomer.Address;

            }
            return jsSerializer.Serialize(new { Session = 1, retCode = 1, UserName = UserName, B2C_Id = B2C_Id, Mobile = Mobile, Email = Email, Phone = Phone, City = City, Address = Address });
        }

        [WebMethod(EnableSession = true)]
        public string UpdateDetails(string Mobile, string Phone, string City, string Address)
        {
            DBHelper.DBReturnCode retCode = DashBoardManager.UpdateDetails(Mobile, Phone, City, Address);
            if (DBHelper.DBReturnCode.SUCCESS == retCode)
            {
                GlobalDefault objGlobalDefaultCustomer = (GlobalDefault)HttpContext.Current.Session["LoginCustomer"];
                objGlobalDefaultCustomer.Mobile = Mobile;
                objGlobalDefaultCustomer.Phone = Phone;
                objGlobalDefaultCustomer.Address = Address;
                objGlobalDefaultCustomer.City = City;
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
        }


        [WebMethod(EnableSession = true)]
        public string ChangePassword(string Old, string NewPassword)
        {

            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                GlobalDefault objGlobalDefaultCustomer = (GlobalDefault)HttpContext.Current.Session["LoginCustomer"];
                string Match = objGlobalDefaultCustomer.password;

                if (Old != Match)
                {
                    json = "{\"Session\":\"0\",\"Retcode\":\"2\"}";
                    return json;
                }
                else
                {
                    Int64 Sid = objGlobalDefaultCustomer.sid;
                    DBHelper.DBReturnCode retCode;
                    string sEncryptedPasswords = CUT.Common.Cryptography.EncryptText(objGlobalDefaultCustomer.password);
                    DataTable dtResult = null;
                    SqlParameter[] sqlParams = new SqlParameter[2];
                    sqlParams[0] = new SqlParameter("@Email", objGlobalDefaultCustomer.Email);
                    //string sEncryptedPassword = CUT.Common.Cryptography.EncryptText(sEncryptedPasswords);
                    sqlParams[1] = new SqlParameter("@Password", sEncryptedPasswords);
                    retCode = DBHelper.GetDataTable("Proc_tbl_B2C_CustomerLoginLoad", out dtResult, sqlParams);
                    string sEncryptedPassword = CUT.Common.Cryptography.EncryptText(NewPassword);
                    Sid =Convert.ToInt16(dtResult.Rows[0]["Sid"]) ;
                    retCode = DashBoardManager.ChangePassword(Sid, sEncryptedPassword);
                    if (DBHelper.DBReturnCode.SUCCESS == retCode)
                    {
                        objGlobalDefaultCustomer.password = NewPassword;
                        HttpContext.Current.Session["LoginUser"] = objGlobalDefaultCustomer;
                        json = "{\"Session\":\"0\",\"Retcode\":\"1\"}";
                    }
                    else
                        json = "{\"Session\":\"0\",\"Retcode\":\"0\"}";
                    //  tbl_B2C_CustomerLogin NewLogin = DB.tbl_B2C_CustomerLogins.Single(x => x.Sid == Sid);
                    //  NewLogin.Password = sEncryptedPassword;
                    ////  NewLogin.Confirm_Password = NewPassword;
                    //  DB.SubmitChanges();
                }
            }
            catch
            {
                json = "{\"Session\":\"0\",\"Retcode\":\"-1\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string BookingList()
        {
            Int32 Get = 10;
            string jsonString = "";
            DataTable dtResult;
            GlobalDefault objGlobalDefaultCustomer = (GlobalDefault)HttpContext.Current.Session["LoginCustomer"];
            string AgencyName = objGlobalDefaultCustomer.AgencyName;
            Int64 Uid = objGlobalDefaultCustomer.sid;

            DBHelper.DBReturnCode retcode = DashBoardManager.BookingList(Uid, out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                DBHandlerDataContext DB=new DBHandlerDataContext();
                var List = (from Obj in DB.tbl_HotelReservations where Obj.ReservationID == dtResult.Rows[0]["ReservationID"].ToString() select Obj).ToList();
                DataView myDataView = dtResult.DefaultView;
                myDataView.Sort = "Sid DESC";
                DataTable SorteddtResult = myDataView.ToTable();
                Session["PaggingSession"] = SorteddtResult;
                try
                {
                    IEnumerable<DataRow> allButFirst = SorteddtResult.AsEnumerable().Skip(0).Take(Get);
                    SorteddtResult = allButFirst.CopyToDataTable();
                    string Count = (dtResult.Rows.Count).ToString();
                    jsonString = "";

                    foreach (DataRow dr in SorteddtResult.Rows)
                    {
                        jsonString += "{";
                        foreach (DataColumn dc in SorteddtResult.Columns)
                        {
                            jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                        }
                        jsonString = jsonString.Trim(',') + "},";
                    }
                    jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"Count\":\"" + Count + "\",\"ReservationDetails\":[" + jsonString.Trim(',') + "],\"Type\":\"" + List[0].Source + "\"}";
                    SorteddtResult.Dispose();

                }
                catch
                {
                    jsonString = "{\"Session\":\"1\",\"retCode\":\"2\"}";
                    SorteddtResult = dtResult.Clone();
                }

            }
            else if (retcode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        static List<string> arrCategory = new List<string> { "Standard", "Deluxe", "Premium", "Luxury" };
        [WebMethod(EnableSession = true)]
        public string PackageBooking()
        {
            DBHandlerDataContext DB = new DBHandlerDataContext();
            try
            {
                GlobalDefault objGlobalDefaultCustomer = (GlobalDefault)HttpContext.Current.Session["LoginCustomer"];
                string CustomereId = objGlobalDefaultCustomer.B2C_Id;

                var List = (from obj in DB.tbl_PackageReservations
                            join meta in DB.tbl_Packages on obj.PackageID equals meta.nID
                            where obj.B2CId == CustomereId
                            
                            select new
                            {
                               TravelDate = obj.TravelDate,
                               PaxName = obj.LeadingPax,
                               PackageName = meta.sPackageName,
                               CatID = arrCategory[Convert.ToInt32(obj.CategoryId) - 1],
                               Location = meta.sPackageDestination,
                               StartDate = obj.StartFrom,
                               EndDate = obj.EndDate,
                               Status = obj.Status
                            }).ToList();

                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

    }
}
