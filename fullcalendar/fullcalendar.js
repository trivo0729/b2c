$(document).ready(function () {
    GetSearch();
});
function GetSearch() {
    $.ajax({
        type: "POST",
        url: "../handler/FlightHandler.asmx/Get_CalendarSerchFlightevents",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            debugger;
            $('#fullCalendar').fullCalendar('destroy')
            var arrEvents = new Array();
            var SearchResult = result.CalendarList.SearchResults;
            for (var i = 0; i < SearchResult.length; i++) {
                if (SearchResult[i].IsLowestFareMonth == true)
                    Color = "#FF0000";
                else
                    Color = "#3584ff";
                var Event = {
                    id: i,
                    title: '<span class=""glyphicons""></span> Event with glyphicons',
                    start: SearchResult[i].DepartureDate,
                    //url: SearchResult.url,
                    end: SearchResult[i].DepartureDate,
                    //Type: result.CalendarList[i].Type,
                    textEscape: false,
                    color: Color
                }
                arrEvents.push(Event)
            }
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            var calendar = $('#fullCalendar').fullCalendar({
                editable: true,
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                events: arrEvents,
                // Convert the allDay from string to boolean
                eventRender: function (event, element, view) {
                    if (event.allDay === 'true') {
                        event.allDay = true;
                    } else {
                        event.allDay = false;
                    }
                    event.editable = false;
                },
                selectable: true,
                selectHelper: true,

                editable: true,
                eventAfterAllRender: function (view) {
                    //$(".fc-event-container").append("<span class='closon'>X</span>");
                },
                eventDrop: function (event, delta) {

                },
                eventResize: function (event) {


                },
                eventClick: function (event) {

                },
            });
        }
    });
}