﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using CUT.BL;
using CUT.DataLayer;
using CUT.Models;
using CUT.ModelsFlight;
using System.Web.Script.Serialization;
using TboFlightLib.Request.Search;
using TboFlightLib.Request.CalenderFare;
using TboFlightLib.Request;
using Newtonsoft.Json;
namespace CUTUK.handler
{
    /// <summary>
    /// Summary description for FlightHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class FlightHandler : System.Web.Services.WebService
    {

        string json = "";
        CUT.handler.FlightHandler obj = new CUT.handler.FlightHandler();
        #region Get Destination
        [WebMethod(EnableSession = true)]
        public string GetCityList(string name)
        {
            string json = "";
            return json = obj.GetCityList(name);
        }
        #endregion

        #region Serch Flights
        [WebMethod(EnableSession = true)]
        public string SerchFlight(Search ObjSearch)
        {
            string json = "";
            return json = obj.SerchFlight(ObjSearch);
        }

        [WebMethod(EnableSession = true)]
        public string CalendarSerchFlight(CalenderFare ObjCalendarSearch)
        {
            string json = "";
            return json = obj.CalendarSerchFlight(ObjCalendarSearch);
        }

        [WebMethod(EnableSession = true)]
        public string Get_CalendarSerchFlightevents()
        {
            string json = "";
            return json = obj.Get_CalendarSerchFlightevents();
        }

        [WebMethod(EnableSession = true)]
        public string Getflight()
        {
            string json = "";
            return json = obj.Getflight();
        }

        [WebMethod(EnableSession = true)]
        public string Filter(CUT.ModelsFlight.Segments ObjSearch)
        {
            string json = "";
            return json = obj.Filter(ObjSearch);
        }
        [WebMethod(EnableSession = true)]
        public string GetBooking(List<string> ResultIndex)
        {
            string json = "";
            return json = obj.GetBooking(ResultIndex);
        }
        [WebMethod(EnableSession = true)]
        public string UpdateCalendarSerchFlight(CalenderFare ObjCalendarSearch)
        {
            string json = "";
            return json = obj.UpdateCalendarSerchFlight(ObjCalendarSearch);
        }

        [WebMethod(EnableSession = true)]
        public string BookCalendarFlight(string Adult, string Child, string Infant)
        {
            string json = "";
            return json = obj.BookCalendarFlight(Adult, Child, Infant);
        }

        [WebMethod(EnableSession = true)]
        public string GetFareRule(string ResultIndex)
        {
            string json = "";
            return json = obj.GetFareRule(ResultIndex);
        }
        #endregion

        #region Get Fare Quote
        [WebMethod(EnableSession = true)]
        public string GetFarQuote(List<string> ReultIndex)
        {
            string json = "";
            return json = obj.GetFarQuote(ReultIndex);
        }
        #endregion

        #region Get Advance Search
        [WebMethod(EnableSession = true)]
        public string GetSegments(string ResultIndex, List<string> FareClass)
        {
            string json = "";
            return json = obj.GetSegments(ResultIndex, FareClass);
        }
        #endregion

        #region Book a Flight
        [WebMethod(EnableSession = true)]
        public string CheckAvailCredit(List<string> arrIndex)
        {
            string json = "";
            return json = obj.CheckAvailCredit(arrIndex);
        }


        //[WebMethod(EnableSession = true)]
        //public string Booking(Booking objFlightBooking)
        //{
        //    string json = "";
        //    return json = obj.Booking(objFlightBooking);
        //}


        [WebMethod(EnableSession = true)]
        public string Booking(Booking objFlightBooking)
        {
            //string json = "";
            //   string json = obj.Booking(objFlightBooking);
            string json = System.IO.File.ReadAllText(@"C:\Users\Nass\Desktop\Res.txt");
            var arrResponse = JsonConvert.DeserializeObject<dynamic>(json);
            string Response = arrResponse.retCode.ToString();

            if (Response == "1")
            {
                for (int i = 0; i < arrResponse.PNR.Count; i++)
                {
                    string BookingID = arrResponse.PNR[i].BookingID.ToString();
                    DBHandlerDataContext DB = new DBHandlerDataContext();
                    GlobalDefault objGlobalDefaultCustomer = (GlobalDefault)HttpContext.Current.Session["LoginCustomer"];
                    string UserID = objGlobalDefaultCustomer.B2C_Id;
                    tbl_AirReservation Update = DB.tbl_AirReservations.Single(x => x.BookingID == BookingID);
                    Update.B2CId = UserID;
                    DB.SubmitChanges();
                }

            }
            return json;

        }

        [WebMethod(EnableSession = true)]
        public string GetBookings()
        {

            string json = "";
            return json = obj.GetBookings();
        }
        #endregion

        #region Get Booking List
        //[WebMethod(EnableSession = true)]
        //public string GetBookingList()
        //{
        //    string json = "";
        //    return json = obj.GetBookingList();
        //}

        [WebMethod(EnableSession = true)]
        public string GetBookingList()
        {
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            DBHandlerDataContext DB = new DBHandlerDataContext();
            GlobalDefault objGlobalDefaultCustomer = (GlobalDefault)HttpContext.Current.Session["LoginCustomer"];
            objSerlizer = new JavaScriptSerializer();
            objSerlizer.MaxJsonLength = Int32.MaxValue;
            try
            {
                var arrResrvation = (from obj in DB.tbl_AirReservations
                                     where obj.B2CId == objGlobalDefaultCustomer.B2C_Id
                                     select obj).ToList();
                return objSerlizer.Serialize(new { retCode = 1, arrAirTicket = arrResrvation });
            }
            catch
            {
                return objSerlizer.Serialize(new { retCode = 0 });
            }
        }

        #endregion

        [WebMethod(EnableSession = true)]
        public string CancelBooking(Int64 BookingID, string Type)
        {
            string json = "";
            return json = obj.CancelBooking(BookingID, Type);
        }

        [WebMethod(EnableSession = true)]
        public string GetselCityList(string sndcountry)
        {
            DBHandlerDataContext DB = new DBHandlerDataContext();
            string jsonString = "";
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            if (sndcountry == "")
            {
                try
                {
                    var dList = (from obj in DB.tbl_TBOCityCodes
                                 select new
                                 {
                                     City_Name = obj.City_Name + "(" + obj.City_Code + ")," + obj.Country_name,
                                     City_Code = obj.City_Code,

                                 }).Distinct().ToList();

                    //json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = dList });
                    return jsonString = objSerlizer.Serialize(new { Session = 1, retCode = 1, Arr = dList });
                }
                catch
                {
                    return "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }
            else
            {
                try
                {
                    var dList = (from obj in DB.tbl_TBOCityCodes
                                 where obj.Country_Code == sndcountry
                                 select new
                                 {
                                     City_Name = obj.City_Name,
                                     City_Code = obj.City_Code,

                                 }).Distinct().ToList();

                    //json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = dList });
                    return jsonString = objSerlizer.Serialize(new { Session = 1, retCode = 1, Arr = dList });
                }
                catch
                {
                    return "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }

        }

    }
}
