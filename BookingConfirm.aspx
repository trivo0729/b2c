﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BookingConfirm.aspx.cs" Inherits="CUTUK.BookingConfirm" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Confirm Booking</title>

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Travelo - Travel, Tour Booking HTML5 Template">
    <meta name="author" content="SoapTheme">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    <!-- Theme Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,500,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/animate.min.css">

    <!-- Current Page Styles -->
    <link rel="stylesheet" type="text/css" href="components/revolution_slider/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/revolution_slider/css/style.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/jquery.bxslider/jquery.bxslider.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/flexslider/flexslider.css" media="screen" />

    <!-- Main Style -->
    <link id="main-style" rel="stylesheet" href="css/style.css">

    <!-- Updated Styles -->
    <link rel="stylesheet" href="css/updates.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="css/custom.css">

    <!-- Responsive Styles -->
    <link rel="stylesheet" href="css/responsive.css">
    <script src="js/jquery-2.1.3.min.js"></script>
    <script src="scripts/ConfirmationDetails.js"></script>
    <script src="scripts/Invoice.js"></script>

    <!-- CSS for IE -->
    <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie.css" />
    <![endif]-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->

</head>
<body>
    <div id="page-wrapper">

        <header id="header" class="navbar-static-top style4">
            <div class="container">
                <h1 class="logo navbar-brand">
                    <a href="Default.aspx" title="Vacaay.com - home">
                       <img src="images/vacaaay1.png" alt="Vacaay logo" style="width: 306px; height: 50px;" />
                    </a>
                </h1>
                <div class="pull-right hidden-mobile">
                    <ul class="social-icons clearfix pull-right hidden-mobile">
                        <li class="twitter"><a title="twitter" href="#" data-toggle="tooltip"><i class="soap-icon-twitter"></i></a></li>
                        <li class="facebook"><a title="facebook" href="#" data-toggle="tooltip"><i class="soap-icon-facebook"></i></a></li>
                        <li class="linkedin"><a title="linkedin" href="#" data-toggle="tooltip"><i class="soap-icon-linkedin"></i></a></li>
                        <li class="vimeo"><a title="vimeo" href="#" data-toggle="tooltip"><i class="soap-icon-vimeo"></i></a></li>
                    </ul>
                </div>
               
            </div>
            <a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle">Mobile Menu Toggle
            </a>

          <div class="main-navigation">
                <div class="container">
                    <nav id="main-menu" role="navigation">
                        <ul class="menu">
                            <li class="menu-item-has-children">
                                <a href="Default.aspx">Home</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="AboutUs.aspx">About Us</a>
                            </li>

                            <%--<li class="menu-item-has-children">
                                <a href="HotelSearch.aspx">Hotels</a>
                            </li>--%>

                            <li class="menu-item-has-children">
                                <a href="Packages.aspx">Packages</a>
                            </li>

                            <%-- <li class="menu-item-has-children">
                                <a href="TourPackages.aspx">Tour Packages</a>
                            </li>--%>


                            <li class="menu-item-has-children">
                                <a href="Contact.aspx">Contact Us</a>
                            </li>

                        </ul>
                        <ul class="menu pull-right">
                            <li><a href="#" onclick="SignUpModal()">login / Register</a></li>
                            <li><a href="http://b2b.vacaaay.com/">B2B Login</a></li>
                        </ul>
                    </nav>
                </div>
            </div>

            <nav id="mobile-menu-01" class="mobile-menu collapse">
                <ul id="mobile-primary-menu" class="menu">
                    <li class="menu-item-has-children">
                        <a href="Default.aspx">Home</a>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="AboutUs.aspx">About Us</a>
                    </li>

                    <%--<li class="menu-item-has-children">
                        <a href="HotelSearch.aspx">Hotels</a>
                    </li>--%>

                    <li class="menu-item-has-children">
                        <a href="Packages.aspx">Packages</a>
                    </li>


                    <li class="menu-item-has-children">
                        <a href="Contact.aspx">Contact Us</a>
                    </li>

                </ul>
                <ul class="menu pull-right">
                    <li><a href="#" onclick="SignUpModal()">login / Register</a></li>
                    <li><a href="http://b2b.vacaaay.com/">B2B Login</a></li>
                </ul>
            </nav>
        </header>

        <section id="content" class="gray-area">
            <div class="container">
                <div class="row">
                    <div id="main" class="col-sm-8 col-md-9">
                        <div class="booking-information travelo-box">
                            <h2>Booking Confirmation</h2>
                            <hr />
                            <div class="booking-confirmation clearfix">
                                <i class="soap-icon-recommend icon circle"></i>
                                <div class="message">
                                    <h4 class="main-message" id="spComplete"></h4>
                                    <br />
                                    <h5 id="spAck"></h5>
                                    <p>A confirmation email has been sent to your provided email address.</p>
                                    <br />
                                </div>
                            </div>
                            <hr />
                            <h2>Booking Details</h2>
                            <dl class="term-description" id="bookingdetails">
                            </dl>
                            <hr />
                        </div>
                    </div>
                    <div class="sidebar col-sm-4 col-md-3">
                        <div class="travelo-box contact-box">
                            <h4>Need Trivo Help?</h4>
                            <p>We would be more than happy to help you. Our team advisor are 24/7 at your service to help you.</p>
                            <address class="contact-details">
                                <%--<span class="contact-phone"><i class="soap-icon-phone"></i> +91 9910084810 </span><br/>
                                <span class="contact-phone"><i class="soap-icon-phone"></i> 0562 -22334810</span><br/>--%>
                                <span class="contact-phone"><i class="soap-icon-message"></i> info@Vacaaay.com</span>
                                <br/>
                            </address>
                        </div>
                        <%--<div class="travelo-box book-with-us-box">
                            <h4>Why Book with us?</h4>
                            <ul>
                                <li>
                                    <i class="soap-icon-hotel-1 circle"></i>
                                    <h5 class="title"><a href="#">135,00+ Hotels</a></h5>
                                    <p>Nunc cursus libero pur congue arut nimspnty.</p>
                                </li>
                                <li>
                                    <i class="soap-icon-savings circle"></i>
                                    <h5 class="title"><a href="#">Low Rates &amp; Savings</a></h5>
                                    <p>Nunc cursus libero pur congue arut nimspnty.</p>
                                </li>
                                <li>
                                    <i class="soap-icon-support circle"></i>
                                    <h5 class="title"><a href="#">Excellent Support</a></h5>
                                    <p>Nunc cursus libero pur congue arut nimspnty.</p>
                                </li>
                            </ul>
                        </div>--%>
                    </div>
                </div>
            </div>
        </section>

          <footer id="footer" class="style5">
              <div class="footer-wrapper" style="background-color: #2d3e52">
                <div class="container">
                    <div class="row">
                        <%-- <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2 style="color:wheat">Address</h2>
                              <p style="text-align: justify ;color:wheat;">
                              Airlines Travel Centre <br />Sdn.Bhd.(1073291-U)
                              KKKP/PL 7592<br />
                              NO41,Level 2, Jalan Sultan Yussuf<br />
                              30000 Ipoh Perak <br />
                              Malaysia
                              </p>
                            </div>--%>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">

                            <h2 style="color: wheat">Discover</h2>
                            <hr />
                            <br />
                            <br />
                            <ul class="discover triangle hover row" style="color: wheat">
                                <li class="col-xs-6"><a href="Default.aspx">HOME</a></li>
                                <li class="col-xs-6"><a href="AboutUs.aspx">ABOUT US</a></li>
                                <%--<li class="col-xs-6"><a href="HotelSearch.aspx">HOTELS</a></li>--%>
                                <li class="col-xs-6"><a href="Packages.aspx">PACKAGES</a></li>
                                <li class="col-xs-6"><a href="Contact.aspx">CONTACT US</a></li>
                            </ul>




                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2 style="color: wheat">Trade Association</h2>
                            <hr />
                            <br />
                            <br />
                            <ul class="travel-news">
                                <li>

                                    <address class="contact-details">

                                        <ul class="social-icons clearfix">



                                            <img src="https://tvlk.imgix.net/imageResource/2017/12/13/1513150321127-5096be77d2a19401b476853e54ba2cc6.svg?auto=compress%2Cformat&amp;cs=srgb&amp;ixlib=java-1.1.12&amp;q=75">
                                        </ul>
                                    </address>
                                </li>
                            </ul>
                            <h2 style="color: wheat">Security Certifications</h2>
                            <hr />
                            <br />
                            <br />
                            <ul class="travel-news">
                                <li>

                                    <address class="contact-details">

                                        <ul class="social-icons clearfix">

                                            <img src="https://tvlk.imgix.net/imageResource/2017/12/13/1513150198216-822560165b4cfa5d5ac17a7987028b03.svg?auto=compress%2Cformat&amp;cs=srgb&amp;ixlib=java-1.1.12&amp;q=75">

                                            <img src="https://tvlk.imgix.net/imageResource/2017/12/13/1513150313470-072f6bdc02c0b73fcf791aa2b2264fbd.svg?auto=compress%2Cformat&amp;cs=srgb&amp;ixlib=java-1.1.12&amp;q=75">
                                        </ul>
                                    </address>
                                </li>
                            </ul>
                        </div>


                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2 style="color: wheat">Payment Partners</h2>
                            <hr />
                            <br />
                            <br />
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/09/29/1506664998558-c5fa770c7be81b3714f2f48ecd85db98.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/10/31/1509421373165-7ebba77e7447077b423012fed9f9f71c.jpeg?auto=compress%2Cformat&amp;cs=srgb&amp;fm=pjpg&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/09/29/1506665015604-7884362b3ea565a394df831ef7e8e7cb.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/09/28/1506602820847-28f78426b005e542fac5390ca84ec551.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/10/31/1509421385548-c79a81200ececb8e100169c32c52a7fa.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/09/29/1506665026169-ce67eef59804c4acc2f7fd36cb5a879a.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2018/09/27/1538041096091-17520ed6a85fbd457d47033d12532b85.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/09/29/1506665040121-f1fc420d69a023a99d3ec4b4ce3fc0e3.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/09/29/1506665046541-a307d453b9345dad33f4d45852ab168f.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/09/29/1506665053371-f97808ddc9481a41412112c1d5751432.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/10/30/1509365256338-d9f84c5a076e5e438423a8443476bc52.jpeg?auto=compress%2Cformat&amp;cs=srgb&amp;fm=pjpg&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/10/31/1509421397012-5407269004e88b979e792cb558f33df6.jpeg?auto=compress%2Cformat&amp;cs=srgb&amp;fm=pjpg&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/10/31/1509421400393-156136b7fc6842065dd4d27f63f704ab.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/09/28/1506602748281-38fa797106a1ca8ef57d689b8a956306.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 45px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/10/31/1509421411326-c2ed81814e071f8d0f32e3c720fc2f51.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/10/31/1509421407130-3d32a6ffd3b308f4665de4e59b95d681.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2 style="color: wheat">About Vacaaay</h2>
                            <hr />
                            <br />
                            <br />
                            <p style="color: wheat">
                                Airlines Travel Centre
                                <br />
                                Sdn.Bhd.(1073291-U)
                              KKKP/PL 7592<br />
                                NO41,Level 2, Jalan Sultan Yussuf<br />
                                30000 Ipoh Perak
                                <br />
                                Malaysia
                            </p>

                            <br />
                            <address class="contact-details" style="color: wheat">
                                <br />
                                <a href="#" class="contact-email"><i class="soap-icon-letter-1"></i>info@vacaaay.com</a>
                            </address>
                            <ul class="social-icons clearfix">
                                <li class="twitter"><a title="twitter" href="#" data-toggle="tooltip"><i class="soap-icon-twitter"></i></a></li>
                                <li class="googleplus"><a title="googleplus" href="#" data-toggle="tooltip"><i class="soap-icon-googleplus"></i></a></li>
                                <li class="facebook"><a title="facebook" href="#" data-toggle="tooltip"><i class="soap-icon-facebook"></i></a></li>
                                <li class="linkedin"><a title="linkedin" href="#" data-toggle="tooltip"><i class="soap-icon-linkedin"></i></a></li>
                                <li class="vimeo"><a title="vimeo" href="#" data-toggle="tooltip"><i class="soap-icon-vimeo"></i></a></li>
                                <li class="dribble"><a title="dribble" href="#" data-toggle="tooltip"><i class="soap-icon-dribble"></i></a></li>
                                <li class="flickr"><a title="flickr" href="#" data-toggle="tooltip"><i class="soap-icon-flickr"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
          </footer>
             <div class="bottom gray-area">
                <div class="container">
                    <div class="logo pull-left">
                        <a href="Default.aspx" title="Vacaay Logo">
                            <img src="images/vacaaay1.png" alt="Vacaay Logo" />
                        </a>
                    </div>
                    <%--<div class="pull-left">
                        <p>Developed by <a href="http://www.trivo.in/" target="_blank">Trivo</a></p>
                    </div>--%>
                    <div class="pull-right">
                        <a id="back-to-top" href="#" class="animated" data-animation-type="bounce"><i class="soap-icon-longarrow-up circle"></i></a>
                    </div>
                    <div class="copyright pull-right">
                        <p>Copyright © 2018 , Airlines Travel Centre Sdn Bhd - KPK/LN 7592 </p>
                    </div>
                </div>
            </div>
         
    </div>

    <!-- Javascript -->
    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.noconflict.js"></script>
    <script type="text/javascript" src="js/modernizr.2.7.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="js/jquery-ui.1.10.4.min.js"></script>

    <!-- Twitter Bootstrap -->
    <script type="text/javascript" src="js/bootstrap.js"></script>

    <!-- load revolution slider scripts -->
    <script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>

    <!-- load BXSlider scripts -->
    <script type="text/javascript" src="components/jquery.bxslider/jquery.bxslider.min.js"></script>

    <!-- load FlexSlider scripts -->
    <script type="text/javascript" src="components/flexslider/jquery.flexslider-min.js"></script>

    <!-- Google Map Api -->
    <script src="http://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>

    <script type="text/javascript" src="js/calendar.js"></script>

    <!-- parallax -->
    <script type="text/javascript" src="js/jquery.stellar.min.js"></script>

    <!-- waypoint -->
    <script type="text/javascript" src="js/waypoints.min.js"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="js/theme-scripts.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>

    <script type="text/javascript">

    </script>
</body>
</html>
