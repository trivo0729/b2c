﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Packages.aspx.cs" Inherits="CUTUK.Packages" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="scripts/Package.js"></script>
    <script src="scripts/PackageDetail.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="content">
        <div class="container">
            <div id="main">
                <div class="row">

                    <div class="col-sm-12 col-md-12">
                        <div class="sort-by-section clearfix">
                            <div class="hotel-list listing-style3 hotel" id="div_package">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
