﻿using CUT.BL;
using CUT.DataLayer;
using CUTUK.DataLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CUTUK
{
    public partial class ViewInvoice : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string CurrencyClass = "";
            string ReservationID = Request.QueryString["ReservationId"];
            //string sUid = Request.QueryString["Uid"];
            if (ReservationID != "")
            {
                DataSet ds = new DataSet();
                DataTable dtHotelReservation, dtBookedPassenger, dtBookedRoom, dtBookingTransactions, dtAgentDetail;
                GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                GlobalDefault objGlobalDefaultCustomer = (GlobalDefault)HttpContext.Current.Session["LoginCustomer"];
                //Int64 Uid = Convert.ToInt64(sUid);
                Int64 Uid = objGlobalDefaults.sid;
                string Night = "";
                SqlParameter[] SQLParams = new SqlParameter[2];
                SQLParams[0] = new SqlParameter("@ReservationID", ReservationID);
                SQLParams[1] = new SqlParameter("@sid", 226);
                DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("Proc_BookingDetails", out ds, SQLParams);

                dtHotelReservation = ds.Tables[0];
                dtBookedPassenger = ds.Tables[1];
                dtBookedRoom = ds.Tables[2];
                dtBookingTransactions = ds.Tables[3];
                dtAgentDetail = ds.Tables[4];
                if (objGlobalDefaults.UserType == "Agent")
                {
                    CurrencyClass = (HttpContext.Current.Session["CurrencyClass"]).ToString();
                    Uid = objGlobalDefaults.sid;
                }
                else
                {
                    CurrencyClass = dtAgentDetail.Rows[0]["CurrencyCode"].ToString();
                    switch (CurrencyClass)
                    {
                        case "AED":
                            CurrencyClass = "Currency-AED";

                            break;
                        case "SAR":
                            CurrencyClass = "Currency-SAR";
                            break;
                        case "EUR":
                            CurrencyClass = "fa fa-eur";
                            break;
                        case "GBP":
                            CurrencyClass = "fa fa-gbp";
                            break;
                        case "USD":
                            CurrencyClass = "fa fa-dollar";
                            break;
                        case "INR":
                            CurrencyClass = "fa fa-inr";
                            break;
                    }

                }

                DBHandlerDataContext DB = new DBHandlerDataContext();
                //CUT.DBHandlerDataContext cDB=new CUT.DBHandlerDataContext();
                var List = (from Obj in DB.tbl_B2C_CustomerLogins where Obj.B2C_Id == objGlobalDefaultCustomer.B2C_Id.ToString() select Obj).ToList();
                var CityCountry = (from Obj in DB.tbl_HCities where Obj.Code == List[0].CityId select Obj).ToList();
                decimal SalesTax = Convert.ToDecimal(dtBookingTransactions.Rows[0]["SeviceTax"]);
                decimal Ammount = Convert.ToDecimal(dtBookingTransactions.Rows[0]["BookingAmt"]);
                // decimal Ammount = Convert.ToDecimal(dtBookingTransactions.Rows[0]["BookingAmtWithTax"]);
                SalesTax = decimal.Round(SalesTax, 2, MidpointRounding.AwayFromZero);
                Ammount = decimal.Round(Ammount, 2, MidpointRounding.AwayFromZero);
                string InvoiceID = dtHotelReservation.Rows[0]["InvoiceID"].ToString();
                string Hoteldestination = dtHotelReservation.Rows[0]["City"].ToString();
                string VoucherID = dtHotelReservation.Rows[0]["VoucherID"].ToString();
                string CheckIn = dtHotelReservation.Rows[0]["CheckIn"].ToString();
                CheckIn = CheckIn.Replace("00:00", "");
                string CheckOut = dtHotelReservation.Rows[0]["CheckOut"].ToString();
                CheckOut = CheckOut.Replace("00:00", "");
                string HotelName = (dtHotelReservation.Rows[0]["HotelName"].ToString());
                string Status = (dtHotelReservation.Rows[0]["Status"].ToString());
                string ReservationDate = dtHotelReservation.Rows[0]["ReservationDate"].ToString();
                ReservationDate = ReservationDate.Replace("00:00:", "");
                string AgentRef = (dtHotelReservation.Rows[0]["AgentRef"].ToString());
                string AgencyName = dtAgentDetail.Rows[0]["AgencyName"].ToString();
                string Address = dtAgentDetail.Rows[0]["Address"].ToString();
                string Description = CityCountry[0].Description;
                //string Description = (dtAgentDetail.Rows[0]["Description"].ToString());
                //string Countryname = (dtAgentDetail.Rows[0]["Countryname"].ToString());
                string Countryname =  CityCountry[0].Countryname;
                string GstNo = dtAgentDetail.Rows[0]["GSTNumber"].ToString();
                string AgentCode = (dtAgentDetail.Rows[0]["Agentuniquecode"].ToString());
                string Pincode = dtAgentDetail.Rows[0]["Pincode"].ToString();
                string AgentCountryname = (dtAgentDetail.Rows[0]["Countryname"].ToString());
                string Fax = dtAgentDetail.Rows[0]["Fax"].ToString();
                if (Fax == "0")
                {
                    Fax = "";
                }
                string phone = dtAgentDetail.Rows[0]["phone"].ToString();
                string email = (dtAgentDetail.Rows[0]["email"].ToString());
                Night = (dtHotelReservation.Rows[0]["NoOfDays"].ToString());
                string words;
                //words = ToWords((Ammount) + SalesTax);
                words = ToWords((Ammount));


                string CanAmtWoutNight = "";
                string CanAmtWithTax = "";

                string Supplier = (dtHotelReservation.Rows[0]["Source"].ToString());
                StringBuilder sb = new StringBuilder();
                //sb.Append("<!DOCTYPE html>");
                //sb.Append("<head>");
                //sb.Append("<meta charset=\"utf-8\" />");
                //sb.Append("</head>");
                //sb.Append("<body style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px; width:90%; border:2px solid gray;\">");

                var AdminUrl = ConfigurationManager.AppSettings["AdminUrl"];
                sb.Append("<div style=\"background-color: #F7B85B; height: 13px\">");
                sb.Append("<span>&nbsp;</span>");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("<table style=\" height: 100px; width: 100%;\">");
                sb.Append("<tbody>");
                sb.Append("<tr>");
                sb.Append("<td style=\"width: auto;padding-left:15px\">");
                sb.Append("<img  src=\"" + AdminUrl + "/AgencyLogos/" + AgentCode + ".jpg\" height=\"auto\" width=\"auto\"></img>");
                sb.Append("</td>");
                sb.Append("<td style=\"width: 20%\"></td>");
                sb.Append("<td style=\"width: auto; padding-right:15px; color: #57585A\" align=\"right\" >");
                sb.Append("<span style=\"margin-right: 15px\">");
                sb.Append("<br>");
                sb.Append("" + Address + ",<br>");
                //sb.Append("Juni Mangalwari, Opp. Rahate Hospital,<br>");
                sb.Append("" + Description + " - " + Pincode + ", " + AgentCountryname + "<br>");
                sb.Append("Tel: " + phone + "<br> Fax: " + Fax + "<br>");
                sb.Append(" Email: " + email + "<br>");
                sb.Append("</span>");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td style=\"color: #57585A\"><span style=\"padding-left:10px\"> <b>GST No:</b> " + GstNo + "</span></td>");
                sb.Append("<td></td>");
                sb.Append("<td></td>");
                sb.Append("</tr>");
                sb.Append("</tbody>");
                sb.Append("</table>");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("<table border=\"1\" style=\"width: 100%; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-bottom-width: 3px; border-bottom-color: #E6DCDC; border-left:none; border-right:none\">");
                sb.Append("<tr>");
                sb.Append("<td rowspan=\"3\" style=\"width:35%; color: #00CCFF; font-size: 30px; text-align: center; border: none; padding-right:35px; padding-left:10px\">");
                sb.Append("<b>SALES INVOICE</b><br><br>");
                sb.Append("<span style=\"font-size:25px\"><b>Status : </b> </span><span id=\"spnStatus\" style=\"font-size:25px; font-weight:700\">" + Status + "</span>");
                sb.Append("</td>");
                sb.Append("<td style=\"border-top: none; border-bottom-width: 6px; border-bottom-color: gray; padding: 10px 0px 0px 3px; margin-bottom:0px; color: #57585A\">");
                sb.Append("<b> Invoice Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</b><span style=\"color: #B0B0B0\"> " + ReservationDate + " </span>");
                sb.Append("</td>");
                sb.Append("<td style=\"border-top: none; border-bottom-width: 6px; border-bottom-color: gray; padding: 10px 0px 0px 3px; margin-bottom: 0px; color: #57585A\">");
                sb.Append("<b>Invoice No  &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;     : </b><span style=\"color: #B0B0B0\">" + InvoiceID + " </span>");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td style=\"border-top: none; border-bottom-width: 6px; border-bottom-color: gray; padding: 0px 0px 0px 3px; color: #57585A\">");
                sb.Append("<b> Voucher No.&nbsp;&nbsp; &nbsp; &nbsp; :</b> <span style=\"color: #B0B0B0\">" + VoucherID + "</span>");
                sb.Append("</td>");
                sb.Append("<td style=\"border-top: none; border-bottom-width: 6px; border-bottom-color: gray; padding: 0px 0px 0px 3px; color: #57585A\">");
                sb.Append("<b>  Agent Code &nbsp;&nbsp; &nbsp; &nbsp; :</b><span style=\" color:#b0b0b0\">" + AgentRef + "</span>");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td colspan=\"2\" style=\"height: 25px; border:none\"></td>");
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("<table border=\"1\" style=\"border-spacing: 0px; height: 150px; width: 100%; border-top: none; padding: 10px 0px 10px 0px; border-width: 0px 0px 3px 0px; border-bottom-color: #E6DCDC\">");
                sb.Append("<tr>");
                sb.Append("<td colspan=\"2\" style=\"border: none; padding-bottom: 10px; padding-left: 10px; font-size: 20px; color: #57585A\"><b>Invoice To</b></td>");
                sb.Append("<td colspan=\"3\" style=\"border: none; padding-bottom: 10px; padding-left: 10px; font-size:20px; color: #57585A\"> <b>Service Details</b></td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td rowspan=\"6\" style=\"width:10%; border-width:3px 0px 0px 0px; border-bottom-color:gray; border-spacing:0px; color: #57585A\">");
                sb.Append("<span style=\"padding-left:10px\"><b>Name</b></span><br>");
                sb.Append("<span style=\"padding-left:10px\"><b>Address</b></span><br>");
                sb.Append("<span style=\"padding-left:10px\"><b>City</b></span><br>");
                sb.Append("<span style=\"padding-left:10px\"><b>Country</b></span><br>");
                sb.Append("<span style=\"padding-left:10px\"><b>Phone</b></span><br>");
                sb.Append("<span style=\"padding-left:10px\"><b>Email</b></span><br>");
                sb.Append("</td>");
                sb.Append("<td rowspan=\"6\" style=\"width: 40%; border-width: 3px 0px 0px 0px; border-bottom-color: gray; padding-left: 10px; color: #57585A\">");
                sb.Append(":<span style=\"padding-left:10px\">" + dtHotelReservation.Rows[0]["bookingname"] + "</span><br>");
                sb.Append(":<span style=\"padding-left:10px\">" +List[0].Address + "</span><br>");
                sb.Append(":<span style=\"padding-left:10px\">" + Description + "</span><br>");
                sb.Append(":<span style=\"padding-left:10px\">" + Countryname + "</span><br>");
                sb.Append(":<span style=\"padding-left:10px\">" + List[0].Contact1 + "</span><br>");
                sb.Append(":<span style=\"padding-left:10px\">" + List[0].Email + "</span><br>");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td colspan=\"3\" style=\"border-width: 3px 0px 0px 0px; border-bottom-color: gray; border-spacing: 0px; background-color: #35C2F1; padding-left: 8px\"><span style=\"color:white;font-size:18px\"><b>Hotel Name  :</b></span> <span style=\"color:white;font-size:18px\">" + HotelName + "</span></td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td colspan=\"3\" style=\"border: none; background-color: #27B4E8; padding-left:8px\"><span style=\"color:white; font-size: 18px\"><b>Destination :</b> <span style=\"color:white;font-size:18px\">" + Hoteldestination + "</span></td>");
                sb.Append("</tr>");
                sb.Append("<tr style=\"color:white;\">");
                sb.Append("<td style=\"width: 150px; border: none; background-color: #00AEEF; padding-bottom:3px\" align=\"center\">");
                sb.Append(" <span><b> Check In</b></span><br>");
                sb.Append("<span>" + CheckIn + "</span>");
                sb.Append("</td>");
                sb.Append("<td style=\"width: 150px; border: none; background-color: #00AEEF; padding-bottom: 3px\" align=\"center\">");
                sb.Append("<span><b>Check Out</b></span><br>");
                sb.Append("<span>" + CheckOut + "</span>");
                sb.Append("</td>");
                sb.Append("<td style=\"width: 150px; border: none; background-color: #A8A9AD; padding-bottom: 3px\" align=\"center\">");
                sb.Append("<span><b>Total Night(s)</b></span><br>");
                sb.Append(" <span>" + Night + "Night(s)</span>");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td colspan=\"3\" style=\"border:none\">&nbsp;</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td colspan=\"3\" style=\"border:none\">&nbsp;</td>");
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("</div>");
                //Room Rate table goes here.............................................
                sb.Append("<div style=\"font-size: 20px;padding-bottom:10px; padding-top:8px\">");
                sb.Append("<span style=\"padding-left:10px; color: #57585A\"><b>Rate</b></span>");
                sb.Append("</div>");
                sb.Append("<div>");

                sb.Append("<table border=\"1\" style=\"margin-top: 3px; width: 100%; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-bottom-width: 3px; border-bottom-color: #E6DCDC; border-left: none; border-right: none\">");
                sb.Append("<tr style=\"border: none\">");
                sb.Append("<td style=\"background-color: #35C2F1; color: white; font-size: 15px; border: none;  font-weight: 700; text-align:left; padding-left:10px \">");
                sb.Append("<span>No.</span>");
                sb.Append("</td>");
                sb.Append("<td style=\"width:170px; height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: left\">");
                sb.Append("<span>Room Type</span>");
                sb.Append("</td>");
                sb.Append("<td style=\"height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none;  font-weight: 700; text-align:center\">");
                sb.Append("<span>Board</span>");
                sb.Append("</td>");
                sb.Append("<td style=\"height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: center\">");
                sb.Append("<span>Rooms</span>");
                sb.Append("</td>");
                sb.Append("<td style=\"height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: center;\">");
                sb.Append("<span>Nights</span>");
                sb.Append("</td>");
                sb.Append("<td style=\"height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: center;\">");
                sb.Append("<span>Rate ( RM )</span>");
                sb.Append("</td>");
                sb.Append("<td style=\"width: 100px; height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: center;\">");
                sb.Append("<span>Total ( RM )</span>");
                sb.Append("</td>");
                sb.Append("</tr>");

                if (Supplier == "MGH")
                {
                    #region MGH
                    for (var i = 0; i < dtBookedRoom.Rows.Count; i++)
                    {
                        sb.Append("<tr style=\"border: none; background-color: #E6E7E9; padding: 15px 0px 0px 10px; text-align: left; color: #57585A\">");
                        sb.Append("<td align=\"center\" style=\"width:30px; border: none; text-align: left; padding-left: 15px;\">" + (i + 1) + "</td>");
                        sb.Append("<td style=\"border: none\">" + dtBookedRoom.Rows[i]["RoomType"].ToString() + "</td>");
                        sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + dtBookedRoom.Rows[i]["BoardText"].ToString() + "</td>");
                        sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + dtBookedRoom.Rows[i]["RoomNumber"].ToString() + "</td>");
                        sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\" >" + (Night) + "</td>");
                        sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + ((decimal.Round(Convert.ToDecimal(dtBookedRoom.Rows[i]["RoomAmount"]), 2, MidpointRounding.AwayFromZero)).ToString("#,##0.00")) + "</td>");
                        sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + ((Convert.ToInt64(Night)) * (decimal.Round(Convert.ToDecimal(dtBookedRoom.Rows[i]["RoomAmount"]), 2, MidpointRounding.AwayFromZero))).ToString("#,##0.00") + "</td>");
                        sb.Append("</tr>");

                    }
                    #endregion
                }

                else
                {
                    #region Other Supplier
                    for (var i = 0; i < dtBookedRoom.Rows.Count; i++)
                    {
                        sb.Append("<tr style=\"border: none; background-color: #E6E7E9; padding: 15px 0px 0px 10px; text-align: left; color: #57585A\">");
                        sb.Append("<td align=\"center\" style=\"width:30px; border: none; text-align: left; padding-left: 15px;\">" + (i + 1) + "</td>");
                        sb.Append("<td style=\"border: none\">" + dtBookedRoom.Rows[i]["RoomType"].ToString() + "</td>");
                        sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + dtBookedRoom.Rows[i]["BoardText"].ToString() + "</td>");
                        sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + dtBookedRoom.Rows[i]["RoomNumber"].ToString() + "</td>");
                        sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\" >" + (Night) + "</td>");
                        sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + ((decimal.Round(Convert.ToDecimal(dtBookedRoom.Rows[i]["RoomAmount"]) / (Convert.ToInt64(Night)), 2, MidpointRounding.AwayFromZero)).ToString("#,##0.00")) + "</td>");
                        sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + ((decimal.Round(Convert.ToDecimal(dtBookedRoom.Rows[i]["RoomAmount"]), 2, MidpointRounding.AwayFromZero))).ToString("#,##0.00") + "</td>");
                        sb.Append("</tr>");

                    }
                    #endregion

                }


                //sb.Append("<tr style=\"border: none; background-color: #E6E7E9; text-align: right\">");
                //sb.Append("<td align=\"right\" colspan=\"6\" style=\"border: none; width: 20px; padding-right: 25px; color: #57585A;\">");
                //sb.Append("<span><b> Service Tax (1.4%)</b></span>");
                //sb.Append("</td>");
                //sb.Append("<td style=\"border: none; width: 20px; text-align: center; color: #57585A\"><b>" + SalesTax.ToString("#,##0.00") + "</b></td>");
                //sb.Append("</tr>");

                sb.Append("<tr border=\"1\" style=\"border-spacing:0px\">");
                sb.Append("<td colspan=\"5\" align=\"left\" style=\"height: 35px;  background-color: #00AEEF; color: white; font-size: 15px; padding-left: 10px;  border-width: 3px 0px 0px 0px; border-top-color: #E6DCDC;\">");
                sb.Append("<b> In Words:</b> <span>: " + words + "</span>");
                sb.Append("</td>");
                sb.Append("<td align=\"center\" style=\"height: 35px; background-color: #27B4E8; color: white; font-size: 18px; padding-left: 10px; font-weight: 700; border-width: 3px 0px 0px 0px; border-top-color: #E6DCDC;\">");
                sb.Append("<span>Total Amount</span>");
                sb.Append("</td>");
                sb.Append("<td align=\"center\" style=\"height: 35px; background-color: #35C2F1; color: white; font-size: 18px; padding-left: 10px; font-weight: 700; border-width: 3px 0px 0px 0px; border-top-color: #E6DCDC;\">");
                //  sb.Append("<span>" + Ammount.ToString("#,##0.00") + "</span>"); ((Ammount * Convert.ToInt32(Night)) + SalesTax)
                sb.Append("<span>" + ((Ammount)).ToString("#,##0.00") + "</span>");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("</div>");

                //Cancellation table goes here.............................................
                sb.Append("<div style=\"font-size: 20px; padding-bottom: 10px; padding-top: 8px; color: #57585A\">");
                sb.Append("<span style=\"padding-left:10px\"><b>Cancellation Charge</b></span>");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("<table border=\"1\" style=\"margin-top: 3px; width: 100%; border-spacing: 0px; border-width:3px 0px 3px 0px; border-top-color: gray;  border-bottom-color: #E6DCDC\">");
                sb.Append("<tr style=\"border: none\">");

                sb.Append("<td align=\"center\" style=\"background-color: #35C2F1; color: white; font-size: 15px; border: none;  font-weight: 700;  padding-left:10px\">");
                sb.Append("<span>No.</span>");
                sb.Append("</td>");
                sb.Append("<td style=\"width:190px; height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: left\">");
                sb.Append("<span>Room Type</span>");
                sb.Append("</td>");
                sb.Append("<td align=\"center\" style=\"height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; padding-left: 10px; font-weight: 700;\">");
                sb.Append("<span>Rooms</span>");
                sb.Append("</td>");
                sb.Append("<td align=\"center\" style=\"height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; padding-left: 10px; font-weight: 700;\">");
                sb.Append("<span>Cancellation After</span>");
                sb.Append("</td>");
                sb.Append("<td align=\"center\" style=\"height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; padding-left: 10px; font-weight: 700;\">");
                sb.Append("<span>Charge/Unit ( RM )</span>");
                sb.Append("</td>");
                sb.Append("<td align=\"center\" style=\"height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; padding-left: 10px; font-weight: 700;\">");
                sb.Append("<span>Total Charge ( RM )</span>");
                sb.Append("</td>");
                sb.Append("</tr>");

                CanAmtWithTax = "";
                CanAmtWoutNight = "";

                for (var i = 0; i < dtBookedRoom.Rows.Count; i++)
                {
                    // string SupplierNoChargeDate = dtBookedRoom.Rows[i]["SupplierNoChargeDate"].ToString();
                    string SupplierNoChargeDate = dtBookedRoom.Rows[i]["CutCancellationDate"].ToString();
                    SupplierNoChargeDate = SupplierNoChargeDate.TrimEnd('|');

                    CanAmtWithTax = dtBookedRoom.Rows[i]["CancellationAmount"].ToString();
                    CanAmtWithTax = CanAmtWithTax.TrimEnd('|');
                    if (Supplier == "MGH")
                    {
                        #region MGH

                        if (CanAmtWithTax.Contains('|'))
                        {
                            string[] AmtWithTax = CanAmtWithTax.Split('|');
                            CanAmtWoutNight = "";
                            CanAmtWithTax = "";
                            for (int c = 0; c < AmtWithTax.Length; c++)
                            {
                                if (AmtWithTax[c] != "")
                                {
                                    if (c != (AmtWithTax.Length - 1))
                                    {
                                        CanAmtWoutNight += (decimal.Round(Convert.ToDecimal(AmtWithTax[c]), 2, MidpointRounding.AwayFromZero)).ToString() + "|";

                                        CanAmtWithTax += (decimal.Round((Convert.ToDecimal(AmtWithTax[c]) * (Convert.ToDecimal(Night))), 2, MidpointRounding.AwayFromZero)).ToString() + "|";
                                    }
                                    else
                                    {
                                        CanAmtWoutNight += (decimal.Round(Convert.ToDecimal(AmtWithTax[c]), 2, MidpointRounding.AwayFromZero)).ToString();

                                        CanAmtWithTax += (decimal.Round((Convert.ToDecimal(AmtWithTax[c]) * (Convert.ToDecimal(Night))), 2, MidpointRounding.AwayFromZero)).ToString();
                                    }
                                }
                            }
                        }
                        else
                        {
                            string Splitt = dtBookedRoom.Rows[i]["CancellationAmount"].ToString();
                            string[] Splitter = Splitt.Split('|');
                            CanAmtWithTax = Splitter[0];
                            CanAmtWoutNight = (decimal.Round(Convert.ToDecimal(CanAmtWithTax), 2, MidpointRounding.AwayFromZero)).ToString();
                            CanAmtWithTax = (decimal.Round((Convert.ToDecimal(CanAmtWithTax)), 2, MidpointRounding.AwayFromZero) * (Convert.ToDecimal(Night))).ToString();
                        }

                        #endregion MGH
                    }
                    else
                    {
                        #region Other Supplier

                        if (CanAmtWithTax.Contains('|'))
                        {
                            string[] AmtWithTax = CanAmtWithTax.Split('|');
                            CanAmtWoutNight = "";
                            CanAmtWithTax = "";
                            for (int c = 0; c < AmtWithTax.Length; c++)
                            {
                                if (AmtWithTax[c] != "")
                                {
                                    if (c != (AmtWithTax.Length - 1))
                                    {
                                        CanAmtWoutNight += (decimal.Round((Convert.ToDecimal(AmtWithTax[c]) / (Convert.ToDecimal(Night))), 2, MidpointRounding.AwayFromZero)).ToString() + "|";

                                        CanAmtWithTax += (decimal.Round((Convert.ToDecimal(AmtWithTax[c])), 2, MidpointRounding.AwayFromZero)).ToString() + "|";
                                    }
                                    else
                                    {
                                        CanAmtWoutNight += (decimal.Round(Convert.ToDecimal(AmtWithTax[c]) / (Convert.ToDecimal(Night)), 2, MidpointRounding.AwayFromZero)).ToString();

                                        CanAmtWithTax += (decimal.Round((Convert.ToDecimal(AmtWithTax[c])), 2, MidpointRounding.AwayFromZero)).ToString();
                                    }
                                }
                            }
                        }
                        else
                        {
                            CanAmtWithTax = dtBookedRoom.Rows[i]["CancellationAmount"].ToString();
                            CanAmtWoutNight = (decimal.Round(Convert.ToDecimal(CanAmtWithTax) / (Convert.ToDecimal(Night)), 2, MidpointRounding.AwayFromZero)).ToString();
                            CanAmtWithTax = (decimal.Round((Convert.ToDecimal(CanAmtWithTax)), 2, MidpointRounding.AwayFromZero)).ToString();
                        }

                        #endregion Other Supplier
                    }

                    sb.Append("<tr style=\"border: none; background-color: #E6E7E9; padding: 15px 0px 0px 10px; text-align: left; color: #57585A\">");
                    sb.Append("<td align=\"center\" style=\"width:30px; border: none; text-align: left; padding-left: 15px;\">" + (i + 1) + "</td>");
                    sb.Append("<td style=\"border: none\">" + dtBookedRoom.Rows[i]["RoomType"].ToString() + "</td>");
                    sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + dtBookedRoom.Rows[i]["RoomNumber"].ToString() + "</td>");
                    sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + SupplierNoChargeDate + "</td>");
                    sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\" >" + CanAmtWoutNight + "</td>");
                    sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + CanAmtWithTax + "</td>");
                    sb.Append("</tr>");
                }
                sb.Append("<tr style=\"border: none; background-color: #E6E7E9; text-align: left;\">");
                sb.Append("<td colspan=\"7\" style=\"border: none; width: 20px; padding: 0px 15px 15px 25px; color: #ECA236;\">");
                sb.Append("*Dates & timing will calculated based on local timing </td>");
                sb.Append("</tr>");

                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("</div>");


                //sb.Append("<div style=\"font-size: 20px; padding-bottom: 10px; padding-top: 8px\">");
                //sb.Append("<span style=\"padding-left:10px;color: #57585A\"><b>Bank Details</b></span>");
                //sb.Append("</div>");
                sb.Append("<div>");
                //sb.Append("<table border=\"1\" style=\"margin-top: 3px; height:100px; width:100%; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-left: none; border-right: none\">");
                //sb.Append("<tr style=\"border: none; background-color: #F7B85B; border-bottom-color: gray; color: #57585A;\">");

                //sb.Append("<td align=\"left\" style=\"width: 33%; height: 35px; font-size: 15px; border: none; padding: 10px 0px 10px 10px; border-bottom: 3px; border-bottom-color: gray;\">");
                //sb.Append("<span>");
                //sb.Append("<b>Bank Name &nbsp;</b>: &nbsp;&nbsp;&nbsp;<span>ICICI Bank</span><br>");
                //sb.Append("<b>Account No&nbsp;</b>: &nbsp;&nbsp;<span>123 456 7890</span><br>");
                //sb.Append("<b>Branch &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;</b> : <span>&nbsp; Lakadguanj, Nagpur</span><br>");
                //sb.Append("<b>Swift Code   </b>&nbsp; : &nbsp;&nbsp;<span>ICICI-123456</span><br>");
                //sb.Append("</span>");
                //sb.Append("</td>");
                //sb.Append("<td align=\"left\" style=\"width: 33%; height: 35px; font-size: 15px; border: none; padding: 10px 0px 10px 10px; border-bottom: 3px; border-bottom-color: gray;\">");
                //sb.Append("<span>");
                //sb.Append("<b>Bank Name &nbsp;</b>: &nbsp;&nbsp;&nbsp; <span>AXIS Bank</span><br>");
                //sb.Append("<b>Account No&nbsp; </b>: &nbsp;&nbsp;<span>123 456 7890</span><br>");
                //sb.Append("<b>Branch &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;</b> : <span>&nbsp; Lakadguanj, Nagpur</span><br>");
                //sb.Append("<b>Swift Code   </b>&nbsp; : &nbsp;&nbsp;<span>UTI-123456</span><br>");
                //sb.Append("</span>");
                //sb.Append("</td>");
                //sb.Append("<td align=\"left\" style=\"width: 33%; height: 35px; font-size: 15px; border: none; padding: 10px 0px 10px 10px; border-bottom: 3px; border-bottom-color: gray;\">");
                //sb.Append("<span>");
                //sb.Append("<b>Bank Name&nbsp; </b>: &nbsp;&nbsp;&nbsp; <span>Bank Of India</span><br>");
                //sb.Append("<b>Account No &nbsp;</b>: &nbsp;&nbsp;<span>123 456 7890</span><br>");
                //sb.Append("<b>Branch &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;</b> : <span>&nbsp;Itwari, Nagpur</span><br>");
                //sb.Append("<b>Swift Code   </b>&nbsp; : &nbsp;&nbsp;<span>BOI-123456</span><br>");
                //sb.Append("</span>");
                //sb.Append("</td>");
                //sb.Append("</tr>");
                //sb.Append("</table>");

                sb.Append("<table border=\"1\" style=\"height:100px; width: 100%; border-spacing: 0px; border-bottom:none; border-left: none; border-right: none\">");
                sb.Append("<tr style=\"font-size: 20px; border-spacing: 0px\">");
                sb.Append("<td colspan=\"5\" height=\"20px\" style=\"width: 70%; background-color: #E6E7E9; padding: 10px 10px 10px 10px; color: #57585A\"><b> Terms & Conditions</b></td>");
                //sb.Append("<td rowspan=\"2\" style=\"border-bottom:none; border-left:none; text-align:center\">");
                ////sb.Append("<img src=\"http://www.clickurtrip.com/images/signature.png\"  height=\"auto\" width=\"auto\"></img>");
                //sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("<tr style=\"font-size: 15px\">");
                sb.Append("<td colspan=\"5\" style=\"background-color: #E6E7E9; border-top-width: 3px; border-top-color: #E6E7E9; padding:10px 10px 10px 10px;color: #57585A\">");

                sb.Append("<ul class=\"circle\">");
                sb.Append("<li>Kindly check all details carefully to avoid un-necessary complications</li>");
                sb.Append("<li> Cheque to be drawn in our company name on presentation of invoice</li>");
                //sb.Append("<li>Subject to NAGPUR (INDIA) jurisdiction </li>");
                sb.Append("</ul>");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("</div>");
                sb.Append("<div style=\"background-color: #00AEEF; text-align: center; font-size: 21px; color: white; height: 25px\">");
                sb.Append("<span>");
                sb.Append("Computer generated invoice do not require signature...");

                sb.Append("</span>");
                sb.Append("</div>");
                maincontainer.InnerHtml = sb.ToString();
                //sb.Append("</body>");
                //sb.Append("</html>");
            }
        }
        private static string[] ones = {
    "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", 
    "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen",
};

        private static string[] tens = { "Zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

        private static string[] thous = { "Hundred,", "Thousand,", "Million,", "Billion,", "Trillion,", "Quadrillion," };

        public static string ToWords(decimal number)
        {
            if (number < 0)
                return "negative " + ToWords(Math.Abs(number));

            int intPortion = (int)number;
            int decPortion = (int)((number - intPortion) * (decimal)100);

            return string.Format("{0} Ringgit and {1} sen", ToWords(intPortion), ToWords(decPortion));
        }

        private static string ToWords(int number, string appendScale = "")
        {
            string numString = "";
            if (number < 100)
            {
                if (number < 20)
                    numString = ones[number];
                else
                {
                    numString = tens[number / 10];
                    if ((number % 10) > 0)
                        numString += "-" + ones[number % 10];
                }
            }
            else
            {
                int pow = 0;
                string powStr = "";

                if (number < 1000) // number is between 100 and 1000
                {
                    pow = 100;
                    powStr = thous[0];
                }
                else // find the scale of the number
                {
                    int log = (int)Math.Log(number, 1000);
                    pow = (int)Math.Pow(1000, log);
                    powStr = thous[log];
                }

                numString = string.Format("{0} {1}", ToWords(number / pow, powStr), ToWords(number % pow)).Trim();
            }

            return string.Format("{0} {1}", numString, appendScale).Trim();
        }


        private static string[] _ones =
        {
                "zero",
                "one",
                "two",
                "three",
                "four",
                "five",
                "six",
                "seven",
                "eight",
                "nine"
         };



        private string[] _teens =
        {
        "ten",
        "eleven",
        "twelve",
        "thirteen",
        "fourteen",
        "fifteen",
        "sixteen",
        "seventeen",
        "eighteen",
        "nineteen"
        };




        private string[] _tens =
        {
        "",
        "ten",
        "twenty",
        "thirty",
        "forty",
        "fifty",
        "sixty",
        "seventy",
        "eighty",
        "ninety"
        };

        // US Nnumbering`:

        private string[] _thousands =
    {
    "",
    "thousand",
    "million",
    "billion",
    "trillion",
    "quadrillion"
    };



        /// <summary>
        /// Converts a numeric value to words suitable for the portion of
        /// a check that writes out the amount.
        /// </summary>
        /// <param name="value">Value to be converted
        /// <returns></returns>
        public string ConvertToNum(decimal value)
        {
            string digits, temp;
            bool showThousands = false;
            bool allZeros = true;
            StringBuilder builder = new StringBuilder();
            // Convert integer portion of value to string
            digits = ((long)value).ToString();
            // Traverse characters in reverse order
            for (int i = digits.Length - 1; i >= 0; i--)
            {
                int ndigit = (int)(digits[i] - '0');
                int column = (digits.Length - (i + 1));

                // Determine if ones, tens, or hundreds column
                switch (column % 3)
                {
                    case 0:        // Ones position
                        showThousands = true;
                        if (i == 0)
                        {
                            // First digit in number (last in loop)
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else if (digits[i - 1] == '1')
                        {
                            // This digit is part of "teen" value
                            temp = String.Format("{0} ", _teens[ndigit]);
                            // Skip tens position
                            i--;
                        }
                        else if (ndigit != 0)
                        {
                            // Any non-zero digit
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else
                        {
                            // This digit is zero. If digit in tens and hundreds
                            // column are also zero, don't show "thousands"
                            temp = String.Empty;
                            // Test for non-zero digit in this grouping
                            if (digits[i - 1] != '0' || (i > 1 && digits[i - 2] != '0'))
                                showThousands = true;
                            else
                                showThousands = false;
                        }

                        // Show "thousands" if non-zero in grouping
                        if (showThousands)
                        {
                            if (column > 0)
                            {
                                temp = String.Format("{0}{1}{2}",
                                temp,
                                _thousands[column / 3],
                                    //allZeros ? " " : ", ");
                                allZeros ? " " : " ");
                            }
                            // Indicate non-zero digit encountered
                            allZeros = false;
                        }
                        builder.Insert(0, temp);
                        break;

                    case 1:        // Tens column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0}{1}",
                            _tens[ndigit],
                            (digits[i + 1] != '0') ? "-" : " ");
                            builder.Insert(0, temp);
                        }
                        break;

                    case 2:        // Hundreds column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0} hundred ", _ones[ndigit]);
                            builder.Insert(0, temp);
                        }
                        break;
                }
            }

            // Append fractional portion/cents
            builder.AppendFormat(" DOLLARS and {0:00} / 100", (value - (long)value) * 100);//Replace Dollars with paisa if you are using indian currencry

            // Capitalize first letter
            return String.Format("{0}{1}",
            Char.ToUpper(builder[0]),
            builder.ToString(1, builder.Length - 1));
        }
    }
}