﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FlightSearch.aspx.cs" Inherits="CUTUK.FlightSearch" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- Page Title -->
    <title>Flight Search</title>

    <!-- Meta Tags -->
    <meta charset="utf-8" />
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Travelo - Travel, Tour Booking HTML5 Template" />
    <meta name="author" content="SoapTheme" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />

    <!-- Theme Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="css/animate.min.css" />

    <!-- Current Page Styles -->
    <link rel="stylesheet" type="text/css" href="components/revolution_slider/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/revolution_slider/css/style.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/jquery.bxslider/jquery.bxslider.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/flexslider/flexslider.css" media="screen" />

    <!-- Main Style -->
    <link rel="stylesheet" href="css/style.css" />

    <!-- Updated Styles -->
    <link rel="stylesheet" href="css/updates.css" />

    <!-- Custom Styles -->
    <link rel="stylesheet" href="css/custom.css" />

    <!-- Responsive Styles -->
    <link rel="stylesheet" href="css/responsive.css" />
    <link href="css/jquery-ui.css" rel="stylesheet" />
    <!-- CSS for IE -->
    <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie.css" />
    <![endif]-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->
    <script src="js/jquery-2.1.3.min.js"></script>
    <script src="js/jquery-ui.js"></script>
    <%--<script src="scripts/CountryCityCode.js?v=1.0"></script>--%>
    <script src="scripts/flightAuto.js"></script>
    <script src="scripts/CountryCityCode.js"></script>
    <script src="scripts/flightSearch.js"></script>





    <%--    <script src="scripts/login.js?v=1.0"></script>
    <script src="scripts/B2CCustomerLogin.js"></script>--%>


    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800,700,600' rel='stylesheet' type='text/css' />
    <style>
        section#content {
            min-height: 1000px;
            padding: 0;
            position: relative;
            overflow: hidden;
        }

        #main {
            padding-top: 200px;
        }

        .page-title, .page-description {
            color: #fff;
        }

        .page-title {
            font-size: 4.1667em;
            font-weight: bold;
        }

        .page-description {
            font-size: 2.5em;
            margin-bottom: 50px;
        }

        .featured {
            position: absolute;
            right: 50px;
            bottom: 50px;
            z-index: 9;
            margin-bottom: 0;
            text-align: right;
        }

            .featured figure a {
                border: 2px solid #fff;
            }

            .featured .details {
                margin-right: 10px;
            }

                .featured .details > * {
                    color: #fff;
                    line-height: 1.25em;
                    margin: 0;
                    font-weight: bold;
                    text-shadow: 2px -2px rgba(0, 0, 0, 0.2);
                }
    </style>
</head>
<body>
    <div id="page-wrapper">
        <header id="header" class="navbar-static-top">

            <div class="main-header">

                <a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle">Mobile Menu Toggle
                </a>

                <div class="container">
                    <h1 class="logo navbar-brand">
                        <a href="#" title="Travelo - home">
                            <img src="images/logo.png" alt="Travelo HTML5 Template" />
                        </a>
                    </h1>


                </div>
            </div>

            <div class="topnav hidden-xs">
                <div class="container">
                    <ul class="quick-menu pull-left">
                        <li><a href="#">MY ACCOUNT</a></li>
                    </ul>
                    <%--  <ul class="quick-menu pull-right">
                        <li><a href="#travelo-login" class="soap-popupbox">LOGIN</a></li>
                        <li><a href="#travelo-signup" class="soap-popupbox">SIGNUP</a></li>
                    </ul>--%>
                </div>
            </div>

            <div id="travelo-signup" class="travelo-signup-box travelo-box">
                <div class="login-social">
                    <a href="#" class="button login-facebook"><i class="soap-icon-facebook"></i>Login with Facebook</a>
                    <a href="#" class="button login-googleplus"><i class="soap-icon-googleplus"></i>Login with Google+</a>
                </div>
                <div class="seperator">
                    <label>OR</label>
                </div>
                <div class="simple-signup">
                    <div class="text-center signup-email-section">
                        <a href="#" class="signup-email"><i class="soap-icon-letter"></i>Sign up with Email</a>
                    </div>
                    <p class="description">By signing up, I agree to Travelo's Terms of Service, Privacy Policy, Guest Refund olicy, and Host Guarantee Terms.</p>
                </div>
                <div class="email-signup">
                    <form>
                        <div class="form-group">
                            <input type="text" class="input-text full-width" placeholder="first name">
                        </div>
                        <div class="form-group">
                            <input type="text" class="input-text full-width" placeholder="last name">
                        </div>
                        <div class="form-group">
                            <input type="text" class="input-text full-width" placeholder="email address">
                        </div>
                        <div class="form-group">
                            <input type="password" class="input-text full-width" placeholder="password">
                        </div>
                        <div class="form-group">
                            <input type="password" class="input-text full-width" placeholder="confirm password">
                        </div>
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox">
                                    Tell me about Travelo news
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <p class="description">By signing up, I agree to Travelo's Terms of Service, Privacy Policy, Guest Refund Policy, and Host Guarantee Terms.</p>
                        </div>
                        <button type="submit" class="full-width btn-medium">SIGNUP</button>
                    </form>
                </div>
                <div class="seperator"></div>
                <p>Already a Travelo member? <a href="#travelo-login" class="goto-login soap-popupbox">Login</a></p>
            </div>
            <div id="travelo-login" class="travelo-login-box travelo-box">
                <div class="login-social">
                    <a href="#" class="button login-facebook"><i class="soap-icon-facebook"></i>Login with Facebook</a>
                    <a href="#" class="button login-googleplus"><i class="soap-icon-googleplus"></i>Login with Google+</a>
                </div>
                <div class="seperator">
                    <label>OR</label>
                </div>
                <form>
                    <div class="form-group">
                        <input type="text" class="input-text full-width" placeholder="email address">
                    </div>
                    <div class="form-group">
                        <input type="password" class="input-text full-width" placeholder="password">
                    </div>
                    <div class="form-group">
                        <a href="#" class="forgot-password pull-right">Forgot password?</a>
                        <div class="checkbox checkbox-inline">
                            <label>
                                <input type="checkbox">
                                Remember me
                            </label>
                        </div>
                    </div>
                </form>
                <div class="seperator"></div>
                <p>Don't have an account? <a href="#travelo-signup" class="goto-signup soap-popupbox">Sign up</a></p>
            </div>
        </header>

        <div class="container" style="margin-top: -25%">
            <div id="main">
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <h2 class="page-title" style="color: black">Fly with us in Comfort!</h2>
                <div class="search-box-wrapper style2">
                    <div class="search-box">

                        <div class="search-tab-content">
                            <div class="tab-pane fade active in" id="hotels-tab">
                                <form>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <h4 style="color: black; margin-top: 0px">Book Domestic &amp; International Flight Tickets</h4>
                                        </div>
                                        <div style="width: 50%; float: right;">
                                            <div class="col-sm-4">
                                                <input type="radio" name="JourneyType" id="radio01" value="O" class="JourneyType" title="One Way" onclick="OneWayRadioFunc()" checked="" />
                                                <label for="radio01">
                                                    <span></span>
                                                    <span style="color: black">One Way</span>
                                                </label>
                                            </div>
                                            <div class="col-sm-4">
                                                <input type="radio" name="JourneyType" id="radio02" value="R" class="JourneyType" title="Round Way" onclick="RoundWayRadioFunc()" />
                                                <label for="radio02">
                                                    <span></span>
                                                    <span style="color: black">Round Way</span>
                                                </label>
                                            </div>
                                            <div class="col-sm-4">
                                                <input type="radio" name="JourneyType" id="radio03" value="M" class="JourneyType" title="Multi Way" onclick="MultiWayRadioFunc()" />
                                                <label for="radio03">
                                                    <span></span>
                                                    <span style="color: black">Multi Way</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-8" id="fromToOnwardDateDiv">
                                        </div>

                                        <div class="col-lg-4" style="display: none" id="returnDateDivId">
                                            <br />
                                            <div class="form-group row">
                                                <div class="datepicker-wrap">
                                                    <input id="datepicker_Return" type="text" name="date_from" class="input-text full-width" placeholder="return On" />
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <br />
                                    <div>
                                        <div id="addDestinationDivId" class="row white" style="float: right; padding-right: 300px; display: none; margin-bottom: 20px;">

                                            <img src="../images/Bond/plus.png" id="plus" alt="plus" onclick="addDestination()" title="Add Destination" style="width: 25px" />
                                        </div>
                                        <div id="removeLastDestnDivId" class="row white" style="float: right; padding-right: 300px; display: none; margin-bottom: 20px;">

                                            <img src="../images/Bond/minus.png" id="minus" alt="minus" onclick="removeLastDestn()" title="Remove Destination" style="width: 30px" />
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <div class="col-md-6">
                                                    <div class="selector">
                                                        <select class="full-width" id="sel_Airlines">
                                                            <option selected="selected" value="0">Airlines</option>
                                                            <option value="SG">Spice Jet</option>
                                                            <option value="6E">Indigo</option>
                                                            <option value="G8">Go Air</option>
                                                            <option value="G9">Air Arabia</option>
                                                            <option value="FZ">Fly Dubai</option>
                                                            <option value="IX">Air India Express</option>
                                                            <option value="AK">Air Asia</option>
                                                            <option value="LB">Air Costa</option>
                                                            <option value="GDS">GDS</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="selector">
                                                        <select class="full-width" id="sel_Class">
                                                            <option selected="selected" value="0">Class</option>
                                                            <option value="1">All</option>
                                                            <option value="2">Economy</option>
                                                            <option value="3">Premium Economy</option>
                                                            <option value="4">Business</option>
                                                            <option value="5">Premium Business</option>
                                                            <option value="6">First</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <div class="selector">
                                                        <select class="full-width" id="sel_Adults">
                                                            <option value="">Adults</option>
                                                        
                                                            <option value="1">01</option>
                                                            <option value="2">02</option>
                                                            <option value="3">03</option>
                                                            <option value="4">04</option>
                                                            <option value="">05</option>
                                                            <option value="1">06</option>
                                                            <option value="2">07</option>
                                                            <option value="3">08</option>
                                                            <option value="4">09</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="selector">
                                                        <select class="full-width" id="sel_Child">
                                                            <option value="">Kids</option>
                                                            <option value="0">00</option>
                                                            <option value="1">01</option>
                                                            <option value="2">02</option>
                                                            <option value="3">03</option>
                                                            <option value="4">04</option>
                                                            <option value="">05</option>
                                                            <option value="1">06</option>
                                                            <option value="2">07</option>
                                                            <option value="3">08</option>
                                                            <option value="4">09</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="selector">
                                                        <select class="full-width" id="sel_Infant">
                                                            <option value="">Infants</option>
                                                            <option value="0">00</option>
                                                            <option value="1">01</option>
                                                            <option value="2">02</option>
                                                            <option value="3">03</option>
                                                            <option value="4">04</option>
                                                            <option value="">05</option>
                                                            <option value="1">06</option>
                                                            <option value="2">07</option>
                                                            <option value="3">08</option>
                                                            <option value="4">09</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <div class="col-xs-3">
                                                </div>
                                                <div class="col-xs-6 pull-right">
                                                    <button class="full-width" onclick="SearchFlight()">SERACH NOW</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                        <div class="searchpanel1 hidden-xs">
                            <span class="size18 black">Your last 5 search</span>
                            <div class="padding10" id="tblsearch" style="background-color: rgba(255, 255, 255, 0.9)">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <footer id="footer">

            <div class="bottom gray-area">
                <div class="container">
                    <div class="logo pull-left">
                        <a href="#" title="Travelo - home">
                            <img src="images/logo.png" alt="Travelo HTML5 Template" />
                        </a>
                    </div>
                    <div class="pull-right">
                        <a id="back-to-top" href="#" class="animated" data-animation-type="bounce"><i class="soap-icon-longarrow-up circle"></i></a>
                    </div>
                    <div class="copyright pull-right">
                        <p>&copy; 2018 Travelo</p>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <!-- Javascript -->
    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.noconflict.js"></script>
    <script type="text/javascript" src="js/modernizr.2.7.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="js/jquery-ui.1.10.4.min.js"></script>
    <script src="js/moment.js"></script>
    <!-- Twitter Bootstrap -->
    <script type="text/javascript" src="js/bootstrap.js"></script>

    <!-- load revolution slider scripts -->
    <script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>

    <!-- Flex Slider -->
    <script type="text/javascript" src="components/flexslider/jquery.flexslider-min.js"></script>

    <!-- load BXSlider scripts -->
    <script type="text/javascript" src="components/jquery.bxslider/jquery.bxslider.min.js"></script>

    <!-- parallax -->
    <script type="text/javascript" src="js/jquery.stellar.min.js"></script>

    <!-- waypoint -->
    <script type="text/javascript" src="js/waypoints.min.js"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="js/theme-scripts.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>

</body>
</html>
