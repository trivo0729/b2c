﻿using CUT.BL;
using CUT.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using System.Web;

namespace CUTUK.DataLayer
{
    public class HotelManager
    {
        public static CUT.Common.ReservationDetails objReserDetails { get; set; }

        public static DBHelper.DBReturnCode Get(string name, string destination, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@HotelName", name);
            sqlParams[1] = new SqlParameter("@DestinationCode", String.IsNullOrEmpty(destination) ? null : destination);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("usp_GetHotel", out dtResult, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetCategory(string code, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@LaungageCode", code);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("usp_GetHotelCategory", out dtResult, sqlParams);
            return retCode;
        }
        public static string GetDescription(string code, string LanCode)
        {
            string m_description = "N/A";
            DataTable dtResult = new DataTable();
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@HotelCode", code);
            sqlParams[1] = new SqlParameter("@LanCode", LanCode);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("usp_GetHotelDescripton", out dtResult, sqlParams);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
                m_description = dtResult.Rows[0]["Descriptions"].ToString().Length > 150 ? dtResult.Rows[0]["Descriptions"].ToString().Remove(150) : dtResult.Rows[0]["Descriptions"].ToString();
            return m_description;
        }
        public static DBHelper.DBReturnCode GetHotelFacility(string m_hotel, string m_lang, out DataTable dtResult)
        {
            dtResult = null;
            SqlParameter[] sqlparam = new SqlParameter[2];
            sqlparam[0] = new SqlParameter("@LanguageCode", m_lang);
            sqlparam[1] = new SqlParameter("@HotelCode", m_hotel);
            return DBHelper.GetDataTable("usp_List_Hotel_Facility", out dtResult, sqlparam);
        }
        public static float GetUserRating(string m_hotel, string m_lang)
        {
            float rating = 0;
            DataTable dtResult = new DataTable();
            SqlParameter[] sqlparam = new SqlParameter[2];
            sqlparam[0] = new SqlParameter("@LanguageCode", m_lang);
            sqlparam[1] = new SqlParameter("@HotelCode", m_hotel);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("usp_GetHotelUserRating", out dtResult, sqlparam);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
                float.TryParse(dtResult.Rows[0]["AverageValue"].ToString(), out rating);
            return rating;
        }
        public static int GetHotelReview(string m_hotel, string m_lang)
        {
            int review = 0;
            DataTable dtResult = new DataTable();
            SqlParameter[] sqlparam = new SqlParameter[2];
            sqlparam[0] = new SqlParameter("@LanguageCode", m_lang);
            sqlparam[1] = new SqlParameter("@HotelCode", m_hotel);
            DBHelper.DBReturnCode retcode = DBHelper.GetDataTable("usp_GetHotelUserReview", out dtResult, sqlparam);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
                review = Convert.ToInt32(dtResult.Rows[0][0]);
            return review;
        }
        public static DBHelper.DBReturnCode GetHotelAccomodation(string m_hotel, string m_lang, out DataTable dtResult)
        {
            dtResult = null;
            SqlParameter[] sqlparam = new SqlParameter[2];
            sqlparam[0] = new SqlParameter("@LanguageCode", m_lang);
            sqlparam[1] = new SqlParameter("@HotelCode", m_hotel);
            return DBHelper.GetDataTable("usp_Get_Accommodation", out dtResult, sqlparam);
        }
        public static DBHelper.DBReturnCode GetHotelRoomFacility(string m_hotel, string m_lang, string roomType, out DataTable dtResult)
        {
            dtResult = null;
            SqlParameter[] sqlparam = new SqlParameter[3];
            sqlparam[0] = new SqlParameter("@LanguageCode", m_lang);
            sqlparam[1] = new SqlParameter("@HotelCode", m_hotel);
            sqlparam[2] = new SqlParameter("@RoomType", roomType);
            return DBHelper.GetDataTable("usp_GET_ROOM_FACILITIES", out dtResult, sqlparam);
        }
        public static DBHelper.DBReturnCode GetHotelDetail(string LangCode, List<String> List_HotelCode, out DataSet ds)
        {
            ds = null;
            DataTable dtHotel = new DataTable();
            DataColumn dcHotel = new DataColumn("HotelCode", typeof(string));
            dtHotel.Columns.Add(dcHotel);
            foreach (string data in List_HotelCode)
            {
                DataRow dr = dtHotel.NewRow();
                dr[0] = data;
                dtHotel.Rows.Add(dr);
            }
            SqlParameter[] sqlparam = new SqlParameter[2];
            sqlparam[0] = new SqlParameter("@LangCode", LangCode);
            sqlparam[1] = new SqlParameter("@HotelCode", SqlDbType.Structured);
            sqlparam[1].Value = dtHotel;
            return DBHelper.GetDataSet("usp_GetHotelDetail", out ds, sqlparam);
        }
        public static DBHelper.DBReturnCode GetMarkupDetail(string Supplier, Int64 UserID, out DataTable dtResult)
        {
            dtResult = null;
            SqlParameter[] sqlparam = new SqlParameter[2];
            sqlparam[0] = new SqlParameter("@UserID", UserID);
            sqlparam[1] = new SqlParameter("@Supplier", Supplier);
            return DBHelper.GetDataTable("usp_GetMarkUpDetails", out dtResult, sqlparam);
        }
        public static DBHelper.DBReturnCode Get_Hotel_Facility(string HotelCode, out DataTable dtResult)
        {
            dtResult = null;
            SqlParameter[] sqlparam = new SqlParameter[1];
            sqlparam[0] = new SqlParameter("@HotelCode", HotelCode);
            return DBHelper.GetDataTable("usp_GET_HOTEL_FACILITY", out dtResult, sqlparam);
        }
        public static DBHelper.DBReturnCode Get_Hotel_Room_Facility(string HotelCode, List<string> RoomType, out DataTable dtResult)
        {
            dtResult = null;
            DataTable dtRoomType = new DataTable();
            DataColumn dcCol = new DataColumn("RoomType", typeof(string));
            dtRoomType.Columns.Add(dcCol);
            foreach (string roomType in RoomType)
            {
                DataRow dr = dtRoomType.NewRow();
                dr[0] = roomType;
                dtRoomType.Rows.Add(dr);
            }
            SqlParameter[] sqlparam = new SqlParameter[2];
            sqlparam[0] = new SqlParameter("@HotelCode", HotelCode);
            sqlparam[1] = new SqlParameter("@RoomType", SqlDbType.Structured);
            sqlparam[1].Value = dtRoomType;
            return DBHelper.GetDataTable("usp_GET_HOTEL_ROOM_FACILITY", out dtResult, sqlparam);
        }
        public static DBHelper.DBReturnCode Get_Hotel_Image(string HotelCode, out DataTable dtResult)
        {
            dtResult = null;
            SqlParameter[] sqlparam = new SqlParameter[1];
            sqlparam[0] = new SqlParameter("@HotelCode", HotelCode);
            return DBHelper.GetDataTable("usp_GET_HOTEL_IMAGE", out dtResult, sqlparam);
        }

        public static DBHelper.DBReturnCode Get_Hotel_Address(string HotelCode, out DataTable dtResult)
        {
            dtResult = null;
            SqlParameter[] sqlparam = new SqlParameter[1];
            sqlparam[0] = new SqlParameter("@HotelCode", HotelCode);
            return DBHelper.GetDataTable("usp_GET_HOTEL_ADDRESS", out dtResult, sqlparam);
        }

        public static DBHelper.DBReturnCode AddB2C_CustomerBooking(string HotelName, string ReservationId, string VoucherId, string Passenger, string CheckIn, string CheckOut, string Status, string DeadLine, string TotalFare, string City)
        {
            try
            {
                GlobalDefault objGlobalDefaultCustomer = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                SqlParameter[] sqlparam = new SqlParameter[13];
                int rowEff;
                sqlparam[0] = new SqlParameter("@HotelName", HotelName);
                sqlparam[1] = new SqlParameter("@Passenger", Passenger);
                sqlparam[2] = new SqlParameter("@CheckIn", CheckIn);
                sqlparam[3] = new SqlParameter("@CheckOut", CheckOut);
                sqlparam[4] = new SqlParameter("@Status", "Vouchered");
                sqlparam[5] = new SqlParameter("@DeadLine", DeadLine);
                sqlparam[6] = new SqlParameter("@TotalFare", TotalFare);
                sqlparam[7] = new SqlParameter("@City", City);
                sqlparam[8] = new SqlParameter("@Date", DateTime.Now.ToString());
                sqlparam[9] = new SqlParameter("@ReservationId", ReservationId);
                sqlparam[10] = new SqlParameter("@VoucherId", VoucherId);
                sqlparam[11] = new SqlParameter("@InvoiceId", "");
                sqlparam[12] = new SqlParameter("@B2C_Id", objGlobalDefaultCustomer.B2C_Id);
                return DBHelper.ExecuteNonQuery("Proc_tbl_B2C_CustomerBookingAdd", out rowEff, sqlparam);
            }
            catch
            {
                return DBHelper.DBReturnCode.SUCCESS;
            }
         
          
        }
        //...............................................MGH............................B........................//

        public static bool BookingRequest()
        {
            bool retCode = true;
            try
            {
                objReserDetails.CurrentStatus = "Reservation Details Updated";
                DBHelper.DBReturnCode RetCodeStatus = HotelReservationManager.UpdateBookingStatus(objReserDetails.AffilateCode, objReserDetails.CurrentStatus, objReserDetails.DumReservation);
                DBHelper.DBReturnCode retcode = HotelReservationManager.HotelReservationUpdate(objReserDetails.AffilateCode, objReserDetails.BookingStatus, objReserDetails.Supplier, "", objReserDetails.ReservationId, "", 0, objReserDetails.Remark);
                if (objReserDetails.BookingStatus == "Vouchered")
                {
                    DBHelper.DBReturnCode retCodeUpdateTXN;
                    using (TransactionScope objTransactionScope5 = new TransactionScope())
                    {
                        retCodeUpdateTXN = HotelBookingTxnManager.HotelBookingTxnUpdate(objReserDetails.ReservationId, objReserDetails.DumReservation);

                        if (retCodeUpdateTXN == DBHelper.DBReturnCode.SUCCESS)
                        {
                            objReserDetails.CurrentStatus = "Reservation-Id Added Success";
                            CUT.DataLayer.EmailManager.GenrateMail(objReserDetails);
                            retCodeUpdateTXN = HotelReservationManager.UpdateBookingStatus(objReserDetails.AffilateCode, objReserDetails.CurrentStatus, objReserDetails.DumReservation);
                            retCodeUpdateTXN = HotelBookingTxnManager.UpdateReservationIdRoomLog(objReserDetails.DumReservation, objReserDetails.ReservationId);
                            objTransactionScope5.Complete();
                        }
                        else if (retCodeUpdateTXN != DBHelper.DBReturnCode.SUCCESS)
                        {
                            retCode = false;
                            objReserDetails.CurrentStatus = "Error while Updating transaction details";
                            CUT.DataLayer.EmailManager.GenrateMail(objReserDetails);
                            objTransactionScope5.Dispose();
                            DBHelper.DBReturnCode MyRetCode = HotelBookingTxnManager.UpdateBookingStatus(objReserDetails.ReservationId);
                            throw new Exception(objReserDetails.CurrentStatus);
                        }
                    }
                }
                else
                {
                    objReserDetails.CurrentStatus = "Reservation-Id Added Success";
                    DBHelper.DBReturnCode MyRetCode = HotelBookingTxnManager.HotelBookingTxnUpdate(objReserDetails.ReservationId, objReserDetails.DumReservation);
                    //return bResponseBooking;
                }
            }
            catch (Exception)
            {
                retCode = false;
            }
            return retCode;
        }
    }
}