﻿using CUT.BL;
using CUT.DataLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace CUTUK.DataLayer
{
    public class EmailManager
    {


        public static string GetInvoice(string ReservationID, Int64 Uid, out string title)
        {
            DBHandlerDataContext DB = new DBHandlerDataContext();
            string CurrencyClass = "";
            title = "";
            // CUT.Agent.DataLayer.AutoEmailOnBooking.SendMail(ReservationID);
            string AllPassengers = "";
            if (ReservationID != "")
            {
                DataSet ds = new DataSet();
                DataTable dtHotelReservation, dtBookedPassenger, dtBookedRoom, dtBookingTransactions, dtAgentDetail;
                CUT.DataLayer.GlobalDefault objGlobalDefaults = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
                GlobalDefault objGlobalDefaultCustomer = (GlobalDefault)HttpContext.Current.Session["LoginCustomer"];

                string Night = "";
                SqlParameter[] SQLParams = new SqlParameter[2];
                SQLParams[0] = new SqlParameter("@ReservationID", ReservationID);
                SQLParams[1] = new SqlParameter("@sid", Uid);
                DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("Proc_BookingDetails", out ds, SQLParams);

                dtHotelReservation = ds.Tables[0];
                dtBookedPassenger = ds.Tables[1];
                dtBookedRoom = ds.Tables[2];
                dtBookingTransactions = ds.Tables[3];
                dtAgentDetail = ds.Tables[4];
                if (objGlobalDefaults.UserType == "Agent")
                {
                    CurrencyClass = (HttpContext.Current.Session["CurrencyClass"]).ToString();
                    Uid = objGlobalDefaults.sid;
                }
                else
                {
                    CurrencyClass = dtAgentDetail.Rows[0]["CurrencyCode"].ToString();
                    switch (CurrencyClass)
                    {
                        case "AED":
                            CurrencyClass = "Currency-AED";

                            break;
                        case "SAR":
                            CurrencyClass = "Currency-SAR";
                            break;
                        case "EUR":
                            CurrencyClass = "fa fa-eur";
                            break;
                        case "GBP":
                            CurrencyClass = "fa fa-gbp";
                            break;
                        case "USD":
                            CurrencyClass = "fa fa-dollar";
                            break;
                        case "INR":
                            CurrencyClass = "fa fa-inr";
                            break;
                    }

                }
                string ReservationDate = dtHotelReservation.Rows[0]["ReservationDate"].ToString();
                ReservationDate = ReservationDate.Replace("00:00:", "");
                string Status = (dtHotelReservation.Rows[0]["Status"].ToString());
                if (dtHotelReservation.Rows[0]["Status"].ToString() == "Vouchered" || dtHotelReservation.Rows[0]["Status"].ToString() == "Cancelled")
                    title = "Hotel Invoice -  " + dtHotelReservation.Rows[0]["Status"].ToString() + " on " + ReservationDate + " For " + dtHotelReservation.Rows[0]["bookingname"].ToString();
                else
                    title = "Hotel Invoice - ";



                //CUT.DBHandlerDataContext cDB=new CUT.DBHandlerDataContext();
                var CustemerList = (from Obj in DB.tbl_B2C_CustomerLogins where Obj.B2C_Id == objGlobalDefaultCustomer.B2C_Id.ToString() select Obj).ToList();
                var CityCountry = (from Obj in DB.tbl_HCities where Obj.Code == CustemerList[0].CityId select Obj).ToList();
                decimal SalesTax = Convert.ToDecimal(dtBookingTransactions.Rows[0]["SeviceTax"]);
                decimal Ammount = Convert.ToDecimal(dtBookingTransactions.Rows[0]["BookingAmt"]);
                // decimal Ammount = Convert.ToDecimal(dtBookingTransactions.Rows[0]["BookingAmtWithTax"]);
                SalesTax = decimal.Round(SalesTax, 2, MidpointRounding.AwayFromZero);
                Ammount = decimal.Round(Ammount, 2, MidpointRounding.AwayFromZero);
                string InvoiceID = dtHotelReservation.Rows[0]["InvoiceID"].ToString();
                string Hoteldestination = dtHotelReservation.Rows[0]["City"].ToString();
                string VoucherID = dtHotelReservation.Rows[0]["VoucherID"].ToString();
                string CheckIn = dtHotelReservation.Rows[0]["CheckIn"].ToString();
                CheckIn = CheckIn.Replace("00:00", "");
                string CheckOut = dtHotelReservation.Rows[0]["CheckOut"].ToString();
                CheckOut = CheckOut.Replace("00:00", "");
                string HotelName = (dtHotelReservation.Rows[0]["HotelName"].ToString());
                ReservationDate = ReservationDate.Replace("00:00:", "");
                string AgentRef = (dtHotelReservation.Rows[0]["AgentRef"].ToString());
                string AgencyName = dtAgentDetail.Rows[0]["AgencyName"].ToString();
                string Address = dtAgentDetail.Rows[0]["Address"].ToString();
                string Description = CityCountry[0].Description;
                //string Description = (dtAgentDetail.Rows[0]["Description"].ToString());
                //string Countryname = (dtAgentDetail.Rows[0]["Countryname"].ToString());
                string Countryname = CityCountry[0].Countryname;
                string phone = dtAgentDetail.Rows[0]["phone"].ToString();
                string email = (dtAgentDetail.Rows[0]["email"].ToString());
                Night = (dtHotelReservation.Rows[0]["NoOfDays"].ToString());
                string GstNo = dtAgentDetail.Rows[0]["GSTNumber"].ToString();
                string AgentCode = (dtAgentDetail.Rows[0]["Agentuniquecode"].ToString());
                string Pincode = dtAgentDetail.Rows[0]["Pincode"].ToString();
                string AgentCountryname = (dtAgentDetail.Rows[0]["Countryname"].ToString());
                string Fax = dtAgentDetail.Rows[0]["Fax"].ToString();
                if (Fax == "0")
                {
                    Fax = "";
                }
                string words;
                //words = ToWords((Ammount) + SalesTax);
                words = ToWords((Ammount), CurrencyClass);


                string CanAmtWoutNight = "";
                string CanAmtWithTax = "";

                string Supplier = (dtHotelReservation.Rows[0]["Source"].ToString());
                StringBuilder sb = new StringBuilder();
                //sb.Append("<!DOCTYPE html>");
                //sb.Append("<head>");
                //sb.Append("<meta charset=\"utf-8\" />");
                //sb.Append("</head>");
                //sb.Append("<body style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px; width:90%; border:2px solid gray;\">");
                //sb.Append("<input type=\"button\" class=\"btn btn-info\" value=\"Cancel Booking\" id=\"btn_Cancel\" onclick=\"OpenCancellationPopup('" + ReservationID + "', '" + Status + "')\" style=\"float: right;position: fixed;\">");
                var AdminUrl = ConfigurationManager.AppSettings["AdminUrl"];
                sb.Append("<div style=\"background-color: #F7B85B; height: 13px\">");
                sb.Append("<span>&nbsp;</span>");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("<table style=\" height: 100px; width: 100%;\">");
                sb.Append("<tbody>");
                sb.Append("<tr>");
                sb.Append("<td style=\"width: auto;padding-left:15px\">");
                sb.Append("<img  src=\"" + AdminUrl + "/AgencyLogos/" + AgentCode + ".jpg\" height=\"auto\" width=\"auto\"></img>");
                sb.Append("</td>");
                sb.Append("<td style=\"width: 20%\"></td>");
                sb.Append("<td style=\"width: auto; padding-right:15px; color: #57585A\" align=\"right\" >");
                sb.Append("<span style=\"margin-right: 15px\">");
                sb.Append("<br>");
                sb.Append("" + Address + ",<br>");
                //sb.Append("Juni Mangalwari, Opp. Rahate Hospital,<br>");
                sb.Append("" + Description + " - " + Pincode + ", " + AgentCountryname + "<br>");
                sb.Append("Tel: " + phone + "<br> Fax: " + Fax + "<br>");
                sb.Append(" Email: " + email + "<br>");
                sb.Append("</span>");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td style=\"color: #57585A\"><span style=\"padding-left:10px\"> <b>GST No:</b> " + GstNo + "</span></td>");
                sb.Append("<td></td>");
                sb.Append("<td></td>");
                sb.Append("</tr>");
                sb.Append("</tbody>");
                sb.Append("</table>");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("<table border=\"1\" style=\"width: 100%; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-bottom-width: 3px; border-bottom-color: #E6DCDC; border-left:none; border-right:none\">");
                sb.Append("<tr>");
                sb.Append("<td rowspan=\"3\" style=\"width:35%; color: #00CCFF; font-size: 30px; text-align: center; border: none; padding-right:35px; padding-left:10px\">");
                sb.Append("<b>SALES INVOICE</b><br>");
                if (Status == "Cancelled")
                {
                    sb.Append("<span style=\"font-size:25px\"><b>Status : </b> </span><span id=\"spnStatus\" style=\"font-size:25px; font-weight:700\">Cancelled Booking</span>");
                }
                else
                {
                    sb.Append("<span style=\"font-size:25px\"><b>Status : </b> </span><span id=\"spnStatus\" style=\"font-size:25px; font-weight:700\">" + Status + "</span>");
                }
                //sb.Append("<span style=\"font-size:25px\"><b>Status : </b> </span><span id=\"spnStatus\" style=\"font-size:25px; font-weight:700\">" + Status + "</span>");
                sb.Append("</td>");
                sb.Append("<td style=\"border-top: none; border-bottom-width: 6px; border-bottom-color: gray; padding: 10px 0px 0px 3px; margin-bottom:0px; color: #57585A\">");
                sb.Append("<b> Invoice Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</b><span style=\"color: #757575\"> " + ReservationDate + " </span>");
                sb.Append("</td>");
                sb.Append("<td style=\"border-top: none; border-bottom-width: 6px; border-bottom-color: gray; padding: 10px 0px 0px 3px; margin-bottom: 0px; color: #57585A\">");
                sb.Append("<b>Invoice No  &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;     : </b><span style=\"color: #757575\">" + InvoiceID + " </span>");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td style=\"border-top: none; border-bottom-width: 6px; border-bottom-color: gray; padding: 0px 0px 0px 3px; color: #57585A\">");
                sb.Append("<b> Voucher No.&nbsp;&nbsp; &nbsp; &nbsp; :</b> <span style=\"color: #757575\">" + VoucherID + "</span>");
                sb.Append("</td>");
                sb.Append("<td style=\"border-top: none; border-bottom-width: 6px; border-bottom-color: gray; padding: 0px 0px 0px 3px; color: #57585A\">");
                sb.Append("<b>  Agent Code &nbsp;&nbsp; &nbsp; &nbsp; :</b><span style=\" color:#757575\">" + AgentRef + "</span>");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td colspan=\"2\" style=\"height: 25px; border:none\"></td>");
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("<table border=\"1\" style=\"border-spacing: 0px; height: 150px; width: 100%; border-top: none; padding: 10px 0px 10px 0px; border-width: 0px 0px 3px 0px; border-bottom-color: #E6DCDC\">");
                sb.Append("<tr>");
                sb.Append("<td colspan=\"2\" style=\"border: none; padding-bottom: 10px; padding-left: 10px; font-size: 20px; color: #57585A\"><b>Invoice To</b></td>");
                sb.Append("<td colspan=\"3\" style=\"border: none; padding-bottom: 10px; padding-left: 10px; font-size:20px; color: #57585A\"> <b>Service Details</b></td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td rowspan=\"6\" style=\"width:10%; border-width:3px 0px 0px 0px; border-bottom-color:gray; border-spacing:0px; color: #57585A\">");
                sb.Append("<span style=\"padding-left:10px\"><b>Name</b></span><br>");
                sb.Append("<span style=\"padding-left:10px\"><b>Address</b></span><br>");
                sb.Append("<span style=\"padding-left:10px\"><b>City</b></span><br>");
                sb.Append("<span style=\"padding-left:10px\"><b>Country</b></span><br>");
                sb.Append("<span style=\"padding-left:10px\"><b>Phone</b></span><br>");
                sb.Append("<span style=\"padding-left:10px\"><b>Email</b></span>");
                //sb.Append("<span style=\"padding-left:10px\"><b>GST No.</b></span><br>");
                sb.Append("</td>");
                sb.Append("<td rowspan=\"6\" style=\"width: 40%; border-width: 3px 0px 0px 0px; border-bottom-color: gray; padding-left: 10px; color: #57585A\">");
                sb.Append(":<span style=\"padding-left:10px\">" + dtHotelReservation.Rows[0]["bookingname"] + "</span><br>");
                sb.Append(":<span style=\"padding-left:10px\" >" + CustemerList[0].Address + "</span><br>");
                sb.Append(":<span style=\"padding-left:10px\">" + Description + "</span><br>");
                sb.Append(":<span style=\"padding-left:10px\">" + Countryname + "</span><br>");
                sb.Append(":<span style=\"padding-left:10px\">" + CustemerList[0].Contact1 + "</span><br>");
                sb.Append(":<span style=\"padding-left:10px\">" + CustemerList[0].Email + "</span>");
                //sb.Append(":<span style=\"padding-left:10px\">" + dtAgentDetail.Rows[0]["GSTNumber"] + "</span><br>");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td colspan=\"3\" style=\"border-width: 3px 0px 0px 0px; border-bottom-color: gray; border-spacing: 0px; background-color: #35C2F1; padding-left: 8px\"><span style=\"color:white;font-size:18px\"><b>Hotel Name  :</b></span> <span style=\"color:white;font-size:18px\">" + HotelName + "</span></td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td colspan=\"3\" style=\"border: none; background-color: #27B4E8; padding-left:8px\"><span style=\"color:white; font-size: 18px\"><b>Destination :</b> <span style=\"color:white;font-size:18px\">" + Hoteldestination + "</span></td>");
                sb.Append("</tr>");
                sb.Append("<tr style=\"color:white;\">");
                sb.Append("<td style=\"width: 150px; border: none; background-color: #00AEEF; padding-bottom:3px\" align=\"center\">");
                sb.Append(" <span><b> Check In</b></span><br>");
                sb.Append("<span>" + CheckIn + "</span>");
                sb.Append("</td>");
                sb.Append("<td style=\"width: 150px; border: none; background-color: #00AEEF; padding-bottom: 3px\" align=\"center\">");
                sb.Append("<span><b>Check Out</b></span><br>");
                sb.Append("<span>" + CheckOut + "</span>");
                sb.Append("</td>");
                sb.Append("<td style=\"width: 150px; border: none; background-color: #A8A9AD; padding-bottom: 3px\" align=\"center\">");
                sb.Append("<span><b>Total Night(s)</b></span><br>");
                sb.Append(" <span>" + Night + "Night(s)</span>");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td colspan=\"3\" style=\"border:none\">&nbsp;</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td colspan=\"3\" style=\"border:none\">&nbsp;</td>");
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("</div>");
                //Room Rate table goes here.............................................
                sb.Append("<div style=\"font-size: 20px;padding-bottom:10px; padding-top:8px\">");
                //sb.Append("<span style=\"padding-left:10px; color: #57585A\"><b>Rate</b></span>");
                sb.Append("<span style=\"padding-left:10px; color: #57585A\"><b>Booking Details</b></span>");
                sb.Append("</div>");
                sb.Append("<div>");

                sb.Append("<table border=\"1\" style=\"margin-top: 3px; width: 100%; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-bottom-width: 3px; border-bottom-color: #E6DCDC; border-left: none; border-right: none\">");
                sb.Append("<tr style=\"border: none\">");
                sb.Append("<td style=\"background-color: #35C2F1; color: white; font-size: 15px; border: none;  font-weight: 700; text-align:left; padding-left:10px \">");
                sb.Append("<span>No.</span>");
                sb.Append("</td>");
                sb.Append("<td style=\" height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: left\">");
                sb.Append("<span>Room Type</span>");
                sb.Append("</td>");
                sb.Append("<td style=\" height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none;  font-weight: 700; text-align:center\">");
                sb.Append("<span>Board</span>");
                sb.Append("</td>");
                sb.Append("<td style=\" height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: center\">");
                sb.Append("<span>Rooms</span>");
                sb.Append("</td>");
                sb.Append("<td style=\" height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: center;\">");
                sb.Append("<span>Nights</span>");
                sb.Append("</td>");
                //passenger details
                sb.Append("<td style=\" background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: center;\">");
                sb.Append("<span>Guest Name</span>");
                sb.Append("</td>");
                //passenger
                sb.Append("<td style=\"height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: center;\">");
                sb.Append("<span>Rate ( RM )</span>");
                sb.Append("</td>");
                sb.Append("<td style=\" height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: center;\">");
                sb.Append("<span>Total ( RM )</i></span>");
                sb.Append("</td>");
                sb.Append("</tr>");
                decimal GST = 0;
                string GstDetails = "";
                List<CommonLib.Response.GSTdetails> ListGst = new List<CommonLib.Response.GSTdetails>();
                //foreach (DataRow item in dtBookedRoom.Rows)
                //{


                //}
                if (Supplier == "MGHds")
                {
                    #region MGH
                    #region Offer per Night Calculation
                    var List = (from Offer in DB.tbl_OfferRates where Offer.BookingId == InvoiceID select Offer).ToList();
                    if (List.Count > 0)
                    {
                        if (dtBookedRoom.Rows.Count > 1)
                        {
                            for (var i = 0; i < dtBookedRoom.Rows.Count; i++)
                            {
                                sb.Append("<tr style=\"border: none; background-color: #E6E7E9; padding: 15px 0px 0px 10px; text-align: left; color: #57585A\">");
                                sb.Append("<td align=\"center\" style=\"width:30px; border: none; text-align: left; padding-left: 15px;\">" + (i + 1) + "</td>");
                                sb.Append("<td style=\"border: none\">" + dtBookedRoom.Rows[0]["RoomType"].ToString() + "</td>");
                                sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + dtBookedRoom.Rows[0]["BoardText"].ToString() + "</td>");
                                sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">Room " + (i + 1) + "</td>");
                                sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\" >" + Night + "</td>");
                                AllPassengers = "";
                                for (int p = 0; p < dtBookedPassenger.Rows.Count; p++)
                                {
                                    string brrn = (dtBookedRoom.Rows[0]["RoomNumber"]).ToString();
                                    string bprn = (dtBookedPassenger.Rows[p]["RoomNumber"]).ToString();

                                    if ((dtBookedPassenger.Rows[p]["RoomNumber"]).ToString() == (dtBookedRoom.Rows[i]["RoomNumber"]).ToString())
                                    {
                                        AllPassengers += dtBookedPassenger.Rows[p]["Name"] + " " + dtBookedPassenger.Rows[p]["LastName"] + ", ";
                                    }
                                }
                                AllPassengers = AllPassengers.TrimEnd(' ');
                                AllPassengers = AllPassengers.TrimEnd(',');
                                sb.Append("<td align=\"left\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + AllPassengers + "</td>");
                                sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">(" + List[0].Date.Split(' ')[0] + ") " + ((decimal.Round((decimal)List[0].OfferNightRate, 2, MidpointRounding.AwayFromZero)).ToString("#,##0.00")) + "</td>");
                                sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + ((decimal.Round((decimal)List[0].OfferNightRate, 2, MidpointRounding.AwayFromZero)).ToString("#,##0.00")) + "</td>");
                                sb.Append("</tr>");
                                for (int s = 1; s < List.Count; s++)
                                {
                                    sb.Append("<tr style=\"border: none; background-color: #E6E7E9; padding: 15px 0px 0px 10px; text-align: left; color: #57585A\">");
                                    sb.Append("<td align=\"center\" style=\"width:30px; border: none; text-align: left; padding-left: 15px;\"></td>");
                                    sb.Append("<td style=\"border: none\"></td>");
                                    sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\"></td>");
                                    sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\"></td>");
                                    sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\" ></td>");
                                    sb.Append("<td align=\"left\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\"></td>");
                                    sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">(" + List[s].Date.Split(' ')[0] + ") " + ((decimal.Round((decimal)List[s].OfferNightRate, 2, MidpointRounding.AwayFromZero)).ToString("#,##0.00")) + "</td>");
                                    sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + ((decimal.Round((decimal)List[s].OfferNightRate, 2, MidpointRounding.AwayFromZero)).ToString("#,##0.00")) + "</td>");
                                    sb.Append("</tr>");
                                }
                            }
                        }
                        else
                        {
                            for (var i = 0; i < dtBookedRoom.Rows.Count; i++)
                            {
                                sb.Append("<tr style=\"border: none; background-color: #E6E7E9; padding: 15px 0px 0px 10px; text-align: left; color: #57585A\">");
                                sb.Append("<td align=\"center\" style=\"width:30px; border: none; text-align: left; padding-left: 15px;\">" + (i + 1) + "</td>");
                                sb.Append("<td style=\"border: none\">" + dtBookedRoom.Rows[i]["RoomType"].ToString() + "</td>");
                                sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + dtBookedRoom.Rows[i]["BoardText"].ToString() + "</td>");
                                sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + dtBookedRoom.Rows[i]["RoomNumber"].ToString() + "</td>");
                                sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\" >" + (Night) + "</td>");
                                AllPassengers = "";
                                for (int p = 0; p < dtBookedPassenger.Rows.Count; p++)
                                {
                                    string brrn = (dtBookedRoom.Rows[i]["RoomNumber"]).ToString();
                                    string bprn = (dtBookedPassenger.Rows[p]["RoomNumber"]).ToString();

                                    if ((dtBookedPassenger.Rows[p]["RoomNumber"]).ToString() == (dtBookedRoom.Rows[i]["RoomNumber"]).ToString())
                                    {
                                        AllPassengers += dtBookedPassenger.Rows[p]["Name"] + " " + dtBookedPassenger.Rows[p]["LastName"] + ", ";
                                    }
                                }
                                AllPassengers = AllPassengers.TrimEnd(' ');
                                AllPassengers = AllPassengers.TrimEnd(',');
                                sb.Append("<td align=\"left\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + AllPassengers + "</td>");
                                sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">(" + List[0].Date.Split(' ')[0] + ") " + ((decimal.Round((decimal)List[i].OfferNightRate, 2, MidpointRounding.AwayFromZero)).ToString("#,##0.00")) + "</td>");
                                sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + ((decimal.Round((decimal)List[i].OfferNightRate, 2, MidpointRounding.AwayFromZero)).ToString("#,##0.00")) + "</td>");
                                sb.Append("</tr>");
                                for (int s = 1; s < List.Count; s++)
                                {
                                    sb.Append("<tr style=\"border: none; background-color: #E6E7E9; padding: 15px 0px 0px 10px; text-align: left; color: #57585A\">");
                                    sb.Append("<td align=\"center\" style=\"width:30px; border: none; text-align: left; padding-left: 15px;\"></td>");
                                    sb.Append("<td style=\"border: none\"></td>");
                                    sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\"></td>");
                                    sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\"></td>");
                                    sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\" ></td>");
                                    sb.Append("<td align=\"left\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\"></td>");
                                    sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">(" + List[s].Date.Split(' ')[0] + ") " + ((decimal.Round((decimal)List[s].OfferNightRate, 2, MidpointRounding.AwayFromZero)).ToString("#,##0.00")) + "</td>");
                                    sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + ((decimal.Round((decimal)List[s].OfferNightRate, 2, MidpointRounding.AwayFromZero)).ToString("#,##0.00")) + "</td>");
                                    sb.Append("</tr>");
                                }
                            }
                        }
                    }
                    #endregion
                    else
                    {
                        for (var i = 0; i < dtBookedRoom.Rows.Count; i++)
                        {
                            sb.Append("<tr style=\"border: none; background-color: #E6E7E9; padding: 15px 0px 0px 10px; text-align: left; color: #57585A\">");
                            sb.Append("<td align=\"center\" style=\"width:30px; border: none; text-align: left; padding-left: 15px;\">" + (i + 1) + "</td>");
                            sb.Append("<td style=\"border: none\">" + dtBookedRoom.Rows[i]["RoomType"].ToString() + "</td>");
                            sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + dtBookedRoom.Rows[i]["BoardText"].ToString() + "</td>");
                            sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + dtBookedRoom.Rows[i]["RoomNumber"].ToString() + "</td>");
                            sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\" >" + (Night) + "</td>");
                            AllPassengers = "";
                            for (int p = 0; p < dtBookedPassenger.Rows.Count; p++)
                            {
                                string brrn = (dtBookedRoom.Rows[i]["RoomNumber"]).ToString();
                                string bprn = (dtBookedPassenger.Rows[p]["RoomNumber"]).ToString();

                                if ((dtBookedPassenger.Rows[p]["RoomNumber"]).ToString() == (dtBookedRoom.Rows[i]["RoomNumber"]).ToString())
                                {
                                    AllPassengers += dtBookedPassenger.Rows[p]["Name"] + " " + dtBookedPassenger.Rows[p]["LastName"] + ", ";
                                }
                            }
                            AllPassengers = AllPassengers.TrimEnd(' ');
                            AllPassengers = AllPassengers.TrimEnd(',');
                            sb.Append("<td align=\"left\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + AllPassengers + "</td>");
                            sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + ((decimal.Round(Convert.ToDecimal(dtBookedRoom.Rows[i]["RoomAmount"]), 2, MidpointRounding.AwayFromZero)).ToString("#,##0.00")) + "</td>");
                            sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + ((Convert.ToInt64(Night)) * (decimal.Round(Convert.ToDecimal(dtBookedRoom.Rows[i]["RoomAmount"]), 2, MidpointRounding.AwayFromZero))).ToString("#,##0.00") + "</td>");
                            sb.Append("</tr>");
                        }
                    }
                    #endregion
                }
                else
                {
                    #region Other Supplier
                    for (var i = 0; i < dtBookedRoom.Rows.Count; i++)
                    {
                        decimal RoomGst = 0;
                        if (dtBookedRoom.Rows[i]["TaxDetails"] != null)
                        {
                            string[] Taxces = dtBookedRoom.Rows[i]["TaxDetails"].ToString().Split('^');
                            foreach (string Tax in Taxces)
                            {
                                if (Tax != "")
                                {
                                    GST += Convert.ToDecimal(Tax.Split('_')[1]);
                                    RoomGst += Convert.ToDecimal(Tax.Split('_')[1]);
                                    ListGst.Add(new CommonLib.Response.GSTdetails
                                    {
                                        //Ammount = Convert.ToDecimal(Tax.Split('_')[1]),
                                        OnMarkup = false,
                                        PerCentage = "",
                                        Amount = Convert.ToSingle(Tax.Split('_')[1]),
                                        Type = Tax.Split('_')[0],
                                    });
                                }
                            }
                        }
                        sb.Append("<tr style=\"border: none; background-color: #E6E7E9; padding: 15px 0px 0px 10px; text-align: left; color: #57585A\">");
                        sb.Append("<td align=\"center\" style=\"width:30px; border: none; text-align: left; padding-left: 15px;\">" + (i + 1) + "</td>");
                        sb.Append("<td style=\"border: none\">" + dtBookedRoom.Rows[i]["RoomType"].ToString() + "</td>");
                        sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + dtBookedRoom.Rows[i]["BoardText"].ToString() + "</td>");
                        sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + dtBookedRoom.Rows[i]["RoomNumber"].ToString() + "</td>");
                        sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\" >" + (Night) + "</td>");
                        AllPassengers = "";
                        for (int p = 0; p < dtBookedPassenger.Rows.Count; p++)
                        {
                            string brrn = (dtBookedRoom.Rows[i]["RoomNumber"]).ToString();
                            string bprn = (dtBookedPassenger.Rows[p]["RoomNumber"]).ToString();

                            if ((dtBookedPassenger.Rows[p]["RoomNumber"]).ToString() == (dtBookedRoom.Rows[i]["RoomNumber"]).ToString())
                            {
                                AllPassengers += dtBookedPassenger.Rows[p]["Name"] + " " + dtBookedPassenger.Rows[p]["LastName"] + ", ";

                            }

                        }

                        AllPassengers = AllPassengers.TrimEnd(' ');
                        AllPassengers = AllPassengers.TrimEnd(',');
                        sb.Append("<td align=\"left\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + AllPassengers + "</td>");
                        sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + ((decimal.Round((Convert.ToDecimal(dtBookedRoom.Rows[i]["RoomAmount"]) - RoomGst) / (Convert.ToInt64(Night)), 2, MidpointRounding.AwayFromZero)).ToString("#,##0.00")) + "</td>");
                        sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + (Convert.ToDecimal(dtBookedRoom.Rows[i]["RoomAmount"]) - RoomGst).ToString("#,##0.00") + "</td>");
                        sb.Append("</tr>");

                    }
                    #endregion

                }


                if (GST != 0)
                {
                    List<CommonLib.Response.GSTdetails> Gst = ListGst.GroupBy(x => x.Type)
                                 .Select(g => g.First())
                                 .ToList();
                    string Type = "";
                    foreach (CommonLib.Response.GSTdetails GstItem in Gst)
                    {
                        float Tax = ListGst.Where(d => d.Type == GstItem.Type).ToList().Select(d => d.Amount).ToList().Sum();
                        GstDetails += Tax.ToString("#,##0.00") + "<br/>";
                        Type += GstItem.Type + "<br/>";
                    }
                    sb.Append("<tr style=\"border: none; background-color: #E6E7E9; text-align: right\">");
                    sb.Append("<td align=\"right\" colspan=\"7\" style=\"border: none; width: 20px; padding-right: 25px; color: #57585A;\">");
                    sb.Append("<span><b> " + Type + " </b></span>");
                    sb.Append("</td>");
                    sb.Append("<td style=\"border: none; width: 20px; text-align: center; color: #57585A\"><b>" + GstDetails + "</b></td>");
                    sb.Append("</tr>");
                }
                sb.Append("<tr border=\"1\" style=\"border-spacing:0px\">");
                sb.Append("<td colspan=\"5\" align=\"left\" style=\"height: 35px;  background-color: #00AEEF; color: white; font-size: 15px; padding-left: 10px;  border-width: 3px 0px 0px 0px; border-top-color: #E6DCDC;\">");
                sb.Append("<b> In Words:</b> <span>: " + words + "</span>");
                sb.Append("</td>");
                sb.Append("<td align=\"center\" style=\"height: 35px; background-color: #27B4E8; color: white; font-size: 18px; padding-left: 10px; font-weight: 700; border-width: 3px 0px 0px 0px; border-top-color: #E6DCDC;\">");
                sb.Append("<span>Total Amount</span>");
                sb.Append("</td>");
                sb.Append("<td align=\"center\" style=\"height: 35px; background-color: #27B4E8; color: white; font-size: 18px; padding-left: 10px; font-weight: 700; border-width: 3px 0px 0px 0px; border-top-color: #E6DCDC;\">");
                sb.Append("</td>");
                sb.Append("<td align=\"center\" style=\"height: 35px; background-color: #35C2F1; color: white; font-size: 18px; padding-left: 10px; font-weight: 700; border-width: 3px 0px 0px 0px; border-top-color: #E6DCDC;\">");
                //  sb.Append("<span>" + Ammount.ToString("#,##0.00") + "</span>"); ((Ammount * Convert.ToInt32(Night)) + SalesTax)
                sb.Append("<span>" + ((Ammount)).ToString("#,##0.00") + "</span>");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("</div>");

                //Cancellation table goes here.............................................
                sb.Append("<div style=\"font-size: 20px; padding-bottom: 10px; padding-top: 8px; color: #57585A\">");
                sb.Append("<span style=\"padding-left:10px\"><b>Cancellation Charge</b></span>");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("<table border=\"1\" style=\"margin-top: 3px; width: 100%; border-spacing: 0px; border-width:3px 0px 3px 0px; border-top-color: gray;  border-bottom-color: #E6DCDC\">");
                sb.Append("<tr style=\"border: none\">");

                sb.Append("<td align=\"center\" style=\"background-color: #35C2F1; color: white; font-size: 15px; border: none;  font-weight: 700;  padding-left:10px\">");
                sb.Append("<span>No.</span>");
                sb.Append("</td>");
                sb.Append("<td style=\"width:190px; height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: left\">");
                sb.Append("<span>Room Type</span>");
                sb.Append("</td>");
                sb.Append("<td align=\"center\" style=\"height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; padding-left: 10px; font-weight: 700;\">");
                sb.Append("<span>Rooms</span>");
                sb.Append("</td>");
                sb.Append("<td align=\"center\" style=\"height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; padding-left: 10px; font-weight: 700;\">");
                sb.Append("<span>Cancellation After</span>");
                sb.Append("</td>");
                sb.Append("<td align=\"center\" style=\"height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; padding-left: 10px; font-weight: 700;\">");
                sb.Append("<span>Charge/Unit ( RM ) </span>");
                sb.Append("</td>");
                sb.Append("<td align=\"center\" style=\"height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; padding-left: 10px; font-weight: 700;\">");
                sb.Append("<span>Total Charge ( RM )</span>");
                sb.Append("</td>");
                sb.Append("</tr>");

                CanAmtWithTax = "";
                CanAmtWoutNight = "";

                for (var i = 0; i < dtBookedRoom.Rows.Count; i++)
                {
                    // string SupplierNoChargeDate = dtBookedRoom.Rows[i]["SupplierNoChargeDate"].ToString();
                    string SupplierNoChargeDate = dtBookedRoom.Rows[i]["CutCancellationDate"].ToString();
                    SupplierNoChargeDate = SupplierNoChargeDate.TrimEnd('|');

                    CanAmtWithTax = dtBookedRoom.Rows[i]["CancellationAmount"].ToString();
                    CanAmtWithTax = CanAmtWithTax.TrimEnd('|');
                    if (Supplier == "MGHs")
                    {
                        #region MGH

                        if (CanAmtWithTax.Contains('|'))
                        {
                            string[] AmtWithTax = CanAmtWithTax.Split('|');
                            CanAmtWoutNight = "";
                            CanAmtWithTax = "";
                            for (int c = 0; c < AmtWithTax.Length; c++)
                            {
                                if (AmtWithTax[c] != "")
                                {
                                    if (c != (AmtWithTax.Length - 1))
                                    {
                                        CanAmtWoutNight += (decimal.Round(Convert.ToDecimal(AmtWithTax[c]), 2, MidpointRounding.AwayFromZero)).ToString() + "|";

                                        CanAmtWithTax += (decimal.Round((Convert.ToDecimal(AmtWithTax[c]) * (Convert.ToDecimal(Night))), 2, MidpointRounding.AwayFromZero)).ToString() + "|";
                                    }
                                    else
                                    {
                                        CanAmtWoutNight += (decimal.Round(Convert.ToDecimal(AmtWithTax[c]), 2, MidpointRounding.AwayFromZero)).ToString();

                                        CanAmtWithTax += (decimal.Round((Convert.ToDecimal(AmtWithTax[c]) * (Convert.ToDecimal(Night))), 2, MidpointRounding.AwayFromZero)).ToString();
                                    }
                                }
                            }
                        }
                        else
                        {
                            //CanAmtWithTax = dtBookedRoom.Rows[i]["CancellationAmount"].ToString();
                            CanAmtWoutNight = (decimal.Round(Convert.ToDecimal(CanAmtWithTax), 2, MidpointRounding.AwayFromZero)).ToString();
                            CanAmtWithTax = (decimal.Round((Convert.ToDecimal(CanAmtWithTax)), 2, MidpointRounding.AwayFromZero) * (Convert.ToDecimal(Night))).ToString();
                        }

                        #endregion MGH
                    }
                    else
                    {
                        #region Other Supplier

                        if (CanAmtWithTax.Contains('|'))
                        {
                            string[] AmtWithTax = CanAmtWithTax.Split('|');
                            CanAmtWoutNight = "";
                            CanAmtWithTax = "";
                            for (int c = 0; c < AmtWithTax.Length; c++)
                            {
                                if (AmtWithTax[c] != "")
                                {
                                    if (c != (AmtWithTax.Length - 1))
                                    {
                                        CanAmtWoutNight += (decimal.Round((Convert.ToDecimal(AmtWithTax[c]) / (Convert.ToDecimal(Night))), 2, MidpointRounding.AwayFromZero)).ToString() + "|";

                                        CanAmtWithTax += (decimal.Round((Convert.ToDecimal(AmtWithTax[c])), 2, MidpointRounding.AwayFromZero)).ToString() + "|";
                                    }
                                    else
                                    {
                                        CanAmtWoutNight += (decimal.Round(Convert.ToDecimal(AmtWithTax[c]) / (Convert.ToDecimal(Night)), 2, MidpointRounding.AwayFromZero)).ToString();

                                        CanAmtWithTax += (decimal.Round((Convert.ToDecimal(AmtWithTax[c])), 2, MidpointRounding.AwayFromZero)).ToString();
                                    }
                                }
                            }
                        }
                        else
                        {
                            //CanAmtWithTax = dtBookedRoom.Rows[i]["CancellationAmount"].ToString();
                            CanAmtWoutNight = (decimal.Round(Convert.ToDecimal(CanAmtWithTax) / (Convert.ToDecimal(Night)), 2, MidpointRounding.AwayFromZero)).ToString();
                            CanAmtWithTax = (decimal.Round((Convert.ToDecimal(CanAmtWithTax)), 2, MidpointRounding.AwayFromZero)).ToString();
                        }

                        #endregion Other Supplier
                    }

                    sb.Append("<tr style=\"border: none; background-color: #E6E7E9; padding: 15px 0px 0px 10px; text-align: left; color: #57585A\">");
                    sb.Append("<td align=\"center\" style=\"width:30px; border: none; text-align: left; padding-left: 15px;\">" + (i + 1) + "</td>");
                    sb.Append("<td style=\"border: none\">" + dtBookedRoom.Rows[i]["RoomType"].ToString() + "</td>");
                    sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + dtBookedRoom.Rows[i]["RoomNumber"].ToString() + "</td>");
                    sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + SupplierNoChargeDate + "</td>");
                    sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\" >" + CanAmtWoutNight + "</td>");
                    sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + CanAmtWithTax + "</td>");
                    sb.Append("</tr>");
                }
                sb.Append("<tr style=\"border: none; background-color: #E6E7E9; text-align: left;\">");
                sb.Append("<td colspan=\"7\" style=\"border: none; width: 20px; padding: 0px 15px 15px 25px; color: #ECA236;\">");
                sb.Append("*Dates & timing will calculated based on local timing </td>");
                sb.Append("</tr>");

                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("</div>");


                //sb.Append("<div style=\"font-size: 20px; padding-bottom: 10px; padding-top: 8px\">");
                //sb.Append("<span style=\"padding-left:10px;color: #57585A\"><b>Bank Details</b></span>");
                //sb.Append("</div>");
                sb.Append("<div>");
                //sb.Append("<table border=\"1\" style=\"margin-top: 3px; height:100px; width:100%; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-left: none; border-right: none\">");
                //sb.Append("<tr style=\"border: none; background-color: #F7B85B; border-bottom-color: gray; color: #57585A;\">");

                //sb.Append("<td align=\"left\" style=\"width: 33%; height: 35px; font-size: 15px; border: none; padding: 10px 0px 10px 10px; border-bottom: 3px; border-bottom-color: gray;\">");
                //sb.Append("<span>");
                //sb.Append("<b>Bank Name &nbsp;</b>: &nbsp;&nbsp;&nbsp;<span>ICICI Bank</span><br>");
                //sb.Append("<b>Account No&nbsp;</b>: &nbsp;&nbsp;<span>123 456 7890</span><br>");
                //sb.Append("<b>Branch &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;</b> : <span>&nbsp; Lakadguanj, Nagpur</span><br>");
                //sb.Append("<b>Swift Code   </b>&nbsp; : &nbsp;&nbsp;<span>ICICI-123456</span><br>");
                //sb.Append("</span>");
                //sb.Append("</td>");
                //sb.Append("<td align=\"left\" style=\"width: 33%; height: 35px; font-size: 15px; border: none; padding: 10px 0px 10px 10px; border-bottom: 3px; border-bottom-color: gray;\">");
                //sb.Append("<span>");
                //sb.Append("<b>Bank Name &nbsp;</b>: &nbsp;&nbsp;&nbsp; <span>AXIS Bank</span><br>");
                //sb.Append("<b>Account No&nbsp; </b>: &nbsp;&nbsp;<span>123 456 7890</span><br>");
                //sb.Append("<b>Branch &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;</b> : <span>&nbsp; Lakadguanj, Nagpur</span><br>");
                //sb.Append("<b>Swift Code   </b>&nbsp; : &nbsp;&nbsp;<span>UTI-123456</span><br>");
                //sb.Append("</span>");
                //sb.Append("</td>");
                //sb.Append("<td align=\"left\" style=\"width: 33%; height: 35px; font-size: 15px; border: none; padding: 10px 0px 10px 10px; border-bottom: 3px; border-bottom-color: gray;\">");
                //sb.Append("<span>");
                //sb.Append("<b>Bank Name&nbsp; </b>: &nbsp;&nbsp;&nbsp; <span>Bank Of India</span><br>");
                //sb.Append("<b>Account No &nbsp;</b>: &nbsp;&nbsp;<span>123 456 7890</span><br>");
                //sb.Append("<b>Branch &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;</b> : <span>&nbsp;Itwari, Nagpur</span><br>");
                //sb.Append("<b>Swift Code   </b>&nbsp; : &nbsp;&nbsp;<span>BOI-123456</span><br>");
                //sb.Append("</span>");
                //sb.Append("</td>");
                //sb.Append("</tr>");
                //sb.Append("</table>");

                sb.Append("<table border=\"1\" style=\"height:100px; width: 100%; border-spacing: 0px; border-bottom:none; border-left: none; border-right: none\">");
                sb.Append("<tr style=\"font-size: 20px; border-spacing: 0px\">");
                sb.Append("<td colspan=\"5\" height=\"20px\" style=\"width: 70%; background-color: #E6E7E9; padding: 10px 10px 10px 10px; color: #57585A\"><b> Terms & Conditions</b></td>");
                //sb.Append("<td rowspan=\"2\" style=\"border-bottom:none; border-left:none; text-align:center\">");
                ////sb.Append("<img src=\"http://www.clickurtrip.com/images/signature.png\"  height=\"auto\" width=\"auto\"></img>");
                //sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("<tr style=\"font-size: 15px\">");
                sb.Append("<td colspan=\"5\" style=\"background-color: #E6E7E9; border-top-width: 3px; border-top-color: #E6E7E9; padding:10px 10px 10px 10px;color: #57585A\">");

                sb.Append("<ul class=\"circle\">");
                sb.Append("<li>Kindly check all details carefully to avoid un-necessary complications</li>");
                sb.Append("<li> Cheque to be drawn in our company name on presentation of invoice</li>");
                //sb.Append("<li>Subject to AGRA (INDIA) jurisdiction </li>");
                sb.Append("</ul>");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("</div>");
                sb.Append("<div style=\"background-color: #00AEEF; text-align: center; font-size: 21px; color: white\">");
                sb.Append("<span>");
                sb.Append("Computer generated invoice do not require signature...");

                sb.Append("</span>");
                sb.Append("</div>");
                return sb.ToString();
            }
            else
                return "";
        }



        public static bool SendInvoice(string sEmail, string ReservationId, string UID, string Night)
        {
            DataSet ds = new DataSet();
            DataTable dtHotelReservation, dtBookedPassenger, dtBookedRoom, dtBookingTransactions, dtAgentDetail;
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid;
            if (objGlobalDefaults.UserType != "Admin")
            {
                Uid = objGlobalDefaults.sid;
            }
            else
                Uid = Convert.ToInt64(UID);

            string invoice = "";
            string Mail = EmailManager.GetInvoice(ReservationId, Uid, out invoice);

            try
            {
                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["supportMail"]));

                List<string> DocLinksList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();

                foreach (string mail in sEmail.Split(','))
                {
                    if (mail != "")
                    {
                        Email1List.Add(mail, mail);
                    }
                }
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);

                bool reponse = CUT.DataLayer.MailManager.SendMail(accessKey, Email1List, "Your Booking Detail", Mail, from, DocLinksList);
                return reponse;
            }
            catch
            {
                return false;
            }

        }

        public static bool SendFlightInvoice(string sEmail, string ReservationId)
        {

            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            string Mail = HttpContext.Current.Session["Invoice"].ToString();
            try
            {
                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["supportMail"]));

                List<string> DocLinksList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();

                foreach (string mail in sEmail.Split(','))
                {
                    if (mail != "")
                    {
                        Email1List.Add(mail, mail);
                    }
                }
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);

                bool reponse = CUT.DataLayer.MailManager.SendMail(accessKey, Email1List, "Your Booking Detail", Mail, from, DocLinksList);
                return reponse;
            }
            catch
            {
                return false;
            }

        }


        public static bool SendVoucher(string sEmail, string ReservationId, string UID, string Latitude, string Longitude, string Night, string Supp)
        {

            DataSet ds = new DataSet();
            DataTable dtHotelReservation, dtBookedPassenger, dtBookedRoom, dtBookingTransactions, dtAgentDetail;
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid;
            if (objGlobalDefaults.UserType != "Admin")
            {
                Uid = objGlobalDefaults.sid;
            }
            else
                Uid = Convert.ToInt64(UID);

            string invoice = "";
            string Mail = EmailManager.GetVoucher(sEmail, ReservationId, UID, Latitude, Longitude, Night, Supp);

            try
            {
                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["supportMail"]));

                List<string> DocLinksList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();

                foreach (string mail in sEmail.Split(','))
                {
                    if (mail != "")
                    {
                        Email1List.Add(mail, mail);
                    }
                }
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);

                bool reponse = CUT.DataLayer.MailManager.SendMail(accessKey, Email1List, "Hotel Voucher", Mail, from, DocLinksList);
                return reponse;
            }
            catch
            {
                return false;
            }

        }


        public static string GetVoucher(string sEmail, string ReservationId, string UID, string Latitude, string Longitude, string Night, string Supp)
        {
            StringBuilder sb = new StringBuilder();
            string sSubject = "Voucher Details";
            Night = "";
            string Supplier = "";
            Latitude = "";
            Longitude = "";
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            UID = objGlobalDefault.sid.ToString();
            string HotelAddress = "";
            string HotelCity = "";
            string HotelCountry = "";
            string HotelPhone = "";
            string HotelPostal = "";
            string City = "";
            string AllPassengers = "";
            DataSet ds = new DataSet();
            DataTable dtHotelReservation, dtBookedPassenger, dtBookedRoom, dtBookingTransactions, dtAgentDetail, dtHotelAdd;


            SqlParameter[] SQLParams = new SqlParameter[2];
            SQLParams[0] = new SqlParameter("@ReservationID", ReservationId);
            SQLParams[1] = new SqlParameter("@sid", UID);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("Proc_BookingDetails", out ds, SQLParams);

            dtHotelReservation = ds.Tables[0];
            dtBookedPassenger = ds.Tables[1];
            dtBookedRoom = ds.Tables[2];
            dtBookingTransactions = ds.Tables[3];
            dtAgentDetail = ds.Tables[4];
            dtHotelAdd = ds.Tables[5];
            Night = dtHotelReservation.Rows[0]["NoOfDays"].ToString();
            Supplier = dtHotelReservation.Rows[0]["Source"].ToString();
            if (Supplier == "MGH" || Supplier == "DoTW")
            {
                Latitude = dtHotelAdd.Rows[0]["LatitudeMGH"].ToString();
                Longitude = dtHotelAdd.Rows[0]["LongitudeMGH"].ToString();
                HotelAddress = dtHotelAdd.Rows[0]["Address"].ToString();
                HotelCity = dtHotelAdd.Rows[0]["City"].ToString();
                HotelCountry = dtHotelAdd.Rows[0]["Country"].ToString();
                HotelPhone = dtHotelAdd.Rows[0]["ContactPhone"].ToString();
                HotelPostal = dtHotelAdd.Rows[0]["PostalCode"].ToString();

            }
            else
            {
                Latitude = dtHotelReservation.Rows[0]["Latitude"].ToString();
                Longitude = dtHotelReservation.Rows[0]["Longitude"].ToString();

                HotelAddress = dtHotelAdd.Rows[0]["Address"].ToString();
                City = dtHotelReservation.Rows[0]["City"].ToString();
                string[] CitySplit = City.Split(',');
                HotelCity = CitySplit[0];
                HotelCountry = CitySplit[1];
                HotelPhone = dtHotelAdd.Rows[0]["Number_"].ToString();
                HotelPostal = dtHotelAdd.Rows[0]["PostalCode"].ToString();
            }




            Double SalesTax = Convert.ToDouble(dtBookingTransactions.Rows[0]["SeviceTax"]);
            Double Ammount = Convert.ToDouble(dtBookingTransactions.Rows[0]["BookingAmtWithTax"]);

            string Hoteldestination = dtHotelReservation.Rows[0]["City"].ToString();
            string InvoiceID = dtHotelReservation.Rows[0]["InvoiceID"].ToString();
            string VoucherID = dtHotelReservation.Rows[0]["VoucherID"].ToString();
            string CheckIn = dtHotelReservation.Rows[0]["CheckIn"].ToString();


            CheckIn = CheckIn.Replace("00:00", "");
            if (CheckIn.Contains('/'))
            {
                string[] CheckInDate = CheckIn.Split('/');
                CheckIn = CheckInDate[2] + '-' + CheckInDate[1] + '-' + CheckInDate[0];
            }
            string CheckOut = dtHotelReservation.Rows[0]["CheckOut"].ToString();
            CheckOut = CheckOut.Replace("00:00", "");
            if (CheckIn.Contains('/'))
            {
                string[] CheckOutDate = CheckOut.Split('/');
                CheckOut = CheckOutDate[2] + '-' + CheckOutDate[1] + '-' + CheckOutDate[0];
            }


            string ReservationDate = dtHotelReservation.Rows[0]["ReservationDate"].ToString();
            ReservationDate = ReservationDate.Replace("00:00", "");
            if (CheckIn.Contains('/'))
            {
                string[] ReservationBookingDate = ReservationDate.Split('/');
                ReservationDate = ReservationBookingDate[2] + '-' + ReservationBookingDate[1] + '-' + ReservationBookingDate[0];

            }



            string HotelName = (dtHotelReservation.Rows[0]["HotelName"].ToString());
            string AgentRef = (dtHotelReservation.Rows[0]["AgentRef"].ToString());
            string AgencyName = dtAgentDetail.Rows[0]["AgencyName"].ToString();
            string Address = dtAgentDetail.Rows[0]["Address"].ToString();
            string Description = (dtAgentDetail.Rows[0]["Description"].ToString());
            string Countryname = (dtAgentDetail.Rows[0]["Countryname"].ToString());
            string phone = dtAgentDetail.Rows[0]["phone"].ToString();
            string email = (dtAgentDetail.Rows[0]["email"].ToString());
            string agentcode = (dtAgentDetail.Rows[0]["Agentuniquecode"].ToString());
            string GstNo = dtAgentDetail.Rows[0]["GSTNumber"].ToString();
            string AgentCode = (dtAgentDetail.Rows[0]["Agentuniquecode"].ToString());
            string Pincode = dtAgentDetail.Rows[0]["Pincode"].ToString();
            string AgentCountryname = (dtAgentDetail.Rows[0]["Countryname"].ToString());
            string Fax = dtAgentDetail.Rows[0]["Fax"].ToString();
            if (Fax == "0")
            {
                Fax = "";
            }


            sb.Append("<!DOCTYPE html>");
            sb.Append("<head>");
            sb.Append("<meta charset=\"utf-8\"/>");
            sb.Append("<title></title>");
            sb.Append("</head>");
            sb.Append("<body style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px; width:auto; border:2px solid gray\">");
            var AdminUrl = ConfigurationManager.AppSettings["AdminUrl"];
            sb.Append("<div style=\"background-color: #F7B85B; height: 13px\">");
            sb.Append("<span>&nbsp;</span>");
            sb.Append("</div>");
            sb.Append("<div>");
            sb.Append("<table style=\" height: 100px; width: 100%;\">");
            sb.Append("<tbody>");
            sb.Append("<tr>");
            sb.Append("<td style=\"width: auto;padding-left:15px\">");
            sb.Append("<img  src=\"" + AdminUrl + "/AgencyLogos/" + AgentCode + ".jpg\" height=\"auto\" width=\"auto\"></img>");
            sb.Append("</td>");
            sb.Append("<td style=\"width: 20%\"></td>");
            sb.Append("<td style=\"width: auto; padding-right:15px; color: #57585A\" align=\"right\" >");
            sb.Append("<span style=\"margin-right: 15px\">");
            sb.Append("<br>");
            sb.Append("" + Address + ",<br>");
            //sb.Append("Juni Mangalwari, Opp. Rahate Hospital,<br>");
            sb.Append("" + Description + " - " + Pincode + ", " + AgentCountryname + "<br>");
            sb.Append("Tel: " + phone + "<br> Fax: " + Fax + "<br>");
            sb.Append(" Email: " + email + "<br>");
            sb.Append("</span>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td style=\"color: #57585A\"><span style=\"padding-left:10px\"> <b>GST No:</b> " + GstNo + "</span></td>");
            sb.Append("<td></td>");
            sb.Append("<td></td>");
            sb.Append("</tr>");
            sb.Append("</tbody>");
            sb.Append("</table>");
            sb.Append("</div>");
            sb.Append("<div>");
            sb.Append("<table border=\"1\" style=\"width: 100%; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-bottom-width: 3px; border-bottom-color: #E6DCDC; border-left:none; border-right:none\">");
            sb.Append("<tr>");
            sb.Append("<td rowspan=\"3\" style=\"width:35%; color: #00CCFF; font-size: 22px; text-align: center; border: none; padding-right:35px; padding-left:10px\">");
            sb.Append("<b>HOTEL VOUCHER</b>");
            sb.Append("</td>");
            sb.Append("<td style=\"border-top: none; border-bottom-width: 6px; border-bottom-color: gray; padding: 10px 0px 0px 3px; margin-bottom:0px;color: #57585A\">");
            sb.Append("<b> Booking Date &nbsp;&nbsp; &nbsp; &nbsp;     :</b><span style=\"color: #757575\">" + ReservationDate + "</span>");
            sb.Append("</td>");
            sb.Append("<td style=\"border-top: none; border-bottom-width: 6px; border-bottom-color: gray; padding: 10px 0px 0px 3px; margin-bottom: 0px;color: #57585A\">");
            sb.Append("<b>Voucher No  &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;     : </b><span style=\"color: #757575\">" + VoucherID + "</span>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td style=\"border-top: none; border-bottom-width: 6px; border-bottom-color: gray; padding: 0px 0px 0px 3px;color: #57585A\">");
            sb.Append("<b> Agent Ref No.&nbsp;&nbsp; &nbsp; &nbsp; :</b> <span style=\"color: #757575\">" + AgentRef + " </span>");
            sb.Append("</td>");
            sb.Append("<td style=\"border-top: none; border-bottom-width: 6px; border-bottom-color: gray; padding: 0px 0px 0px 3px;color: #57585A\">");
            sb.Append("<b>  Supplier Ref No &nbsp;&nbsp; &nbsp; &nbsp; :</b><span style=\"color:#757575\">");
            sb.Append("436-985128");
            sb.Append("</span>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td colspan=\"2\" style=\"height: 25px; border:none\"></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("</div>");

            sb.Append("<div style=\"font-size: 20px; padding-bottom: 10px; padding-top: 8px\">");
            sb.Append("<span style=\"padding-left:10px;color: #57585A\"><b>BOOKING DETAILS</b></span>");
            sb.Append("</div>");
            sb.Append("<div>");
            sb.Append("<table border=\"1\" style=\"margin-top: 3px; width: 100%; height: 250px; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-bottom-width: 3px; border-bottom-color: #E6DCDC; border-left: none; border-right: none\">");
            sb.Append("<tr style=\"border: none\">");
            sb.Append("<td rowspan=\"4\" style=\"width: auto; border: none\" id=\"googleMap\">");
            sb.Append("<img  src='https://maps.googleapis.com/maps/api/staticmap?zoom=16&size=400x250&sensor=false&maptype=roadmap&markers=color:red|" + Latitude + "," + Longitude + "' style='width:auto; height:auto' >");
            sb.Append("</td>");
            sb.Append("<td style=\"height: 45px; background-color: #00AEEF; font-size: 24px; color: white; border: none; width:20%\">");
            sb.Append("<span style=\"margin-left: 15px\"><b>Check In</span>");
            sb.Append("</br>");
            sb.Append("</td>");
            sb.Append("<td colspan=\"2\" style=\"height: 45px; background-color: #00AEEF; font-size: 24px; color: white; border: none; width:47%\">");
            sb.Append(": <span style=\"margin-left: 15px\"><b>" + CheckIn + "</b></span>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr style=\"border: none\">");
            sb.Append("<td style=\"height: 45px; background-color: #27B4E8; font-size: 24px; color: white; border: none\">");
            sb.Append("<span style=\"margin-left: 15px\"><b>Check Out</b></span>");
            sb.Append("</td>");
            sb.Append("<td colspan=\"2\" style=\"height: 45px; background-color: #27B4E8; font-size: 24px; color: white; border: none\">");
            sb.Append(": <span style=\"margin-left: 15px\"><b>" + CheckOut + "</b></span>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr style=\"border: none\">");
            sb.Append("<td style=\"height: 45px; background-color: #35C2F1; font-size: 24px; color: white; border: none\">");
            sb.Append("<span style=\" margin-left: 15px\"><b>Hotel Name</b></span>");
            sb.Append("</td>");
            sb.Append("<td style=\"height: 45px; background-color: #35C2F1; font-size: 22px; color: white; border: none\">");
            sb.Append(": <span style=\" margin-left: 15px\"><b>" + HotelName + "</b></span>");
            sb.Append("</td>");
            sb.Append("<td rowspan=\"2\" align=\"center\" style=\"background-color: #35C2F1; font-size: 22px; color: white; border: none; width:10%\">");
            sb.Append("<br> <br><b>Nights</b>");
            sb.Append("<br>");
            sb.Append("<span style=\"font-size:40px; font-weight:700\">" + Night + "</span>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr style=\"border: none\">");
            sb.Append("<td style=\"border: none;color: #57585A\">");
            sb.Append("<span style=\" margin-left: 15px\"><b>Address</b></span><br>");
            sb.Append("<span style=\" margin-left: 15px\"><b>City</b></span><br>");
            sb.Append("<span style=\" margin-left: 15px\"><b>Country</b></span><br>");
            sb.Append("<span style=\" margin-left: 15px\"><b>Phone</b></span><br>");
            sb.Append("<span style=\" margin-left: 15px\"><b>Postal Code</b></span>");
            sb.Append("</td>");
            sb.Append("<td style=\"border: none;color: #57585A\">");



            sb.Append(": <span style=\" margin-left: 15px\">" + HotelAddress + "</span><br>");
            sb.Append(": <span style=\" margin-left: 15px\">" + HotelCity + "</span><br>");
            sb.Append(": <span style=\" margin-left: 15px\">" + HotelCountry + "</span><br>");
            sb.Append(": <span style=\" margin-left: 15px\">" + HotelPhone + "</span><br>");
            sb.Append(": <span style=\" margin-left: 15px\">" + HotelPostal + "</span>");
            sb.Append("</td>");
            sb.Append("<!--<td colspan=\"2\">Sum: $180</td>-->");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("</div>");


            sb.Append("<div style=\"font-size: 20px; padding-bottom: 10px; padding-top: 8px\">");
            sb.Append("<span style=\"padding-left:10px;color: #57585A\"><b>GUEST & ROOM DETAILS</b></span>");
            sb.Append("</div>");
            sb.Append("<div>");
            sb.Append("<table border=\"1\" style=\"margin-top: 3px; width: 100%; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-bottom-width: 3px; border-bottom-color: #E6DCDC; border-left: none; border-right: none\">");
            for (var i = 0; i < dtBookedRoom.Rows.Count; i++)
            {
                AllPassengers = "";
                for (int p = 0; p < dtBookedPassenger.Rows.Count; p++)
                {
                    string brrn = (dtBookedRoom.Rows[i]["RoomNumber"]).ToString();
                    string bprn = (dtBookedPassenger.Rows[p]["RoomNumber"]).ToString();

                    if ((dtBookedPassenger.Rows[p]["RoomNumber"]).ToString() == (dtBookedRoom.Rows[i]["RoomNumber"]).ToString())
                    {
                        AllPassengers += dtBookedPassenger.Rows[p]["Name"] + " " + dtBookedPassenger.Rows[p]["LastName"] + ", ";

                    }

                }

                AllPassengers = AllPassengers.TrimEnd(' ');
                AllPassengers = AllPassengers.TrimEnd(',');
                string Chid_Age = dtBookedRoom.Rows[i]["ChildAge"].ToString();
                string Remark = dtBookedRoom.Rows[i]["Remark"].ToString();
                if (Chid_Age == "")
                {
                    Chid_Age = "-";
                }
                if (Remark == "")
                {
                    Remark = "-";
                }
                sb.Append("<tr style=\"border-spacing: 0px;\">");
                sb.Append("<td style=\"border-left: none; border-right: none;border-top-width: 3px; border-top-color: white; background-color: #00AEEF; color: white; font-size: 18px;  padding-left:10px; font-weight:700\">" + dtBookedRoom.Rows[i]["RoomNumber"].ToString() + "</td>");
                //sb.Append("<td colspan=\"7\" style=\"border-left: none; border-right: none; border-top-width: 3px; border-top-color: white; height: 35px; background-color: #35C2F1; color: white; font-size: 18px;  padding-left: 10px; font-weight: 700;\">");
                sb.Append("<td colspan=\"7\" style=\"border-left: none; border-right: none; border-top-width: 3px; border-top-color: white; height: 35px; background-color: #f1d135; color: white; font-size: 18px;  padding-left: 10px; font-weight: 700;\">");
                //  sb.Append("Guest Name :&nbsp;&nbsp;&nbsp; <span>" + dtBookedRoom.Rows[i]["LeadingGuest"].ToString() + "</span></td>");
                sb.Append("Guest Name :&nbsp;&nbsp;&nbsp; <span>" + AllPassengers + "</span></td>");
                sb.Append("</tr>");
                sb.Append("<tr style=\"border: none; text-align: center; color: #57585A; font-weight:600\">");
                sb.Append("<td align=\"left\" style=\"border: none; padding-top: 15px; padding: 15px 0px 0px 10px; \">Room Type</td>");
                sb.Append("<td style=\"border: none; padding-top: 15px\"> Board </td>");
                sb.Append("<td style=\"border: none; padding-top: 15px\">Total Rooms</td>");
                sb.Append("<td style=\"border: none; padding-top: 15px\"> Adults </td>");
                sb.Append("<td style=\"border: none; padding-top: 15px\">Child </td>");
                sb.Append("<td style=\"border: none; padding-top: 15px\">Child Age</td>");
                sb.Append("<td align=\"left\" style=\"border: none; padding-top: 15px\">Remark</td>");
                sb.Append("</tr>");
                sb.Append("<tr style=\"color: #B0B0B0; border: none; text-align: center; color: #57585A;\">");
                sb.Append("<td align=\"left\" style=\"border: none; padding:12px 0px 15px 10px;\">" + dtBookedRoom.Rows[i]["RoomType"].ToString() + "</td>");
                sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\">" + dtBookedRoom.Rows[i]["BoardText"].ToString() + "</td>");
                sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\">" + dtBookedRoom.Rows[i]["TotalRooms"].ToString() + "</td>");
                sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\">" + dtBookedRoom.Rows[i]["Adults"].ToString() + "</td>");
                sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\">" + dtBookedRoom.Rows[i]["Child"].ToString() + "</td>");
                sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\">" + Chid_Age + "</td>");
                sb.Append("<td align=\"left\" style=\"border: none; padding-left: 8px; padding-top: 12px; padding-bottom: 15px;\">" + Remark + "</td>");
                sb.Append("</tr>");
            }

            sb.Append("</table>");
            sb.Append("</div>");

            sb.Append("<div style=\"font-size: 20px; padding-bottom: 10px; padding-top: 8px\">");
            sb.Append("<span style=\"padding-left:10px;color: #57585A\"><b>Terms & Conditions</b></span>");
            sb.Append("</div>");
            sb.Append("<hr style=\"border-top-width: 3px\">");
            sb.Append("<div style=\"height:auto; color: #57585A; font-size:small\">");
            sb.Append("<ul style=\"list-style-type: disc\">");
            sb.Append("<li>Please collect all extras directly from clients prior to departure</li>");
            sb.Append("<li>");
            sb.Append("It is mandatory to present valid Passport at the time of check-in for International");
            sb.Append("hotel booking & any valid photo ID for domestic hotel booking");
            sb.Append("</li>");
            sb.Append("<li>");
            sb.Append("Hotel holds right to reject check in or charge supplement due to wrongly selection");
            sb.Append("of Nationality or Residency at the time of booking");
            sb.Append("</li>");
            sb.Append("<li>");
            sb.Append("Early check-in & late check-out will be subject to availability and approval by");
            sb.Append("respective hotel only");
            sb.Append("</li>");
            sb.Append("<li>");
            sb.Append("Kindly inform hotel in advance for any late check-in, as hotel may automatically");
            sb.Append("cancel the room in not check-in by 18:00Hrs local time");
            sb.Append("</li>");
            sb.Append("<li>");
            sb.Append(" You may need to deposit security amount at some destination as per the local rule");
            sb.Append("and regulations at the time of check-in");
            sb.Append("</li>");
            sb.Append("<li>");
            sb.Append(" Tourism fees may apply at the time of check-in at some destinations like Dubai,Paris, etc...");

            sb.Append("</li>");
            sb.Append("<li>General check-in time is 14:00Hrs & check-out time is 12:00Hrs</li>");
            sb.Append("</ul>");
            sb.Append("</div>");
            sb.Append("<br>");

            sb.Append("<div style=\"background-color: #00AEEF; text-align: center; font-size: 20px; color: white\">");
            sb.Append("<span>");
            sb.Append("Check your booking details carefully and inform us immediately before its too late...");
            sb.Append("</span>");
            sb.Append("</div><br>");
            sb.Append("</body>");
            sb.Append("</html>");
            return sb.ToString();

        }




        private static string[] ones = {
    "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", 
    "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen",
};

        private static string[] tens = { "Zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

        private static string[] thous = { "Hundred,", "Thousand,", "Million,", "Billion,", "Trillion,", "Quadrillion," };

        public static string ToWords(decimal number, string CurrencyName)
        {
            if (number < 0)
                return "negative " + ToWords(Math.Abs(number), CurrencyName);

            int intPortion = (int)number;
            int decPortion = (int)((number - intPortion) * (decimal)100);

            if (CurrencyName == "Currency-AED")
            {
                return string.Format("{0} AED and {1} Fils", ToWords(intPortion), ToWords(decPortion));
            }
            else if (CurrencyName == "Currency-SAR")
            {
                return string.Format("{0} SAR and {1} Halalah", ToWords(intPortion), ToWords(decPortion));
            }
            else if (CurrencyName == "fa fa-eur")
            {
                return string.Format("{0} Euro and {1} 	Cent", ToWords(intPortion), ToWords(decPortion));
            }
            //else if (CurrencyName == "fa fa-gbp")
            //{
            //    return string.Format("{0} Great Britain Pounds and {1} Penny", ToWords(intPortion), ToWords(decPortion));
            //}
            else if (CurrencyName == "fa fa-gbp")
            {
                return string.Format("{0} Ringgit and {1} sen", ToWords(intPortion), ToWords(decPortion));
            }
            else if (CurrencyName == "fa fa-dollar")
            {
                return string.Format("{0} Dollar and {1} Cent", ToWords(intPortion), ToWords(decPortion));
            }
            else
                return string.Format("{0} Rupees and {1} Paise", ToWords(intPortion), ToWords(decPortion));
            //return string.Format("{0} Rupees and {1} Paise", ToWords(intPortion), ToWords(decPortion));        //orig line without any conditions
        }

        private static string ToWords(int number, string appendScale = "")
        {
            string numString = "";
            if (number < 100)
            {
                if (number < 20)
                    numString = ones[number];
                else
                {
                    numString = tens[number / 10];
                    if ((number % 10) > 0)
                        numString += "-" + ones[number % 10];
                }
            }
            else
            {
                int pow = 0;
                string powStr = "";

                if (number < 1000) // number is between 100 and 1000
                {
                    pow = 100;
                    powStr = thous[0];
                }
                else // find the scale of the number
                {
                    int log = (int)Math.Log(number, 1000);
                    pow = (int)Math.Pow(1000, log);
                    powStr = thous[log];
                }

                numString = string.Format("{0} {1}", ToWords(number / pow, powStr), ToWords(number % pow)).Trim();
            }

            return string.Format("{0} {1}", numString, appendScale).Trim();
        }

        public static string ToWords(decimal number)
        {
            if (number < 0)
                return "negative " + ToWords(Math.Abs(number));

            int intPortion = (int)number;
            int decPortion = (int)((number - intPortion) * (decimal)100);


            return string.Format("{0} Rupees and {1} Paise", ToWords(intPortion), ToWords(decPortion));
            //return string.Format("{0} Rupees and {1} Paise", ToWords(intPortion), ToWords(decPortion));        //orig line without any conditions
        }
    }
}