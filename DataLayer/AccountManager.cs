﻿using CUTUK.BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;

namespace CUTUK.DataLayer
{
    public class AccountManager
    {
        public static DBHelper.DBReturnCode AddDeposit(string BankName, string AccountNumber, string AmountDeposit, string Remarks, string TypeOfDeposit, string TypeOfCash, string TypeOfCheque, string EmployeeName, string ReceiptNumber, string ChequeDDNumber, string ChequeDrawn, string Date)
        {
            int rowsAffected = 0;

            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 uId = objGlobalDefaults.sid;
            string Franchisee = objGlobalDefaults.Franchisee;
            string DepositDate = Date;
            decimal dlAmountDeposit;
            decimal.TryParse(AmountDeposit, out dlAmountDeposit);
            string sCreationdate = DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture);


            SqlParameter[] SQLParams = new SqlParameter[24];
            SQLParams[0] = new SqlParameter("@uId", uId);
            SQLParams[1] = new SqlParameter("@DepositAmount", dlAmountDeposit);
            SQLParams[2] = new SqlParameter("@BankName", BankName);
            SQLParams[3] = new SqlParameter("@AccountNumber", AccountNumber);
            SQLParams[4] = new SqlParameter("@ChequeDetail", "");
            SQLParams[5] = new SqlParameter("@CreationDate", sCreationdate);
            SQLParams[6] = new SqlParameter("@Remarks", Remarks);
            SQLParams[7] = new SqlParameter("@DepositUpdateFlag", 0);
            SQLParams[8] = new SqlParameter("@CreditedAmount", 000);
            SQLParams[9] = new SqlParameter("@Updatedate", "");
            SQLParams[10] = new SqlParameter("@BankNarration", "");
            SQLParams[11] = new SqlParameter("@ExecutiveName", "");
            SQLParams[12] = new SqlParameter("@DepositDate", DepositDate);
            SQLParams[13] = new SqlParameter("@Typeofdeposit", TypeOfDeposit);
            SQLParams[14] = new SqlParameter("@Typeofcash", TypeOfCash);
            SQLParams[15] = new SqlParameter("@Typeofcheque", TypeOfCheque);
            SQLParams[16] = new SqlParameter("@EmployeeName", EmployeeName);
            SQLParams[17] = new SqlParameter("@ReceiptNumber", ReceiptNumber);
            SQLParams[18] = new SqlParameter("@TransactionId", ReceiptNumber);
            SQLParams[19] = new SqlParameter("@ChequeorDDNumber", ChequeDDNumber);
            SQLParams[20] = new SqlParameter("@ChequeDrawn", ChequeDrawn);
            SQLParams[21] = new SqlParameter("@ExecutiveRemarks", "");
            SQLParams[22] = new SqlParameter("@Mobile", "N/A");
            SQLParams[23] = new SqlParameter("@Franchisee", Franchisee);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_tbl_BankDepositAddByFranchisee", out rowsAffected, SQLParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode CreditInformation(out DataSet ds)
        {
            //ds = new DataSet();
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 uId = objGlobalDefaults.sid;
            SqlParameter[] SQLParams = new SqlParameter[1];
            SQLParams[0] = new SqlParameter("@uid", uId);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("Proc_CreditInformationLoad", out ds, SQLParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetMarkupGroup(out DataTable ds)
        {
            //ds = new DataSet();
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 uId = objGlobalDefaults.sid;
            SqlParameter[] SQLParams = new SqlParameter[1];
            SQLParams[0] = new SqlParameter("@uid", uId);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_MarkUpVisaGroupDetailsLoadByAgent", out ds, SQLParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetAvailableCredit(out DataTable dt)
        {
            //ds = new DataSet();
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 uId = 0;
            //if (objGlobalDefaults.UserType == "Admin")
            //{
            //    AgentDetailsOnAdmin objAgentDetailsOnAdmin = (AgentDetailsOnAdmin)HttpContext.Current.Session["AgentDetailsOnAdmin"];
            //    uId = objAgentDetailsOnAdmin.sid;
            //}
            if (objGlobalDefaults.UserType == "Agent")
            {
                uId = objGlobalDefaults.sid;
            }
            else if (objGlobalDefaults.UserType == "AgentStaff")
            {
                uId = objGlobalDefaults.sid;
            }
            SqlParameter[] SQLParams = new SqlParameter[1];
            SQLParams[0] = new SqlParameter("@uid", uId);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_GetAvailableCredit", out dt, SQLParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetFranchiseeOnAgent(out DataTable dt)
        {
            //ds = new DataSet();
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 uId = 0;
            //if (objGlobalDefaults.UserType == "Admin")
            //{
            //    AgentDetailsOnAdmin objAgentDetailsOnAdmin = (AgentDetailsOnAdmin)HttpContext.Current.Session["AgentDetailsOnAdmin"];
            //    uId = objAgentDetailsOnAdmin.sid;
            //}
            //else
                uId = objGlobalDefaults.sid;
            SqlParameter[] SQLParams = new SqlParameter[1];
            SQLParams[0] = new SqlParameter("@uid", uId);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_AdminLoginByFranchiseeContact", out dt, SQLParams);
            return retCode;
        }
        public static DateTime ConvertDateTime(string Date)
        {
            DateTime date = new DateTime();
            try
            {
                string CurrentPattern = Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;
                string[] Split = new string[] { "-", "/", @"\", "." };
                string[] Patternvalue = CurrentPattern.Split(Split, StringSplitOptions.None);
                string[] DateSplit = Date.Split(Split, StringSplitOptions.None);
                string NewDate = "";
                if (Patternvalue[0].ToLower().Contains("d") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[0] + "/" + DateSplit[1] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("m") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[0] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("d") == true)
                {
                    NewDate = DateSplit[2] + "/" + DateSplit[1] + "/" + DateSplit[0];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("m") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[2] + "/" + DateSplit[0];
                }
                date = DateTime.Parse(NewDate, Thread.CurrentThread.CurrentCulture);
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }

            return date;

        }
        public static DBHelper.DBReturnCode GetCreditInformationBySearch(string Type, string sFromdt, string sTodt, string RefNo, string ApproveDate, out DataTable dtResult)
        {


            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 uId = objGlobalDefaults.sid;
            if (ApproveDate != "")
            {
                string[] AppArray = ApproveDate.Split('-');
                if (Convert.ToInt16(AppArray[1]) < 10 || Convert.ToInt16(AppArray[0]) < 10)
                {
                    AppArray[0] = "0" + AppArray[0];
                    AppArray[1] = "0" + AppArray[1];
                }
                ApproveDate = AppArray[0] + "/" + AppArray[1] + "/" + AppArray[2];
            }
            SqlParameter[] SQLParams = new SqlParameter[6];
            SQLParams[0] = new SqlParameter("@uId", uId);
            SQLParams[1] = new SqlParameter("@Type", Type);
            SQLParams[2] = new SqlParameter("@From", sFromdt);
            SQLParams[3] = new SqlParameter("@To", sTodt);
            SQLParams[4] = new SqlParameter("@RefNo", RefNo);
            SQLParams[5] = new SqlParameter("@ApproveDate", ApproveDate);

            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_GetCreditInformationBySearch", out dtResult, SQLParams);
            return retCode;
        }

    }
}