﻿using CUT.BL;
using CUT.DataLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CUTUK.DataLayer
{
    public class MarkupTaxManager
    {
        public static DBHelper.DBReturnCode AssignMarkupTax()
        {
            DataSet ds = new DataSet();
            GlobalDefault objGlobalDefaults = new GlobalDefault();
            DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("Proc_Get_MarkpDetails", out ds);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                try
                {
                    MarkupsAndTaxes objMarkupsAndTaxes = new MarkupsAndTaxes();
                    DataTable dtIndividualMarkup, dtGroupMarkup, dtGlobalMarkup, dtAgentOwnMarkp, dtServiceTax, dtTransIndividualMarkup, dtTransGroupMarkup, dtTransGlobalMarkup, dtTransAgentOwnMarkp, dtExchangeRate;
                    dtGlobalMarkup = ds.Tables[0];
                    dtGroupMarkup = ds.Tables[1];
                    //dtIndividualMarkup = new DataTable();
                    dtIndividualMarkup = ds.Tables[4];


                    dtExchangeRate = ds.Tables[3];
                    //dtAgentOwnMarkp = ds.Tables[3];
                    dtServiceTax = ds.Tables[2];
                    List<IndividualMarkup> listIndividualMarkup = new List<IndividualMarkup>();
                    List<GroupMarkup> listGroupMarkup = new List<GroupMarkup>();
                    List<GlobalMarkup> listGlobalMarkup = new List<GlobalMarkup>();
                    List<AgentMarkup> listAgentMarkup = new List<AgentMarkup>();

                    if (dtIndividualMarkup.Rows.Count != 0)
                    {
                        //List<IndividualMarkup> listIndividualMarkup = new List<IndividualMarkup>();
                        for (int i = 0; i < dtIndividualMarkup.Rows.Count; i++)
                        {
                            IndividualMarkup objIndividualMarkup = new IndividualMarkup();
                            objIndividualMarkup.Supplier = Convert.ToString(dtIndividualMarkup.Rows[i]["Supplier"]);
                            objIndividualMarkup.IndMarkupPer = Convert.ToSingle(dtIndividualMarkup.Rows[i]["MarkupPercentage"]);
                            objIndividualMarkup.IndMarkAmt = Convert.ToSingle(dtIndividualMarkup.Rows[i]["MarkupAmmount"]);
                            objIndividualMarkup.IndCommPer = Convert.ToSingle(dtIndividualMarkup.Rows[i]["CommessionPercentage"]);
                            objIndividualMarkup.IndCommAmt = Convert.ToSingle(dtIndividualMarkup.Rows[i]["CommessionAmmount"]);
                            listIndividualMarkup.Add(objIndividualMarkup);
                        }
                        //objMarkupsAndTaxes.listIndividualMarkup = listIndividualMarkup;

                    }
                    if (dtGroupMarkup.Rows.Count != 0)
                    {
                        //List<GroupMarkup> listGroupMarkup = new List<GroupMarkup>();
                        for (int i = 0; i < dtGroupMarkup.Rows.Count; i++)
                        {
                            GroupMarkup objGroupMarkup = new GroupMarkup();
                            objGroupMarkup.Supplier = dtGroupMarkup.Rows[i]["Supplier"].ToString();
                            objGroupMarkup.GroupMarkupPer = Convert.ToSingle(dtGroupMarkup.Rows[i]["MarkupPercentage"]);
                            objGroupMarkup.GroupMarkAmt = Convert.ToSingle(dtGroupMarkup.Rows[i]["MarkupAmmount"]);
                            objGroupMarkup.GroupCommPer = Convert.ToSingle(dtGroupMarkup.Rows[i]["CommessionPercentage"]);
                            objGroupMarkup.GroupCommAmt = Convert.ToSingle(dtGroupMarkup.Rows[i]["CommessionAmmount"]);
                            objGroupMarkup.TaxOnMarkup = Convert.ToBoolean(dtGroupMarkup.Rows[i]["TaxApplicable"]);
                            listGroupMarkup.Add(objGroupMarkup);
                        }
                        //objMarkupsAndTaxes.listGroupMarkup = listGroupMarkup;
                    }
                    if (dtGlobalMarkup.Rows.Count != 0)
                    {
                        //List<GlobalMarkup> listGlobalMarkup = new List<GlobalMarkup>();
                        for (int i = 0; i < dtGlobalMarkup.Rows.Count; i++)
                        {
                            GlobalMarkup objGlobalMarkup = new GlobalMarkup();
                            objGlobalMarkup.Supplier = dtGlobalMarkup.Rows[i]["Supplier"].ToString();
                            objGlobalMarkup.GlobalMarkupPer = Convert.ToSingle(dtGlobalMarkup.Rows[i]["MarkupPercentage"]);
                            objGlobalMarkup.GlobalMarkAmt = Convert.ToSingle(dtGlobalMarkup.Rows[i]["MarkupAmmount"]);
                            objGlobalMarkup.GlobalCommPer = Convert.ToSingle(dtGlobalMarkup.Rows[i]["CommessionPercentage"]);
                            objGlobalMarkup.GlobalCommAmt = Convert.ToSingle(dtGlobalMarkup.Rows[i]["CommessionAmmount"]);
                            listGlobalMarkup.Add(objGlobalMarkup);
                        }
                        //objMarkupsAndTaxes.listGlobalMarkup = listGlobalMarkup;
                    }
                    if (dtServiceTax.Rows.Count != 0)
                    {
                        objMarkupsAndTaxes.PerServiceTax = Convert.ToSingle(dtServiceTax.Rows[0]["ServiceTax"]);
                        objMarkupsAndTaxes.TimeGap = Convert.ToInt32(dtServiceTax.Rows[0]["TimeGap"]);
                    }
                    if (dtExchangeRate.Rows.Count != 0)
                    {
                        List<CUT.DataLayer.ExchangeRate> listExchangeRate = new List<CUT.DataLayer.ExchangeRate>();
                        for (int i = 0; i < dtExchangeRate.Rows.Count; i++)
                        {

                            CUT.DataLayer.ExchangeRate objExchangeRate = new CUT.DataLayer.ExchangeRate();
                            objExchangeRate.Currency = Convert.ToString(dtExchangeRate.Rows[i]["Currency"]);
                            objExchangeRate.Rate = Convert.ToSingle(dtExchangeRate.Rows[i]["INR"]);
                            objExchangeRate.LastUpdateDt = Convert.ToString(dtExchangeRate.Rows[i]["LastUpdateDt"]);
                            objExchangeRate.MailFlag = Convert.ToBoolean(dtExchangeRate.Rows[i]["MailFlag"]);
                            objExchangeRate.Exchange = Convert.ToSingle(dtExchangeRate.Rows[i]["ExchangeRate"]);
                            listExchangeRate.Add(objExchangeRate);
                        }
                        objGlobalDefaults.sid = Convert.ToInt64(ConfigurationManager.AppSettings["AdminKey"]);
                        objGlobalDefaults.ParentId = 232;
                        objGlobalDefaults.UserType = "Agent";
                        objGlobalDefaults.Currency = ConfigurationManager.AppSettings["Currency"];
                        HttpContext.Current.Session["CurrencyClass"] = ConfigurationManager.AppSettings["CurrencyClass"];
                        objGlobalDefaults.ExchangeRate = listExchangeRate;
                    }
                    if (dtIndividualMarkup.Rows.Count != 0)
                    {
                        objMarkupsAndTaxes.listIndividualMarkup = listIndividualMarkup;
                    }
                    if (dtGroupMarkup.Rows.Count != 0)
                    {
                        objMarkupsAndTaxes.listGroupMarkup = listGroupMarkup;
                    }
                    if (dtGlobalMarkup.Rows.Count != 0)
                    {
                        objMarkupsAndTaxes.listGlobalMarkup = listGlobalMarkup;
                    }
                    HttpContext.Current.Session["LoginUser"] = objGlobalDefaults;
                    HttpContext.Current.Session["markups"] = objMarkupsAndTaxes;
                    CUT.DataLayer.MarkupTaxManager.GetMarkupTax(objGlobalDefaults.ParentId, objGlobalDefaults.sid, "Hotel");
                    CUT.DataLayer.DefaultManager.GetAgentFormList();
                }
                catch
                {
                    retCode = DBHelper.DBReturnCode.EXCEPTION;
                }


            }
            return retCode;
        }
    }
}