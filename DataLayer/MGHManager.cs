﻿using CUTUK.BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CUTUK.DataLayer
{
    public class MGHManager
    {
        //............................................old start.........................................................//
        public static DBHelper.DBReturnCode GetLocationCode(string name, string lang, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@name", name);
            //sqlParams[1] = new SqlParameter("@LanguageCode", lang);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_mgh_GetDestination", out dtResult, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetCategory(string id, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@categoryId", id);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("GetMGHCategory", out dtResult, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetFacility(string id, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@facilityId", id);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("GetMGHFacility", out dtResult, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetMGHHotelDetail(string LangCode, List<String> List_HotelCode, out DataSet ds)
        {
            ds = null;
            DataTable dtHotel = new DataTable();
            DataColumn dcHotel = new DataColumn("HotelCode", typeof(string));
            dtHotel.Columns.Add(dcHotel);
            foreach (string data in List_HotelCode)
            {
                DataRow dr = dtHotel.NewRow();
                dr[0] = data;
                dtHotel.Rows.Add(dr);
            }
            SqlParameter[] sqlparam = new SqlParameter[2];
            sqlparam[0] = new SqlParameter("@LangCode", LangCode);
            sqlparam[1] = new SqlParameter("@HotelCode", SqlDbType.Structured);
            sqlparam[1].Value = dtHotel;
            return DBHelper.GetDataSet("usp_GetHotelDetail", out ds, sqlparam);
        }
        //............................................old end......................................................//

        public static DBHelper.DBReturnCode GetCategoryById(int id, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@categoryId", id);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("usp_MGHGetCategoryId", out dtResult, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetFacility(out DataTable dtResult)
        {
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("usp_MGHGetFacilities", out dtResult);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetLocationById(int nLocationId, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@Id", nLocationId);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("usp_MGHGetLocationById", out dtResult, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetCategoryName(int id, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@categoryId", id);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("usp_MGHGetRoomCategoryId", out dtResult, sqlParams);
            // DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("usp_MGHGetCategoryNameById", out dtResult, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetTypeName(int id, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@RoomTypeId", id);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("usp_MGHGetRoomTypeNameById", out dtResult, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetRoomFacilityNameById(Int64 id, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@id", id);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("usp_MGHGetRoomFacilityNameById", out dtResult, sqlParams);
            return retCode;
        }
    }
}