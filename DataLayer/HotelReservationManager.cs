﻿using CUTUK.BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace CUTUK.DataLayer
{
    public class HotelReservationManager
    {
        public static DBHelper.DBReturnCode HotelReservationAddUpdate(Int64 Sid, string HotelCode, string HotelName, string City, string CheckIn, string CheckOut, int NoOfNights, int TotalRooms, float TotalFare, string CurrencyCode, int NoOfAdults, int Children, DateTime ChargeDate, string HotelDetails, DateTime DeadLine, out string AffilateCode, string ReservationStatus, string AgencyRefernce, string LeadingGuestName, string ComparedCurrency, float CurrencyFare, string AgentContactNumber, string AgentEmail, string AgentRemark)
        {
            int rowsAffected = 0;

            CUTUK.DataLayer.MarkupsAndTaxes objMarkupsAndTaxes = new MarkupsAndTaxes();
            if (HttpContext.Current.Session["markups"] != null)
                objMarkupsAndTaxes = (CUTUK.DataLayer.MarkupsAndTaxes)HttpContext.Current.Session["markups"];
            else if (HttpContext.Current.Session["AgentMarkupsOnAdmin"] != null)
                objMarkupsAndTaxes = (CUTUK.DataLayer.MarkupsAndTaxes)HttpContext.Current.Session["AgentMarkupsOnAdmin"];

            Int64 ParentID = 0;
            GlobalDefault objGlobalDefault = new GlobalDefault();
            objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            ParentID = objGlobalDefault.sid;

            string ReservationId = "";
            string GTAId = "";
            //int NoOfDays = 0;
            decimal SalesTax = 0;
            float Servicecharge = (float)((TotalFare * objMarkupsAndTaxes.PerServiceTax) / 100);
            decimal LuxuryTax = 0;
            float Discount = 0;
            int infants = 0;
            int extrabed = 0;
            bool CancelFlag = false;
            string CancelDate = "";
            string remarks = "";
            //string Status = "";
            float AgentMarkUp_Per = objMarkupsAndTaxes.PerAgentMarkup;
            string BookingStatus = "";
            string Source = "HotelBeds";
            decimal ExchangeValue = 0;
            string ChildAges = "";
            string HotelBookingData = "";
            string AgencyName = objGlobalDefault.AgencyName;
            string terms = "";
            //string bookingname = objGlobalDefault.ContactPerson;
            int holdbooking = 0;
            bool deadlineemail = false;
            decimal sightseeing = 0;
            string mealplan = "";
            decimal mealplan_Amt = 0;
            AffilateCode = "HTL-AE-" + GenerateRandomString(5);
            string RefAgency = "";
            string AgentRef = objGlobalDefault.Agentuniquecode;
            string Type = "b2b";
            string VoucherID = "VCH-" + GenerateRandomNumber();
            string InvoiceID = "INV-" + GenerateRandomNumber();
            SqlParameter[] sqlParams = new SqlParameter[58];
            sqlParams[0] = new SqlParameter("@Sid", Sid);
            sqlParams[1] = new SqlParameter("@ReservationID", ReservationId);
            sqlParams[2] = new SqlParameter("@GTAId", GTAId);
            sqlParams[3] = new SqlParameter("@Uid", ParentID);
            sqlParams[4] = new SqlParameter("@HotelCode", HotelCode);
            sqlParams[5] = new SqlParameter("@City", City);
            sqlParams[6] = new SqlParameter("@RoomCode", "");
            sqlParams[7] = new SqlParameter("@CheckIn", CheckIn);
            sqlParams[8] = new SqlParameter("@CheckOut", CheckOut);
            sqlParams[9] = new SqlParameter("@NoOfDays", NoOfNights);
            sqlParams[10] = new SqlParameter("@TotalRooms", TotalRooms);
            sqlParams[11] = new SqlParameter("@TotalFare", TotalFare);
            sqlParams[12] = new SqlParameter("@RoomRate", 0.0);
            sqlParams[13] = new SqlParameter("@SupplierCurrency", CurrencyCode);
            sqlParams[14] = new SqlParameter("@SalesTax", SalesTax);
            sqlParams[15] = new SqlParameter("@Servicecharge", Servicecharge);
            sqlParams[16] = new SqlParameter("@LuxuryTax", LuxuryTax);
            sqlParams[17] = new SqlParameter("@Discount", Discount);
            sqlParams[18] = new SqlParameter("@NoOfAdults", NoOfAdults);
            sqlParams[19] = new SqlParameter("@Children", Children);
            sqlParams[20] = new SqlParameter("@Infants", infants);
            sqlParams[21] = new SqlParameter("@ExtraBed", extrabed);
            sqlParams[22] = new SqlParameter("@CancelFlag", CancelFlag);
            sqlParams[23] = new SqlParameter("@CancelDate", CancelDate);
            sqlParams[24] = new SqlParameter("@Remarks", remarks);
            sqlParams[25] = new SqlParameter("@ReservationDate", DateTime.Now.ToString("dd/MM/yyy", CultureInfo.CurrentCulture));
            sqlParams[26] = new SqlParameter("@ReservationTime", DateTime.Now.ToString("HH-mm"));
            sqlParams[27] = new SqlParameter("@Status", ReservationStatus);
            sqlParams[28] = new SqlParameter("@AgentMarkUp_Per", AgentMarkUp_Per);
            sqlParams[29] = new SqlParameter("@BookingStatus", BookingStatus);
            sqlParams[30] = new SqlParameter("@Source", Source);
            sqlParams[31] = new SqlParameter("@ExchangeValue", ExchangeValue);
            sqlParams[32] = new SqlParameter("@Updateid", ParentID);
            sqlParams[33] = new SqlParameter("@Updatedate", DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture));
            sqlParams[34] = new SqlParameter("@HotelDetails", "");
            sqlParams[35] = new SqlParameter("@ChildAges", ChildAges);
            sqlParams[36] = new SqlParameter("@HotelBookingData", HotelBookingData);
            sqlParams[37] = new SqlParameter("@AgencyName", AgencyName);
            sqlParams[38] = new SqlParameter("@terms", terms);
            sqlParams[39] = new SqlParameter("@bookingname", LeadingGuestName);
            sqlParams[40] = new SqlParameter("@DeadLine", DeadLine.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture));
            sqlParams[41] = new SqlParameter("@holdbooking", holdbooking);
            sqlParams[42] = new SqlParameter("@deadlineemail", deadlineemail);
            sqlParams[43] = new SqlParameter("@sightseeing", sightseeing);
            sqlParams[44] = new SqlParameter("@mealplan", mealplan);
            sqlParams[45] = new SqlParameter("@mealplan_Amt", mealplan_Amt);
            sqlParams[46] = new SqlParameter("@AffilateCode", AffilateCode);
            sqlParams[47] = new SqlParameter("@Type", Type);
            sqlParams[48] = new SqlParameter("@RefAgency", AgencyRefernce);
            sqlParams[49] = new SqlParameter("@AgentRef", AgentRef);
            sqlParams[50] = new SqlParameter("@HotelName", HotelName);
            sqlParams[51] = new SqlParameter("@VoucherID", VoucherID);
            sqlParams[52] = new SqlParameter("@InvoiceID", InvoiceID);
            sqlParams[53] = new SqlParameter("@ComparedCurrency", ComparedCurrency);
            sqlParams[54] = new SqlParameter("@ComparedFare", CurrencyFare);
            sqlParams[55] = new SqlParameter("@AgentContactNumber", AgentContactNumber);
            sqlParams[56] = new SqlParameter("@AgentEmail", AgentEmail);
            sqlParams[57] = new SqlParameter("@AgentRemark", AgentRemark);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("usp_Proc_tbl_HotelReservationAddUpdate", out rowsAffected, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode HotelMGHAdd(string ReservationID, string HotelCode, string HotelName, string Address, string PostalCode, string Description, string ContactPerson, string ContactEmail, string ContactMobile, string ContactPhone, string LatitudeMGH, string LongitudeMGH, string Country, string City)
        {
            int rowsAffected = 0;
            SqlParameter[] sqlParams = new SqlParameter[14];
            sqlParams[0] = new SqlParameter("@ReservationID", ReservationID);
            sqlParams[1] = new SqlParameter("@HotelCode", HotelCode);
            sqlParams[2] = new SqlParameter("@HotelName", HotelName);
            sqlParams[3] = new SqlParameter("@Address", Address);
            sqlParams[4] = new SqlParameter("@PostalCode", PostalCode);
            sqlParams[5] = new SqlParameter("@Description", Description);
            sqlParams[6] = new SqlParameter("@ContactPerson", ContactPerson);
            sqlParams[7] = new SqlParameter("@ContactEmail", ContactEmail);
            sqlParams[8] = new SqlParameter("@ContactMobile", ContactMobile);
            sqlParams[9] = new SqlParameter("@ContactPhone", ContactPhone);
            sqlParams[10] = new SqlParameter("@LatitudeMGH", LatitudeMGH);
            sqlParams[11] = new SqlParameter("@LongitudeMGH", LongitudeMGH);
            sqlParams[12] = new SqlParameter("@Country", Country);
            sqlParams[13] = new SqlParameter("@City", City);

            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_HotelAddMGH", out rowsAffected, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode HotelReservationAddUpdateMGH(Int64 Sid, string HotelCode, string HotelName, string City, string CheckIn, string CheckOut, int NoOfNights, int TotalRooms, float TotalFare, string CurrencyCode, int NoOfAdults, int Children, string ChargeDate, string HotelDetails, string DeadLine, out string AffilateCode, string ReservationStatus, string AgencyRefernce, string LeadingGuestName, string ComparedCurrency, float CurrencyFare, string AgentContactNumber, string AgentEmail, string AgentRemark, string Latitude, string Longitude,out string VoucherID)
        {
            int rowsAffected = 0;

            CUTUK.DataLayer.MarkupsAndTaxes objMarkupsAndTaxes = new MarkupsAndTaxes();
            if (HttpContext.Current.Session["markups"] != null)
                objMarkupsAndTaxes = (CUTUK.DataLayer.MarkupsAndTaxes)HttpContext.Current.Session["markups"];
            else if (HttpContext.Current.Session["AgentMarkupsOnAdmin"] != null)
                objMarkupsAndTaxes = (CUTUK.DataLayer.MarkupsAndTaxes)HttpContext.Current.Session["AgentMarkupsOnAdmin"];

            Int64 ParentID = 0;
            GlobalDefault objGlobalDefault = new GlobalDefault();

            objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            ParentID = objGlobalDefault.sid;

            string ReservationId = "";
            string GTAId = "";
            //int NoOfDays = 0;
            decimal SalesTax = 0;
            float Servicecharge = (float)((TotalFare * objMarkupsAndTaxes.PerServiceTax) / 100);
            decimal LuxuryTax = 0;
            float Discount = 0;
            int infants = 0;
            int extrabed = 0;
            bool CancelFlag = false;
            string CancelDate = "";
            string remarks = "";
            //string Status = "";
            float AgentMarkUp_Per = objMarkupsAndTaxes.PerAgentMarkup;
            string BookingStatus = "";
            string Source = "HotelBeds";
            decimal ExchangeValue = 0;
            string ChildAges = "";
            string HotelBookingData = "";
            string AgencyName = objGlobalDefault.AgencyName;
            string terms = "";
            //string bookingname = objGlobalDefault.ContactPerson;
            int holdbooking = 0;
            bool deadlineemail = false;
            decimal sightseeing = 0;
            string mealplan = "";
            decimal mealplan_Amt = 0;
            AffilateCode = "HTL-AE-" + GenerateRandomString(5);
            string RefAgency = "";
            string AgentRef = objGlobalDefault.Agentuniquecode;
            string Type = "b2b";
            VoucherID = "VCH-" + GenerateRandomNumber();
            string InvoiceID = "INV-" + GenerateRandomNumber();
            SqlParameter[] sqlParams = new SqlParameter[60];
            sqlParams[0] = new SqlParameter("@Sid", Sid);
            sqlParams[1] = new SqlParameter("@ReservationID", ReservationId);
            sqlParams[2] = new SqlParameter("@GTAId", GTAId);
            sqlParams[3] = new SqlParameter("@Uid", ParentID);
            sqlParams[4] = new SqlParameter("@HotelCode", HotelCode);
            sqlParams[5] = new SqlParameter("@City", City);
            sqlParams[6] = new SqlParameter("@RoomCode", "");
            sqlParams[7] = new SqlParameter("@CheckIn", CheckIn);
            sqlParams[8] = new SqlParameter("@CheckOut", CheckOut);
            sqlParams[9] = new SqlParameter("@NoOfDays", NoOfNights);
            sqlParams[10] = new SqlParameter("@TotalRooms", TotalRooms);
            sqlParams[11] = new SqlParameter("@TotalFare", TotalFare);
            sqlParams[12] = new SqlParameter("@RoomRate", 0.0);
            sqlParams[13] = new SqlParameter("@SupplierCurrency", CurrencyCode);
            sqlParams[14] = new SqlParameter("@SalesTax", SalesTax);
            sqlParams[15] = new SqlParameter("@Servicecharge", Servicecharge);
            sqlParams[16] = new SqlParameter("@LuxuryTax", LuxuryTax);
            sqlParams[17] = new SqlParameter("@Discount", Discount);
            sqlParams[18] = new SqlParameter("@NoOfAdults", NoOfAdults);
            sqlParams[19] = new SqlParameter("@Children", Children);
            sqlParams[20] = new SqlParameter("@Infants", infants);
            sqlParams[21] = new SqlParameter("@ExtraBed", extrabed);
            sqlParams[22] = new SqlParameter("@CancelFlag", CancelFlag);
            sqlParams[23] = new SqlParameter("@CancelDate", CancelDate);
            sqlParams[24] = new SqlParameter("@Remarks", remarks);
            sqlParams[25] = new SqlParameter("@ReservationDate", DateTime.Now.ToString("dd/MM/yyy", CultureInfo.CurrentCulture));
            sqlParams[26] = new SqlParameter("@ReservationTime", DateTime.Now.ToString("HH-mm"));
            sqlParams[27] = new SqlParameter("@Status", ReservationStatus);
            sqlParams[28] = new SqlParameter("@AgentMarkUp_Per", AgentMarkUp_Per);
            sqlParams[29] = new SqlParameter("@BookingStatus", BookingStatus);
            sqlParams[30] = new SqlParameter("@Source", Source = "MGH");
            sqlParams[31] = new SqlParameter("@ExchangeValue", ExchangeValue);
            sqlParams[32] = new SqlParameter("@Updateid", ParentID);
            sqlParams[33] = new SqlParameter("@Updatedate", DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture));
            sqlParams[34] = new SqlParameter("@HotelDetails", "");
            sqlParams[35] = new SqlParameter("@ChildAges", ChildAges);
            sqlParams[36] = new SqlParameter("@HotelBookingData", HotelBookingData);
            sqlParams[37] = new SqlParameter("@AgencyName", AgencyName);
            sqlParams[38] = new SqlParameter("@terms", terms);
            sqlParams[39] = new SqlParameter("@bookingname", LeadingGuestName);
            sqlParams[40] = new SqlParameter("@DeadLine", DeadLine);
            // sqlParams[40] = new SqlParameter("@DeadLine", DeadLine.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture));
            sqlParams[41] = new SqlParameter("@holdbooking", holdbooking);
            sqlParams[42] = new SqlParameter("@deadlineemail", deadlineemail);
            sqlParams[43] = new SqlParameter("@sightseeing", sightseeing);
            sqlParams[44] = new SqlParameter("@mealplan", mealplan);
            sqlParams[45] = new SqlParameter("@mealplan_Amt", mealplan_Amt);
            sqlParams[46] = new SqlParameter("@AffilateCode", AffilateCode);
            sqlParams[47] = new SqlParameter("@Type", Type);
            sqlParams[48] = new SqlParameter("@RefAgency", AgencyRefernce);
            sqlParams[49] = new SqlParameter("@AgentRef", AgentRef);
            sqlParams[50] = new SqlParameter("@HotelName", HotelName);
            sqlParams[51] = new SqlParameter("@VoucherID", VoucherID);
            sqlParams[52] = new SqlParameter("@InvoiceID", InvoiceID);
            sqlParams[53] = new SqlParameter("@ComparedCurrency", ComparedCurrency);
            sqlParams[54] = new SqlParameter("@ComparedFare", CurrencyFare);
            sqlParams[55] = new SqlParameter("@AgentContactNumber", AgentContactNumber);
            sqlParams[56] = new SqlParameter("@AgentEmail", AgentEmail);
            sqlParams[57] = new SqlParameter("@AgentRemark", AgentRemark);
            sqlParams[58] = new SqlParameter("@Latitude", Latitude);
            sqlParams[59] = new SqlParameter("@Longitude", Longitude);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("usp_Proc_tbl_HotelReservationAddUpdateMGH", out rowsAffected, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode HotelReservationAddUpdateExpedia(Int64 Sid, string HotelCode, string HotelName, string City, string CheckIn, string CheckOut, Int64 NoOfNights, Int64 TotalRooms, float TotalFare, string CurrencyCode, int NoOfAdults, int Children, string ChargeDate, string HotelDetails, string DeadLine, out string AffilateCode, string ReservationStatus, string AgencyRefernce, string LeadingGuestName, string ComparedCurrency, float CurrencyFare, string AgentContactNumber, string AgentEmail, string AgentRemark, string Latitude, string Longitude)
        {
            int rowsAffected = 0;

            CUTUK.DataLayer.MarkupsAndTaxes objMarkupsAndTaxes = new MarkupsAndTaxes();
            if (HttpContext.Current.Session["markups"] != null)
                objMarkupsAndTaxes = (CUTUK.DataLayer.MarkupsAndTaxes)HttpContext.Current.Session["markups"];
            else if (HttpContext.Current.Session["AgentMarkupsOnAdmin"] != null)
                objMarkupsAndTaxes = (CUTUK.DataLayer.MarkupsAndTaxes)HttpContext.Current.Session["AgentMarkupsOnAdmin"];

            Int64 ParentID = 0;
            GlobalDefault objGlobalDefault = new GlobalDefault();
            //AgentDetailsOnAdmin objAgentDetailsOnAdmin = new AgentDetailsOnAdmin();
            objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            ParentID = objGlobalDefault.sid;

            string ReservationId = "";
            string GTAId = "";
            //int NoOfDays = 0;
            decimal SalesTax = 0;
            float Servicecharge = (float)((TotalFare * objMarkupsAndTaxes.PerServiceTax) / 100);
            decimal LuxuryTax = 0;
            float Discount = 0;
            int infants = 0;
            int extrabed = 0;
            bool CancelFlag = false;
            string CancelDate = "";
            string remarks = "";
            //string Status = "";
            float AgentMarkUp_Per = objMarkupsAndTaxes.PerAgentMarkup;
            string BookingStatus = "";
            string Source = "Expedia";
            decimal ExchangeValue = 0;
            string ChildAges = "";
            string HotelBookingData = "";
            string AgencyName = objGlobalDefault.AgencyName;
            string terms = "";
            //string bookingname = objGlobalDefault.ContactPerson;
            int holdbooking = 0;
            bool deadlineemail = false;
            decimal sightseeing = 0;
            string mealplan = "";
            decimal mealplan_Amt = 0;
            AffilateCode = "HTL-AE-" + GenerateRandomString(5);
            string RefAgency = "";
            string AgentRef = objGlobalDefault.Agentuniquecode;
            string Type = "b2b";
            string VoucherID = "VCH-" + GenerateRandomNumber();
            string InvoiceID = "INV-" + GenerateRandomNumber();
            SqlParameter[] sqlParams = new SqlParameter[60];
            sqlParams[0] = new SqlParameter("@Sid", Sid);
            sqlParams[1] = new SqlParameter("@ReservationID", ReservationId);
            sqlParams[2] = new SqlParameter("@GTAId", GTAId);
            sqlParams[3] = new SqlParameter("@Uid", ParentID);
            sqlParams[4] = new SqlParameter("@HotelCode", HotelCode);
            sqlParams[5] = new SqlParameter("@City", City);
            sqlParams[6] = new SqlParameter("@RoomCode", "");
            sqlParams[7] = new SqlParameter("@CheckIn", CheckIn);
            sqlParams[8] = new SqlParameter("@CheckOut", CheckOut);
            sqlParams[9] = new SqlParameter("@NoOfDays", NoOfNights);
            sqlParams[10] = new SqlParameter("@TotalRooms", TotalRooms);
            sqlParams[11] = new SqlParameter("@TotalFare", TotalFare);
            sqlParams[12] = new SqlParameter("@RoomRate", 0.0);
            sqlParams[13] = new SqlParameter("@SupplierCurrency", CurrencyCode);
            sqlParams[14] = new SqlParameter("@SalesTax", SalesTax);
            sqlParams[15] = new SqlParameter("@Servicecharge", Servicecharge);
            sqlParams[16] = new SqlParameter("@LuxuryTax", LuxuryTax);
            sqlParams[17] = new SqlParameter("@Discount", Discount);
            sqlParams[18] = new SqlParameter("@NoOfAdults", NoOfAdults);
            sqlParams[19] = new SqlParameter("@Children", Children);
            sqlParams[20] = new SqlParameter("@Infants", infants);
            sqlParams[21] = new SqlParameter("@ExtraBed", extrabed);
            sqlParams[22] = new SqlParameter("@CancelFlag", CancelFlag);
            sqlParams[23] = new SqlParameter("@CancelDate", CancelDate);
            sqlParams[24] = new SqlParameter("@Remarks", remarks);
            sqlParams[25] = new SqlParameter("@ReservationDate", DateTime.Now.ToString("dd/MM/yyy", CultureInfo.CurrentCulture));
            sqlParams[26] = new SqlParameter("@ReservationTime", DateTime.Now.ToString("HH-mm"));
            sqlParams[27] = new SqlParameter("@Status", ReservationStatus);
            sqlParams[28] = new SqlParameter("@AgentMarkUp_Per", AgentMarkUp_Per);
            sqlParams[29] = new SqlParameter("@BookingStatus", BookingStatus);
            sqlParams[30] = new SqlParameter("@Source", Source = "Expedia");
            sqlParams[31] = new SqlParameter("@ExchangeValue", ExchangeValue);
            sqlParams[32] = new SqlParameter("@Updateid", ParentID);
            sqlParams[33] = new SqlParameter("@Updatedate", DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture));
            sqlParams[34] = new SqlParameter("@HotelDetails", "");
            sqlParams[35] = new SqlParameter("@ChildAges", ChildAges);
            sqlParams[36] = new SqlParameter("@HotelBookingData", HotelBookingData);
            sqlParams[37] = new SqlParameter("@AgencyName", AgencyName);
            sqlParams[38] = new SqlParameter("@terms", terms);
            sqlParams[39] = new SqlParameter("@bookingname", LeadingGuestName);
            sqlParams[40] = new SqlParameter("@DeadLine", DeadLine);
            // sqlParams[40] = new SqlParameter("@DeadLine", DeadLine.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture));
            sqlParams[41] = new SqlParameter("@holdbooking", holdbooking);
            sqlParams[42] = new SqlParameter("@deadlineemail", deadlineemail);
            sqlParams[43] = new SqlParameter("@sightseeing", sightseeing);
            sqlParams[44] = new SqlParameter("@mealplan", mealplan);
            sqlParams[45] = new SqlParameter("@mealplan_Amt", mealplan_Amt);
            sqlParams[46] = new SqlParameter("@AffilateCode", AffilateCode);
            sqlParams[47] = new SqlParameter("@Type", Type);
            sqlParams[48] = new SqlParameter("@RefAgency", AgencyRefernce);
            sqlParams[49] = new SqlParameter("@AgentRef", AgentRef);
            sqlParams[50] = new SqlParameter("@HotelName", HotelName);
            sqlParams[51] = new SqlParameter("@VoucherID", VoucherID);
            sqlParams[52] = new SqlParameter("@InvoiceID", InvoiceID);
            sqlParams[53] = new SqlParameter("@ComparedCurrency", ComparedCurrency);
            sqlParams[54] = new SqlParameter("@ComparedFare", CurrencyFare);
            sqlParams[55] = new SqlParameter("@AgentContactNumber", AgentContactNumber);
            sqlParams[56] = new SqlParameter("@AgentEmail", AgentEmail);
            sqlParams[57] = new SqlParameter("@AgentRemark", AgentRemark);
            sqlParams[58] = new SqlParameter("@Latitude", Latitude);
            sqlParams[59] = new SqlParameter("@Longitude", Longitude);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("usp_Proc_tbl_HotelReservationAddUpdateMGH", out rowsAffected, sqlParams);
            return retCode;
        }



        public static DBHelper.DBReturnCode HotelReservationUpdate(string AffilateCode, string Status, string Source, string VatNumber, string ReservationId, string GTAId, int ReferenceCode, string Comment)
        {
            int rowsAffected = 0;
            // DataTable dtResult;
            //GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            //int NoOfDays = 0;
            //decimal SalesTax = 14;
            //decimal Servicecharge = 500;
            //decimal LuxuryTax = 11;
            //float Discount = 0;
            //int infants = 0;
            //int extrabed = 0;       
            //string remarks = "";
            //string BookingStatus = "";
            //decimal ExchangeValue = 0;
            //string ChildAges = "";
            //string HotelBookingData = ""; 
            //string terms = ""; 
            //int holdbooking = 0;
            //decimal sightseeing = 0;
            //string mealplan = "";
            //decimal mealplan_Amt = 0;
            //string RefAgency = "";
            SqlParameter[] sqlParams = new SqlParameter[9];
            sqlParams[0] = new SqlParameter("@ReservationID", ReservationId);
            sqlParams[1] = new SqlParameter("@GTAId", GTAId);
            sqlParams[2] = new SqlParameter("@Status", Status);
            sqlParams[3] = new SqlParameter("@Source", Source);
            sqlParams[4] = new SqlParameter("@HotelBookingData", VatNumber);
            sqlParams[5] = new SqlParameter("@Updatedate", DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture));
            sqlParams[6] = new SqlParameter("@AffilateCode", AffilateCode);
            sqlParams[7] = new SqlParameter("@ReferenceCode", ReferenceCode.ToString());
            sqlParams[8] = new SqlParameter("@terms", Comment);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("usp_Proc_tbl_HotelReservationUpdate", out rowsAffected, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode HotelReservationLoadByResID(string ReservationID, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@ReservationID", ReservationID);
            sqlParams[1] = new SqlParameter("@Updatedate", DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture));
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_HotelReservationById", out dtResult, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode HotelReservationLoadByDate(string Checkin, string Checkout, out DataTable dtResult)
        {
            //DateTime dCheckin = ConvertDateTime(Checkin);
            //DateTime dCheckout = ConvertDateTime(Checkout);

            //Checkin = dCheckin.ToString("dd/MM/yyy ", CultureInfo.CurrentCulture);
            //Checkout = dCheckout.ToString("dd/MM/yyy", CultureInfo.CurrentCulture);
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@Checkin", Checkin);
            sqlParams[1] = new SqlParameter("@Checkout", Checkout);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("usp_Proc_tbl_HotelReservationLoadByDate", out dtResult, sqlParams);
            return retCode;
        }

        //public static DateTime ConvertDateTime(string Date)
        //{
        //    DateTime date = new DateTime();
        //    try
        //    {
        //        string CurrentPattern = Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;
        //        string[] Split = new string[] { "-", "/", @"\", "." };
        //        string[] Patternvalue = CurrentPattern.Split(Split, StringSplitOptions.None);
        //        string[] DateSplit = Date.Split(Split, StringSplitOptions.None);
        //        string NewDate = "";
        //        if (Patternvalue[0].ToLower().Contains("d") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("y") == true)
        //        {
        //            NewDate = DateSplit[0] + "/" + DateSplit[1] + "/" + DateSplit[2];
        //        }
        //        else if (Patternvalue[0].ToLower().Contains("m") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("y") == true)
        //        {
        //            NewDate = DateSplit[1] + "/" + DateSplit[0] + "/" + DateSplit[2];
        //        }
        //        else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("d") == true)
        //        {
        //            NewDate = DateSplit[2] + "/" + DateSplit[1] + "/" + DateSplit[0];
        //        }
        //        else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("m") == true)
        //        {
        //            NewDate = DateSplit[1] + "/" + DateSplit[2] + "/" + DateSplit[0];
        //        }
        //        date = DateTime.Parse(NewDate, Thread.CurrentThread.CurrentCulture);
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    finally
        //    {

        //    }

        //    return date;

        //}

        public static DBHelper.DBReturnCode HotelReservationDeleteByKey(string uid, out int rows)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@uid", uid);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("usp_Proc_tbl_HotelReservationDeleteByKey", out rows, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode BookedPaseengerAdd(string RoomCode, string RoomNumber, string Name, string LastName, int Age, string PassengerType, int IsLeading, string ReservationID)
        {
            int rowsAffected = 0;
            SqlParameter[] sqlParams = new SqlParameter[8];
            sqlParams[0] = new SqlParameter("@RoomCode", RoomCode);
            sqlParams[1] = new SqlParameter("@RoomNumber", RoomNumber);
            sqlParams[2] = new SqlParameter("@Name", Name);
            sqlParams[3] = new SqlParameter("@LastName", LastName);
            sqlParams[4] = new SqlParameter("@Age", Age);
            sqlParams[5] = new SqlParameter("@PassengerType", PassengerType);
            sqlParams[6] = new SqlParameter("@IsLeading", IsLeading);
            sqlParams[7] = new SqlParameter("@ReservationID", ReservationID);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_tbl_BookedPassengerAdd", out rowsAffected, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode BookedRoomAdd(string RoomCode, string RoomType, string RoomNumber, string BoardText, int TotalRooms, string LeadingGuest, int Adults, int Child, string ChildAge, string Remark, string CutCancellationDate, string SupplierNoChargeDate, string CancellationAmount, string CanServiceTax, string CanAmtWithTax, float RoomAmount, float RoomServiceTax, float RoomAmtWithTax, string ReservationID)
        {
            int rowsAffected = 0;
            SqlParameter[] sqlParams = new SqlParameter[19];
            sqlParams[0] = new SqlParameter("@RoomCode", RoomCode);
            sqlParams[1] = new SqlParameter("@RoomType", RoomType);
            sqlParams[2] = new SqlParameter("@RoomNumber", RoomNumber);
            sqlParams[3] = new SqlParameter("@BoardText", BoardText);
            sqlParams[4] = new SqlParameter("@TotalRooms", TotalRooms);
            sqlParams[5] = new SqlParameter("@LeadingGuest", LeadingGuest);
            sqlParams[6] = new SqlParameter("@Adults", Adults);
            sqlParams[7] = new SqlParameter("@Child", Child);
            sqlParams[8] = new SqlParameter("@ChildAge", ChildAge);
            sqlParams[9] = new SqlParameter("@Remark", Remark);
            sqlParams[10] = new SqlParameter("@CutCancellationDate", CutCancellationDate);
            sqlParams[11] = new SqlParameter("@SupplierNoChargeDate", SupplierNoChargeDate);
            sqlParams[12] = new SqlParameter("@CancellationAmount", CancellationAmount);
            sqlParams[13] = new SqlParameter("@CanServiceTax", CanServiceTax);
            sqlParams[14] = new SqlParameter("@CanAmtWithTax", CanAmtWithTax);
            sqlParams[15] = new SqlParameter("@RoomAmount", RoomAmount);
            sqlParams[16] = new SqlParameter("@RoomServiceTax", RoomServiceTax);
            sqlParams[17] = new SqlParameter("@RoomAmtWithTax", RoomAmtWithTax);
            sqlParams[18] = new SqlParameter("@ReservationID", ReservationID);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_tbl_BookedRoomAdd", out rowsAffected, sqlParams);
            return retCode;
        }
        //public static DBHelper.DBReturnCode ReservedPassengerAdd(string ReservationId, string GTAID, string FirstName, string LastName, int Age)
        //{
        //    int rowsAffected = 0;
        //    // DataTable dtResult;
        //    //GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //    string[] ArrName = FirstName.Split(' ');
        //    string Title = ArrName[0].ToString();
        //    string firstname = ArrName[1].ToString();
        //    SqlParameter[] sqlParams = new SqlParameter[6];
        //    sqlParams[0] = new SqlParameter("@ReservationID", ReservationId);
        //    sqlParams[1] = new SqlParameter("@GTAID", GTAID);
        //    sqlParams[2] = new SqlParameter("@Title", Title);
        //    sqlParams[3] = new SqlParameter("@FirstName", firstname);
        //    sqlParams[4] = new SqlParameter("@LastName", LastName);
        //    sqlParams[5] = new SqlParameter("@Age", Age);
        //    DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("usp_Proc_tbl_ReservedPassengerAdd", out rowsAffected, sqlParams);
        //    return retCode;
        //}

        public static DBHelper.DBReturnCode GetCancellationDetails(string ReservationId, out DataSet dsResult)
        {
            SqlParameter[] sqlparam = new SqlParameter[1];
            sqlparam[0] = new SqlParameter("@ReservationID", ReservationId);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("Proc_GetCancellationDetails", out dsResult, sqlparam);
            return retCode;
        }

        public static DBHelper.DBReturnCode CancelBooking(string ReservationID, string CancellationAmount, string BookingStatus, string Remark, string TotalFare, string ServiceCharge, string Total)
        {
            int rowsAffected = 0;
            //CUT.DataLayer.MarkupsAndTaxes objMarkupsAndTaxes = (CUT.DataLayer.MarkupsAndTaxes)HttpContext.Current.Session["markups"];
            //GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            //Int64 uid = objGlobalDefault.sid;
            CUTUK.DataLayer.MarkupsAndTaxes objMarkupsAndTaxes = new MarkupsAndTaxes();
            if (HttpContext.Current.Session["markups"] != null)
                objMarkupsAndTaxes = (CUTUK.DataLayer.MarkupsAndTaxes)HttpContext.Current.Session["markups"];
            else if (HttpContext.Current.Session["AgentMarkupsOnAdmin"] != null)
                objMarkupsAndTaxes = (CUTUK.DataLayer.MarkupsAndTaxes)HttpContext.Current.Session["AgentMarkupsOnAdmin"];


            Int64 ParentID = 0;
            GlobalDefault objGlobalDefault = new GlobalDefault();
            //AgentDetailsOnAdmin objAgentDetailsOnAdmin = new AgentDetailsOnAdmin();
            if (HttpContext.Current.Session["LoginUser"] != null)
            {
                objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                ParentID = objGlobalDefault.sid;
            }
          

            //float CanAmountWithTax = Convert.ToSingle(CancellationAmount);
            //float CanAmountWithoutTax = (CanAmountWithTax / (objMarkupsAndTaxes.PerServiceTax + 100)) * 100;
            //float CanServiceTax = (CanAmountWithoutTax * objMarkupsAndTaxes.PerServiceTax) / 100;
            //float SupplierCanAmount = (CanAmountWithoutTax / (objMarkupsAndTaxes.PerCutMarkup + 100)) * 100;
            //float CutComm = (SupplierCanAmount * objMarkupsAndTaxes.PerCutMarkup) / 100;
            //float AgenComm = (CanAmountWithTax * objMarkupsAndTaxes.PerAgentMarkup) / 100;
            float CanAmountWithTax = 0;
            float CanAmountWithoutTax = 0;
            float CanServiceTax = 0;
            float SupplierCanAmount = 0;
            if (Convert.ToSingle(CancellationAmount) != 0)
            {
                CanAmountWithTax = Convert.ToSingle(CancellationAmount) + Convert.ToSingle(ServiceCharge);
                CanServiceTax = (CanAmountWithoutTax * objMarkupsAndTaxes.PerServiceTax) / 100;
                CanAmountWithoutTax = CanAmountWithTax;
                //CanAmountWithoutTax = Convert.ToSingle(CancellationAmount);
                //CanServiceTax = (CanAmountWithoutTax * objMarkupsAndTaxes.PerServiceTax) / 100;
                //CanAmountWithTax = CanAmountWithoutTax + CanServiceTax;
            }
            else
            {
                CanAmountWithoutTax = Convert.ToSingle(CancellationAmount);
                CanServiceTax = (CanAmountWithoutTax * objMarkupsAndTaxes.PerServiceTax) / 100;
                CanAmountWithTax = Convert.ToSingle(CancellationAmount) + Convert.ToSingle(ServiceCharge);
            }
            //CanAmountWithoutTax = CanAmountWithTax - Convert.ToSingle(ServiceCharge);
            //CanServiceTax = (CanAmountWithoutTax * objMarkupsAndTaxes.PerServiceTax) / 100;
            SupplierCanAmount = (CanAmountWithoutTax / (objMarkupsAndTaxes.PerCutMarkup + 100)) * 100;
            float CutComm = (SupplierCanAmount * objMarkupsAndTaxes.PerCutMarkup) / 100;
            float AgenComm = (CanAmountWithTax * objMarkupsAndTaxes.PerAgentMarkup) / 100;
            //bool CancelFlag = true;
            SqlParameter[] sqlParams = new SqlParameter[11];
            sqlParams[0] = new SqlParameter("@uid", ParentID);
            sqlParams[1] = new SqlParameter("@ReservationID", ReservationID);
            sqlParams[2] = new SqlParameter("@BookingStatus", BookingStatus);
            sqlParams[3] = new SqlParameter("@CanAmountWithoutTax", CanAmountWithoutTax);
            sqlParams[4] = new SqlParameter("@CanServiceTax", CanServiceTax);
            sqlParams[5] = new SqlParameter("@CanAmountWithTax", CanAmountWithTax);
            sqlParams[6] = new SqlParameter("@SupplierCanAmount", SupplierCanAmount);
            sqlParams[7] = new SqlParameter("@CutCanComm", CutComm);
            sqlParams[8] = new SqlParameter("@AgentCanComm", AgenComm);
            sqlParams[9] = new SqlParameter("@CancellationDate", DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture));
            sqlParams[10] = new SqlParameter("@Remark", Remark);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_CancelBooking", out rowsAffected, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetMaxOccupancy(string RoomType, out DataTable dtResult)
        {
            //DataTable dtResult;
            //dtResult = null;
            SqlParameter[] sqlparam = new SqlParameter[1];
            sqlparam[0] = new SqlParameter("@RoomType", RoomType);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_ROOM_TYPESGetMaximumOccupancy", out dtResult, sqlparam);
            return retCode;
            //return dtResult.Rows[0]["MaxOccupation"].ToString();
        }

        public static DBHelper.DBReturnCode UpdateBookingStatus(string AffilateCode, string CurrentStatus, string DumReservation)
        {
            int rowsAffected = 0;

            SqlParameter[] sqlParams = new SqlParameter[3];
            sqlParams[0] = new SqlParameter("@AffilateCode", AffilateCode);
            sqlParams[1] = new SqlParameter("@CurrentStatus", CurrentStatus);
            sqlParams[2] = new SqlParameter("@DumReservation", DumReservation);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_UpdateCurrentStatus", out rowsAffected, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode UpdateTransactionStatus(string DumReservation, string ReservationID)
        {
            int rowsAffected = 0;

            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@DumReservation", DumReservation);
            sqlParams[1] = new SqlParameter("@ReservationID", ReservationID);

            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_UpdateTransactionStatus", out rowsAffected, sqlParams);
            return retCode;
        }


        public static DBHelper.DBReturnCode UpdateTransactionStatusOnline(string DumReservation, string ReservationID)
        {
            int rowsAffected = 0;

            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@DumReservation", DumReservation);
            sqlParams[1] = new SqlParameter("@ReservationID", ReservationID);

            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_UpdateTransactionStatusOnline", out rowsAffected, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetBookingCode(string ReservationID, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@ReservationId", ReservationID);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("usp_Proc_tbl_HotelReservationGetGTAId", out dtResult, sqlParams);
            return retCode;
        }

        public static string GenerateRandomString(int length)
        {
            string rndstring = "";
            bool IsRndlength = false;
            Random rnd = new Random((int)DateTime.Now.Ticks);
            if (IsRndlength)
                length = rnd.Next(4, length);
            for (int i = 0; i < length; i++)
            {
                int toss = rnd.Next(1, 10);
                if (toss > 5)
                    rndstring += (char)rnd.Next((int)'A', (int)'Z');
                else
                    rndstring += rnd.Next(0, 9).ToString();
            }
            return rndstring;
        }

        public static int GenerateRandomNumber()
        {
            int rndnumber = 0;
            Random rnd = new Random((int)DateTime.Now.Ticks);
            rndnumber = rnd.Next();
            return rndnumber;
        }

        public static string TrimWhiteSpace(string Value)
        {
            StringBuilder sbOut = new StringBuilder();
            if (!string.IsNullOrEmpty(Value))
            {
                bool IsWhiteSpace = false;
                for (int i = 0; i < Value.Length; i++)
                {
                    if (char.IsWhiteSpace(Value[i])) //Comparion with WhiteSpace
                    {
                        if (!IsWhiteSpace) //Comparison with previous Char
                        {
                            sbOut.Append(Value[i]);
                            IsWhiteSpace = true;
                        }
                    }
                    else
                    {
                        IsWhiteSpace = false;
                        sbOut.Append(Value[i]);
                    }
                }
            }
            return sbOut.ToString();
        }

        public static DBHelper.DBReturnCode BookingDetailForSMS(string ReservationID, out DataTable dt)
        {
            GlobalDefault objGlobalDefault = new GlobalDefault();
            objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            string ContactId = (objGlobalDefault.ContactID).ToString();
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@ContactId", ContactId);
            sqlParams[1] = new SqlParameter("@ReservationID", ReservationID);

            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_SMS_BookingDetail", out dt, sqlParams);
            return retCode;
        }
    }
}