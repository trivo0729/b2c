﻿using CUTUK.BL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CUTUK.DataLayer
{
    public class HotelBookingTxnManager
    {
        public static DBHelper.DBReturnCode HotelBookingTxnAdd(string ReservationID, string TransactionDate, float BookingAmt, float SeviceTax, float BookingAmtWithTax, float CanAmount, float CanServiceTax, float CanAmtWithTax, float Balance, float SalesTax, float LuxuryTax, int DiscountPer, string BookingCreationDate, string BookingRemarks, int BookingCancelFlag, string Supplier, float SupplierBasicAmt, float AgentComm, float CUTComm, string BookedBy, string BookingStatus)
        {
            int rowsaffected;

            Int64 uid = 0;
            GlobalDefault objGlobalDefault = new GlobalDefault();
            //AgentDetailsOnAdmin objAgentDetailsOnAdmin = new AgentDetailsOnAdmin();
            //if (HttpContext.Current.Session["AgentDetailsOnAdmin"] != null)
            //{
            //    objAgentDetailsOnAdmin = (AgentDetailsOnAdmin)HttpContext.Current.Session["AgentDetailsOnAdmin"];
            //    uid = objAgentDetailsOnAdmin.sid;
            //    if (BookedBy == "")
            //        BookedBy = objAgentDetailsOnAdmin.Agentuniquecode;
            //}
            //else if (HttpContext.Current.Session["LoginUser"] != null)
            //{
            //    objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            //    uid = objGlobalDefault.sid;
            //    if (BookedBy == "")
            //        BookedBy = objGlobalDefault.Agentuniquecode;
            //}
            objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            uid = objGlobalDefault.sid;
            if (BookedBy == "")
                BookedBy = objGlobalDefault.Agentuniquecode;

            SqlParameter[] sqlParams = new SqlParameter[21];
            sqlParams[0] = new SqlParameter("@uid", uid);
            sqlParams[1] = new SqlParameter("@ReservationID", ReservationID);
            sqlParams[2] = new SqlParameter("@TransactionDate", TransactionDate);
            sqlParams[3] = new SqlParameter("@BookingAmt", BookingAmt);
            sqlParams[4] = new SqlParameter("@SeviceTax", SeviceTax);
            sqlParams[5] = new SqlParameter("@BookingAmtWithTax", BookingAmtWithTax);
            sqlParams[6] = new SqlParameter("@CanAmt", CanAmount);
            sqlParams[7] = new SqlParameter("@CanServiceTax", CanServiceTax);
            sqlParams[8] = new SqlParameter("@CanAmtWithTax", CanAmtWithTax);
            sqlParams[9] = new SqlParameter("@Balance", Balance);
            sqlParams[10] = new SqlParameter("@SalesTax", SalesTax);
            sqlParams[11] = new SqlParameter("@LuxuryTax", LuxuryTax);
            sqlParams[12] = new SqlParameter("@DiscountPer", DiscountPer);
            sqlParams[13] = new SqlParameter("@BookingCreationDate", BookingCreationDate);
            sqlParams[14] = new SqlParameter("@BookingRemarks", BookingRemarks);
            sqlParams[15] = new SqlParameter("@BookingCancelFlag", BookingCancelFlag);
            sqlParams[16] = new SqlParameter("@Supplier", Supplier);
            sqlParams[17] = new SqlParameter("@SupplierBasicAmt", SupplierBasicAmt);
            sqlParams[18] = new SqlParameter("@AgentComm", AgentComm);
            sqlParams[19] = new SqlParameter("@CUTComm", CUTComm);
            sqlParams[20] = new SqlParameter("@BookedBy", BookedBy);

            DBHelper.DBReturnCode retCode;
            if (BookingStatus == "Vouchered")
            {
                retCode = DBHelper.ExecuteNonQuery("Proc_tbl_BookingTransactionAdd", out rowsaffected, sqlParams);
            }
            else
            {
                retCode = DBHelper.ExecuteNonQuery("Proc_tbl_BookingTransactionAddHold", out rowsaffected, sqlParams);
            }
            return retCode;
        }


        public static DBHelper.DBReturnCode HotelBookingTxnAddOnline(string ReservationID, string TransactionDate, float BookingAmt, float SeviceTax, float BookingAmtWithTax, float CanAmount, float CanServiceTax, float CanAmtWithTax, float Balance, float SalesTax, float LuxuryTax, int DiscountPer, string BookingCreationDate, string BookingRemarks, int BookingCancelFlag, string Supplier, float SupplierBasicAmt, float AgentComm, float CUTComm, string BookedBy)
        {
            int rowsaffected;

            Int64 uid = 0;
            GlobalDefault objGlobalDefault = new GlobalDefault();
           // AgentDetailsOnAdmin objAgentDetailsOnAdmin = new AgentDetailsOnAdmin();
            //if (HttpContext.Current.Session["AgentDetailsOnAdmin"] != null)
            //{
            //    objAgentDetailsOnAdmin = (AgentDetailsOnAdmin)HttpContext.Current.Session["AgentDetailsOnAdmin"];
            //    uid = objAgentDetailsOnAdmin.sid;
            //    if (BookedBy == "")
            //        BookedBy = objAgentDetailsOnAdmin.Agentuniquecode;
            //}
            //else if (HttpContext.Current.Session["LoginUser"] != null)
            //{
            //    objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            //    uid = objGlobalDefault.sid;
            //    if (BookedBy == "")
            //        BookedBy = objGlobalDefault.Agentuniquecode;
            //}
            objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            uid = objGlobalDefault.sid;
            BookedBy = objGlobalDefault.Agentuniquecode;

            SqlParameter[] sqlParams = new SqlParameter[21];
            sqlParams[0] = new SqlParameter("@uid", uid);
            sqlParams[1] = new SqlParameter("@ReservationID", ReservationID);
            sqlParams[2] = new SqlParameter("@TransactionDate", TransactionDate);
            sqlParams[3] = new SqlParameter("@BookingAmt", BookingAmt);
            sqlParams[4] = new SqlParameter("@SeviceTax", SeviceTax);
            sqlParams[5] = new SqlParameter("@BookingAmtWithTax", BookingAmtWithTax);
            sqlParams[6] = new SqlParameter("@CanAmt", CanAmount);
            sqlParams[7] = new SqlParameter("@CanServiceTax", CanServiceTax);
            sqlParams[8] = new SqlParameter("@CanAmtWithTax", CanAmtWithTax);
            sqlParams[9] = new SqlParameter("@Balance", Balance);
            sqlParams[10] = new SqlParameter("@SalesTax", SalesTax);
            sqlParams[11] = new SqlParameter("@LuxuryTax", LuxuryTax);
            sqlParams[12] = new SqlParameter("@DiscountPer", DiscountPer);
            sqlParams[13] = new SqlParameter("@BookingCreationDate", BookingCreationDate);
            sqlParams[14] = new SqlParameter("@BookingRemarks", BookingRemarks);
            sqlParams[15] = new SqlParameter("@BookingCancelFlag", BookingCancelFlag);
            sqlParams[16] = new SqlParameter("@Supplier", Supplier);
            sqlParams[17] = new SqlParameter("@SupplierBasicAmt", SupplierBasicAmt);
            sqlParams[18] = new SqlParameter("@AgentComm", AgentComm);
            sqlParams[19] = new SqlParameter("@CUTComm", CUTComm);
            sqlParams[20] = new SqlParameter("@BookedBy", BookedBy);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_tbl_BookingTransactionAddOnline", out rowsaffected, sqlParams);
            return retCode;
        }


        public static DBHelper.DBReturnCode HotelBookingTxnUpdate(string ReservationID, string DumReservation)
        {

            int rowsaffected;
            int sid = 0;
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            string AgencyName = objGlobalDefault.AgencyName;
            Int64 uid = objGlobalDefault.sid;
            SqlParameter[] sqlParams = new SqlParameter[3];
            sqlParams[0] = new SqlParameter("@uid", uid);
            sqlParams[1] = new SqlParameter("@ReservationID", ReservationID);
            sqlParams[2] = new SqlParameter("@DumReservation", DumReservation);



            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_tbl_BookingTransactionUpdate", out rowsaffected, sqlParams);
            return retCode;
        }

    }
}