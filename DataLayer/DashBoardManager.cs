﻿using CUT.BL;
using CUT.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CUTUK.DataLayer
{
    public class DashBoardManager
    {

        public static DBHelper.DBReturnCode UpdateDetails( string Mobile, string Phone, string City, string Address)
        {
            int rowsAffected = 0;
            SqlParameter[] sqlParams1 = new SqlParameter[5];
            if (HttpContext.Current.Session["LoginCustomer"] != null)
            {
                GlobalDefault objGlobalDefaultCustomer = (GlobalDefault)HttpContext.Current.Session["LoginCustomer"];

                //string UserType = "1";

                Int64 sid = objGlobalDefaultCustomer.sid;

                sqlParams1[0] = new SqlParameter("@Mobile", Mobile);
                sqlParams1[1] = new SqlParameter("@Phone", Phone);
                sqlParams1[2] = new SqlParameter("@CityId", City);
                sqlParams1[3] = new SqlParameter("@Address", Address);
                //sqlParams1[8] = new SqlParameter("@PinCode", sPinCode);
                
                sqlParams1[4] = new SqlParameter("@Sid", sid);
            }

            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_tbl_UpdateProfile_B2CCustomer", out rowsAffected, sqlParams1);
            return retCode;
        }

        public static DBHelper.DBReturnCode BookingList(Int64 Uid, out DataTable dtResult)
        {
            GlobalDefault objGlobalDefaultCustomer = (GlobalDefault)HttpContext.Current.Session["LoginCustomer"];
            SqlParameter[] SQLParams = new SqlParameter[1];
            SQLParams[0] = new SqlParameter("@B2C_Id", objGlobalDefaultCustomer.B2C_Id);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_B2CCustomerBookingList", out dtResult, SQLParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode ChangePassword(Int64 Sid, string NewPassword)
        {
            int rowsaffected = 0;
            SqlParameter[] SQLParams = new SqlParameter[2];
            SQLParams[0] = new SqlParameter("@Sid", Sid);
            SQLParams[1] = new SqlParameter("@Password", NewPassword);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_B2CCustomerUpdatePassword",out rowsaffected, SQLParams);
            return retCode;
        }
    }
}