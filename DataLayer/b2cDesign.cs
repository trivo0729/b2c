﻿using CommonLib.Response;
using CUT.Common;
using CUT.DataLayer;
using CUT.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace CUTUK.DataLayer
{
    public class b2cDesign : HTMLCommonAvailBuilder
    {

        public static List<CommonMap> MapList;
        public static string GetRatings(string Category)
        {
            string HotelRating = "";
            if (Category == "1EST" || Category == "559" || Category == "1" || Category == "1.5")
            { HotelRating = "../HotelRating/1.png"; }
            else if (Category == "2EST" || Category == "560" || Category == "2" || Category == "2.5")
            { HotelRating = "../HotelRating/2.png"; }
            else if (Category == "3EST" || Category == "561" || Category == "3" || Category == "3.5")
            { HotelRating = "../HotelRating/3.png"; }
            else if (Category == "4EST" || Category == "562" || Category == "4" || Category == "4.5")
            { HotelRating = "../HotelRating/4.png"; }
            else if (Category == "5EST" || Category == "563" || Category == "5")
            { HotelRating = "../HotelRating/5.png"; }
            else { HotelRating = "../HotelRating/0.png"; }
            return HotelRating;
        }
        public static List<CommonMap> GetMaps(List<CommonLib.Response.CommonHotelDetails> HotelDetail)
        {
            List<CommonMap> objMaps = new List<CommonMap>();
            foreach (CommonLib.Response.CommonHotelDetails objHotelDetail in HotelDetail)
            {
                try
                {
                    if (objHotelDetail.Langitude == "")
                    {
                        objHotelDetail.Langitude = "0";
                    }
                    if (objHotelDetail.Latitude == "")
                    {
                        objHotelDetail.Latitude = "0";
                    }
                    string MapImage = "";
                    if (objHotelDetail.Image.Count != 0)
                        MapImage = objHotelDetail.Image[0].Url;
                    objMaps.Add(new CommonMap
                    {
                        HotelId = objHotelDetail.HotelId,
                        MapName = (objHotelDetail.HotelName),
                        MapLangitude = Convert.ToSingle(objHotelDetail.Langitude),
                        Maplatitude = Convert.ToSingle(objHotelDetail.Latitude),
                        MapImageurl = MapImage,
                        Rating = GetRatings(objHotelDetail.Category.Replace(".png", "")),
                        Decription = objHotelDetail.Description.Split('.')[0],
                        PriceList = NumberWithComma(Math.Round((Decimal)((objHotelDetail.CUTPrice + objHotelDetail.AgentMarkup) / 1.00), 2, MidpointRounding.AwayFromZero)),

                    });
                }
                catch
                {
                    continue;
                }

                //ListHotelName.Add(objHotelDetail.HotelName);
            }
            return objMaps;

        }
        public static object GetHotels(CUT.Models.CommonHotels objCommonHotel, int pageNo)
        {
            object arrHotel = new object();
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            object m_filtertrip = new object(), m_Price = new object();
            List<object> m_StarRating = new List<object>(), m_Facility = new List<object>(), m_Locations = new List<object>();
            HotelFilterState objHotelFilterState = new HotelFilterState();
            List<CommonLib.Response.CommonHotelDetails> List_CommonHotel = new List<CommonHotelDetails>();
            Pager objPager = new Pager();
            MapList = new List<CommonMap>();

            if (HttpContext.Current.Session["FiterHistory"] != null)
            {
                objHotelFilterState = (HotelFilterState)HttpContext.Current.Session["FiterHistory"];
                List<CommonLib.Response.CommonHotelDetails> List_HotelDetail_Filter = (List<CommonLib.Response.CommonHotelDetails>)HttpContext.Current.Session["List_HotelDetail_Filter"];
                objPager = new Pager();
                objPager.currentPage = objHotelFilterState.PageNo;
                objPager.TototalRowsCount = List_HotelDetail_Filter.Count;
                List_CommonHotel = List_HotelDetail_Filter.Skip((objHotelFilterState.PageNo - 1) * objPager.PageSize).Take(objPager.PageSize).ToList();
                List<CommonLib.Response.Category> arrRatting = new List<CommonLib.Response.Category>();
                foreach (string Category in List_HotelDetail_Filter.Select(d => d.Category.Replace(".png", "")).Distinct().ToList())
                {
                    arrRatting.Add(new Category
                    {
                        Name = Category,
                        Supplier = "",
                        HotelCode = "",
                        Count = List_HotelDetail_Filter.Where(d => d.Category == Category).ToList().Count,
                    });
                }
                objCommonHotel = new CommonHotels
                {
                    CommonHotelDetails = List_HotelDetail_Filter,
                    CountHotel = List_HotelDetail_Filter.Count,
                    Page = objHotelFilterState.PageNo,
                    Location = new List<CommonLib.Response.Location>(),
                    Facility = new List<CommonLib.Response.Facility>(),
                    Category = arrRatting,
                    DisplayRequest = (CUT.Models.AvailCommonRequest)HttpContext.Current.Session["DisplayRequest"],
                    MinPrice = List_HotelDetail_Filter.Select(d => d.Charge.TotalPrice).ToList().Min(),
                    MaxPrice = List_HotelDetail_Filter.Select(d => d.Charge.TotalPrice).ToList().Max()

                };
            }
            else
            {
                List_CommonHotel = objCommonHotel.CommonHotelDetails.Take(50).Skip(0).ToList();
                objPager = new Pager();
                objPager.currentPage = pageNo;
                objPager.TototalRowsCount = objCommonHotel.CommonHotelDetails.Count;
            }

            /* filtertrip */
            m_filtertrip = new
            {
                TotalCount = List_CommonHotel.Count,
                Displayed = objCommonHotel.CommonHotelDetails.Count,
                Location = objCommonHotel.DisplayRequest.Location,
            };

            /*  Rating */
            if (objCommonHotel.Category != null)
            {

                foreach (var Rating in objCommonHotel.Category.OrderBy(data => data.Name).ToList())
                {
                    if (Rating.Name == "Other")
                    {
                        Rating.Name = "0";
                    }
                    bool Checked = false;
                    if (HttpContext.Current.Session["FiterHistory"] != null && objHotelFilterState.StarRating.Contains(Convert.ToInt32(Rating.Name)))
                        Checked = true;
                    m_StarRating.Add(new
                    {
                        Count = Rating.Count,
                        Checked = Checked,
                        Rating = Rating.Name,
                    });
                }
            }
            /* m_Price */
            m_Price = new { MinPrice = objCommonHotel.MinPrice, objCommonHotel.MaxPrice };

            string RoomPrice = "0", NettRoomPrice = "0";
            float ExchangeRate = 0, NettAmountInr = 0;
            int noRooms = 1;
            foreach (CommonLib.Response.CommonHotelDetails objHotelDetail in List_CommonHotel)
            {
                noRooms = ExchangeRateManger.GetNoRoom(objHotelDetail.Supplier);
                ExchangeRate = ExchangeRateManger.GetExchange();
                string tooltipRs = "", NettTooltip = "";
                string CurrencyClass = ExchangeRateManger.GetCurrency();
                AgentDetailsOnAdmin objAgentDetailsOnAdmin = new AgentDetailsOnAdmin();
                RoomPrice = NumberWithComma((Decimal)ExchangeRateManger.RoomPrice(objHotelDetail.Charge, objHotelDetail.Supplier, noRooms, out NettAmountInr, out NettTooltip, out tooltipRs));
                NettRoomPrice = NumberWithComma((Decimal)NettAmountInr);
                objHotelDetail.MinPrice = RoomPrice;
                objHotelDetail.MinToolTip = tooltipRs;
                objHotelDetail.MinNettPrice = NettRoomPrice;
                objHotelDetail.MinNettToolTip = NettTooltip;

            }

            /*  Facility */
            if (objCommonHotel.Facility != null)
            {
                foreach (var Facility in objCommonHotel.Facility.OrderBy(data => data.FacilitiesName).ToList())
                {
                    bool Checked = false;
                    if (HttpContext.Current.Session["FiterHistory"] != null && objHotelFilterState.HotelFacility.Contains(Facility.FacilitiesName))
                        Checked = true;
                    m_Facility.Add(new
                    {
                        Checked = Checked,
                        Facility = Facility.FacilitiesName,
                    });
                }
            }

            if (objCommonHotel.Location != null)
            {
                foreach (var Location in objCommonHotel.Location.OrderBy(data => data.Name).ToList())
                {
                    bool Checked = false;
                    if (HttpContext.Current.Session["FiterHistory"] != null && objHotelFilterState.NearByPlaces.Contains(Location.Name))
                        Checked = true;
                    m_Locations.Add(new
                    {
                        Checked = Checked,
                        Facility = Location.Name,
                    });
                }
            }

            /* Session */



            /* Map */
            MapList = GetMaps(objCommonHotel.CommonHotelDetails);
            #region New Objects
            arrHotel = new
            {
                ListHotel = List_CommonHotel,
                MapList = MapList,
                Ratings = m_StarRating,
                FilteTip = m_filtertrip,
                ListFacility = m_Facility,
                NearByLocation = m_Locations,
                m_Price = m_Price,
            };

            #endregion
            return arrHotel;
        }


        #region GenerateHotelRoomDetails

        public static List<object> GenerateRoomDetails(string HotelCode, string Supplier)
        {
            string CurrencyClass = ExchangeRateManger.GetCurrency();
            List<object> objRateGroup = new List<object>();
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            string PreferedCurrency = objGlobalDefault.Currency;
            CUT.DataLayer.MarkupsAndTaxes objMarkupsAndTaxes = new CUT.DataLayer.MarkupsAndTaxes();
            if (HttpContext.Current.Session["markups"] != null)
                objMarkupsAndTaxes = (CUT.DataLayer.MarkupsAndTaxes)HttpContext.Current.Session["markups"];
            if (HttpContext.Current.Session["AgentDetailsOnAdmin"] != null)
            {
                AgentDetailsOnAdmin objAgentDetailsOnAdmin = (AgentDetailsOnAdmin)HttpContext.Current.Session["AgentDetailsOnAdmin"];
                PreferedCurrency = objAgentDetailsOnAdmin.Currency;
            }
            string NettToolTip = ""; float AmountInINR = 0, NettAmountInINR = 0;
            float DefaultRate = 0;
            float netDefaultRate = 0;
            var TotalRomms = 0;
            string RoomAmt = "", NettRoomAmt;
            string tooltip = "";
            int noRooms = 0;
            List<CommonLib.Response.RateGroup> ListRateGroup = HTMLCommonAvailBuilder.GetRateGroup(HotelCode, Supplier);
            foreach (CommonLib.Response.RateGroup objGroup in ListRateGroup)
            {
                List<object> RoomOccupancy = new List<object>();

                int r = 1;
                foreach (CommonLib.Response.HotelOccupancy objOccupancy in objGroup.RoomOccupancy)
                {
                    List<bool> isNonRefundGroup = new List<bool>();
                    var Group_Refundable = objOccupancy.Rooms.Select(d => d.CancellationPolicy).ToList();
                    for (int j = 0; j < Group_Refundable.Count; j++)
                    {
                        isNonRefundGroup.Add(Group_Refundable[j].Any(d => d.AmendRestricted == true && d.CancelRestricted == true && d != null));
                    }
                    bool ClassRefund = isNonRefundGroup.All(d => d == true);
                    string Rooms = "";
                    #region No Room in Seach
                    foreach (int roomNo in objOccupancy.RoomNo)
                    {
                        if (objOccupancy.RoomNo.IndexOf(roomNo) == objOccupancy.RoomNo.Count - 1)
                        {
                            if (objOccupancy.RoomNo.Count != 1)
                            {
                                Rooms = Rooms.Remove(Rooms.Length - 1);
                                Rooms += " & " + roomNo;
                            }
                            else
                            {
                                Rooms += roomNo;
                                break;
                            }

                        }
                        else
                        {
                            Rooms += roomNo + ",";
                        }
                    }
                    Rooms += "(" + objOccupancy.AdultCount + "Adults " + "& " + objOccupancy.ChildCount + " Children";
                    if (objOccupancy.ChildCount != 0)
                        Rooms += " with Ages : " + objOccupancy.ChildAges;
                    Rooms += ")";
                    #endregion

                    #region ListRooms
                    List<object> objRooms = new List<object>();
                    List<CommonLib.Response.RoomType> List_LRoomType = new List<RoomType>();
                    List_LRoomType = GetRoomPrice.GeUniqueRates(objOccupancy).ToList();
                    List_LRoomType = List_LRoomType.Distinct().ToList();
                    foreach (RoomType objRoomType in List_LRoomType)
                    {
                        bool nonRefund = false;
                        nonRefund = objRoomType.CancellationPolicy.Any(d => d.AmendRestricted == true && d.CancelRestricted == true && d != null);
                        string Refundable = "Refundable";
                        if (nonRefund)
                            Refundable = "nonRefundable";
                        DateTime date = Convert.ToDateTime(Convert.ToDateTime(objRoomType.CancellationPolicy.Select(d => d.dayMax).First()).ToShortDateString());

                        #region Offer & Parmotions
                        string Promotion = String.Empty;
                        string MinStay = String.Empty;
                        string FreeCancel = String.Empty;
                        MinStay = String.Empty;
                        string Tittle = String.Empty;
                        if (objRoomType.special != null)
                        {
                            foreach (CommonLib.Response.special objspecial in objRoomType.special)
                            {
                                if (objspecial.runno == objRoomType.specialsApplied)
                                {
                                    Tittle = objspecial.specialName;
                                    if (objspecial.type == "stayXGetDiscountPromotion")
                                    {
                                        MinStay = "On booking of <b>" + objspecial.stay + " Night(s) <br/>" + objspecial.discount + "% Discount </b> for each night.";
                                    }

                                    else if (objspecial.type == "FNT" || objspecial.type == "FNN" || objspecial.type == "EB" || objspecial.type == "LM" || objspecial.type == "MN" || objspecial.type == "FN")
                                    {
                                        MinStay = "On booking of <b><i class='" + CurrencyClass + "'  style='font-size: 11px;'></i>" + NumberWithComma(Math.Round((Decimal)(Convert.ToDecimal(objspecial.discount)), 2, MidpointRounding.AwayFromZero)) + " Discount.</b>.";
                                    }
                                }
                            }
                        }
                        #endregion
                        objRooms.Add(new
                        {
                            objRoomType.RoomTypeName,
                            objRoomType.RoomTypeId,
                            objRoomType.RoomDescription,
                            objRoomType.RoomDescriptionId,
                            objRoomType.AvailCount,
                            objRoomType.SharingBedding,
                            objRoomType.Total,
                            Cancelation = HotelBookingHelperDoTW.GetCancellaions(objRoomType, Supplier),
                            FreeCancel = new DateTime(date.Year, date.Month, date.Day, 00, 00, 00).AddHours(-objMarkupsAndTaxes.TimeGap).AddSeconds(-1).ToString("dd/MM/yyy hh:mm", CultureInfo.CurrentCulture) + " PM",
                            Refundable,
                            MinStay,
                            objRoomType.tariffNotes,
                            RoomAmt = NumberWithComma((Decimal)ExchangeRateManger.RoomPrice(objRoomType.Charges, objGroup.Name.Split('_')[0], 1, out NettAmountInINR, out NettToolTip, out tooltip)),
                            RoomAmtToolTip = NettToolTip,
                            NettRoomAmt = NumberWithComma((Decimal)NettAmountInINR),
                            NettToolTip = NettToolTip,

                        });
                    }
                    #endregion
                    DefaultRate += ExchangeRateManger.RoomPrice(objOccupancy.Rooms[0].Charges, objGroup.Name.Split('_')[0], 1, out NettAmountInINR, out NettToolTip, out tooltip) * objOccupancy.RoomCount;
                    netDefaultRate += NettAmountInINR * objOccupancy.RoomCount;
                    RoomOccupancy.Add(new
                    {
                        OcuupancyNo = r,
                        OccupancyDetails = Rooms,
                        Rooms = objRooms,
                        objOccupancy.RoomCount,
                        noRooms = objGroup.RoomOccupancy.Select(d => d.RoomNo.Sum()).ToList().Sum(),
                        ClassRefund = ClassRefund
                    });
                    r++;
                }

                objRateGroup.Add(new
                {
                    RateGroupNo = "RateGroup " + (ListRateGroup.IndexOf(objGroup) + 1).ToString(),
                    Name = objGroup.Name.Replace("DoTW", "C").Replace("MGH", "B").Replace("HotelBeds", "A").Replace("GTA", "F"),
                    DefaultRate,
                    netDefaultRateTip = CUT.DataLayer.TooltipManager.GetCurencyTip(DefaultRate),
                    objGroup.AvailToken,
                    RoomOccupancy = RoomOccupancy,

                });
            }

            return objRateGroup;
        }


        #endregion

        #region Booking Hotel Details
        public static object GetCurrentHotel(CommonLib.Response.CommonHotelDetails objHotelDetails)
        {
            CUT.Models.CommonHotels objCommonHotels = (CUT.Models.CommonHotels)HttpContext.Current.Session["Common"];
            DateTime Checkin = Convert.ToDateTime(objCommonHotels.DisplayRequest.CheckIn.ToString(), new System.Globalization.CultureInfo("en-GB"));
            DateTime Checkout = Convert.ToDateTime(objCommonHotels.DisplayRequest.CheckOut.ToString(), new System.Globalization.CultureInfo("en-GB"));
            string CheckIn = Checkin.ToString("dd/MM/yyyy");
            string CheckOut = Checkout.ToString("dd/MM/yyyy");
            double NoDays = Convert.ToInt32((Checkout - Checkin).TotalDays);

            object Details = new
            {
                HotelName = objHotelDetails.HotelName,
                Address = objHotelDetails.Address,
                Checkin = CheckIn,
                CheckOut = CheckOut,
                NoDays = NoDays,
            };
            return Details;

        }
        #endregion

        #region Genrate booking Details
        public static List<object> GenerateDisplayRequest(CommonLib.Response.CommonHotelDetails objHotelDetails, string HotelId, string CutCode, string Supplier, List<string> RoomCodes, List<string> RoomDescriptionId, List<float> _CutPrice, List<string> _noRooms)
        {
            List<object> arrPax = new List<object>();
            List<CommonLib.Response.RoomType> ListRoomType = GetRoomPrice.ListRate(Supplier, HotelId, objHotelDetails.HotelId, RoomCodes, RoomDescriptionId, _CutPrice);
            List<CommonLib.Response.HotelOccupancy> List = CUT.Common.ParseCommonResponse.GetSearchOccupancy();
            foreach (var Search in List)
            {
                arrPax.Add(new
                {
                    ROOMID = ListRoomType[List.IndexOf(Search)].RoomTypeId,
                    RoomName = ListRoomType[List.IndexOf(Search)].RoomTypeName,
                    ROOMDescID = ListRoomType[List.IndexOf(Search)].RoomDescriptionId,
                    RoomDescription = ListRoomType[List.IndexOf(Search)].RoomDescription,
                    RoomNo = List.IndexOf(Search) + 1,
                    AdultCout = Search.AdultCount,
                    ChilldCount = Search.ChildCount,
                    ChildAges = Search.ChildAges,
                    Cancelation = HotelBookingHelperDoTW.GetCancellaions(ListRoomType[List.IndexOf(Search)], Supplier),
                    TarrifNote = ListRoomType[List.IndexOf(Search)].tariffNotes,

                });

            }
            return arrPax;
        }
        #endregion


        #region Right Contents
        //public static object GetRateBrackups(string HotelCode, string Supplier,string RoomID,string RoomDescID,float _CutPrice)
        public static object GetRateBrackups(string HotelCode, string Supplier)
        {
            StringBuilder sb = new StringBuilder();
            CUT.Models.CommonHotels objCommonHotels = (CUT.Models.CommonHotels)HttpContext.Current.Session["Common"];
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            string PreferedCurrency = objGlobalDefault.Currency;
            List<CommonLib.Response.HotelOccupancy> List_Occupancy = new List<CommonLib.Response.HotelOccupancy>();
            List<Room> ListRoom = objCommonHotels.CommonHotelDetails.Where(d => d.HotelId == HotelCode).FirstOrDefault().Room;
            List_Occupancy = ParseCommonResponse.GetSearchOccupancy();
            List<CommonLib.Response.GSTdetails> GstDetail = new List<GSTdetails>();
            List<CommonLib.Response.ServiceCharge> List_Charges = new List<ServiceCharge>();

            List<object> RateBrackups = new List<object>();

            if (objCommonHotels.CommonHotelDetails.Where(d => d.HotelId == HotelCode).FirstOrDefault().RateBreakups != null)
            {

                string CurrencyClass = ExchangeRateManger.GetCurrency();
                string PayableTooltip = "";
                int r = 0;
                float Total = 0;
                List<object> RateBrackupDetails = new List<object>();
                List<object> Details = new List<object>();
                foreach (RateBreakup objRate in objCommonHotels.CommonHotelDetails.Where(d => d.HotelId == HotelCode).FirstOrDefault().RateBreakups.ToList()/*.Where(br => br.RoomID == RoomID && br.RoomDescpID == RoomDescID && Math.Round(br.ActuatlRate, 2, MidpointRounding.AwayFromZero) == Math.Round(_CutPrice, 2, MidpointRounding.AwayFromZero))*/)
                {
                    float RoomPrice = 0;
                    string Tooltip = "";
                    string NumbWithComma = "";
                    string CurencyTip = "";
                    List<object> ListNights = new List<object>();
                    foreach (date Night in objRate.dates)
                    {
                        decimal NigtRate = Convert.ToDecimal(Night.Total);
                        RoomPrice += Night.Total;
                        Total += Night.Total;
                        ListNights.Add(new
                        {
                            Date = Night.datetime,
                            NightRateTip = TooltipManager.GetCurencyTip(objGlobalDefault.ExchangeRate, PreferedCurrency, Night.Total),
                            NightRate = NumberWithComma(NigtRate),

                        });
                    }

                    #region GST Details
                    if (objRate.charges.GSTdetails != null)
                        foreach (CommonLib.Response.GSTdetails objGst in objRate.charges.GSTdetails)
                        {
                            Total += objGst.Amount;
                            GstDetail.Add(objGst);
                        }
                    #endregion

                    CurencyTip = TooltipManager.GetCurencyTip(objGlobalDefault.ExchangeRate, PreferedCurrency, RoomPrice);
                    string Curency = NumberWithComma(Math.Round(Convert.ToDecimal(RoomPrice), 2, MidpointRounding.AwayFromZero));
                    List_Charges.Add(objRate.charges);
                    r++;

                    RateBrackupDetails.Add(new
                    {
                        RoomName = objRate.RoomName,
                        MealPlan = objRate.MealPlan,
                        CurencyTip = CurencyTip,
                        Curency = Curency,
                        RoomPrice = RoomPrice,
                        Tooltip = Tooltip,
                        NigtRate = ListNights,
                        NumbWithComma = NumbWithComma,
                        Charges = List_Charges,
                    });
                }

                PayableTooltip = TooltipManager.GetCurencyTip(objGlobalDefault.ExchangeRate, PreferedCurrency, Total);
                string TotalPayable = NumberWithComma(Convert.ToDecimal(Total));
                HttpContext.Current.Session["PayableAmount"] = NumberWithComma(Math.Round((Decimal)(Total / 1.00), 2, MidpointRounding.AwayFromZero));

                RateBrackups.Add(new
                {
                    List_Occupancy = List_Occupancy,
                    CurrencyClass = CurrencyClass,
                    PayableTooltip = PayableTooltip,
                    TotalPayable = TotalPayable,
                    RateBrackupDetails = RateBrackupDetails,
                });
            }
            return RateBrackups;
        }
        #endregion


        #region Get Images
        //public static string GelImageSlider(string HotelCode)
        //{

        //    CUT.Models.CommonHotels objCommonHotels = new CUT.Models.CommonHotels();
        //    objCommonHotels = (CUT.Models.CommonHotels)HttpContext.Current.Session["Common"];
        //    CommonLib.Response.CommonHotelDetails objHotelDetail = objCommonHotels.CommonHotelDetails.Where(d => d.HotelId == HotelCode).FirstOrDefault();
        //    // BuildMyString.com generated code. Please enjoy your string responsibly.
        //    if (objHotelDetail.Supplier == "HotelBeds")
        //    {
        //        DataTable dtHotelImage = new DataTable();
        //        CUT.BL.DBHelper.DBReturnCode retCode = DataLayer.HotelManager.Get_Hotel_Image(HotelCode.Split('_')[0], out dtHotelImage);
        //        if (retCode == CUT.BL.DBHelper.DBReturnCode.SUCCESS)
        //        {
        //            foreach (DataRow row in dtHotelImage.Rows)
        //            {
        //                objHotelDetail.Image.Add(new Image
        //                {
        //                    Count = 1,
        //                    IsDefault = true,
        //                    Url = String.Format("{0}/{1}", "http://www.hotelbeds.com/giata", row["ImagePath"].ToString()),
        //                });
        //            }
        //        }
        //    }
        //    else if (objHotelDetail.Supplier == "MGH" || objHotelDetail.Supplier == "GRN")
        //    {
        //        List<Image> ListImage = new List<Image>();
        //        ListImage = HotelDataManager.GetImages(objHotelDetail.Supplier, HotelCode);
        //        foreach (Image objImage in ListImage)
        //        {
        //            objHotelDetail.Image.Add(new Image
        //            {
        //                Count = 1,
        //                IsDefault = true,
        //                Title = objImage.Title,
        //                Url = objImage.Url,
        //            });
        //        }
        //    }



        //    StringBuilder sb = new StringBuilder();
        //    #region test
        //    sb.Append("<div id=\"carousel\" class=\"carousel slide\" data-ride=\"carousel\">");
        //    sb.Append("  <!-- Wrapper for slides -->");
        //    sb.Append("  <div class=\"carousel-inner\" role=\"listbox\">");
        //    int i = 0;

        //    foreach (Image Image in objHotelDetail.Image)
        //    {
        //        if (Image.Title == "jpg")
        //            Image.Title = "";


        //        if (i == 0)
        //            sb.Append("<div class=\"item active\">");
        //        else
        //            sb.Append("<div class=\"item\">");
        //        sb.Append("<img src=\"" + Image.Url + "\" class=\"img-responsive\">");
        //        //sb.Append("<div class=\"carousel-caption\">" + Image.Title + "</div>");
        //        sb.Append("</div>");
        //        i++;
        //    }
        //    sb.Append("</div>");
        //    sb.Append("  <!-- Controls -->");
        //    sb.Append("  <a class=\"left carousel-control hiden-xs\" href=\"#carousel\" role=\"button\" data-slide=\"prev\">");
        //    sb.Append("    <span class=\"glyphicon glyphicon-chevron-left\" aria-hidden=\"true\"></span>");
        //    sb.Append("  </a>");
        //    sb.Append("  <a class=\"right carousel-control hiden-xs\" href=\"#carousel\" role=\"button\" data-slide=\"next\">");
        //    sb.Append("    <span class=\"glyphicon glyphicon-chevron-right\" aria-hidden=\"true\"></span>");
        //    sb.Append("  </a>");
        //    if (objHotelDetail.Supplier == "GTA")
        //    {
        //        sb.Append("<center> <span class='red'><b>Images from VFM Leonardo, Inc</b></span></center> ");
        //    }
        //    sb.Append("</div>");
        //    #endregion

        //    return sb.ToString();

        //}

        public static object GelImageSlider(string HotelCode)
        {

            CUT.Models.CommonHotels objCommonHotels = new CUT.Models.CommonHotels();
            objCommonHotels = (CUT.Models.CommonHotels)HttpContext.Current.Session["Common"];
            CommonLib.Response.CommonHotelDetails objHotelDetail = objCommonHotels.CommonHotelDetails.Where(d => d.HotelId == HotelCode).FirstOrDefault();
            // BuildMyString.com generated code. Please enjoy your string responsibly.
            if (objHotelDetail.Supplier == "HotelBeds")
            {
                DataTable dtHotelImage = new DataTable();
                CUT.BL.DBHelper.DBReturnCode retCode = DataLayer.HotelManager.Get_Hotel_Image(HotelCode.Split('_')[0], out dtHotelImage);
                if (retCode == CUT.BL.DBHelper.DBReturnCode.SUCCESS)
                {
                    foreach (DataRow row in dtHotelImage.Rows)
                    {
                        objHotelDetail.Image.Add(new Image
                        {
                            Count = 1,
                            IsDefault = true,
                            Url = String.Format("{0}/{1}", "http://www.hotelbeds.com/giata", row["ImagePath"].ToString()),
                        });
                    }
                }
            }
            else if (objHotelDetail.Supplier == "MGH" || objHotelDetail.Supplier == "GRN")
            {
                List<Image> ListImage = new List<Image>();
                ListImage = HotelDataManager.GetImages(objHotelDetail.Supplier, HotelCode);
                foreach (Image objImage in ListImage)
                {
                    objHotelDetail.Image.Add(new Image
                    {
                        Count = 1,
                        IsDefault = true,
                        Title = objImage.Title,
                        Url = objImage.Url,
                    });
                }
            }





            return objHotelDetail.Image;

        }
        #endregion

        public static string GetHotelInfo(CommonHotelDetails ObjCommonHotelDetails)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<div class=\"panel style2\">");
            sb.AppendLine("<h4 class=\"panel-title\">");
            sb.AppendLine("<a class=\"collapsed\" href=\"#tgg4\" data-toggle=\"collapse\" aria-expanded=\"false\">Description</a>");
            sb.AppendLine("</h4>");
            sb.AppendLine("<br />");
            sb.AppendLine("<div class=\"panel-collapse collapse\" id=\"tgg4\" aria-expanded=\"false\" style=\"height: 0px;\">");
            sb.AppendLine("<div class=\"panel-content\">");
            if (ObjCommonHotelDetails.Description != "")
                ObjCommonHotelDetails.Description = Regex.Replace(ObjCommonHotelDetails.Description, @"[\\-\\+\\^:,]", "");
            else
                ObjCommonHotelDetails.Description = "Not Available..";
            sb.AppendLine("<p>" + ObjCommonHotelDetails.Description + "</p>");
            sb.AppendLine("</div>");
            sb.AppendLine("</div>");
            sb.AppendLine("</div>");


            sb.AppendLine("<div class=\"panel style2\">");
            sb.AppendLine("<h4 class=\"panel-title\">");
            sb.AppendLine("<a href=\"#tgg5\" data-toggle=\"collapse\" aria-expanded=\"false\" class=\"collapsed\">Hotel Facilities</a>");
            sb.AppendLine("</h4>");
            sb.AppendLine("<div class=\"panel-collapse collapse\" id=\"tgg5\" aria-expanded=\"false\" style=\"height: 0px;\">");
            sb.AppendLine("<div class=\"panel-content\">");
            if (ObjCommonHotelDetails.Facility.Count > 0)
            {
                string text1 = "", text2 = "", text3 = "";
                for (int i = 0; i < ObjCommonHotelDetails.Facility.Count; i++)
                {
                    string text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(ObjCommonHotelDetails.Facility[i].Name);
                    if (ObjCommonHotelDetails.Facility[i].Name == "S" || ObjCommonHotelDetails.Facility[i].Name == "Y")
                        text = text + "*";
                    if (text.Length > 2)
                    {
                        if (i < 10)
                            text1 = text1 + "<li>" + text + "</li>";
                        else if (i < 20 && i >= 10)
                            text2 = text2 + "<li>" + text + "</li>";
                        else if (i < 30 && i >= 20)
                            text3 = text3 + "<li>" + text + "</li>";
                    }
                }
                sb.AppendLine("<div class=\"col-sm-12 col-md-12\">");
                sb.AppendLine("<ul class=\"circle\">");
                sb.AppendLine("" + text1 + "");
                sb.AppendLine("</ul>");
                sb.AppendLine("</div>");
                sb.AppendLine("<div class=\"col-sm-12 col-md-12\">");
                sb.AppendLine("<ul class=\"circle\">");
                sb.AppendLine("" + text2 + "");
                sb.AppendLine("</ul>");
                sb.AppendLine("</div>");
                sb.AppendLine("<div class=\"col-sm-12 col-md-12\">");
                sb.AppendLine("<ul class=\"circle\">");
                sb.AppendLine("" + text3 + "");
                sb.AppendLine("</ul>");
                sb.AppendLine("</div>");
                sb.AppendLine("<br/>");
                sb.AppendLine("<br/>");
                sb.AppendLine("<span><b>* marked facilities have extra fee.</b></span>");
            }
            sb.AppendLine("</div>");
            sb.AppendLine("</div>");
            sb.AppendLine("</div>");

            return sb.ToString();
        }

        //public static List<object> GetHotelInfo(CommonHotelDetails ObjCommonHotelDetails)
        //{
        //    List<object> Description = new List<object>();
        //    Description.Add(new
        //    {
        //        Description = ObjCommonHotelDetails.Description,
        //        Facility = ObjCommonHotelDetails.Facility,

        //    });
        //    return Description;
        //}
    }
}