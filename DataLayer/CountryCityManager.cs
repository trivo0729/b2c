﻿using CUTUK.BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CUTUK.DataLayer
{
    public class CountryCityManager
    {
        public static DBHelper.DBReturnCode GetCountry(out DataTable dtResult)
        {
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_HCityLoadAllCountry", out dtResult);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetCity(string country, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@Country", country);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_HCityLoadAllCityByCountry", out dtResult, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetCityCode(string Description, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@Description", Description);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_HCityLoadAllCityCode", out dtResult, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetNationalityCOR(out DataTable dtResult)
        {
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_NationalityCOR", out dtResult);
            return retCode;
        }
    }
}