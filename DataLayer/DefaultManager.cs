﻿using CUT.BL;
using CUT.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CUTUK.DataLayer
{
    public class DefaultManager
    {
        public static DBHelper.DBReturnCode UserLogin(string sUserName, string sPassword, out DataTable dtResult)
        {
            SqlParameter[] sqlParamsLogin = new SqlParameter[1];
            sqlParamsLogin[0] = new SqlParameter("@UserName", sUserName);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_B2C_CustomerLoginCheckUser", out dtResult, sqlParamsLogin);
            if (DBHelper.DBReturnCode.SUCCESS == retCode)
            {
                string sEncryptedPassword = CUT.Common.Cryptography.EncryptText(sPassword);
                HttpContext.Current.Session["LoginCustomer"] = null;

                dtResult = null;
                GlobalDefault objGlobalDefault;
                SqlParameter[] sqlParams = new SqlParameter[2];
                sqlParams[0] = new SqlParameter("@Email", sUserName);
                sqlParams[1] = new SqlParameter("@Password", sEncryptedPassword);
                retCode = DBHelper.GetDataTable("Proc_tbl_B2C_CustomerLoginLoad", out dtResult, sqlParams);
                DataTable firstTable = dtResult;

                if (DBHelper.DBReturnCode.SUCCESS == retCode)
                {
                    if (firstTable.Rows.Count > 0)
                    {
                        objGlobalDefault = new GlobalDefault();
                        objGlobalDefault.sid = Convert.ToInt64(firstTable.Rows[0]["sid"]);
                        //objGlobalDefault.uid = firstTable.Rows[0]["uid"].ToString();
                        //objGlobalDefault.UserType = firstTable.Rows[0]["UserType"].ToString();
                        objGlobalDefault.password = CUT.Common.Cryptography.DecryptText(firstTable.Rows[0]["Password"].ToString());
                        //objGlobalDefault.Designation = firstTable.Rows[0]["Designation"].ToString();
                        //objGlobalDefault.ContactID = Convert.ToInt64(firstTable.Rows[0]["ContactID"]);
                        //objGlobalDefault.RoleID = Convert.ToInt64(firstTable.Rows[0]["RoleID"]);
                        objGlobalDefault.UserName = firstTable.Rows[0]["UserName"].ToString();
                        objGlobalDefault.Email = firstTable.Rows[0]["Email"].ToString();
                        objGlobalDefault.Mobile = firstTable.Rows[0]["Contact1"].ToString();
                        objGlobalDefault.Phone = firstTable.Rows[0]["Contact2"].ToString();
                        objGlobalDefault.Address = firstTable.Rows[0]["Address"].ToString();
                        objGlobalDefault.B2C_Id = firstTable.Rows[0]["B2C_Id"].ToString();
                        objGlobalDefault.City = firstTable.Rows[0]["CityId"].ToString();
                        //CurrencyClass = firstTable.Rows[0]["CurrencyCode"].ToString();
                        HttpContext.Current.Session["LoginCustomer"] = objGlobalDefault;
                    }
                }
            }
            else
            {
                retCode = DBHelper.DBReturnCode.SUCCESSNORESULT;
            }


            return retCode;
        }

        public static DBHelper.DBReturnCode GetCountry(out DataTable dtResult)
        {
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_HCityLoadAllCountry", out dtResult);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetCity(string country, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@Country", country);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_HCityLoadAllCityByCountry", out dtResult, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode ConfirmationDetails(string ReservationID, out DataTable dtResult)
        {
            SqlParameter[] SQLParams = new SqlParameter[1];
            SQLParams[0] = new SqlParameter("@ReservationID", ReservationID);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_ConfirmationDetails", out dtResult, SQLParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode B2CUserLogin(string sUserName, string sPassword)
        {
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            try
            {
                HttpContext.Current.Session["LoginCustomer"] = null;
                DataTable dtResult = null;
                SqlParameter[] sqlParams = new SqlParameter[2];
                sqlParams[0] = new SqlParameter("@Email", sUserName);
                string sEncryptedPassword = CUT.Common.Cryptography.EncryptText(sPassword);
                sqlParams[1] = new SqlParameter("@Password", sEncryptedPassword);
                retCode = DBHelper.GetDataTable("Proc_tbl_B2C_CustomerLoginLoad", out dtResult, sqlParams);

                DBHelper.DBReturnCode retcode = CUTUK.DataLayer.MarkupTaxManager.AssignMarkupTax();

                //GlobalDefault objGlobalDefaultCustomer = (GlobalDefault)HttpContext.Current.Session["LoginCustomer"];
                GlobalDefault objGlobalDefaultCustomer = new GlobalDefault();
                objGlobalDefaultCustomer.Mobile = dtResult.Rows[0]["Contact1"].ToString();
                objGlobalDefaultCustomer.Phone = dtResult.Rows[0]["Contact1"].ToString();
                objGlobalDefaultCustomer.Email = dtResult.Rows[0]["Email"].ToString();
                objGlobalDefaultCustomer.Address = dtResult.Rows[0]["Address"].ToString();
                objGlobalDefaultCustomer.City = dtResult.Rows[0]["CityId"].ToString();
                objGlobalDefaultCustomer.UserName = sUserName;
                objGlobalDefaultCustomer.password = sPassword;
                objGlobalDefaultCustomer.B2C_Id = dtResult.Rows[0]["B2C_Id"].ToString();
                objGlobalDefaultCustomer.sid = Convert.ToInt64(dtResult.Rows[0]["Sid"]);
                HttpContext.Current.Session["LoginCustomer"] = objGlobalDefaultCustomer;

                GlobalDefault objGlobalDefault = new GlobalDefault();
                objGlobalDefault.Mobile = dtResult.Rows[0]["Contact1"].ToString();
                objGlobalDefault.Phone = dtResult.Rows[0]["Contact1"].ToString();
                objGlobalDefault.Email = dtResult.Rows[0]["Email"].ToString();
                objGlobalDefault.Address = dtResult.Rows[0]["Address"].ToString();
                objGlobalDefault.City = dtResult.Rows[0]["CityId"].ToString();
                objGlobalDefault.UserName = sUserName;
                objGlobalDefault.password = sPassword;
                objGlobalDefault.B2C_Id = dtResult.Rows[0]["B2C_Id"].ToString();
                objGlobalDefault.sid = Convert.ToInt64(dtResult.Rows[0]["Sid"]);
                HttpContext.Current.Session["LoginUsers"] = objGlobalDefault;
                retCode = DBHelper.DBReturnCode.SUCCESS;
                //}
                //else
                //    retCode = DBHelper.DBReturnCode.EXCEPTION;
            }
            catch (Exception ex)
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
            }
            return retCode;
        }
    }
}