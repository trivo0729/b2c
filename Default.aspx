﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CUTUK.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/jquery-ui.css" rel="stylesheet" />
    <script src="js/jquery-2.1.3.min.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="scripts/HotelSearch.js"></script>
    <script src="scripts/B2CCustomerLogin.js"></script>
    <script src="scripts/login.js"></script>
    <script src="scripts/CountryCityCode.js"></script>
    <script src="scripts/flightAuto.js"></script>
    <script src="scripts/flightSearch.js"></script>
    <link href="css/multiple-select.css" rel="stylesheet" />
    <script src="css/jquery.multiple.select.js"></script>
    <script src="js/modal.js"></script>
    <script src="js/moment.js"></script>
    <script type="text/javascript">

        var chkinDate;
        var chkinMonth;
        var chkoutDate;
        var chkoutMonth;
        //tpj.noConflict();

        $(document).ready(function () {
            $('#sel_Airlines').change(function () {
                console.log($(this).val());
            }).multipleSelect({
                width: '100%'
            });
            //.....................................................................................................//
            $("#txtCity").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "../HotelHandler.asmx/GetDestinationCode",
                        data: "{'name':'" + document.getElementById('txtCity').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            response(result);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    debugger;
                    $('#hdnDCode').val(ui.item.id);
                }
            });
            $("#txt_HotelName").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "../HotelHandler.asmx/GetHotel",
                        data: "{'name':'" + $('#txt_HotelName').val() + "','destination':'" + $('#hdnDCode').val() + "'}",
                        dataType: "json",
                        success: function (data) {
                            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            response(result);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    $('#hdnHCode').val(ui.item.id);
                }
            });
        });
    </script>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800,700,600' rel='stylesheet' type='text/css' />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="flexslider photo-gallery style4">
        <ul class="slides">
            <li>
                <img src="images/uploads/revslider1/holiday/background1.jpg" alt="" class="img-responsive" /></li>
            <li>
                <img src="images/uploads/revslider1/holiday/background2.jpg"
                    alt="" class="img-responsive" /></li>
            <li>
                <img src="images/uploads/revslider1/holiday/background3.jpg" alt="" class="img-responsive" /></li>

        </ul>
    </div>
    <section id="content" style="background-color: white;">
        <div class="search-box-wrapper">
            <div class="search-box container">
                <ul class="search-tabs clearfix">
                    <li class="active"><a href="#flights-tab" data-toggle="tab">FLIGHTS</a></li>
                    <li><a href="#hotels-tab" data-toggle="tab">HOTELS</a></li>
                    <li><a href="Packages.aspx">PACKAGES</a></li>
                    <%--<li><a href="#flight-and-hotel-tab" data-toggle="tab">FLIGHT &amp; HOTELS</a></li>--%>
                    <%--<li><a href="#cars-tab" data-toggle="tab">CARS</a></li>
                        <li><a href="#cruises-tab" data-toggle="tab">CRUISES</a></li>--%>
                    <%--<li><a href="#flight-status-tab" data-toggle="tab">ACTIVITIES</a></li>--%>
                    <%--<li><a href="#online-checkin-tab" data-toggle="tab">ONLINE CHECK IN</a></li>--%>
                </ul>
                <div class="visible-mobile">
                    <ul id="mobile-search-tabs" class="search-tabs clearfix">
                        <li class="active"><a href="#flights-tab">FLIGHTS</a></li>
                        <li><a href="#hotels-tab">HOTELS</a></li>
                        <li><a href="Packages.aspx">PACKAGES</a></li>
                        <%--  <li><a href="#flight-and-hotel-tab">ACTIVITIES</a></li>--%>
                        <%--  <li><a href="#cars-tab">CARS</a></li>
                            <li><a href="#cruises-tab">CRUISES</a></li>--%>
                        <%--<li><a href="#flight-status-tab">ACTIVITIES</a></li>--%>
                        <%-- <li><a href="#online-checkin-tab">ONLINE CHECK IN</a></li>--%>
                    </ul>
                    -
                </div>

                <div class="search-tab-content">
                    <div class="tab-pane fade" id="hotels-tab">
                        <form>
                            <div class="row">
                                <div class="col-md-4">
                                    <span class="text-left" style="color: #838383">Where do you want to go?</span>
                                    <br />
                                    <div class="input-group" style="width: 100%">
                                        <input type="text" style="background-color: none" placeholder="Enter City Here" id="txtCity" class="form-control" />
                                    </div>
                                    <input type="hidden" id="hdnDCode" />
                                </div>
                                <div class="col-md-3">
                                    <span class="text-left" style="color: #2d3e52">Check-In:</span>
                                    <div class="datepicker-wrap">
                                        <input type="text" name="date_from" id="datepicker_HotelCheckin" style="cursor: pointer" class="form-control" placeholder="Check In" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <span class="text-left" style="color: #2d3e52">Check-Out:</span>
                                    <div class="datepicker-wrap">
                                        <input type="text" name="date_to" id="datepicker_HotelCheckout" style="cursor: pointer" class=" form-control" placeholder="Check Out" />
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <span class="text-left" style="color: #838383">Total Nights:</span>
                                    <br />
                                    <select id="Select_TotalNights" class="form-control">
                                        <option selected="selected" value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                    </select>
                                </div>
                            </div>
                            <br />

                            <br id="br1" style="display: none" />
                            <div class="row">
                                <div class="col-md-4">
                                    <span class="text-left" style="color: #838383">Nationality:</span>
                                    <br />
                                    <select id="Select_Nationality" class="form-control">
                                    </select>
                                </div>

                                <div class="col-md-3" style="display: none">
                                    <span class="text-left" style="color: #838383">Star Rating:</span>
                                    <br />
                                    <select id="Select_StarRating" class="form-control"></select>
                                </div>

                                <div class="col-md-5">
                                    <span class="text-left" style="color: #838383">Hotel Name:</span>
                                    <br />
                                    <input class="form-control" type="text" placeholder="Enter Hotel Here" id="txt_HotelName" />
                                    <input type="hidden" id="hdnHCode" />
                                </div>

                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-2">
                                    <span class="text-left" style="color: #838383">Rooms:</span>
                                    <select id="Select_Rooms" class="form-control">
                                        <option selected="selected" value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <span class="text-left" style="color: #838383">Room Type:</span>
                                    <input type="text" id="spn_RoomType1" style="cursor: default;" disabled="disabled" class="form-control" value="Room 1" />
                                </div>
                                <div class="col-md-2">
                                    <span class="text-left" style="color: #838383">Adults:</span>
                                    <select id="Select_Adults1" class="form-control">
                                        <option value="1">1</option>
                                        <option value="2" selected="selected">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <span class="text-left" style="color: #838383">Child:</span>
                                    <select id="Select_Children1" class="form-control">
                                        <option selected="selected" value="0">0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <span id="Span_AgeChildFirst1" class="text-left" style="display: none; color: #838383">Age of Child 1:</span>
                                    <select id="Select_AgeChildFirst1" class="form-control" style="display: none">
                                        <option selected="selected" value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <span id="Span_AgeChildSecond1" class="text-left" style="display: none; color: #838383">Age of Child 2:</span>
                                    <select id="Select_AgeChildSecond1" class="form-control" style="display: none;">
                                        <option selected="selected" value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                    </select>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-2">
                                    <span id="Span_RoomType2" class="text-left" style="display: none; color: #838383">Room Type:</span>
                                    <input type="text" id="spn_RoomType2" class="form-control" disabled="disabled" value="Room 2" style="display: none; cursor: default;" />
                                </div>
                                <div class="col-md-2">
                                    <span id="Span_Adults2" class="text-left" style="display: none; color: #838383">Adults:</span>
                                    <select id="Select_Adults2" class="form-control" style="display: none">
                                        <option value="1">1</option>
                                        <option value="2" selected="selected">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <span id="Span_Children2" class="text-left" style="display: none; color: #838383">Child:</span>
                                    <select id="Select_Children2" class="form-control" style="display: none">
                                        <option selected="selected" value="0">0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <span id="Span_AgeChildFirst2" class="text-left" style="display: none; color: #838383">Age of Child 1:</span>
                                    <select id="Select_AgeChildFirst2" class="form-control" style="display: none">
                                        <option selected="selected" value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <span id="Span_AgeChildSecond2" class="text-left" style="display: none; color: #838383">Age of Child 2:</span>
                                    <select id="Select_AgeChildSecond2" class="form-control" style="display: none;">
                                        <option selected="selected" value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                    </select>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-2">
                                    <span id="Span_RoomType3" class="text-left" style="display: none; color: #838383">Room Type:</span>
                                    <input type="text" id="spn_RoomType3" class="form-control" disabled="disabled" value="Room 3" style="display: none; cursor: default;" />
                                </div>
                                <div class="col-md-2">
                                    <span id="Span_Adults3" class="text-left" style="display: none; color: #838383">Adults:</span>
                                    <select id="Select_Adults3" class="form-control" style="display: none">
                                        <option value="1">1</option>
                                        <option value="2" selected="selected">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <span id="Span_Children3" class="text-left" style="display: none; color: #838383">Child:</span>
                                    <select id="Select_Children3" class="form-control" style="display: none">
                                        <option selected="selected" value="0">0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <span id="Span_AgeChildFirst3" class="text-left" style="display: none; color: #838383">Age of Child 1:</span>
                                    <select id="Select_AgeChildFirst3" class="form-control" style="display: none">
                                        <option selected="selected" value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <span id="Span_AgeChildSecond3" class="text-left" style="display: none; color: #838383">Age of Child 2:</span>
                                    <select id="Select_AgeChildSecond3" class="form-control" style="display: none;">
                                        <option selected="selected" value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                    </select>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-2">
                                    <span id="Span_RoomType4" class="text-left" style="display: none; color: #838383">Room Type:</span>
                                    <input type="text" id="spn_RoomType4" class="form-control" value="Room 4" disabled="disabled" style="display: none; cursor: default;" />
                                </div>
                                <div class="col-md-2">
                                    <span id="Span_Adults4" class="text-left" style="display: none; color: #838383">Adults:</span>
                                    <select id="Select_Adults4" class="form-control" style="display: none">
                                        <option value="1">1</option>
                                        <option value="2" selected="selected">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <span id="Span_Children4" class="text-left" style="display: none; color: #838383">Child:</span>
                                    <select id="Select_Children4" class="form-control" style="display: none">
                                        <option selected="selected" value="0">0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <span id="Span_AgeChildFirst4" class="text-left" style="display: none; color: #838383">Age of Child 1:</span>
                                    <select id="Select_AgeChildFirst4" class="form-control" style="display: none">
                                        <option selected="selected" value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <span id="Span_AgeChildSecond4" class="text-left" style="display: none; color: #838383">Age of Child 2:</span>
                                    <select id="Select_AgeChildSecond4" class="form-control" style="display: none;">
                                        <option selected="selected" value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2" style="float: right">
                                <button type="button" class="button btn-medium sky-blue1" id="btn_Search" onclick="Redirect();">SEARCH</button>
                            </div>
                            <br />
                            <br />

                        </form>
                    </div>
                    <div class="tab-pane fade active in" id="flights-tab">
                        <form>
                            <div class="row">
                                <div class="col-sm-4">
                                    <h5 style="color: black; margin-top: 0px">Book Domestic &amp; International Flight Tickets</h5>
                                </div>
                                <div>
                                    <div class="col-sm-1">
                                        <input type="radio" name="JourneyType" id="radio01" value="O" title="One Way" onclick="OneWayRadioFunc()" checked="" />
                                        <label for="radio01">
                                            <span></span>
                                            <span style="color: black">One Way</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-1">
                                        <input type="radio" name="JourneyType" id="radio02" value="R" title="Round Way" onclick="RoundWayRadioFunc()" />
                                        <label for="radio02">
                                            <span></span>
                                            <span style="color: black">Round Way</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="radio" name="JourneyType" id="radio03" value="M" title="Multiple Destinations" onclick="MultiWayRadioFunc()" />
                                        <label for="radio03">
                                            <span></span>
                                            <span style="color: black">Multiple Destinations</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-2" style="display: none">
                                        <input type="radio" name="JourneyType" id="radio04" value="M" title="Multi Way" onclick="CalendarFareRadioFunc()" />
                                        <label for="radio04">
                                            <span></span>
                                            <span style="color: black">Calendar Fare</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-2" style="display: none">
                                        <input type="radio" name="JourneyType" id="radioAdvance" value="M" title="Multi Way" onclick="OneWayRadioFunc()" />
                                        <label for="radioAdvance">
                                            <span></span>
                                            <span style="color: black">Advance Search</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-2" style="float: left">
                                        <input type="checkbox" name="NonStop" id="chkNonStop" value="NonStop" checked="" title="Non Stop Flights" />
                                        <label for="chkNonStop">
                                            <span></span>
                                            <span style="color: black">Non Stop Flights</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="display: none">
                                <div class="col-sm-4">
                                </div>
                                <div>
                                    <div class="col-sm-2" style="display: none">
                                        <input type="radio" name="RoundWay" id="rdb_Normal" value="O" style="display: none" class="RoundWay" title="Normal Return" checked="" style="display: inline-block;" />
                                        <label for="rdb_Normal" class="RoundWay" style="display: none">
                                            <span></span>
                                            <span style="color: black">Normal Return</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-2" style="display: none">
                                        <input type="radio" name="RoundWay" id="rdb_SP" value="R" style="display: none" class="RoundWay" title="Special Return" style="display: inline-block;" />
                                        <label for="rdb_SP" class="RoundWay" style="display: none">
                                            <span></span>
                                            <span style="color: black">LCC Special Returns</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-2" style="display: none">
                                        <input type="radio" name="RoundWay" id="rdb_GDS" value="M" style="display: none" class="RoundWay" title="Special GDS Return" style="display: inline-block;" />
                                        <label for="rdb_GDS" class="RoundWay" style="display: none">
                                            <span></span>
                                            <span style="color: black">GDS Special Returns</span>
                                        </label>
                                    </div>
                                    <%--<div class="col-sm-2" style="float: left">
                                        <input type="checkbox" name="NonStop" id="chkNonStop" value="NonStop" checked="" class="JourneyType" title="Non Stop Flights" />
                                        <label for="chkNonStop">
                                            <span></span>
                                            <span style="color: black">Non Stop Flights</span>
                                        </label>
                                    </div>--%>
                                </div>
                            </div>

                            <div id="GeneralSearch">
                                <div class="row">
                                    <div class="col-md-8" id="fromToOnwardDateDiv">
                                        <div class="form-group row"></div>
                                    </div>

                                    <div class="col-md-4" style="display: none" id="returnDateDivId">
                                        <br />
                                        <div class="form-group row">
                                            <div class="col-md-8">
                                                <span class="text-left" style="color: #838383">Return Date</span>
                                                <div class="datepicker-wrap">
                                                    <input id="datepicker_Return" type="text" name="date_from" class="form-control" placeholder="return On" />
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <br />
                                <div>
                                    <div id="addDestinationDivId" class="row white" style="float: right; padding-right: 300px; display: none; margin-bottom: 20px;">
                                        <img src="../images/Bond/plus.png" id="plus" alt="plus" onclick="addDestination()" title="Add Destination" style="width: 25px" />
                                    </div>
                                    <div id="removeLastDestnDivId" class="row white" style="float: right; padding-right: 300px; display: none; margin-bottom: 20px;">

                                        <img src="../images/Bond/minus.png" id="minus" alt="minus" onclick="removeLastDestn()" title="Remove Destination" style="width: 30px" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <span class="text-left" style="color: #838383">Airlines</span>
                                                <select id="sel_Airlines" class="form-control" multiple="multiple">
                                                    <option value="SG">Spice Jet</option>
                                                    <option value="6E">Indigo</option>
                                                    <option value="G8">Go Air</option>
                                                    <option value="G9">Air Arabia</option>
                                                    <option value="FZ">Fly Dubai</option>
                                                    <option value="IX">Air India Express</option>
                                                    <option value="AK">Air Asia</option>
                                                    <option value="LB">Air Costa</option>
                                                    <option value="UK">Air Vistara</option>
                                                    <option value="UK">Air Vistara</option>
                                                    <option value="AI">Air India</option>
                                                    <option value="9W">Jet Airways</option>
                                                    <option value="S2">JetLite</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <span class="text-left" style="color: #838383">Class</span>
                                                <select class="form-control" id="sel_Class">
                                                    <option selected="selected" value="1">All</option>
                                                    <option value="2">Economy</option>
                                                    <option value="3">Premium Economy</option>
                                                    <option value="4">Business</option>
                                                    <option value="5">Premium Business</option>
                                                    <option value="6">First</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span class="text-left" style="color: #838383">Adults</span>
                                                <select class="form-control" id="sel_Adults">
                                                    <option selected="selected" value="1">01</option>
                                                    <option value="2">02</option>
                                                    <option value="3">03</option>
                                                    <option value="4">04</option>
                                                    <option value="">05</option>
                                                    <option value="1">06</option>
                                                    <option value="2">07</option>
                                                    <option value="3">08</option>
                                                    <option value="4">09</option>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <span class="text-left" style="color: #838383">Kids</span>
                                                <select class="form-control" id="sel_Child">
                                                    <option selected="selected" value="0">00</option>
                                                    <option value="1">01</option>
                                                    <option value="2">02</option>
                                                    <option value="3">03</option>
                                                    <option value="4">04</option>
                                                    <option value="">05</option>
                                                    <option value="1">06</option>
                                                    <option value="2">07</option>
                                                    <option value="3">08</option>
                                                    <option value="4">09</option>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <span class="text-left" style="color: #838383">Infants</span>
                                                <select class="form-control" id="sel_Infant">
                                                    <option selected="selected" value="0">00</option>
                                                    <option value="1">01</option>
                                                    <option value="2">02</option>
                                                    <option value="3">03</option>
                                                    <option value="4">04</option>
                                                    <option value="">05</option>
                                                    <option value="1">06</option>
                                                    <option value="2">07</option>
                                                    <option value="3">08</option>
                                                    <option value="4">09</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-xs-3">
                                            </div>
                                            <div class="col-xs-6 pull-right">
                                                <input type="button" class="button btn-medium sky-blue1" value="Search" onclick="SearchFlight()" />
                                            </div>
                                            <br />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="CalendarSearch" style="display: none">
                                <br />
                                <div class="row">
                                    <div class="col-md-3">
                                        <span class="text-left" style="color: #838383">From</span>
                                        <br />
                                        <input type="text" id="CalenderFrom" class="form-control" />
                                        <input type="hidden" id="hdCalenderFromCode" />
                                    </div>
                                    <div class="col-md-3">
                                        <span class="text-left" style="color: #838383">To</span>
                                        <br />
                                        <input type="text" id="CalenderTo" class="form-control" />
                                        <input type="hidden" id="hdCalenderToCode" />
                                    </div>
                                    <div class="col-md-3">
                                        <span class="text-left" style="color: #838383">Month & Year</span>
                                        <div class="datepicker-wrap">
                                            <input id="dt_CalendarMonthYear" type="text" name="date_from" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-3">
                                        <span class="text-left" style="color: #838383">Preferred Carrier</span>
                                        <select class="form-control" id="PreferredCarrier">
                                            <option value="0">select</option>
                                            <option value="SG">Spice Jet</option>
                                            <option value="6E">Indigo</option>
                                            <option value="G8">Go Air</option>
                                            <option value="G9">Air Arabia</option>
                                            <option value="FZ">Fly Dubai</option>
                                            <option value="IX">Air India Express</option>
                                            <option value="AK">Air Asia</option>
                                            <option value="LB">Air Costa</option>
                                            <option value="GDS">GDS</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <span class="text-left" style="color: #838383">Class</span>
                                        <select class="form-control" id="sel_CalendarClass">
                                            <option value="1">All</option>
                                            <option value="2">Economy</option>
                                            <option value="3">Premium Economy</option>
                                            <option value="4">Business</option>
                                            <option value="5">Premium Business</option>
                                            <option value="6">First</option>
                                        </select>
                                    </div>
                                </div>
                                <br />
                                <input type="button" class="button btn-medium sky-blue1" id="btn_CalendarSearch" style="float: right" value="Search" onclick="CalendarSearchFlight()" />
                                <br />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="section white-bg">
            <div class="container">
                <div class="text-center description block">
                    <h1>Offers And Deals</h1>
                    <%--<p>Nunc cursus libero purus ac congue ar lorem cursus ut sed pulvinar massa idend porta nequetiam</p>--%>
                </div>
                <div class="tour-packages row add-clearfix image-box">
                    <div class="col-sm-6 col-md-4">
                        <article class="box animated" data-animation-type="fadeInLeft">
                            <figure>
                                <a href="#">
                                    <img src="images/tour/packages/3-col/1.jpg" alt=""></a>
                                <figcaption>
                                    <span class="price">$257</span>
                                    <h2 class="caption-title">Paris Tour</h2>
                                </figcaption>
                            </figure>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <article class="box animated" data-animation-type="fadeInDown">
                            <figure>
                                <a href="#">
                                    <img src="images/tour/packages/3-col/2.jpg" alt=""></a>
                                <figcaption>
                                    <span class="price">$122</span>
                                    <h2 class="caption-title">Rome City Tour</h2>
                                </figcaption>
                            </figure>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <article class="box animated" data-animation-type="fadeInRight">
                            <span class="discount"><span class="discount-text">10% Discount</span></span>
                            <figure>
                                <a href="#">
                                    <img src="images/tour/packages/3-col/3.jpg" alt=""></a>
                                <figcaption>
                                    <span class="price">$483</span>
                                    <h2 class="caption-title">Dubai City Tour</h2>
                                </figcaption>
                            </figure>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <article class="box animated" data-animation-type="fadeInLeft">
                            <figure>
                                <a href="#">
                                    <img src="images/tour/packages/3-col/4.jpg" alt=""></a>
                                <figcaption>
                                    <span class="price">$352</span>
                                    <h2 class="caption-title">Hawaii Life Style</h2>
                                </figcaption>
                            </figure>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <article class="box animated" data-animation-type="fadeInUp">
                            <span class="discount"><span class="discount-text">10% Discount</span></span>
                            <figure>
                                <a href="#">
                                    <img src="images/tour/packages/3-col/5.jpg" alt=""></a>
                                <figcaption>
                                    <span class="price">$478</span>
                                    <h2 class="caption-title">Rome, Milan, Madrid</h2>
                                </figcaption>
                            </figure>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <article class="box animated" data-animation-type="fadeInRight">
                            <figure>
                                <a href="#">
                                    <img src="images/tour/packages/3-col/6.jpg" alt=""></a>
                                <figcaption>
                                    <span class="price">$175</span>
                                    <h2 class="caption-title">Italy Family Beach</h2>
                                </figcaption>
                            </figure>
                        </article>
                    </div>
                </div>
            </div>
        </div>


        <br />
        <br />


        <div class="section white-bg">

            <div class="container">

                <div id="main">
                    <h1 class="title" style="text-align: center">Airline Partners</h1>
                    <div class="row">
                        <div class="col-sm-2">
                        </div>
                        <div class="col-sm-8">
                            <div class="image-style style1 large-block ">
                                <ul class="image-block column-3 pull-left  clearfix">
                                    <li><a href="#" class="middle-block middle-block-auto-height" title="Air Asia" style="position: relative; height: 60px;">
                                        <img class="middle-item" src="images/airlinelogo/a1.png" alt="" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -62px; width: 40px; height: 40px;" />
                                    </a></li>

                                    <li><a href="#" class="middle-block middle-block-auto-height" title="Biman Bangladesh Airlines" style="position: relative; height: 60px;">
                                        <img class="middle-item" src="images/airlinelogo/a2.jpg" alt="" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -40px; width: 40px; height: 40px;" />
                                    </a></li>

                                    <li><a href="#" class="middle-block middle-block-auto-height" title="Etihad" style="position: relative; height: 80px;">
                                        <img class="middle-item" src="images/airlinelogo/a4.png" alt="" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -62px; width: 40px; height: 40px;" />
                                    </a></li>

                                    <li><a href="#" class="middle-block middle-block-auto-height" title="China Southern Airlines" style="position: relative; height: 80px;">
                                        <img class="middle-item" src="images/airlinelogo/a5.png" alt="" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -71px; width: 40px; height: 40px;" />
                                    </a></li>

                                    <li><a href="#" class="middle-block middle-block-auto-height" title="Firefly" style="position: relative; height: 80px;">
                                        <img class="middle-item" src="images/airlinelogo/a6.png" alt="" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -68px; width: 40px; height: 40px;" />
                                    </a></li>

                                    <li><a href="#" class="middle-block middle-block-auto-height" title="Jetstar" style="position: relative; height: 80px;">
                                        <img class="middle-item" src="images/airlinelogo/a7.png" alt="" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -36px; width: 40px; height: 40px;" />
                                    </a></li>

                                    <li><a href="#" class="middle-block middle-block-auto-height" title="Malindo Air" style="position: relative; height: 80px;">
                                        <img class="middle-item" src="images/airlinelogo/a8.png" alt="" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -118px; width: 40px; height: 40px;">
                                    </a></li>

                                    <li><a href="#" class="middle-block middle-block-auto-height" title="Malaysia Airlines" style="position: relative; height: 80px;">
                                        <img class="middle-item" src="images/airlinelogo/a9.png" alt="" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -106px; width: 40px; height: 40px;">
                                    </a></li>

                                    <li><a href="#" class="middle-block middle-block-auto-height" title="Tiger Air" style="position: relative; height: 80px;">
                                        <img class="middle-item" src="images/airlinelogo/a10.png" alt="" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -105px; width: 40px; height: 40px;">
                                    </a></li>

                                    <li><a href="#" class="middle-block middle-block-auto-height" title="Silk Air" style="position: relative; height: 80px;">
                                        <img class="middle-item" src="images/airlinelogo/a20.png" alt="" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -118px; width: 40px; height: 40px;">
                                    </a></li>

                                    <li><a href="#" class="middle-block middle-block-auto-height" title="Qatar Airways" style="position: relative; height: 80px;">
                                        <img class="middle-item" src="images/airlinelogo/a21.png" alt="" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -106px; width: 40px; height: 40px;">
                                    </a></li>

                                    <li><a href="#" class="middle-block middle-block-auto-height" title="Air China" style="position: relative; height: 80px;">
                                        <img class="middle-item" src="images/airlinelogo/a22.png" alt="" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -105px; width: 40px; height: 40px;">
                                    </a></li>

                                </ul>
                                <ul class="image-block column-3 pull-right clearfix">
                                    <li><a href="#" class="middle-block middle-block-auto-height" title="KLM" style="position: relative; height: 60px;">
                                        <img class="middle-item" src="images/airlinelogo/a11.png" alt="" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -62px; width: 40px; height: 40px;" />
                                    </a></li>

                                    <li><a href="#" class="middle-block middle-block-auto-height" title="Cathay Pacific" style="position: relative; height: 60px;">
                                        <img class="middle-item" src="images/airlinelogo/a12.png" alt="" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -40px; width: 40px; height: 40px;" />
                                    </a></li>

                                    <li><a href="#" class="middle-block middle-block-auto-height" title=" Turkish Airlines " style="position: relative; height: 80px;">
                                        <img class="middle-item" src="images/airlinelogo/a13.png" alt="" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -62px; width: 40px; height: 40px;" />
                                    </a></li>

                                    <li><a href="#" class="middle-block middle-block-auto-height" title="All Nippon Airways" style="position: relative; height: 80px;">
                                        <img class="middle-item" src="images/airlinelogo/a14.png" alt="" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -71px; width: 40px; height: 40px;" />
                                    </a></li>

                                    <li><a href="#" class="middle-block middle-block-auto-height" title="Emirates" style="position: relative; height: 80px;">
                                        <img class="middle-item" src="images/airlinelogo/a15.png" alt="" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -68px; width: 40px; height: 40px;" />
                                    </a></li>

                                    <li><a href="#" class="middle-block middle-block-auto-height" title="Garuda Indonesia" style="position: relative; height: 80px;">
                                        <img class="middle-item" src="images/airlinelogo/a16.png" alt="" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -36px; width: 40px; height: 40px;" />
                                    </a></li>

                                    <li><a href="#" class="middle-block middle-block-auto-height" title="SriLankan Airlines" style="position: relative; height: 80px;">
                                        <img class="middle-item" src="images/airlinelogo/a17.png" alt="" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -118px; width: 40px; height: 40px;">
                                    </a></li>

                                    <li><a href="#" class="middle-block middle-block-auto-height" title="spicejet" style="position: relative; height: 80px;">
                                        <img class="middle-item" src="images/airlinelogo/a18.png" alt="" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -106px; width: 40px; height: 40px;">
                                    </a></li>

                                    <li><a href="#" class="middle-block middle-block-auto-height" title="Philippine Airlines" style="position: relative; height: 80px;">
                                        <img class="middle-item" src="images/airlinelogo/a19.png" alt="" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -105px; width: 40px; height: 40px;">
                                    </a></li>

                                    <li><a href="#" class="middle-block middle-block-auto-height" title="Vietnam Airlines" style="position: relative; height: 80px;">
                                        <img class="middle-item" src="images/airlinelogo/a25.png" alt="" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -118px; width: 40px; height: 40px;">
                                    </a></li>

                                    <li><a href="#" class="middle-block middle-block-auto-height" title="Vistara Airlines" style="position: relative; height: 80px;">
                                        <img class="middle-item" src="images/airlinelogo/a24.png" alt="" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -106px; width: 40px; height: 40px;">
                                    </a></li>

                                    <li><a href="#" class="middle-block middle-block-auto-height" title="Singapore Airlines" style="position: relative; height: 80px;">
                                        <img class="middle-item" src="images/airlinelogo/a23.png" alt="" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -105px; width: 40px; height: 40px;">
                                    </a></li>

                                </ul>

                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-sm-2">
                        </div>
                    </div>
                </div>
                <!-- end main -->
            </div>
        </div>
        <br />
        <br />
    </section>

</asp:Content>
