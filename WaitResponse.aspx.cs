﻿using CUT.BL;
using CUT.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com = CUT.Common;

namespace CUTUK
{
    public partial class WaitResponse : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GlobalDefault objGlobalDefault = null;
            string input = String.Empty;
            DBHelper.DBReturnCode retcode = CUTUK.DataLayer.MarkupTaxManager.AssignMarkupTax();
            if (!IsPostBack && com.Common.Session(out objGlobalDefault) && retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                input = Request.QueryString["Session"];
                Session["SearchSession"] = input;
                if (!String.IsNullOrEmpty(input))
                {
                    string[] array_input = input.Split('_');
                    DateTime fDate = Convert.ToDateTime(array_input[2].ToString(), new System.Globalization.CultureInfo("en-GB"));
                    DateTime tDate = Convert.ToDateTime(array_input[3].ToString(), new System.Globalization.CultureInfo("en-GB"));
                    lblCity.Text = array_input[1].ToString();
                    lblDepart.Text = array_input[2].ToString();
                    lblReturn.Text = array_input[3].ToString();
                    lblRoom.Text = array_input[4].ToString();
                    string occpancy = array_input[5].ToString();
                    string[] array_occupancy = occpancy.Split('$');
                    int adult = 0;
                    int child = 0;
                    foreach (string str in array_occupancy)
                    {
                        string[] m_array = str.Split('|');
                        adult = adult + Convert.ToInt32(m_array[0]);
                        string[] n_array = m_array[1].ToString().Split('^');
                        child = child + Convert.ToInt32(n_array[0]);
                    }
                    lblAdult.Text = adult.ToString();
                    lblChild.Text = child.ToString();
                }
                else
                {
                    Response.Redirect("~/Default.aspx", true);
                }
            }
            else
            {
                Response.Redirect("~/Default.aspx", true);
            }
        }
    }
}