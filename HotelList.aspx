﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HotelList.aspx.cs" Inherits="CUTUK.HotelList" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Hotel Details</title>

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Travelo - Travel, Tour Booking HTML5 Template">
    <meta name="author" content="SoapTheme">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <script src="js/tooltip.js"></script>
    <!-- Theme Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyBnIPCXTY_ul30N9GMmcSmJPLjPEYzGI7c" type="text/javascript"></script>
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/animate.min.css">
    <link href="css/jquery-ui.css" rel="stylesheet" />
    <!-- Current Page Styles -->
    <link rel="stylesheet" type="text/css" href="components/revolution_slider/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/revolution_slider/css/style.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/jquery.bxslider/jquery.bxslider.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/flexslider/flexslider.css" media="screen" />

    <!-- Main Style -->
    <link id="main-style" rel="stylesheet" href="css/style.css" />

    <!-- Updated Styles -->
    <link rel="stylesheet" href="css/updates.css" />

    <!-- Custom Styles -->
    <link rel="stylesheet" href="css/custom.css">

    <!-- Responsive Styles -->
    <link rel="stylesheet" href="css/responsive.css">

    <!-- CSS for IE -->
    <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie.css" />
    <![endif]-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->
    <script src="js/moment.js"></script>
    <script src="js/jquery-2.0.2.min.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="scripts/Alerts.js"></script>
    <script src="js/modal.js"></script>
    <!-- ........................................................................................... -->
    <script src="scripts/CountryCityCode.js?V=1.6" type="text/javascript"></script>
    <script src="scripts/HotelSearch.js?V=2.3" type="text/javascript"></script>
    <script src="scripts/HotelOccupancy.js?v=2.2"></script>
    <script src="scripts/HotelList.js?v=1.2"></script>

    <style>
        .carousel img {
            display: block;
            float: left;
            height: 50%;
            width: 100%;
        }
    </style>
    <script type="text/javascript">

        var chkinDate;
        var chkinMonth;
        var chkoutDate;
        var chkoutMonth;
        //tpj.noConflict();

        $(document).ready(function () {
            //.....................................................................................................//
            $("#txtCity").autocomplete({

                source: function (request, response) {

                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "../HotelHandler.asmx/GetDestinationCode",
                        data: "{'name':'" + document.getElementById('txtCity').value + "'}",
                        dataType: "json",
                        success: function (data) {

                            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            response(result);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    debugger;
                    $('#hdnDCode').val(ui.item.id);
                }
            });
            $("#txt_HotelName").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "../HotelHandler.asmx/GetHotel",
                        data: "{'name':'" + $('#txt_HotelName').val() + "','destination':'" + $('#hdnDCode').val() + "'}",
                        dataType: "json",
                        success: function (data) {
                            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            response(result);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    $('#hdnHCode').val(ui.item.id);
                }
            });
        });
    </script>

    <%--<script type="text/javascript">
        $body = $("body");
        $(document).on({
            ajaxStart: function () { $("#loders").css("display", "") },
            ajaxStop: function () { $("#loders").css("display", "none") }
        });
        $(document).ready(function () {
            GetNationality();
            GetCountry();
            GetHotelCategory();

        });
        $(function () {

            debugger;
            $('[data-toggle="tooltip"]').tooltip()
            $('[data-toggle="popover"]').popover();
        })
        function SearchCategory(Item) {
            if (Item.id == "HotelName") {
                document.getElementById("Select_StarRating").setAttribute("style", "display:none");
                document.getElementById("txt_HotelName").setAttribute("style", "display:''");
            }
            else {
                document.getElementById("txt_HotelName").setAttribute("style", "display:none");
                document.getElementById("Select_StarRating").setAttribute("style", "display:''");
            }
        }
    </script>--%>

    <script>
        $('.btn-close').click(function (e) {
            e.preventDefault();
            $(this).parent().parent().parent().fadeOut();
        });
        $('.btn-minimize').click(function (e) {
            e.preventDefault();
            var $target = $(this).parent().parent().next('.box-content');
            if ($target.is(':visible')) $('i', $(this)).removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
            else $('i', $(this)).removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
            $target.slideToggle();
        });
        $('.btn-setting').click(function (e) {
            e.preventDefault();
            $('#myModal').modal('show');
        });
        function Cancel() {
            $("#ConformModal").modal("hide")
        }
        function close() {

            $("#AlertModal").modal("hide")
        }
        function Ok(Message, Method, arg) {
            debugger;
            $("#btnOk").css("display", "")
            var id = [];
            if (arg != null) {
                for (var i = 0; i < arg.length; i++) {
                    id.push('"' + arg[i] + '"')
                }
            }

            $("#ConformMessage").html(Message);
            if (arg == null)
                document.getElementById("btnOk").setAttribute("onclick", Method + "()")
            else
                document.getElementById("btnOk").setAttribute("onclick", Method + "(" + id + ")")
            $("#ConformModal").modal("show")
        }
        function Success(Message) {
            $("#AlertMessage").html(Message);
            //$("#btnOk").css("display", "none")
            //$("#Cancel").val("Close")
            $("#AlertModal").modal("show")
        }
    </script>

    <script type="text/javascript">
        var CurrencyClass = '<%=Session["CurrencyClass"]%>';
    </script>
</head>
<body>

    <div id="page-wrapper">
        <header id="header" class="navbar-static-top style4">
            <div class="container">
                <h1 class="logo navbar-brand">
                   <a href="Default.aspx" title="Vacaay.com - home">
                       <img src="images/vacaaay1.png" alt="Vacaay logo" style="width: 306px; height: 50px;" />
                    </a>
                </h1>
                <div class="pull-right hidden-mobile">
                    <ul class="social-icons clearfix pull-right hidden-mobile">
                        <li class="twitter"><a title="twitter" href="#" data-toggle="tooltip"><i class="soap-icon-twitter"></i></a></li>
                        <li class="facebook"><a title="facebook" href="#" data-toggle="tooltip"><i class="soap-icon-facebook"></i></a></li>
                        <li class="linkedin"><a title="linkedin" href="#" data-toggle="tooltip"><i class="soap-icon-linkedin"></i></a></li>
                        <li class="vimeo"><a title="vimeo" href="#" data-toggle="tooltip"><i class="soap-icon-vimeo"></i></a></li>
                    </ul>
                </div>
               
            </div>
            <a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle">Mobile Menu Toggle
            </a>

             <div class="main-navigation">
                <div class="container">
                    <nav id="main-menu" role="navigation">
                        <ul class="menu">
                            <li class="menu-item-has-children">
                                <a href="Default.aspx">Home</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="AboutUs.aspx">About Us</a>
                            </li>

                           <%-- <li class="menu-item-has-children">
                                <a href="HotelSearch.aspx">Hotels</a>
                            </li>--%>

                            <li class="menu-item-has-children">
                                <a href="Packages.aspx">Packages</a>
                            </li>

                            <%-- <li class="menu-item-has-children">
                                <a href="TourPackages.aspx">Tour Packages</a>
                            </li>--%>


                            <li class="menu-item-has-children">
                                <a href="Contact.aspx">Contact Us</a>
                            </li>

                        </ul>
                        <ul class="menu pull-right">
                            <li><a href="#" onclick="SignUpModal()">login / Register</a></li>
                            <li><a href="http://b2b.vacaaay.com/">B2B Login</a></li>
                        </ul>
                    </nav>
                </div>
            </div>

            <nav id="mobile-menu-01" class="mobile-menu collapse">
                <ul id="mobile-primary-menu" class="menu">
                    <li class="menu-item-has-children">
                        <a href="Default.aspx">Home</a>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="AboutUs.aspx">About Us</a>
                    </li>

                    <%--<li class="menu-item-has-children">
                        <a href="HotelSearch.aspx">Hotels</a>
                    </li>--%>

                    <li class="menu-item-has-children">
                        <a href="Packages.aspx">Packages</a>
                    </li>


                    <li class="menu-item-has-children">
                        <a href="Contact.aspx">Contact Us</a>
                    </li>

                </ul>
                <ul class="menu pull-right">
                    <li><a href="#" onclick="SignUpModal()">login / Register</a></li>
                    <li><a href="http://b2b.vacaaay.com/">B2B Login</a></li>
                </ul>
            </nav>
        </header>
        <section id="content">
            <div class="container">
                <div id="main">
                    <div class="row">
                        <div class="col-sm-4 col-md-3">
                            <h4 class="search-results-title" id="FilterTrip"></h4>

                            <%--<div class="row image-box flight listing-style1" style="margin-right: 0px; margin-left: 0px;">
                                <article class="box" style="margin-bottom: 5px;">
                                    <div class="details">
                                        <div class="time">
                                            <div class="take-off">
                                                <div>
                                                    <span class="skin-color">Check In :
                                                        <label id="CheckIn"></label>
                                                    </span>
                                                    <br />
                                                </div>
                                            </div>
                                            <div class="landing">
                                                <div>
                                                    <span class="skin-color">Check Out :
                                                        <label id="CheckOut"></label>
                                                    </span>
                                                    <br />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="time">
                                            <div class="take-off">
                                                <div>
                                                    <span class="skin-color">Nights :
                                                        <label id="TLNights"></label>
                                                    </span>
                                                    <br />
                                                </div>
                                            </div>
                                            <div class="landing">
                                                <div>
                                                    <span class="skin-color">Rooms :
                                                        <label id="NoOfRooms"></label>
                                                    </span>
                                                    <br />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="time">
                                            <div class="take-off">
                                                <div>
                                                    <span class="skin-color" id="NoOfAdults">Adults :</span><br />
                                                </div>
                                            </div>
                                            <div class="landing">
                                                <div>
                                                    <span class="skin-color" id="NoOfChildrens">Childrens :</span><br />
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </article>
                            </div>--%>
                            <div class="toggle-container filters-container">

                                <%--<div class="panel style1 arrow-right">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#amenities-filter" class="collapsed">Rate Type</a>
                                    </h4>
                                    <div id="amenities-filter" class="panel-collapse collapse in">
                                        <div class="panel-content">
                                            <div class="form-group">
                                                <label class="checkbox">
                                                    <input type="checkbox" value="Refundable" name="chkRating" checked="checked" onclick="Refunplolicy(this)" id="Refundable">Refundable
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label class="checkbox">
                                                    <input type="checkbox" value="nonRefundable" name="chkRating" checked="checked" onclick="Refunplolicy(this)" id="NonRefundable">Non Refundable
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>--%>

                                <div class="panel style1 arrow-right">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#modify-search-panel" class="collapsed">Hotel Name</a>
                                    </h4>
                                    <div id="modify-search-panel" class="panel-collapse collapse in">
                                        <div class="panel-content">
                                            <form method="post">
                                                <div class="form-group">
                                                    <label>Hotel Name</label>
                                                    <input type="text" class="form-control" placeholder="Eg: Xyz Hotel" id="txtSearchHotel" />
                                                </div>
                                                <br />
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <button type="button" class="btn-medium icon-check uppercase full-width" onclick="HotelName()">search</button>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <button type="button" class="btn-medium icon-check uppercase full-width" id="spn_Hotel" onclick="ResetName()">Clear</button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel style1 arrow-right">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#price-filter" class="">Price range</a>
                                    </h4>
                                    <div id="price-filter" class="panel-collapse collapse in">
                                        <div class="panel-content">
                                            <div id="price-range"></div>
                                            <br />
                                            <span class="min-price-label pull-left" id="spn_Max"></span>
                                            <span class="max-price-label pull-right" onchange="HotelFilter();"></span>
                                            <div class="clearer"></div>
                                        </div>
                                        <!-- end content -->
                                    </div>
                                </div>

                                <div class="panel style1 arrow-right">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#accomodation-type-filter" class="">Star Ratings</a>
                                    </h4>
                                    <div id="accomodation-type-filter" class="panel-collapse collapse in">
                                        <div class="panel-content">
                                            <ul class="check-square filters-option Ratings">
                                                <%-- <li class="" id="ALLstr" onclick="HotelFilter()"><a href="#">All<small><label id="Stars"></label></small></a></li>--%>
                                                <li class="active" onclick="HotelFilter()" id="0str"><a href="#">
                                                    <img src="HotelRating/0.png" /><small><label id="Star_0"></label></small></a>
                                                </li>
                                                <li class="active" onclick="HotelFilter()" id="1str"><a href="#">
                                                    <img src="HotelRating/1.png" /><small><label id="Star_1"></label></small></a>
                                                </li>
                                                <li class="active" onclick="HotelFilter()" id="2str"><a href="#">
                                                    <img src="HotelRating/2.png" /><small><label id="Star_2"></label></small></a>

                                                </li>
                                                <li class="active" onclick="HotelFilter()" id="3str"><a href="#">
                                                    <img src="HotelRating/3.png" /><small><label id="Star_3"></label></small></a>

                                                </li>
                                                <li class="active" onclick="HotelFilter()" id="4str"><a href="#">
                                                    <img src="HotelRating/4.png" /><small><label id="Star_4"></label></small></a>

                                                </li>
                                                <li class="active" onclick="HotelFilter()" id="5str"><a href="#">
                                                    <img src="HotelRating/5.png" /><small><label id="Star_5"></label></small></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8 col-md-9">
                            <div class="sort-by-section clearfix">
                                <h4 class="sort-by-title block-sm">Sort results by:</h4>
                                <ul class="sort-bar clearfix block-sm">
                                    <li class="sort-by-name"><a class="sort-by-container" href="#"><span onclick="SortByHotel()" title="Sort By Hotel">Hotel Name</span></a>
                                        <label id="lblHotelName" style="display: none">0</label>
                                    </li>
                                    <li class="sort-by-price"><a class="sort-by-container" href="#"><span onclick="SortByPrice()" title="Sort By Pice">Price</span></a></li>
                                    <li class="clearer visible-sms"></li>
                                    <li class="sort-by-rating "><a class="sort-by-container" href="#"><span onclick="StarRating()" title="Sort By Category">Category</span></a></li>
                                    <li class="sort-by-popularity"><a class="sort-by-container" href="#"><span onclick="GetPreferred()" id="btn_Preffered">Preferred Hotels</span></a>
                                        <input type="hidden" id="txt_Preferred" value="0" />
                                    </li>
                                </ul>

                                <div class="col-md-2" style="margin-top: 15px;">
                                    <button type="button" style="background-color: #f5f5f5; color: #9e9e9e" class="full-width" id="btn_Show" onclick="Show();">Modify Search</button>
                                    <button type="button" style="background-color: #f5f5f5; color: #9e9e9e; display: none" class="full-width" id="btn_hide" onclick="Hide();">Modify Search</button>
                                </div>
                                <h1 style="cursor: pointer"><a class="sort-by-title block-sm" onclick="GetMapAll()"><i class="soap-icon-departure" title="Map View"></i></a></h1>
                                <br />
                            </div>

                            <div class="hotel-list listing-style3 hotel" style="margin-top: 0px; display: none" id="SearchDive">
                                <div>
                                    <div class="search-box-wrapper style2">
                                        <div class="search-box">

                                            <div class="search-tab-content">
                                                <div class="tab-pane fade active in" id="hotels-tab">
                                                    <form action="hotel-list-view.html" method="post">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <span class="text-left" style="color: #838383">Where do you want to go?</span>
                                                                <br />
                                                                <div class="input-group" style="width: 100%">
                                                                    <input type="text" style="background-color: none" placeholder="Enter City Here" id="txtCity" class="form-control" />
                                                                </div>
                                                                <input type="hidden" id="hdnDCode" />
                                                            </div>

                                                            <div class="col-md-3">
                                                                <span class="text-left" style="color: #2d3e52">Check-In:</span>
                                                                <div class="datepicker-wrap">
                                                                    <input type="text" name="date_from" id="datepicker_HotelCheckin" class="input-text full-width" placeholder="Check In" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <span class="text-left" style="color: #2d3e52">Check-Out:</span>
                                                                <div class="datepicker-wrap">
                                                                    <input type="text" name="date_to" id="datepicker_HotelCheckout" class="input-text full-width" placeholder="Check Out" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <span class="text-left" style="color: #838383">Total Nights:</span>
                                                                <br />
                                                                <select id="Select_TotalNights" class="form-control">
                                                                    <option selected="selected" value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                    <option value="12">12</option>
                                                                    <option value="13">13</option>
                                                                    <option value="14">14</option>
                                                                    <option value="15">15</option>
                                                                    <option value="16">16</option>
                                                                    <option value="17">17</option>
                                                                    <option value="18">18</option>
                                                                    <option value="19">19</option>
                                                                    <option value="20">20</option>
                                                                    <option value="21">21</option>
                                                                    <option value="22">22</option>
                                                                    <option value="23">23</option>
                                                                    <option value="24">24</option>
                                                                    <option value="25">25</option>
                                                                    <option value="26">26</option>
                                                                    <option value="27">27</option>
                                                                    <option value="28">28</option>
                                                                    <option value="29">29</option>
                                                                    <option value="30">30</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <br />

                                                        <br id="br1" style="display: none" />
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <span class="text-left" style="color: #838383">Nationality:</span>
                                                                <br />
                                                                <select id="Select_Nationality" class="form-control">
                                                                </select>
                                                            </div>

                                                            <div class="col-md-3" style="display: none">
                                                                <span class="text-left" style="color: #838383">Star Rating:</span>
                                                                <br />
                                                                <select id="Select_StarRating" class="form-control"></select>
                                                            </div>

                                                            <div class="col-md-5" style="display: none">
                                                                <span class="text-left" style="color: #838383">Hotel Name:</span>
                                                                <br />
                                                                <input class="form-control" type="text" placeholder="Enter Hotel Here" id="txt_HotelName" />
                                                                <input type="hidden" id="hdnHCode" />
                                                            </div>

                                                            <div class="col-md-1">
                                                                <span class="text-left" style="color: #838383">Rooms:</span>
                                                                <select id="Select_Rooms" class="form-control">
                                                                    <option selected="selected" value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <span class="text-left" style="color: #838383">Room Type:</span>
                                                                <input type="text" id="spn_RoomType1" style="cursor: default;" disabled="disabled" class="form-control" value="Room 1" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span class="text-left" style="color: #838383">Adults:</span>
                                                                <select id="Select_Adults1" class="form-control">
                                                                    <option value="1">1</option>
                                                                    <option value="2" selected="selected">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span class="text-left" style="color: #838383">Child:</span>
                                                                <select id="Select_Children1" class="form-control">
                                                                    <option selected="selected" value="0">0</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span id="Span_AgeChildFirst1" class="text-left" style="display: none; color: #838383">Age 1:</span>
                                                                <select id="Select_AgeChildFirst1" class="form-control" style="display: none">
                                                                    <option selected="selected" value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span id="Span_AgeChildSecond1" class="text-left" style="display: none; color: #838383">Age 2:</span>
                                                                <select id="Select_AgeChildSecond1" class="form-control" style="display: none;">
                                                                    <option selected="selected" value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                </select>
                                                            </div>

                                                        </div>

                                                        <br />
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                            </div>
                                                            <div class="col-md-1">
                                                            </div>
                                                            <div class="col-md-2">
                                                                <span id="Span_RoomType2" class="text-left" style="display: none; color: #838383">Room Type:</span>
                                                                <input type="text" id="spn_RoomType2" class="form-control" disabled="disabled" value="Room 2" style="display: none; cursor: default;" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span id="Span_Adults2" class="text-left" style="display: none; color: #838383">Adults:</span>
                                                                <select id="Select_Adults2" class="form-control" style="display: none">
                                                                    <option value="1">1</option>
                                                                    <option value="2" selected="selected">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span id="Span_Children2" class="text-left" style="display: none; color: #838383">Child:</span>
                                                                <select id="Select_Children2" class="form-control" style="display: none">
                                                                    <option selected="selected" value="0">0</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span id="Span_AgeChildFirst2" class="text-left" style="display: none; color: #838383">Age 1:</span>
                                                                <select id="Select_AgeChildFirst2" class="form-control" style="display: none">
                                                                    <option selected="selected" value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span id="Span_AgeChildSecond2" class="text-left" style="display: none; color: #838383">Age 2:</span>
                                                                <select id="Select_AgeChildSecond2" class="form-control" style="display: none;">
                                                                    <option selected="selected" value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                            </div>
                                                            <div class="col-md-1">
                                                            </div>
                                                            <div class="col-md-2">
                                                                <span id="Span_RoomType3" class="text-left" style="display: none; color: #838383">Room Type:</span>
                                                                <input type="text" id="spn_RoomType3" class="form-control" disabled="disabled" value="Room 3" style="display: none; cursor: default;" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span id="Span_Adults3" class="text-left" style="display: none; color: #838383">Adults:</span>
                                                                <select id="Select_Adults3" class="form-control" style="display: none">
                                                                    <option value="1">1</option>
                                                                    <option value="2" selected="selected">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span id="Span_Children3" class="text-left" style="display: none; color: #838383">Child:</span>
                                                                <select id="Select_Children3" class="form-control" style="display: none">
                                                                    <option selected="selected" value="0">0</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span id="Span_AgeChildFirst3" class="text-left" style="display: none; color: #838383">Age 1:</span>
                                                                <select id="Select_AgeChildFirst3" class="form-control" style="display: none">
                                                                    <option selected="selected" value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span id="Span_AgeChildSecond3" class="text-left" style="display: none; color: #838383">Age 2:</span>
                                                                <select id="Select_AgeChildSecond3" class="form-control" style="display: none;">
                                                                    <option selected="selected" value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                            </div>
                                                            <div class="col-md-1">
                                                            </div>
                                                            <div class="col-md-2">
                                                                <span id="Span_RoomType4" class="text-left" style="display: none; color: #838383">Room Type:</span>
                                                                <input type="text" id="spn_RoomType4" class="form-control" value="Room 4" disabled="disabled" style="display: none; cursor: default;" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span id="Span_Adults4" class="text-left" style="display: none; color: #838383">Adults:</span>
                                                                <select id="Select_Adults4" class="form-control" style="display: none">
                                                                    <option value="1">1</option>
                                                                    <option value="2" selected="selected">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span id="Span_Children4" class="text-left" style="display: none; color: #838383">Child:</span>
                                                                <select id="Select_Children4" class="form-control" style="display: none">
                                                                    <option selected="selected" value="0">0</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span id="Span_AgeChildFirst4" class="text-left" style="display: none; color: #838383">Age 1:</span>
                                                                <select id="Select_AgeChildFirst4" class="form-control" style="display: none">
                                                                    <option selected="selected" value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span id="Span_AgeChildSecond4" class="text-left" style="display: none; color: #838383">Age 2:</span>
                                                                <select id="Select_AgeChildSecond4" class="form-control" style="display: none;">
                                                                    <option selected="selected" value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <div class="col-md-2" style="float: right">
                                                            <button type="button" class="full-width" id="btn_Search" onclick="Redirect();">SEARCH</button>
                                                        </div>
                                                        <br />
                                                        <br />

                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="hotel-list listing-style3 hotel" id="Design">
                            </div>

                            <div class="hpadding20" id="divpagingbottom" style="float: right">
                                <%--   <ul class="pagination right paddingbtm20">
                                    <li class="disabled"><a>«</a></li>
                                    <li class="active"><a onclick="HotelPager(1);" style="cursor: pointer">1</a></li>
                                    <li><a onclick="HotelPager(2);" style="cursor: pointer">2</a></li>
                                    <li><a onclick="HotelPager(3);" style="cursor: pointer">3</a></li>
                                    <li><a onclick="HotelPager(4);" style="cursor: pointer">4</a></li>
                                    <li><a onclick="HotelPager(5);" style="cursor: pointer">5</a></li>
                                    <li><a onclick="HotelPager(6);" style="cursor: pointer">6</a></li>
                                    <li><a onclick="HotelPager(7);" style="cursor: pointer">7</a></li>
                                    <li><a onclick="HotelPager(2);" style="cursor: pointer">»</a></li>
                                </ul>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <footer id="footer" class="style5">
              <div class="footer-wrapper" style="background-color: #2d3e52">
                <div class="container">
                    <div class="row">
                        <%-- <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2 style="color:wheat">Address</h2>
                              <p style="text-align: justify ;color:wheat;">
                              Airlines Travel Centre <br />Sdn.Bhd.(1073291-U)
                              KKKP/PL 7592<br />
                              NO41,Level 2, Jalan Sultan Yussuf<br />
                              30000 Ipoh Perak <br />
                              Malaysia
                              </p>
                            </div>--%>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">

                            <h2 style="color: wheat">Discover</h2>
                            <hr />
                            <br />
                            <br />
                            <ul class="discover triangle hover row" style="color: wheat">
                                <li class="col-xs-6"><a href="Default.aspx">HOME</a></li>
                                <li class="col-xs-6"><a href="AboutUs.aspx">ABOUT US</a></li>
                                <%--<li class="col-xs-6"><a href="HotelSearch.aspx">HOTELS</a></li>--%>
                                <li class="col-xs-6"><a href="Packages.aspx">PACKAGES</a></li>
                                <li class="col-xs-6"><a href="Contact.aspx">CONTACT US</a></li>
                            </ul>




                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2 style="color: wheat">Trade Association</h2>
                            <hr />
                            <br />
                            <br />
                            <ul class="travel-news">
                                <li>

                                    <address class="contact-details">

                                        <ul class="social-icons clearfix">



                                            <img src="https://tvlk.imgix.net/imageResource/2017/12/13/1513150321127-5096be77d2a19401b476853e54ba2cc6.svg?auto=compress%2Cformat&amp;cs=srgb&amp;ixlib=java-1.1.12&amp;q=75">
                                        </ul>
                                    </address>
                                </li>
                            </ul>
                            <h2 style="color: wheat">Security Certifications</h2>
                            <hr />
                            <br />
                            <br />
                            <ul class="travel-news">
                                <li>

                                    <address class="contact-details">

                                        <ul class="social-icons clearfix">

                                            <img src="https://tvlk.imgix.net/imageResource/2017/12/13/1513150198216-822560165b4cfa5d5ac17a7987028b03.svg?auto=compress%2Cformat&amp;cs=srgb&amp;ixlib=java-1.1.12&amp;q=75">

                                            <img src="https://tvlk.imgix.net/imageResource/2017/12/13/1513150313470-072f6bdc02c0b73fcf791aa2b2264fbd.svg?auto=compress%2Cformat&amp;cs=srgb&amp;ixlib=java-1.1.12&amp;q=75">
                                        </ul>
                                    </address>
                                </li>
                            </ul>
                        </div>


                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2 style="color: wheat">Payment Partners</h2>
                            <hr />
                            <br />
                            <br />
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/09/29/1506664998558-c5fa770c7be81b3714f2f48ecd85db98.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/10/31/1509421373165-7ebba77e7447077b423012fed9f9f71c.jpeg?auto=compress%2Cformat&amp;cs=srgb&amp;fm=pjpg&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/09/29/1506665015604-7884362b3ea565a394df831ef7e8e7cb.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/09/28/1506602820847-28f78426b005e542fac5390ca84ec551.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/10/31/1509421385548-c79a81200ececb8e100169c32c52a7fa.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/09/29/1506665026169-ce67eef59804c4acc2f7fd36cb5a879a.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2018/09/27/1538041096091-17520ed6a85fbd457d47033d12532b85.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/09/29/1506665040121-f1fc420d69a023a99d3ec4b4ce3fc0e3.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/09/29/1506665046541-a307d453b9345dad33f4d45852ab168f.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/09/29/1506665053371-f97808ddc9481a41412112c1d5751432.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/10/30/1509365256338-d9f84c5a076e5e438423a8443476bc52.jpeg?auto=compress%2Cformat&amp;cs=srgb&amp;fm=pjpg&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/10/31/1509421397012-5407269004e88b979e792cb558f33df6.jpeg?auto=compress%2Cformat&amp;cs=srgb&amp;fm=pjpg&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/10/31/1509421400393-156136b7fc6842065dd4d27f63f704ab.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/09/28/1506602748281-38fa797106a1ca8ef57d689b8a956306.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 45px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/10/31/1509421411326-c2ed81814e071f8d0f32e3c720fc2f51.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/10/31/1509421407130-3d32a6ffd3b308f4665de4e59b95d681.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2 style="color: wheat">About Vacaaay</h2>
                            <hr />
                            <br />
                            <br />
                            <p style="color: wheat">
                                Airlines Travel Centre
                                <br />
                                Sdn.Bhd.(1073291-U)
                              KKKP/PL 7592<br />
                                NO41,Level 2, Jalan Sultan Yussuf<br />
                                30000 Ipoh Perak
                                <br />
                                Malaysia
                            </p>

                            <br />
                            <address class="contact-details" style="color: wheat">
                                <br />
                                <a href="#" class="contact-email"><i class="soap-icon-letter-1"></i>info@vacaaay.com</a>
                            </address>
                            <ul class="social-icons clearfix">
                                <li class="twitter"><a title="twitter" href="#" data-toggle="tooltip"><i class="soap-icon-twitter"></i></a></li>
                                <li class="googleplus"><a title="googleplus" href="#" data-toggle="tooltip"><i class="soap-icon-googleplus"></i></a></li>
                                <li class="facebook"><a title="facebook" href="#" data-toggle="tooltip"><i class="soap-icon-facebook"></i></a></li>
                                <li class="linkedin"><a title="linkedin" href="#" data-toggle="tooltip"><i class="soap-icon-linkedin"></i></a></li>
                                <li class="vimeo"><a title="vimeo" href="#" data-toggle="tooltip"><i class="soap-icon-vimeo"></i></a></li>
                                <li class="dribble"><a title="dribble" href="#" data-toggle="tooltip"><i class="soap-icon-dribble"></i></a></li>
                                <li class="flickr"><a title="flickr" href="#" data-toggle="tooltip"><i class="soap-icon-flickr"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
          </footer>
             <div class="bottom gray-area">
                <div class="container">
                    <div class="logo pull-left">
                        <a href="Default.aspx" title="Vacaay Logo">
                            <img src="images/vacaaay1.png" alt="Vacaay Logo" />
                        </a>
                    </div>
                    <%--<div class="pull-left">
                        <p>Developed by <a href="http://www.trivo.in/" target="_blank">Trivo</a></p>
                    </div>--%>
                    <div class="pull-right">
                        <a id="back-to-top" href="#" class="animated" data-animation-type="bounce"><i class="soap-icon-longarrow-up circle"></i></a>
                    </div>
                    <div class="copyright pull-right">
                        <p>Copyright © 2018 , Airlines Travel Centre Sdn Bhd - KPK/LN 7592 </p>
                    </div>
                </div>
            </div>
         
    </div>


    <!-- Javascript -->
    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.noconflict.js"></script>
    <script type="text/javascript" src="js/modernizr.2.7.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="js/jquery-ui.1.10.4.min.js"></script>

    <!-- Twitter Bootstrap -->
    <%--<script type="text/javascript" src="js/bootstrap.js"></script>--%>

    <!-- load revolution slider scripts -->
    <script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>

    <!-- load BXSlider scripts -->
    <script type="text/javascript" src="components/jquery.bxslider/jquery.bxslider.min.js"></script>

    <!-- load FlexSlider scripts -->
    <script type="text/javascript" src="components/flexslider/jquery.flexslider-min.js"></script>

    <!-- Google Map Api -->
    <!-- <script type='text/javascript' src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>-->
    <!--<script type="text/javascript" src="js/gmap3.min.js"></script>-->

    <!-- parallax -->
    <script type="text/javascript" src="js/jquery.stellar.min.js"></script>

    <!-- waypoint -->
    <script type="text/javascript" src="js/waypoints.min.js"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="js/theme-scripts.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>

    <%--<script type="text/javascript">

        var tpj = jQuery;
        var chkinDate;
        var chkinMonth;
        var chkoutDate;
        var chkoutMonth;
        tpj(document).ready(function () {

            tpj("#datepicker_HotelCheckin").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "d-m-yy",
                onSelect: insertDepartureDate,
                //dateFormat: "yy-m-d",
                minDate: "dateToday",
                maxDate: "+12M +10D"
            });
            tpj("#datepicker_HotelCheckin").datepicker("setDate", new Date());
            var dateObject = tpj("#datepicker_HotelCheckin").datepicker("getDate", '+1d');
            dateObject.setDate(dateObject.getDate() + 1);
            tpj("#datepicker_HotelCheckout").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "d-m-yy",
                beforeShow: insertDepartureDate,
                onSelect: insertDays,
                //dateFormat: "yy-m-d",
                minDate: "+1D",
                maxDate: "+12M +20D"
            });
            tpj("#datepicker_HotelCheckout").datepicker("setDate", dateObject);

            tpj('#datepicker_HotelCheckin').change(function () {
                var date2 = tpj('#datepicker_HotelCheckin').datepicker('getDate', '+1d');
                chkinDate = date2.getDate();
                //chkinMonth = date2.getMonth();
                date2.setDate(date2.getDate() + 1);
                tpj('#datepicker_HotelCheckout').datepicker('setDate', date2);
                tpj('#Select_TotalNights').val("1");
            });
            tpj('#Select_TotalNights').change(function () {
                manage_date();
                //    tpj('#datepicker_HotelCheckout').datepicker('setDate', '+' + Number(Number(chkinDate) + Number(tpj('#Select_TotalNights').val())) + 'd')
            });
            //....................................................................................................//

            function insertDays() {
                var arrival_date = new Date(tpj('#datepicker_HotelCheckin').datepicker("getDate"));
                var departure_date = new Date(tpj('#datepicker_HotelCheckout').datepicker("getDate"));
                days = (departure_date.getTime() - arrival_date.getTime()) / (1000 * 60 * 60 * 24);
                if (days > 30) {
                    alert("You are allowed to book for a maximum 30 Nights");
                    days = tpj('#Select_TotalNights').val();
                    days = parseInt(days);
                    var departure_date = new Date(arrival_date.getFullYear(), arrival_date.getMonth(), arrival_date.getDate() + days);
                    tpj('#datepicker_HotelCheckout').datepicker('setDate', departure_date);
                    tpj('#Select_TotalNights').val(days);
                    //document.getElementById("sel_days").value = days;
                    return false;
                }
                tpj('#Select_TotalNights').val(days);
                //document.getElementById("sel_days").value = days;
            }

            function manage_date() {
                days = tpj('#Select_TotalNights').val();
                days = parseInt(days);
                var firstDate = new Date(tpj('#datepicker_HotelCheckin').datepicker("getDate"));
                var secondDate = new Date(firstDate.getFullYear(), firstDate.getMonth(), firstDate.getDate() + days);
                tpj('#datepicker_HotelCheckout').datepicker('setDate', secondDate);
                insertDepartureDate('', '', '');

            }
            function insertDepartureDate(value, date, inst) {
                var firstDate = new Date(tpj('#datepicker_HotelCheckin').datepicker("getDate"));
                var dateAdjust = tpj('#Select_TotalNights').val();
                var current_date = new Date();

                current_time = current_date.getTime();
                days = (firstDate.getTime() - current_time) / (1000 * 60 * 60 * 24);
                if (days < 0) {
                    var add_day = 1;
                } else {
                    var add_day = 2;
                }
                days = parseInt(days);

                tpj('#datepicker_HotelCheckout').datepicker("option", "minDate", days + add_day);
                tpj('#datepicker_HotelCheckout').datepicker("option", "maxDate", days + 365);
                dateAdjust = parseInt(dateAdjust);
                var secondDate = new Date(firstDate.getFullYear(), firstDate.getMonth(), firstDate.getDate() + dateAdjust);
                tpj('#datepicker_HotelCheckout').datepicker('setDate', secondDate);
            }
            //....................................................................................................//
            tpj("#txtCity").autocomplete({
                source: function (request, response) {
                    tpj.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "../HotelHandler.asmx/GetDestinationCode",
                        data: "{'name':'" + document.getElementById('txtCity').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            response(result);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    tpj('#hdnDCode').val(ui.item.id);
                }
            });

        });


    </script>--%>

       <script type="text/javascript">

           var tpj = jQuery;
           var chkinDate;
           var chkinMonth;
           var chkoutDate;
           var chkoutMonth;
           tpj.noConflict();

           tpj(document).ready(function () {

               if (tpj.fn.cssOriginal != undefined)
                   tpj.fn.css = tpj.fn.cssOriginal;

               tpj('.fullscreenbanner').revolution(
                   {
                       delay: 9000,
                       startwidth: 1170,
                       startheight: 600,

                       onHoverStop: "on",						// Stop Banner Timet at Hover on Slide on/off

                       thumbWidth: 100,							// Thumb With and Height and Amount (only if navigation Tyope set to thumb !)
                       thumbHeight: 50,
                       thumbAmount: 3,

                       hideThumbs: 0,
                       navigationType: "bullet",				// bullet, thumb, none
                       navigationArrows: "solo",				// nexttobullets, solo (old name verticalcentered), none

                       navigationStyle: false,				// round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom


                       navigationHAlign: "left",				// Vertical Align top,center,bottom
                       navigationVAlign: "bottom",					// Horizontal Align left,center,right
                       navigationHOffset: 30,
                       navigationVOffset: 30,

                       soloArrowLeftHalign: "left",
                       soloArrowLeftValign: "center",
                       soloArrowLeftHOffset: 20,
                       soloArrowLeftVOffset: 0,

                       soloArrowRightHalign: "right",
                       soloArrowRightValign: "center",
                       soloArrowRightHOffset: 20,
                       soloArrowRightVOffset: 0,

                       touchenabled: "on",						// Enable Swipe Function : on/off


                       stopAtSlide: -1,							// Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
                       stopAfterLoops: -1,						// Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic

                       hideCaptionAtLimit: 0,					// It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
                       hideAllCaptionAtLilmit: 0,				// Hide all The Captions if Width of Browser is less then this value
                       hideSliderAtLimit: 0,					// Hide the whole slider, and stop also functions if Width of Browser is less than this value


                       fullWidth: "on",							// Same time only Enable FullScreen of FullWidth !!
                       fullScreen: "off",						// Same time only Enable FullScreen of FullWidth !!


                       shadow: 0								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows -  (No Shadow in Fullwidth Version !)

                   });

               tpj("#datepicker_Departure").datepicker({
                   changeMonth: true,
                   changeYear: true,
                   dateFormat: "d-m-yy",


                   minDate: "dateToday",

               });
               tpj("#datepicker_Return").datepicker({
                   changeMonth: true,
                   changeYear: true,
                   dateFormat: "d-m-yy",
               });
               tpj("#datepicker_HotelCheckin").datepicker({
                   changeMonth: true,
                   changeYear: true,
                   dateFormat: "d-m-yy",
                   onSelect: insertDepartureDate,
                   //dateFormat: "yy-m-d",
                   minDate: "dateToday",
                   //maxDate: "+3M +10D"
               });
               tpj("#datepicker_HotelCheckin").datepicker("setDate", new Date());
               var dateObject = tpj("#datepicker_HotelCheckin").datepicker('getDate', '+1d');
               dateObject.setDate(dateObject.getDate() + 1);
               tpj("#datepicker_HotelCheckout").datepicker({
                   changeMonth: true,
                   changeYear: true,
                   dateFormat: "d-m-yy",
                   beforeShow: insertDepartureDate,
                   onSelect: insertDays,
                   //dateFormat: "yy-m-d",
                   minDate: "+1D",
                   //maxDate: "+3M +20D"
               });
               tpj("#datepicker_HotelCheckout").datepicker("setDate", dateObject);
               Addyear();
               tpj('#datepicker_HotelCheckin').change(function () {
                   var date2 = tpj('#datepicker_HotelCheckin').datepicker('getDate', '+1d');
                   chkinDate = date2.getDate();
                   //chkinMonth = date2.getMonth();
                   date2.setDate(date2.getDate() + 1);
                   tpj('#datepicker_HotelCheckout').datepicker('setDate', date2);
                   tpj('#Select_TotalNights').val("1");
               });
               tpj('#Select_TotalNights').change(function () {
                   manage_date();
                   //    tpj('#datepicker_HotelCheckout').datepicker('setDate', '+' + Number(Number(chkinDate) + Number(tpj('#Select_TotalNights').val())) + 'd')
               });
               //....................................................................................................//

               function insertDays() {
                   var arrival_date = new Date(tpj('#datepicker_HotelCheckin').datepicker("getDate"));
                   var departure_date = new Date(tpj('#datepicker_HotelCheckout').datepicker("getDate"));
                   days = (departure_date.getTime() - arrival_date.getTime()) / (1000 * 60 * 60 * 24);
                   if (days > 30) {
                       alert("You are allowed to book for a maximum 30 Nights");
                       days = tpj('#Select_TotalNights').val();
                       days = parseInt(days);
                       var departure_date = new Date(arrival_date.getFullYear(), arrival_date.getMonth(), arrival_date.getDate() + days);
                       tpj('#datepicker_HotelCheckout').datepicker('setDate', departure_date);
                       tpj('#Select_TotalNights').val(days);
                       //document.getElementById("sel_days").value = days;
                       return false;
                   }
                   tpj('#Select_TotalNights').val(days);
                   //document.getElementById("sel_days").value = days;
               }

               function manage_date() {
                   days = tpj('#Select_TotalNights').val();
                   days = parseInt(days);
                   var firstDate = new Date(tpj('#datepicker_HotelCheckin').datepicker("getDate"));
                   var secondDate = new Date(firstDate.getFullYear(), firstDate.getMonth(), firstDate.getDate() + days);
                   tpj('#datepicker_HotelCheckout').datepicker('setDate', secondDate);
                   insertDepartureDate('', '', '');

               }

               function insertDepartureDate(value, date, inst) {
                   debugger;
                   var firstDate = new Date(tpj('#datepicker_HotelCheckin').datepicker("getDate"));
                   var dateAdjust = tpj('#Select_TotalNights').val();
                   var current_date = new Date();

                   current_time = current_date.getTime();
                   days = (firstDate.getTime() - current_time) / (1000 * 60 * 60 * 24);
                   if (days < 0) {
                       var add_day = 1;
                   } else {
                       var add_day = 2;
                   }
                   days = parseInt(days);
                   tpj('#datepicker_HotelCheckout').datepicker("option", "minDate", days + add_day);
                   tpj('#datepicker_HotelCheckout').datepicker("option", "maxDate", days + 365);
                   dateAdjust = parseInt(dateAdjust);
                   var secondDate = new Date(firstDate.getFullYear(), firstDate.getMonth(), firstDate.getDate() + dateAdjust);
                   tpj('#datepicker_HotelCheckout').datepicker('setDate', secondDate);

               }

               function Addyear() {
                   var current_date = new Date();
                   var firstDate = new Date(tpj('#datepicker_HotelCheckin').datepicker("getDate"));
                   current_time = current_date.getTime();
                   days = (firstDate.getTime() - current_time) / (1000 * 60 * 60 * 24);
                   days = parseInt(days);
                   var i = 0
                   tpj('#datepicker_HotelCheckin').datepicker("option", "minDate", days + i);
                   tpj('#datepicker_HotelCheckin').datepicker("option", "maxDate", days + 365);
               }
           });
    </script>

    <div class="modal fade" id="AlertModal" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="width: 40%; padding-top: 15%">

            <div class="modal-content">
                <div class="modal-header" style="border-bottom: none">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>

                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="frow2">
                            <div>
                                <center><span id="AlertMessage"></span></center>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="ModelMessege" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="width: 40%; padding-top: 15%">

            <div class="modal-content">
                <div class="modal-header" style="border-bottom: none">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>

                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="frow2">
                            <div>
                                <center><span id="SpnMessege"></span></center>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="ConformModal" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="container-fluid">
                        <%-- <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Incomplete Document</b><label id="No"></label></div>--%>
                        <div>
                            <p class="size17" id="ConformMessage">
                            </p>
                            <table align="right" style="margin-left: -50%;">
                                <tbody>
                                    <tr>
                                        <td style="border-top-width: 1px; border-top-style: solid; border-top-color: rgb(255, 255, 255);">
                                            <input type="button" id="Cancel" value="Cancel" onclick="Cancel()" class="btn-search4 margtop20" style="float: none;">
                                        </td>
                                        <td style="border-top-width: 1px; border-top-style: solid; border-top-color: rgb(255, 255, 255);">
                                            <input type="button" value="Ok" id="btnOk" class="btn-search4 margtop20" style="float: none;">
                                        </td>

                                    </tr>
                                </tbody>
                            </table>
                            <br />

                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade modal-lg" id="MapView" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel3" style="text-align: left">Map View</h4>
                </div>
                <div class="modal-body">
                    <div id="map" class="img-responsive" style="height: 450px; width: 800px; width: 100%"></div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bs-example-modal-lg" id="Slider" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="SlideHotelName" style="text-align: left"></h4>
                </div>
                <div class="">
                    <div id="Img" class="container" style="width: 600px">
                    </div>
                </div>

            </div>
        </div>
    </div>
</body>

</html>
