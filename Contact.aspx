﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="CUTUK.Contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-title-container">
        <div class="container">
            <div class="page-title pull-left">
                <h2 class="entry-title">Contact Us</h2>
            </div>
            <ul class="breadcrumbs pull-right">
                <li><a href="#">HOME</a></li>
                <li class="active">Contact Us</li>
            </ul>
        </div>
    </div>

    <section id="content" class="white-bg">
        <div class="container">
            <div id="main">
                 <img src="images/Transparentvacaaayimg.png" alt="vacaaay image" style="background-color:white"/>
                
            </div>
        </div>
        <div class="global-map-area section contact-details parallax" data-stellar-background-ratio="0.5">
            <div class="container">
                       <div class="row">
                        <div class="col-sm-4 col-md-3">
                            <div class="travelo-box contact-us-box">
                                <h4>Contact us</h4>
                                <ul class="contact-address">
                                    <li class="address">
                                        <i class="soap-icon-address circle"></i>
                                        <h5 class="title">Address</h5>
                                        <p>
                                            Airlines Travel Centre Sdn.Bhd.(1073291-U) <br />
                                            KKKP/PL 7592<br />
                                            NO41,Level 2, Jalan Sultan Yussuf<br />
                                            30000 Ipoh Perak <br />
                                            Malaysia.
                                        </p>
                                       
                                    </li>
                                    <li class="phone">
                                        <i class="soap-icon-phone circle"></i>
                                        <h5 class="title">Phone</h5>
                                        <p>Local:+60-174879309</p>

                                    </li>
                                    <li class="email">
                                        <i class="soap-icon-message circle"></i>
                                        <h5 class="title">Email</h5>
                                        <p>info@vacaaay.com</p>
                                        <p><a href="http://www.vacaaay.com/" target="_blank">www.vacaaay.com</a></p>
                                    </li>
                                </ul>
                                <ul class="social-icons full-width">
                                    <li><a href="#" data-toggle="tooltip" title="Twitter"><i class="soap-icon-twitter"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" title="GooglePlus"><i class="soap-icon-googleplus"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" title="Facebook"><i class="soap-icon-facebook"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" title="Linkedin"><i class="soap-icon-linkedin"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" title="Vimeo"><i class="soap-icon-vimeo"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-8 col-md-9">
                            <div class="travelo-box">
                                <form class="contact-form" action="contact-us-handler.php" method="post" onsubmit="return false;">
                                    <h4 class="box-title">Send us a Message</h4>
                                    <div class="alert small-box" style="display: none;"></div>
                                    <div class="row form-group">
                                        <div class="col-xs-6">
                                            <label>Your Name</label>
                                            <input type="text" name="name" class="form-control">
                                        </div>
                                        <div class="col-xs-6">
                                            <label>Your Email</label>
                                            <input type="text" name="email" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Your Message</label>
                                        <textarea name="message" rows="6" class="form-control" placeholder="write message here"></textarea>
                                    </div>
                                    <button type="submit" class="btn-large full-width">SEND MESSAGE</button>
                                </form>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
        <br />
        <br />
        <br />
        <div class="clearfix"></div>
        
        <br />
        <br />
    </section>
</asp:Content>
