﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="PackageBooking.aspx.cs" Inherits="CUTUK.PackageBooking" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="scripts/PackageBooking.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="content" class="gray-area">
        <div class="container">
            <div class="row">
                <div id="main" class="col-sms-6 col-sm-8 col-md-9">
                    <div class="booking-section travelo-box">

                        <form class="booking-form">
                            <div class="card-information">
                                <h2>Passenger Details</h2>
                                <div class="form-group row">
                                    <div class="col-sm-6 col-md-5">
                                        <label>Name</label>
                                        <input type="text" id="txt_name" class="form-control" value="" placeholder="Enter Your Name" />
                                    </div>
                                    <div class="col-sm-6 col-md-5">
                                        <label>Mobile No </label>
                                        <input type="text" id="txt_phone" class="form-control" value="" placeholder="Enter Your Mobile No" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 col-md-5">
                                        <label>E-mail</label>
                                        <input type="text" id="txt_email" class="form-control" value="" placeholder="Enter Your E-mail" />
                                    </div>
                                    <div class="col-sm-6 col-md-5">
                                        <label>Travel date</label>
                                        <div class="datepicker-wrap">
                                            <input type="text" id="txt_date" name="form-control" placeholder="mm/dd/yy" class="form-control" />
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <div class="col-sm-6 col-md-10">
                                        <label>AddOnReq</label>
                                        <textarea name="message" rows="4" id="txt_Remark" class="form-control" placeholder="write message here"></textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-3 col-md-3">
                                        <label>Adults</label>
                                        <select name="card_type" id="Select_Adults" class="form-control">
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                        </select>

                                    </div>
                                    <div class="col-sm-3 col-md-3">
                                        <label>CHILDS</label>
                                        <select name="card_type" id="Select_CHILD" onchange="NoChild(this.value)" class="form-control">
                                            <option>0</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-2 col-md-2" id="div_FirstChild" style="display: none">
                                        <label>CHILD 1 Age</label>
                                        <select name="card_type" id="Select_AgeChildSecond1" class="form-control">
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                            <option>6</option>
                                            <option>7</option>
                                            <option>8</option>
                                            <option>9</option>
                                            <option>10</option>
                                            <option>11</option>
                                        </select>

                                    </div>
                                    <div class="col-sm-2 col-md-2" id="div_SecondChild" style="display: none">
                                        <label>CHILD 2 Age</label>
                                        <select name="card_type" id="Select_AgeChildSecond2" class="form-control">
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                            <option>6</option>
                                            <option>7</option>
                                            <option>8</option>
                                            <option>9</option>
                                            <option>10</option>
                                            <option>11</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-2 col-md-2" style="float: right">
                                    <button type="button" onclick="CheckUserLogin()" class="button btn-small orange">Book</button>
                                </div>
                            </div>
                            <br />
                        </form>
                    </div>
                </div>
                <div class="sidebar col-sms-6 col-sm-4 col-md-3">
                    <div class="booking-details travelo-box" id="Right">
                        <%--<h4>Booking Details</h4>
                        <hr /><br />
                        <article class="tour-detail">
                           <h6><b> Package Name </b> </h6>
                            <div class="details">

                                <div class="icon-box style11 full-width">
                                    <div class="icon-wrapper">
                                        <i class="soap-icon-departure"></i>
                                    </div>
                                    <dl class="details">
                                        <dt class="skin-color">Location</dt>
                                        <dd>Rome</dd>
                                    </dl>
                                </div>
                                 <div class="icon-box style11 full-width">
                                    <div class="icon-wrapper">
                                        <i class="soap-icon-clock"></i>
                                    </div>
                                    <dl class="details">
                                        <dt class="skin-color">Duration</dt>
                                        <dd>1 day 3 hours</dd>
                                    </dl>
                                </div>
                                <div class="icon-box style11 full-width">
                                    <div class="icon-wrapper">
                                        <i class="soap-icon-calendar"></i>
                                    </div>
                                    <dl class="details">
                                        <dt class="skin-color">Date</dt>
                                        <dd>27 jan 2015 to 30 jan 2015</dd>
                                    </dl>
                                </div>
                                
                            </div>
                        </article>--%>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="AlertModal" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="border-bottom: 1px solid #fff">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" title="Close">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <%-- <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Incomplete Document</b><label id="No"></label></div>--%>
                        <div>
                            <p class="size17" id="AlertMessage">
                            </p>
                            <br />

                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade bs-example-modal-lg" id="RegisterationModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="border-bottom: 1px solid #fff">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel3" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="tab-container style1">
                            <ul class="tabs">
                                <li class="active"><a href="#Login" data-toggle="tab" aria-expanded="true">Login</a></li>
                                <li class=""><a href="#Register" data-toggle="tab" aria-expanded="false">Register</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="Login">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <span class="size13 dark">Username / Email : </span>
                                            <input type="text" id="txtUserName" data-trigger="focus" data-placement="top" placeholder="Username" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your B2C_Id" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_txtUserName">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-6">
                                            <span class="size13 dark">Password : </span>
                                            <input type="password" id="txtPassword" data-trigger="focus" data-placement="top" placeholder="Password" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your B2C_Id" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_txtPassword">
                                                <b>* This field is required</b></label>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="block" style="float: right">
                                        <input type="button" onclick="LoginPackageBooking()" class="button btn-small sky-blue1" value="Login" />
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="Register">

                                    <div class="row">
                                        <div class="col-md-4">
                                            <span class="size13 dark">User Name : </span>
                                            <input type="text" id="txt_UserName" data-trigger="focus" data-placement="top" placeholder="User Name" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your User Name" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_UserName">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-4">
                                            <span class="size13 dark">Password: </span>
                                            <input type="password" id="txt_Password" data-trigger="focus" data-placement="top" placeholder="Password" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your Password" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_Password">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-4">
                                            <span class="size13 dark">Confirm Password: </span>
                                            <input type="password" id="txt_ConfirmPassword" data-trigger="focus" data-placement="top" placeholder="Confirm Password" class="form-control" data-content="This field is mandatory" data-original-title="Here you need to confirm your Password" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_ConfirmPassword">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-4">
                                            <br />
                                            <span class="size13 dark">Mobile : </span>
                                            <input type="text" id="txt_Mobile" data-trigger="focus" data-placement="top" placeholder="Mobile" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your mobile number" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_Mobile">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-4">
                                            <br />
                                            <span class="size13 dark">Phone : </span>
                                            <input type="text" id="txt_Phone" data-trigger="focus" data-toggle="popover" data-placement="top" placeholder="Phone" class="form-control" data-content="This field is mandatory" data-original-title="Here you can create your Phone" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_Phone">
                                                <b>* This field is required</b></label>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <span class="size13 dark">Address : </span>
                                            <input type="text" id="txt_Address" data-trigger="focus" data-placement="top" placeholder="Address" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit address" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_Address">
                                                <b>* This field is required</b></label>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <span class="size13 dark">City : </span>
                                            <select id="selCity" class="form-control">
                                                <option selected="selected" value="-">Select Any City</option>
                                            </select>
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_City">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-4">
                                            <span class="size13 dark">Pin Code : </span>
                                            <input type="text" id="txt_PinCode" data-trigger="focus" data-placement="top" placeholder="Pin Code" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit pin code" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_PinCode">
                                                <b>* This field is required</b></label>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="block" style="float: right">
                                        <input type="button" id="btn_RegiterAgent" value="Register" class="button btn-small sky-blue1" onclick="return ValidatePackageRegistration()" />
                                        <input type="button" id="btn_Cancel" value="Cancel" class="button btn-small orange" onclick="ClearPackageRegistration();" />
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
