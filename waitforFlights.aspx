﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="waitforFlights.aspx.cs" Inherits="CUTUK.waitforFlights" %>

<!DOCTYPE html>
<!--[if IE 8]>          <html class="ie ie8"> <![endif]-->
<!--[if IE 9]>          <html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="">
<!--<![endif]-->
<head>
    <!-- Page Title -->
    <title>Wait For Response</title>

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Travelo - Travel, Tour Booking HTML5 Template">
    <meta name="author" content="SoapTheme">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    <!-- Theme Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,200,300,500' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/animate.min.css">
    <script src="js/jquery-2.1.3.min.js"></script>
    <!-- Main Style -->
    <link id="main-style" rel="stylesheet" href="css/style.css">

    <!-- Updated Styles -->
    <link rel="stylesheet" href="css/updates.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="css/custom.css">

    <!-- Responsive Styles -->
    <link rel="stylesheet" href="css/responsive.css">
    <script src="js/modal.js"></script>
    <script src="scripts/Alerts.js"></script>

    <!-- CSS for IE -->
    <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie.css" />
    <![endif]-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->
</head>
<body class="soap-login-page style3 body-blank">
    <div id="page-wrapper" class="wrapper-blank">
        <header id="header" class="navbar-static-top">
            <a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle blue-bg">Mobile Menu Toggle</a>
            <div class="container">
                <h1 class="logo"></h1>
            </div>
        </header>
        <section id="content">
            <div class="container">
                <div id="main">
                    <h4 class="logo block">
                        <a href="Default.aspx" title="">
                            <img src="images/vacaaay1.png" alt="logo" />
                        </a>
                    </h4>
                    <br />
                    <div class="welcome-text box" style="font-size: 1.7em;">WE'RE ON IT! SEARCHING THE BEST PRICES.</div>
                    <img src="images/loading.gif" style="height: 50px" />
                    <br />
                    <a href="Default.aspx" style="margin-left: 40%" title="">Back</a>
                    <%--<p class="white-color block" style="font-size: 1.5em;">In a few moments, you'll be celebrating hotel options galore! </p>--%>
                    <form runat="server">
                        <div class="col-sm-8 col-md-12 col-lg-6 no-float no-padding center-block">
                            <div id="tour-details" class="travelo-box">
                                <div class="intro small-box table-wrapper full-width hidden-table-sms">

                                    <div class="col-sm-12 table-cell">
                                        <div class="detailed-features">
                                            <div class="flights table-wrapper">
                                                <div class="table-row">
                                                    <div class="table-cell">
                                                        <dl style="margin-right: 15px;">
                                                            <dt>Flight Type :</dt>
                                                        </dl>
                                                    </div>
                                                    <div class="table-cell">
                                                        <dl style="margin-left: 15px;">
                                                            <dt>
                                                                <asp:Label ID="lbl_Type" runat="server" Text="Label"></asp:Label>
                                                            </dt>
                                                        </dl>
                                                    </div>
                                                </div>

                                                <div class="table-row">
                                                    <div class="table-cell">
                                                        <dl style="margin-right: 15px;">
                                                            <dt>Departure :</dt>
                                                        </dl>
                                                    </div>
                                                    <div class="table-cell">
                                                        <dl style="margin-left: 15px;">
                                                            <dt>
                                                                <asp:Label ID="lbl_Departure" runat="server" Text="Label"></asp:Label>
                                                                on
                                                                <asp:Label ID="lbl_DepartDate" runat="server" Text="Label"></asp:Label>
                                                            </dt>
                                                        </dl>
                                                    </div>
                                                </div>

                                                <div class="table-row">
                                                    <div class="table-cell">
                                                        <dl style="margin-right: 15px;">
                                                            <dt>Adults :</dt>
                                                        </dl>
                                                    </div>
                                                    <div class="table-cell">
                                                        <dl style="margin-left: 15px;">
                                                            <dt>
                                                                <asp:Label ID="lbl_AdultCount" runat="server" Text="Label"></asp:Label>
                                                            </dt>
                                                        </dl>
                                                    </div>
                                                </div>

                                                <div class="table-row">
                                                    <div class="table-cell">
                                                        <dl style="margin-right: 15px;">
                                                            <dt>Childs :</dt>
                                                        </dl>
                                                    </div>
                                                    <div class="table-cell">
                                                        <dl style="margin-left: 15px;">
                                                            <dt>
                                                                <asp:Label ID="lbl_Childs" runat="server" Text="Label"></asp:Label>
                                                            </dt>
                                                        </dl>
                                                    </div>
                                                </div>

                                                <div class="table-row">
                                                    <div class="table-cell">
                                                        <dl style="margin-right: 15px;">
                                                            <dt>Infant :</dt>
                                                        </dl>
                                                    </div>
                                                    <div class="table-cell">
                                                        <dl style="margin-left: 15px;">
                                                            <dt>
                                                                <asp:Label ID="lbl_Infants" runat="server" Text="Label"></asp:Label>
                                                            </dt>
                                                        </dl>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
    <!-- Javascript -->
    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.noconflict.js"></script>
    <script type="text/javascript" src="js/modernizr.2.7.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="js/jquery-ui.1.10.4.min.js"></script>

    <!-- Twitter Bootstrap -->
    <script type="text/javascript" src="js/bootstrap.js"></script>

    <!-- parallax -->
    <script type="text/javascript" src="js/jquery.stellar.min.js"></script>

    <!-- waypoint -->
    <script type="text/javascript" src="js/waypoints.min.js"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="js/theme-scripts.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>

    <script type="text/javascript">
        enableChaser = 0; //disable chaser
    </script>
    <script>
        function errorMessage() {
            $('.login-wrap').animo({ animation: 'tada' });
        }
	</script>

    <script type="text/javascript">
        function FligtList() {
            var Search = JSON.parse(getParameterByName('Search'));
            $.ajax({
                type: "POST",
                url: "handler/FlightHandler.asmx/SerchFlight",
                data: JSON.stringify({ ObjSearch: Search }),
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.retCode == 1) {
                        window.location.href = "flight.aspx";
                    }
                    if (result.retCode == 0) {
                        setTimeout(Success(result.errormessage), 5000);
                    }
                },
                error: function () {
                    alert("Somethings went wrong")
                }
            });
        }
        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }
        $(document).ready(function () {
            FligtList();
        });
    </script>
    <div class="modal fade" id="AlertModal" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="width: 40%; padding-top: 15%">

            <div class="modal-content">
                <div class="modal-header" style="border-bottom: none">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>

                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="frow2">
                            <div>
                                <center><span id="AlertMessage"></span></center>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="ModelMessege" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="width: 40%; padding-top: 15%">

            <div class="modal-content">
                <div class="modal-header" style="border-bottom: none">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>

                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="frow2">
                            <div>
                                <center><span id="SpnMessege"></span></center>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="ConformModal" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <%--   <div class="modal-header alert-danger" style="border-bottom: none">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>
                    <h4>Conformation</h4>
                </div>--%>
                <div class="modal-body">
                    <p class="size17" id="ConformMessage">
                    </p>
                </div>
                <div class="modal-footer">
                    <input type="button" id="Cancel" value="Cancel" onclick="Cancel()" class="btn-search4" style="float: none;">
                    <input type="button" value="Ok" id="btnOk" class="btn-search4" style="float: none;">
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</body>
</html>
