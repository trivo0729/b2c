﻿using CUT.BL;
using CUTUK.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CUTUK
{
    /// <summary>
    /// Summary description for B2CCustomerLoginHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class B2CCustomerLoginHandler : System.Web.Services.WebService
    {

        [WebMethod(true)]
        public string UserRegistration(string UserName, string Password, string Contact1, string Contact2, string Address, string CityId)
        {
            DBHelper.DBReturnCode retCode = B2CCustomerLoginManager.RegisterUser(UserName, Password, Contact1, Contact2, Address, CityId);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                return "{\"Session\":\"1\",\"retCode\":\"2\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }


        [WebMethod(true)]
        public string GetMerchantDetails()
        {
            Decimal Amount = Convert.ToDecimal(Session["OnlineAmount"]);
            string Installation_ID = System.Configuration.ConfigurationManager.AppSettings["Installation_ID"];// setting URL
            return "{\"Session\":\"1\",\"retCode\":\"1\",\"Amount\":\"" + Amount + "\",\"Installation_ID\":\"" + Installation_ID + "\"}";
        }




    }
}
