﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="FlightBooking.aspx.cs" Inherits="CUTUK.FlightBooking" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="js/jquery.datetimepicker.css" rel="stylesheet" />
    <script src="scripts/BookingFlight.js"></script>
    <script src="js/tooltip.js"></script>
    <script src="js/jquery.datetimepicker.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="content" class="gray-area">
        <img alt="" src="images/Processing.gif" id="loders" style="z-index: 9999; position: fixed; margin-top: 5%; margin-left: 40%; width: 10%">
        <div class="container">
            <div class="row">
                <div id="main" class="col-sms-8 col-sm-8 col-md-8">
                    <div class="booking-section travelo-box">
                        <form class="booking-form" id="div_Pax">
                        </form>
                    </div>
                </div>
                <div class="sidebar col-sms-6 col-sm-4 col-md-4">
                    <div class="booking-details travelo-box" id="Div_Trip">
                    </div>

                    <div class="travelo-box contact-box">
                        <h4>Need vacaaay Help?</h4>
                        <p>We would be more than happy to help you. Our team advisor are 24/7 at your service to help you.</p>
                        <address class="contact-details">
                            <%--<span class="contact-phone"><i class="soap-icon-phone"></i>+91 9910084810 </span>
                            <br>
                            <span class="contact-phone"><i class="soap-icon-phone"></i>0562 -22334810</span><br>--%>
                            <span class="contact-phone"><i class="soap-icon-message"></i>info@Vacaaay.com</span>
                        </address>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <div class="modal fade" id="ConfirmModal" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="margin-top: 10%">
                <div class="modal-header" style="border-bottom: none">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div id="Div_Modal"></div>
                    <hr />
                    <div class="form-group row" style="margin-bottom: 0px">
                        <br>
                        <div class="col-sm-12 col-md-12">
                            <div id="paypal-button" style="float: right"></div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="amount" id="Total_Amount" />
            </div>
        </div>
        <!-- /.modal-content -->
    </div>

    <div class="modal fade bs-example-modal-lg" id="RegisterationModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="border-bottom: 1px solid #fff">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel3" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="tab-container style1">
                            <ul class="tabs">
                                <li class="active"><a href="#Login" data-toggle="tab" aria-expanded="true">Login</a></li>
                                <li class=""><a href="#Register" data-toggle="tab" aria-expanded="false">Register</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="Login">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <span class="size13 dark">Username / Email : </span>
                                            <input type="text" id="txtUserName" data-trigger="focus" data-placement="top" placeholder="Username" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your B2C_Id" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_txtUserName">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-6">
                                            <span class="size13 dark">Password : </span>
                                            <input type="password" id="txtPassword" data-trigger="focus" data-placement="top" placeholder="Password" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your B2C_Id" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_txtPassword">
                                                <b>* This field is required</b></label>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="block" style="float: right">
                                        <input type="button" onclick="LoginForFlightBooking()" class="button btn-small sky-blue1" value="Login" />
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="Register">

                                    <div class="row">
                                        <div class="col-md-4">
                                            <span class="size13 dark">User Name : </span>
                                            <input type="text" id="txt_UserName" data-trigger="focus" data-placement="top" placeholder="User Name" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your User Name" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_UserName">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-4">
                                            <span class="size13 dark">Password: </span>
                                            <input type="password" id="txt_Password" data-trigger="focus" data-placement="top" placeholder="Password" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your Password" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_Password">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-4">
                                            <span class="size13 dark">Confirm Password: </span>
                                            <input type="password" id="txt_ConfirmPassword" data-trigger="focus" data-placement="top" placeholder="Confirm Password" class="form-control" data-content="This field is mandatory" data-original-title="Here you need to confirm your Password" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_ConfirmPassword">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-4">
                                            <br />
                                            <span class="size13 dark">Mobile : </span>
                                            <input type="text" id="txt_Mobile" data-trigger="focus" data-placement="top" placeholder="Mobile" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your mobile number" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_Mobile">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-4">
                                            <br />
                                            <span class="size13 dark">Phone : </span>
                                            <input type="text" id="txt_Phone" data-trigger="focus" data-toggle="popover" data-placement="top" placeholder="Phone" class="form-control" data-content="This field is mandatory" data-original-title="Here you can create your Phone" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_Phone">
                                                <b>* This field is required</b></label>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <span class="size13 dark">Address : </span>
                                            <input type="text" id="txt_Address" data-trigger="focus" data-placement="top" placeholder="Address" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit address" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_Address">
                                                <b>* This field is required</b></label>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <span class="size13 dark">City : </span>
                                            <select id="selCity" class="form-control">
                                                <option selected="selected" value="-">Select Any City</option>
                                            </select>
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_City">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-4">
                                            <span class="size13 dark">Pin Code : </span>
                                            <input type="text" id="txt_PinCode" data-trigger="focus" data-placement="top" placeholder="Pin Code" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit pin code" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_PinCode">
                                                <b>* This field is required</b></label>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="block" style="float: right">
                                        <input type="button" id="btn_RegiterAgent" value="Register" class="button btn-small sky-blue1" onclick="return ValidateRegistrationForFlight()" />
                                        <input type="button" id="btn_Cancel" value="Cancel" class="button btn-small orange" onclick="ClearRegistrationForFlight();" />
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function GetQueryStringParams(sParam) {
            var sPageURL = window.location.search.substring(1);
            var sURLVariables = sPageURL.split('&');
            for (var i = 0; i < sURLVariables.length; i++) {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] == sParam) {
                    return sParameterName[1];
                }
            }
        }
        var FlightId = GetQueryStringParams("FlightID").split(',');
        $(function () {
            GetBookingDetails()
        })
        function GetBookingDetails() {
            var Flights = new Array();
            Flights = FlightId
            $.ajax({
                type: "POST",
                url: "../handler/FlightHandler.asmx/GetBooking",
                data: JSON.stringify({ ResultIndex: Flights }),
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.retCode == 1) {
                        arrPax = result.arrFligts;
                        arrCountry = result.arrCountry;
                        arrFlightDetails = result.arrFlights;
                        GenratePaxDetails();
                        GenrateFlightDetails();
                        GenerateModal();
                        // GetSSR();
                        //GetBreckDown();
                    }
                    else if (result.retCode == 0) {
                        $('#AgencyBookingCancelModal').modal('hide')
                        $('#ModelMessege').modal('show')
                    }
                },
                error: function () {
                    $('#AgencyBookingCancelModal').modal('hide')
                    $('#SpnMessege').text("something went wrong");
                    $('#ModelMessege').modal('show')
                    // alert("something went wrong");
                },
                complete: function () {
                    $("#dlgLoader").css("display", "none");
                }
            });
        }
    </script>
    <script src="https://www.paypalobjects.com/api/checkout.js"></script>

    <script>
        var Total = $("#Total_Amount").val();
        paypal.Button.render({
            //env: 'sandbox',
            env: 'production', // Optional: specify 'sandbox' environment
            client: {
                //sandbox: 'AbnH0yJx-bjsnaBwYmg2_aMG5P3vQ56f5YiJ9PSVXd9l5B8Ej9uGg-TbCcVqzvGDfy2QW2sGtnnOxtbA',
                production: 'AWbBKK_cxfyLPZVf9v5UJYmiJ6EnZ8-FoE0QKUTyO0mLCOfplrOmkJVv8aiPsuQkQ0taytnR5oK9bcYL'
            },

            // Show the buyer a 'Pay Now' button in the checkout flow
            // commit: true,

            // payment() is called when the button is clicked
            payment: function (data, actions) {
                // Make a call to the REST API to set up the payment

                return actions.payment.create({

                    transactions: [{

                        amount: {
                            total: Total,
                            currency: 'MYR'
                        }
                    }],
                    redirect_urls: {
                        return_url: 'https://vacaaay.com',
                        cancel_url: 'https://vacaaay.com'
                    }
                });
            },

            // onAuthorize() is called when the buyer approves the payment
            onAuthorize: function (data, actions) {
                return actions.payment.execute()
                  .then(function () {
                      Book();
                      window.alert('Thank you Booking.');


                      // Make a call to the REST API to execute the payment
                      //actions.redirect();
                  });

                if (error === 'INSTRUMENT_DECLINED') {
                    actions.restart();
                }
            },
            onError: function (err) {
                // Show an error page here, when an error occurs
            },

            onCancel: function (data, actions) {
                // Show a cancel page or return to cart
            }
        }, '#paypal-button');
    </script>


</asp:Content>
