﻿using CUT.DataLayer;
using CUTUK.DataLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CUTUK
{
    /// <summary>
    /// Summary description for PackageHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class PackageHandler : System.Web.Services.WebService
    {
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        DBHandlerDataContext DB = new DBHandlerDataContext();
        string json = "";


        [WebMethod(EnableSession = true)]
        public string GetAllPackages()
        {
            try
            {
                var List = (from Package in DB.tbl_Packages
                            join PackageImg in DB.tbl_PackageImages on Package.nID equals PackageImg.nPackageID
                            where Package.ParentID == Convert.ToInt64(ConfigurationManager.AppSettings["Id"])
                            select new
                            {
                                Package.nID,
                                Package.sPackageName,
                                Package.sPackageDestination,
                                Package.sPackageCategory,
                                Package.sPackageThemes,
                                Package.sPackageDescription,
                                Package.nDuration,
                                Package.sInventory,
                                Package.dValidityFrom,
                                Package.dValidityTo,
                                PackageImg.ImageArray,
                            }).Distinct().ToList();
                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetPrice(Int64 Id)
        {
            try
            {
                var List = (from Price in DB.tbl_PackagePricings
                            where Price.nPackageID == Id
                            select new
                            {
                                Price.dSingleAdult

                            }).ToList().FirstOrDefault();
                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
            }
            catch
            {

                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetPackageDetail(Int64 Id)
        {
            try
            {

                var Pack = (from Package in DB.tbl_Packages
                            where Package.nID == Id && Package.ParentID == Convert.ToInt64(ConfigurationManager.AppSettings["Id"])
                            select Package).ToList();

                var PackImg = (from Images in DB.tbl_PackageImages
                               where Images.nPackageID == Id
                               select Images).ToList();

                var Pric = (from Price in DB.tbl_PackagePricings
                            where Price.nPackageID == Id
                            select Price).ToList();

                var Itn = (from Itinary in DB.tbl_Itineraries
                           where Itinary.nPackageID == Id
                           select Itinary).ToList();

                var ItnHotels = (from Hotels in DB.tbl_ItineraryHotels
                                 where Hotels.nPackageID == Id
                                 select Hotels).ToList();

                var HotelImgs = (from Imgs in DB.tbl_HotelImages
                                 where Imgs.nPackageID == Id
                                 select Imgs).ToList().Distinct();

                HttpContext.Current.Session["Pack_Session"] = Pack;
                HttpContext.Current.Session["Pric_Session"] = Pric;

                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = Pack, PackImg = PackImg, Pric = Pric, Itn = Itn, ItnHotels = ItnHotels, HotelImgs = HotelImgs });
            }
            catch
            {

                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }


        [WebMethod(EnableSession = true)]
        public string GetPackage(Int64 Id)
        {
            try
            {

                var Pack = (from Package in DB.tbl_Packages
                            where Package.nID == Id && Package.ParentID == Convert.ToInt64(ConfigurationManager.AppSettings["Id"])
                            select Package).ToList();

                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = Pack });
            }
            catch
            {

                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }


        [WebMethod(EnableSession = true)]
        public string GetPackagePrice(Int64 Id, string CategoryName)
        {
            try
            {

                var Pack = (from PackagePrice in DB.tbl_PackagePricings
                            where PackagePrice.nPackageID == Id && PackagePrice.sCategoryName == CategoryName
                            select PackagePrice).ToList();

                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = Pack });
            }
            catch
            {

                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }


        [WebMethod(true)]
        public string BookPackage(string nID, string noOfDays, decimal AdultPrice, decimal ChildBedsPrice, decimal ChildWBed, string CategoryID, string Name, string MobileNo, string Email, Int64 noAdults, Int64 noChild, string noInfant, string ChildAges, string TarvelDate, string AddOnReq, string PackageName, string StartDate, string TillDate, string CategoryType)
        {
            string jsonString = "";
            try
            {
                GlobalDefault objGlobalDefaultCustomer = (GlobalDefault)HttpContext.Current.Session["LoginCustomer"];
                string CustomerId = objGlobalDefaultCustomer.B2C_Id;
                tbl_PackageReservation Add = new tbl_PackageReservation();
                Add.PackageID = Convert.ToInt64(nID);
                Add.CategoryId = Convert.ToInt64(CategoryID);
                Add.TravelDate = TarvelDate;
                Add.StartFrom = StartDate;
                Add.EndDate = TillDate;
                Add.noDays = noOfDays;
                Add.Adults = noAdults;
                Add.Childs = noChild;
                Add.Infant = 0;
                Add.AdultPrice = AdultPrice;
                Add.ChildBedsPrice = ChildBedsPrice;
                Add.ChildWBed = ChildWBed;
                Add.LeadingPax = Name;
                Add.email = Email;
                Add.PhoneNumber = MobileNo;
                //Add.Nationality = Nationality;
                Add.Type = CategoryType;
                Add.AddOn = AddOnReq;
                Add.ChildAges = ChildAges;
                Add.B2CId = CustomerId;
                DB.tbl_PackageReservations.InsertOnSubmit(Add);
                DB.SubmitChanges();

                PackageMail.BookPackage(nID, CategoryID, Name, MobileNo, Email, noAdults, noChild, noInfant, ChildAges, TarvelDate, AddOnReq, PackageName, StartDate, TillDate, CategoryType);
                PackageMail.AdminMail(nID, CategoryID, Name, MobileNo, Email, noAdults, noChild, noInfant, ChildAges, TarvelDate, AddOnReq, PackageName, StartDate, TillDate, CategoryType);
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;


        }

    }
}
