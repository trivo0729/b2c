﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HotelBooking.aspx.cs" Inherits="CUTUK.HotelBooking" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Hotel Booking</title>

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Travelo - Travel, Tour Booking HTML5 Template">
    <meta name="author" content="SoapTheme">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    <!-- Theme Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,500,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/animate.min.css">

    <!-- Current Page Styles -->
    <link rel="stylesheet" type="text/css" href="components/revolution_slider/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/revolution_slider/css/style.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/jquery.bxslider/jquery.bxslider.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/flexslider/flexslider.css" media="screen" />

    <!-- Main Style -->
    <link id="main-style" rel="stylesheet" href="css/style.css" />

    <!-- Updated Styles -->
    <link rel="stylesheet" href="css/updates.css" />

    <!-- Custom Styles -->
    <link rel="stylesheet" href="css/custom.css" />

    <!-- Responsive Styles -->
    <link rel="stylesheet" href="css/responsive.css" />
    <script src="scripts/Alerts.js"></script>
    <script src="js/jquery-2.0.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/tooltip.js"></script>
    <script src="scripts/BookingDesign.js"></script>
    <script src="scripts/CountryCityCode.js"></script>
    <script>
        window.onload = function () {
            var d = new Date().getTime();
            document.getElementById("tid").value = d;
        };
    </script>
</head>
<body>
    <div id="page-wrapper">

        <header id="header" class="navbar-static-top style4">
            <div class="container">
                <h1 class="logo navbar-brand">
                    <a href="Default.aspx" title="Vacaay.com - home">
                        <img src="images/vacaaay1.png" alt="Vacaay logo" style="width: 306px; height: 50px;" />
                    </a>
                </h1>
                <div class="pull-right hidden-mobile">
                    <ul class="social-icons clearfix pull-right hidden-mobile">
                        <li class="twitter"><a title="twitter" href="#" data-toggle="tooltip"><i class="soap-icon-twitter"></i></a></li>
                        <li class="facebook"><a title="facebook" href="#" data-toggle="tooltip"><i class="soap-icon-facebook"></i></a></li>
                        <li class="linkedin"><a title="linkedin" href="#" data-toggle="tooltip"><i class="soap-icon-linkedin"></i></a></li>
                        <li class="vimeo"><a title="vimeo" href="#" data-toggle="tooltip"><i class="soap-icon-vimeo"></i></a></li>
                    </ul>
                </div>
            </div>
            <a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle">Mobile Menu Toggle
            </a>
            <div class="main-navigation">
                <div class="container">
                    <nav id="main-menu" role="navigation">
                        <ul class="menu">
                            <li class="menu-item-has-children">
                                <a href="Default.aspx">Home</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="AboutUs.aspx">About Us</a>
                            </li>

                            <%-- <li class="menu-item-has-children">
                                <a href="HotelSearch.aspx">Hotels</a>
                            </li>--%>

                            <li class="menu-item-has-children">
                                <a href="Packages.aspx">Packages</a>
                            </li>

                            <%-- <li class="menu-item-has-children">
                                <a href="TourPackages.aspx">Tour Packages</a>
                            </li>--%>


                            <li class="menu-item-has-children">
                                <a href="Contact.aspx">Contact Us</a>
                            </li>

                        </ul>
                        <ul class="menu pull-right">
                            <li><a href="#" onclick="SignUpModal()">login / Register</a></li>
                            <li><a href="http://b2b.vacaaay.com/">B2B Login</a></li>
                        </ul>
                    </nav>
                </div>
            </div>

            <nav id="mobile-menu-01" class="mobile-menu collapse">
                <ul id="mobile-primary-menu" class="menu">
                    <li class="menu-item-has-children">
                        <a href="Default.aspx">Home</a>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="AboutUs.aspx">About Us</a>
                    </li>

                    <%--<li class="menu-item-has-children">
                        <a href="HotelSearch.aspx">Hotels</a>
                    </li>--%>

                    <li class="menu-item-has-children">
                        <a href="Packages.aspx">Packages</a>
                    </li>


                    <li class="menu-item-has-children">
                        <a href="Contact.aspx">Contact Us</a>
                    </li>

                </ul>
                <ul class="menu pull-right">
                    <li><a href="#" onclick="SignUpModal()">login / Register</a></li>
                    <li><a href="http://b2b.vacaaay.com/">B2B Login</a></li>
                </ul>
            </nav>
        </header>

        <div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">Hotel Booking</h2>
                </div>
                <ul class="breadcrumbs pull-right">
                    <li><a href="Default.aspx">HOME</a></li>
                    <li class="active">Hotel Booking</li>
                </ul>
            </div>
        </div>

        <section id="content" class="gray-area">
            <div class="container">
                <div class="row">
                    <div id="main" class="col-sms-6 col-sm-8 col-md-8">
                        <div class="booking-section travelo-box">


                            <div class="Hoteldetails" style="display: none"></div>
                            <div class="person-information" id="Rooms">
                            </div>


                            <div class="person-information">
                                <h2>Would you please give more details?</h2>
                                <hr />
                                <p>Please provide some additional information about you </p>
                                <br />

                                <div class="row">
                                    <div class="col-md-12">
                                        <form method="post" name="customerData" action="ccavRequestHandler.aspx">
                                            <input type="hidden" name="tid" id="tid" />
                                            <input type="hidden" name="amount" id="Amount" />
                                            <input type="hidden" name="order_id" id="order_id" />
                                            <input type="hidden" name="merchant_id" id="merchant_id" value="174467" />
                                            <input type="hidden" name="currency" id="currency" value="INR" />

                                            <input type="hidden" name="redirect_url" id="redirect_url" />
                                            <input type="hidden" name="cancel_url" value="http://Vacaaay.com/" id="cancel_url" />

                                            <div class="form-group row">
                                                <div class="col-sm-4 col-md-4">
                                                    <label>Your Name :</label>
                                                    <input id="txt_AgentBookingreference" name="billing_name" type="text" class="form-control " placeholder="" required="" />
                                                </div>
                                                <div class="col-sm-4 col-md-4">
                                                    <label>Contact Number :</label>
                                                    <input id="txt_CustomerMobileNumber" name="billing_tel" type="text" class="form-control " placeholder="" required="" />
                                                </div>
                                                <div class="col-sm-4 col-md-4">
                                                    <label>Email :</label>
                                                    <input id="txt_AgentEmail" name="billing_email" type="email" class="form-control " placeholder="" required="" />
                                                </div>
                                            </div>
                                            <button type="submit" style="display: none" id="Btn_CCAvenue" class="button btn-small sky-blue1">BOOKING</button>
                                        </form>
                                        <div class="form-group row">
                                            <div class="col-sm-12 col-md-12">
                                                <label>Remark:</label>
                                                <textarea id="txt_BookingRemarks" class="form-control" cols="40"></textarea>
                                            </div>

                                        </div>
                                    </div>
                                </div>



                                <br />
                            </div>

                            <div class="person-information">
                                <h5><b>Make payment</b></h5>
                                <hr />
                                <br />
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <div class="form-group col-sm-10 col-md-10" id="sptdHoldingTime">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="term" id="chkTermsAndCondition" />
                                                    I accept the cancellation policy and <a href="#" data-toggle="modal" data-target="#TermsConditionModal" class="orange" data-original-title="" title=""><span class="skin-color">Terms and Conditions</span></a>.
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 col-md-2">

                                            <button type="button" onclick="CheckLoginUser()" class="button btn-small sky-blue1">BOOKING</button>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="sidebar col-sms-6 col-sm-4 col-md-4">
                        <div class="booking-details travelo-box" id="SessionDetails">
                        </div>


                        <div id="RigthRooms"></div>
                        <div id="Total"></div>
                        <%--<div class="booking-details travelo-box">
                            <dl class="other-details">
                                <dt class="total-price">Total Price</dt>
                                <dd class="total-price-value">$252</dd>
                            </dl>
                        </div>--%>

                        <div class="travelo-box contact-box">
                            <h4>Need Vacaaay Help?</h4>
                            <p>We would be more than happy to help you. Our team advisor are 24/7 at your service to help you.</p>
                            <address class="contact-details">
                                <%--<span class="contact-phone"><i class="soap-icon-phone"></i> +91 9910084810 </span><br/>
                                <span class="contact-phone"><i class="soap-icon-phone"></i> 0562 -22334810</span><br/>--%>
                                <span class="contact-phone"><i class="soap-icon-message"></i>info@Vacaaay.com</span>
                            </address>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="modal fade bs-example-modal-lg" id="TermsConditionModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <%--   <img src="../images/dash/logo.png" alt="UnisafetTours.com" />--%>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="gridSystemModalLabel2" style="text-align: left">Terms & Conditions</h4>
                    </div>
                    <div class="modal-body">
                        <div class="scrollingDiv">
                            <div class="WordSection1">

                                <p class="MsoNormal" style='text-align: justify'>
                                    <b style='mso-bidi-font-weight: normal'><span style='mso-no-proof: yes'>Bookings<o:p></o:p></span></b>
                                </p>

                                <p class="MsoNormal" style='text-align: justify'>
                                    A booking may be considered to be
a group booking (depending on our internal / hotel / supplier policy) when
there will be 10 or more pax travelling together. WE reserve the right to
cancel any booking made or if we consider them to have been made for the
purpose of holding space for future sale.<o:p></o:p>
                                </p>

                                <p class="MsoNormal" style='text-align: justify'>
                                    In case <span style='mso-no-proof: yes'>UnisafetTours.com </span>is not able to honour confirmed booking through online
allotment received from hotel / supplier for some reason, <span
    style='mso-no-proof: yes'>WE </span>shall offer the full refund for the service
or provide with an alternative or similar category accommodation for the client
instead.<o:p></o:p>
                                </p>

                                <p class="MsoNormal">
                                    <o:p>&nbsp;</o:p>
                                </p>

                                <p class="MsoNormal"><b style='mso-bidi-font-weight: normal'>On Request Bookings<o:p></o:p></b></p>

                                <p class="MsoNormal" style='text-align: justify'>
                                    The hotels with <b
                                        style='mso-bidi-font-weight: normal'>�OnRequest�</b> button and bookings with On
Request status means that the hotel does not have any available allotment,
however <span style='mso-no-proof: yes'>UnisafetTours.com</span> will contact the
hotel to request extra space. Kindly be informed that in very rare occasions,
the availability at the hotel might change until the time you complete your
booking and the final status might turn to OnRequest. In these cases, please do
not attempt to book the same hotel again. You will be informed within 48 hours
if your booking has been confirmed or rejected. If the hotel could be confirmed
at a different rate, you will be advised. Please bear in mind that the
confirmation may not be guaranteed and the hotels have the right to reject ON
REQUEST bookings. If your accommodation request is urgent, it is suggested that
you select a hotel with Instant confirmation.<o:p></o:p>
                                </p>

                                <p class="MsoNormal">
                                    <o:p>&nbsp;</o:p>
                                </p>

                                <p class="MsoNormal" style='text-align: justify'>
                                    <b style='mso-bidi-font-weight: normal'><span style='mso-no-proof: yes'>Hotel Category &amp; Local
Classification and Star (*) Ratings<o:p></o:p></span></b>
                                </p>

                                <p class="MsoNormal" style='text-align: justify'>
                                    <span style='mso-no-proof: yes'>Star
ratings aim to give a general overview of the quality of the hotel and
approximate level of facilities, services and amenities available. However this
rating system does vary from country to country. For example a 5* Bangkok hotel
will not be the same as a 5* London hotel. UnisafetTours.com  are not
responsible for the hotel categories and * ratings as these have been provided
to us and accepted in good faith.<o:p></o:p></span>
                                </p>

                            </div>
                            <hr />
                            <div style="text-align: center;"><a title="" data-original-title="" href="#">UnisafetTours.com</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bs-example-modal-lg" id="HoldTermsConditionModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <%--   <img src="../images/dash/logo.png" alt="UnisafetTours.com" />--%>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="gridSystemModalLabel3" style="text-align: left">Terms & Conditions</h4>
                    </div>
                    <div class="modal-body">
                        <div class="scrollingDiv">
                            <div class="WordSection1">

                                <%--<p class="MsoNormal" style='text-align: justify'>
                                <b style='mso-bidi-font-weight: normal'><span style='mso-no-proof: yes'>Terms & Conditions<o:p></o:p></span></b>
                            </p>--%>

                                <p class="MsoNormal" style='text-align: justify'>
                                    For the benefit of our travel partners we have introduced On-Hold booking feature,
                                 which will enable you to hold booking for 24Hrs / until cancellation deadline (whichever attracts first),
                                 this facility is available even if you don�t have credit limit available on your account, 
                                you can block the room tentatively and reconfirm after getting confirmation from you customer by making the payment from reconfirm booking list
                               
                               

                                    <o:p></o:p>
                                </p>

                                <p class="MsoNormal" style='text-align: justify'>
                                    <b style='mso-bidi-font-weight: normal'><span style='mso-no-proof: yes'>Terms & Conditions<o:p></o:p></span></b>
                                </p>


                                <p class="MsoNormal" style='text-align: justify'>
                                    <ul>
                                        <li>Booking will be hold for 24Hrs from the time of booking or until cancellation deadline (whichever attracts first)</li>
                                        <li>No voucher will be provided for hold booking until booking is confirmed & vouchered</li>
                                        <li>Booking will be cancelled automatically if payment is not made before given deadline</li>
                                        <li>We hold all the rights to cancel any booking in suspect of miss usage of this service</li>
                                        <li>Please read all hotel booking terms & conditions to avoid any miss communication</li>
                                    </ul>
                                </p>

                                <p class="MsoNormal">
                                    <o:p>&nbsp;</o:p>
                                </p>


                            </div>
                            <hr />
                            <div style="text-align: center;"><a title="" data-original-title="" href="#">UnisafetTours.com</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <%-- <asp:HiddenField ID="H1" runat="server" ClientIDMode="Static" Value="" />--%>


        <div class="modal fade bs-example-modal-lg" id="PreviewModalRegisteration" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header" style="border-bottom: 1px solid #fff">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="gridSystemModalLabel3" style="text-align: left"></h4>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12">
                            <div class="tab-container style1">
                                <ul class="tabs">
                                    <li class="active"><a href="#Login" data-toggle="tab" aria-expanded="true">Login</a></li>
                                    <li class=""><a href="#Register" data-toggle="tab" aria-expanded="false">Register</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="Login">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <span class="size13 dark">Username / Email : </span>
                                                <input type="text" id="txtUserName" data-trigger="focus" data-placement="top" placeholder="Username" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your B2C_Id" />
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txtUserName">
                                                    <b>* This field is required</b></label>
                                            </div>
                                            <div class="col-md-6">
                                                <span class="size13 dark">Password : </span>
                                                <input type="password" id="txtPassword" data-trigger="focus" data-placement="top" placeholder="Password" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your B2C_Id" />
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txtPassword">
                                                    <b>* This field is required</b></label>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="block" style="float: right">
                                            <input type="button" onclick="LoginCustomer()" class="button btn-small sky-blue1" value="Login" />
                                            <input type="button" class="button btn-small orange" value="Cancel" onclick="Clear()" />
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="Register">

                                        <div class="row">
                                            <div class="col-md-4">
                                                <span class="size13 dark">User Name : </span>
                                                <input type="text" id="txt_UserName" data-trigger="focus" data-placement="top" placeholder="User Name" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your User Name" />
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_UserName">
                                                    <b>* This field is required</b></label>
                                            </div>
                                            <div class="col-md-4">
                                                <span class="size13 dark">Password: </span>
                                                <input type="password" id="txt_Password" data-trigger="focus" data-placement="top" placeholder="Password" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your Password" />
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_Password">
                                                    <b>* This field is required</b></label>
                                            </div>
                                            <div class="col-md-4">
                                                <span class="size13 dark">Confirm Password: </span>
                                                <input type="password" id="txt_ConfirmPassword" data-trigger="focus" data-placement="top" placeholder="Confirm Password" class="form-control" data-content="This field is mandatory" data-original-title="Here you need to confirm your Password" />
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_ConfirmPassword">
                                                    <b>* This field is required</b></label>
                                            </div>
                                            <div class="col-md-4">
                                                <br />
                                                <span class="size13 dark">Mobile : </span>
                                                <input type="text" id="txt_Mobile" data-trigger="focus" data-placement="top" placeholder="Mobile" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your mobile number" />
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_Mobile">
                                                    <b>* This field is required</b></label>
                                            </div>
                                            <div class="col-md-4">
                                                <br />
                                                <span class="size13 dark">Phone : </span>
                                                <input type="text" id="txt_Phone" data-trigger="focus" data-toggle="popover" data-placement="top" placeholder="Phone" class="form-control" data-content="This field is mandatory" data-original-title="Here you can create your Phone" />
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_Phone">
                                                    <b>* This field is required</b></label>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span class="size13 dark">Address : </span>
                                                <input type="text" id="txt_Address" data-trigger="focus" data-placement="top" placeholder="Address" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit address" />
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_Address">
                                                    <b>* This field is required</b></label>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <span class="size13 dark">City : </span>
                                                <select id="selCity" class="form-control">
                                                    <option selected="selected" value="-">Select Any City</option>
                                                </select>
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_City">
                                                    <b>* This field is required</b></label>
                                            </div>
                                            <div class="col-md-4">
                                                <span class="size13 dark">Pin Code : </span>
                                                <input type="text" id="txt_PinCode" data-trigger="focus" data-placement="top" placeholder="Pin Code" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit pin code" />
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_PinCode">
                                                    <b>* This field is required</b></label>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="block" style="float: right">
                                            <input type="button" id="btn_RegiterAgent" value="Register" class="button btn-small sky-blue1" onclick="return ValidateRegistration()" />
                                            <input type="button" id="btn_Cancel" value="Cancel" class="button btn-small orange" onclick="ClearRegistration();" />
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bs-example-modal-lg" id="PreviewModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" style="width: 700px;">
                    <div class="modal-header" style="border-bottom: 1px solid #fff">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="gridSystemModalLabel3" style="text-align: left"></h4>
                    </div>
                    <div class="modal-body">

                        <%--<div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Booking Details</b></div>--%>
                        <div style="background: rgba(255, 153, 0, 0.4); color: #fff; padding: 3px 10px 3px 10px;"><b>Booking Details</b></div>
                        <div class="frow2">
                            <table>
                                <tr>
                                    <td><span class="dark bold">Hotel Name: </span></td>
                                    <td style="padding-left: 15px;"><span class="dark" id="dspHotelName"></span></td>
                                    <td style="padding-left: 20px;"><span class="dark bold">Rooms: </span></td>
                                    <td style="padding-left: 15px;"><span class="dark" id="dspRooms"></span></td>
                                    <td style="padding-left: 20px;"><span class="dark bold">Nights: </span></td>
                                    <td style="padding-left: 15px;"><span class="dark" id="dspNights"></span></td>
                                </tr>
                                <tr>
                                    <td><span class="dark bold">Check-In: </span></td>
                                    <td style="padding-left: 15px;"><span class="dark" id="dspCheckIn"></span></td>
                                    <td style="padding-left: 20px;"><span class="dark bold">Check-Out: </span></td>
                                    <td style="padding-left: 15px;"><span class="dark" id="dspCheckOut"></span></td>
                                    <%--<td style="padding-left: 20px;"><span class="dark bold">Agency Reference No: </span></td>
                                <td style="padding-left: 15px;"><span class="dark" id="dspAgencyRef"></span></td>--%>
                                </tr>
                            </table>
                            <div class="clearfix"></div>
                            <br />
                        </div>
                        <%--<div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Passenger Details</b></div>--%>
                        <div style="background: rgba(255, 153, 0, 0.4); color: #fff; padding: 3px 10px 3px 10px;"><b>Passenger Details</b></div>
                        <div class="scrollingDiv" style="border-bottom: 1px solid #e5e5e5; margin-bottom: 20px; height: 100px; padding-right: 0px;">
                            <div class="frow2">
                                <table class="table table-bordered" style="text-align: center; margin-bottom: 0px;" id="tblPassengerDetails">
                                </table>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <%--<div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Other Details</b></div>--%>
                        <div style="background: rgba(255, 153, 0, 0.4); color: #fff; padding: 3px 10px 3px 10px;"><b>Other Details</b></div>
                        <div class="frow2">
                            <table>
                                <tbody>
                                    <tr style="display: none">
                                        <td><span class="dark bold">Total Amount: </span></td>
                                        <td style="padding-left: 15px;"><span id="Amount1"></span><span class="dark" id="dspTotalAmount"></span></td>
                                    </tr>
                                    <tr style="display: none">
                                        <td><span class="dark bold">Service Tax: </span></td>
                                        <td style="padding-left: 15px;"><span id="Amount2"></span><span class="dark" id="dspServiceTax"></span></td>
                                        <td style="padding-left: 160px;"><span class="grey bold size18">Status: </span></td>
                                        <td style="padding-left: 15px;"><span class="green size18" id="dspBookingStatus">Booking</span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="dark bold">Total Payable Amount: </span></td>
                                        <td style="padding-left: 15px;"><span id="Amount3"></span><span class="dark" id="dspTotalPayable"></span></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="clearfix"></div>
                            <br />
                        </div>
                        <div class="alert alert-warning">
                            <span id="dspCan1"></span>
                            <span id="dspCan2" style="display: none"></span>
                        </div>

                        <div class="alert alert-warning" id="dspAlertMessage">
                        </div>
                        <div class="frow2" style="text-align: center;">
                            <input type="button" value="Confirm Booking" onclick="HotelConfirmBooking()" id="btnConfirmBooking" class="btn-search4 margtop20" style="float: none;" />
                            <input type="hidden" id="hndBookingStatus" />
                        </div>

                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="ConfirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document" style="width: 740px">
                <div class="modal-content">
                    <div class="modal-header red bold">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="spHoldingTime" style="display: none"></h4>
                    </div>
                    <div class="modal-body">
                        <div class="ConfirmationDetails">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <input type="hidden" name="amount" id="Total_Amount" />
                        <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
                        <input type="button" onclick="ProceedConfirmBooking()" class="btn btn-primary" value="Proceed" style="display: none" />
                        <div id="paypal-button" style="float: right"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bs-example-modal-lg" id="PreviewModalCard" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content " style="width: 700px;">
                    <div class="modal-header" style="border-bottom: 1px solid #fff">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="gridSystemModalLabel3" style="text-align: left"></h4>
                    </div>
                    <div class="modal-body scrollingDiv" style="height: 580px">

                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Booking Details</b></div>
                        <div class="frow2">
                            <table class="table table-responsive">
                                <tr>
                                    <td><span class="dark bold">Hotel Name: </span></td>
                                    <td style="padding-left: 15px;"><span class="dark" id="dspHotelNameC"></span></td>
                                    <td style="padding-left: 20px;"><span class="dark bold">Rooms: </span></td>
                                    <td style="padding-left: 15px;"><span class="dark" id="dspRoomsC"></span></td>
                                    <td style="padding-left: 20px;"><span class="dark bold">Nights: </span></td>
                                    <td style="padding-left: 15px;"><span class="dark" id="dspNightsC"></span></td>
                                </tr>
                                <tr>
                                    <td><span class="dark bold">Check-In: </span></td>
                                    <td style="padding-left: 15px;"><span class="dark" id="dspCheckInC"></span></td>
                                    <td style="padding-left: 20px;"><span class="dark bold">Check-Out: </span></td>
                                    <td style="padding-left: 15px;"><span class="dark" id="dspCheckOutC"></span></td>
                                    <%--<td style="padding-left: 20px;"><span class="dark bold">Agency Reference No: </span></td>
                                <td style="padding-left: 15px;"><span class="dark" id="dspAgencyRef"></span></td>--%>
                                </tr>
                            </table>
                            <div class="clearfix"></div>
                            <br />
                        </div>
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Passenger Details</b></div>
                        <div class="scrollingDiv" style="border-bottom: 1px solid #e5e5e5; margin-bottom: 20px; height: 100px; padding-right: 0px;">
                            <div class="frow2">
                                <table class="table table-bordered" style="text-align: center; margin-bottom: 0px;" id="tblPassengerDetailsC">
                                </table>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Other Details</b></div>
                        <div class="frow2">
                            <table class="table table-responsive">
                                <tbody>
                                    <tr>
                                        <td><span class="dark bold">Total Amount: </span></td>
                                        <td style="padding-left: 15px;"><span id="Amount4"></span><span class="dark" id="dspTotalAmountC"></span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="dark bold">Service Tax: </span></td>
                                        <td style="padding-left: 15px;"><span id="Amount5"></span><span class="dark" id="dspServiceTaxC"></span></td>
                                        <td style="padding-left: 160px;"><span class="grey bold size18">Status: </span></td>
                                        <td style="padding-left: 15px;"><span class="green size18" id="dspBookingStatusC">Booking</span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="dark bold">Total Payable Amount: </span></td>
                                        <td style="padding-left: 15px;"><span id="Amount6"></span><span class="dark" id="dspTotalPayableC"></span></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="clearfix"></div>
                            <br />
                        </div>
                        <div class="alert alert-warning">
                            <span id="dspCan1C"></span>
                            <span id="dspCan2C" style="display: none"></span>
                        </div>

                        <div class="alert alert-warning" id="dspAlertMessageC">
                        </div>
                        <div class="frow2" style="text-align: center;">
                            <input type="button" onclick="window.location.href = 'ConfirmCardBooking.aspx'" value="Confirm Booking" class="btn-search4 margtop20" />

                        </div>

                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade bs-example-modal-lg" id="PreviewModalBank" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" style="width: 700px;">
                    <div class="modal-header" style="border-bottom: 1px solid #fff">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="gridSystemModalLabel3" style="text-align: left"></h4>
                    </div>
                    <div class="modal-body">

                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Booking Details</b></div>
                        <div class="frow2">
                            <table>
                                <tr>
                                    <td><span class="dark bold">Hotel Name: </span></td>
                                    <td style="padding-left: 15px;"><span class="dark" id="dspHotelNameB"></span></td>
                                    <td style="padding-left: 20px;"><span class="dark bold">Rooms: </span></td>
                                    <td style="padding-left: 15px;"><span class="dark" id="dspRoomsB"></span></td>
                                    <td style="padding-left: 20px;"><span class="dark bold">Nights: </span></td>
                                    <td style="padding-left: 15px;"><span class="dark" id="dspNightsB"></span></td>
                                </tr>
                                <tr>
                                    <td><span class="dark bold">Check-In: </span></td>
                                    <td style="padding-left: 15px;"><span class="dark" id="dspCheckInB"></span></td>
                                    <td style="padding-left: 20px;"><span class="dark bold">Check-Out: </span></td>
                                    <td style="padding-left: 15px;"><span class="dark" id="dspCheckOutB"></span></td>
                                    <%--<td style="padding-left: 20px;"><span class="dark bold">Agency Reference No: </span></td>
                                <td style="padding-left: 15px;"><span class="dark" id="dspAgencyRef"></span></td>--%>
                                </tr>
                            </table>
                            <div class="clearfix"></div>
                            <br>
                        </div>
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Passenger Details</b></div>
                        <div class="scrollingDiv" style="border-bottom: 1px solid #e5e5e5; margin-bottom: 20px; height: 100px; padding-right: 0px;">
                            <div class="frow2">
                                <table class="table table-bordered" style="text-align: center; margin-bottom: 0px;" id="tblPassengerDetailsB">
                                </table>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Other Details</b></div>
                        <div class="frow2">
                            <table>
                                <tbody>
                                    <tr>
                                        <td><span class="dark bold">Total Amount: </span></td>
                                        <td style="padding-left: 15px;"><span class='fa fa-inr'></span><span class="dark" id="dspTotalAmountB"></span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="dark bold">Service Tax: </span></td>
                                        <td style="padding-left: 15px;"><span class='fa fa-inr'></span><span class="dark" id="dspServiceTaxB"></span></td>
                                        <td style="padding-left: 160px;"><span class="grey bold size18">Status: </span></td>
                                        <td style="padding-left: 15px;"><span class="green size18" id="dspBookingStatusB">Booking</span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="dark bold">Total Payable Amount: </span></td>
                                        <td style="padding-left: 15px;"><span class='fa fa-inr'></span><span class="dark" id="dspTotalPayableB"></span></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="clearfix"></div>
                            <br />
                        </div>
                        <div class="alert alert-warning">
                            <span id="dspCan1B"></span>
                            <span id="dspCan2B" style="display: none"></span>
                        </div>

                        <div class="alert alert-warning" id="dspAlertMessageB">
                        </div>
                        <div class="frow2" style="text-align: center;">
                            <%--<asp:Button ID="Button1" runat="server" Text="Confirm Booking" class="btn-search4 margtop20" Style="float: none;" OnClick="BtnCard_Click" />--%>
                            <%--<input type="button" value="Confirm Booking" onclick="HotelConfirmBooking()"  class="btn-search4 margtop20" style="float: none;">--%>
                            <input type="hidden" id="hndBookingStatusB" />
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="AlertModal" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

            <div class="modal-dialog" style="width: 40%; padding-top: 15%">

                <div class="modal-content">
                    <div class="modal-header" style="border-bottom: none">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>

                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="frow2">
                                <div>
                                    <center><span id="AlertMessage"></span></center>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="ModelMessege" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

            <div class="modal-dialog" style="width: 40%; padding-top: 15%">

                <div class="modal-content">
                    <div class="modal-header" style="border-bottom: none">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>

                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="frow2">
                                <div>
                                    <center><span id="SpnMessege"></span></center>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="ConformModal" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="container-fluid">
                            <%-- <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Incomplete Document</b><label id="No"></label></div>--%>
                            <div>
                                <p class="size17" id="ConformMessage">
                                </p>
                                <table align="right" style="margin-left: -50%;">
                                    <tbody>
                                        <tr>
                                            <td style="border-top-width: 1px; border-top-style: solid; border-top-color: rgb(255, 255, 255);">
                                                <input type="button" id="Cancel" value="Cancel" onclick="Cancel()" class="btn-search4 margtop20" style="float: none;">
                                            </td>
                                            <td style="border-top-width: 1px; border-top-style: solid; border-top-color: rgb(255, 255, 255);">
                                                <input type="button" value="Ok" id="btnOk" class="btn-search4 margtop20" style="float: none;">
                                            </td>

                                        </tr>
                                    </tbody>
                                </table>
                                <br />

                            </div>

                        </div>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <footer id="footer" class="style5">
            <div class="footer-wrapper" style="background-color: #2d3e52">
                <div class="container">
                    <div class="row">
                        <%-- <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2 style="color:wheat">Address</h2>
                              <p style="text-align: justify ;color:wheat;">
                              Airlines Travel Centre <br />Sdn.Bhd.(1073291-U)
                              KKKP/PL 7592<br />
                              NO41,Level 2, Jalan Sultan Yussuf<br />
                              30000 Ipoh Perak <br />
                              Malaysia
                              </p>
                            </div>--%>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">

                            <h2 style="color: wheat">Discover</h2>
                            <hr />
                            <br />
                            <br />
                            <ul class="discover triangle hover row" style="color: wheat">
                                <li class="col-xs-6"><a href="Default.aspx">HOME</a></li>
                                <li class="col-xs-6"><a href="AboutUs.aspx">ABOUT US</a></li>
                                <%--<li class="col-xs-6"><a href="HotelSearch.aspx">HOTELS</a></li>--%>
                                <li class="col-xs-6"><a href="Packages.aspx">PACKAGES</a></li>
                                <li class="col-xs-6"><a href="Contact.aspx">CONTACT US</a></li>
                            </ul>




                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2 style="color: wheat">Trade Association</h2>
                            <hr />
                            <br />
                            <br />
                            <ul class="travel-news">
                                <li>

                                    <address class="contact-details">

                                        <ul class="social-icons clearfix">



                                            <img src="https://tvlk.imgix.net/imageResource/2017/12/13/1513150321127-5096be77d2a19401b476853e54ba2cc6.svg?auto=compress%2Cformat&amp;cs=srgb&amp;ixlib=java-1.1.12&amp;q=75">
                                        </ul>
                                    </address>
                                </li>
                            </ul>
                            <h2 style="color: wheat">Security Certifications</h2>
                            <hr />
                            <br />
                            <br />
                            <ul class="travel-news">
                                <li>

                                    <address class="contact-details">

                                        <ul class="social-icons clearfix">

                                            <img src="https://tvlk.imgix.net/imageResource/2017/12/13/1513150198216-822560165b4cfa5d5ac17a7987028b03.svg?auto=compress%2Cformat&amp;cs=srgb&amp;ixlib=java-1.1.12&amp;q=75">

                                            <img src="https://tvlk.imgix.net/imageResource/2017/12/13/1513150313470-072f6bdc02c0b73fcf791aa2b2264fbd.svg?auto=compress%2Cformat&amp;cs=srgb&amp;ixlib=java-1.1.12&amp;q=75">
                                        </ul>
                                    </address>
                                </li>
                            </ul>
                        </div>


                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2 style="color: wheat">Payment Partners</h2>
                            <hr />
                            <br />
                            <br />
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/09/29/1506664998558-c5fa770c7be81b3714f2f48ecd85db98.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/10/31/1509421373165-7ebba77e7447077b423012fed9f9f71c.jpeg?auto=compress%2Cformat&amp;cs=srgb&amp;fm=pjpg&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/09/29/1506665015604-7884362b3ea565a394df831ef7e8e7cb.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/09/28/1506602820847-28f78426b005e542fac5390ca84ec551.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/10/31/1509421385548-c79a81200ececb8e100169c32c52a7fa.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/09/29/1506665026169-ce67eef59804c4acc2f7fd36cb5a879a.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2018/09/27/1538041096091-17520ed6a85fbd457d47033d12532b85.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/09/29/1506665040121-f1fc420d69a023a99d3ec4b4ce3fc0e3.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/09/29/1506665046541-a307d453b9345dad33f4d45852ab168f.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/09/29/1506665053371-f97808ddc9481a41412112c1d5751432.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/10/30/1509365256338-d9f84c5a076e5e438423a8443476bc52.jpeg?auto=compress%2Cformat&amp;cs=srgb&amp;fm=pjpg&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/10/31/1509421397012-5407269004e88b979e792cb558f33df6.jpeg?auto=compress%2Cformat&amp;cs=srgb&amp;fm=pjpg&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/10/31/1509421400393-156136b7fc6842065dd4d27f63f704ab.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/09/28/1506602748281-38fa797106a1ca8ef57d689b8a956306.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 45px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/10/31/1509421411326-c2ed81814e071f8d0f32e3c720fc2f51.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                            <div class="bA1tY">
                                <img src="https://tvlk.imgix.net/imageResource/2017/10/31/1509421407130-3d32a6ffd3b308f4665de4e59b95d681.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.12&amp;q=75" style="width: 64px; height: 48px;" />
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2 style="color: wheat">About Vacaaay</h2>
                            <hr />
                            <br />
                            <br />
                            <p style="color: wheat">
                                Airlines Travel Centre
                                <br />
                                Sdn.Bhd.(1073291-U)
                              KKKP/PL 7592<br />
                                NO41,Level 2, Jalan Sultan Yussuf<br />
                                30000 Ipoh Perak
                                <br />
                                Malaysia
                            </p>

                            <br />
                            <address class="contact-details" style="color: wheat">
                                <br />
                                <a href="#" class="contact-email"><i class="soap-icon-letter-1"></i>info@vacaaay.com</a>
                            </address>
                            <ul class="social-icons clearfix">
                                <li class="twitter"><a title="twitter" href="#" data-toggle="tooltip"><i class="soap-icon-twitter"></i></a></li>
                                <li class="googleplus"><a title="googleplus" href="#" data-toggle="tooltip"><i class="soap-icon-googleplus"></i></a></li>
                                <li class="facebook"><a title="facebook" href="#" data-toggle="tooltip"><i class="soap-icon-facebook"></i></a></li>
                                <li class="linkedin"><a title="linkedin" href="#" data-toggle="tooltip"><i class="soap-icon-linkedin"></i></a></li>
                                <li class="vimeo"><a title="vimeo" href="#" data-toggle="tooltip"><i class="soap-icon-vimeo"></i></a></li>
                                <li class="dribble"><a title="dribble" href="#" data-toggle="tooltip"><i class="soap-icon-dribble"></i></a></li>
                                <li class="flickr"><a title="flickr" href="#" data-toggle="tooltip"><i class="soap-icon-flickr"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div class="bottom gray-area">
            <div class="container">
                <div class="logo pull-left">
                    <a href="Default.aspx" title="Vacaay Logo">
                        <img src="images/vacaaay1.png" alt="Vacaay Logo" />
                    </a>
                </div>
                <%--<div class="pull-left">
                        <p>Developed by <a href="http://www.trivo.in/" target="_blank">Trivo</a></p>
                    </div>--%>
                <div class="pull-right">
                    <a id="back-to-top" href="#" class="animated" data-animation-type="bounce"><i class="soap-icon-longarrow-up circle"></i></a>
                </div>
                <div class="copyright pull-right">
                    <p>Copyright © 2018 , Airlines Travel Centre Sdn Bhd - KPK/LN 7592 </p>
                </div>
            </div>
        </div>


        <input type="hidden" id="hdnCode" />
        <input type="hidden" id="hdnRoomID" />
        <input type="hidden" id="hdnHotelRoomID" />
        <input type="hidden" id="hdnbBook" />
        <input type="hidden" id="HotelName" />
        <input type="hidden" id="Supplier" />
        <input type="hidden" id="RoomDescriptionId" />
        <input type="hidden" id="ConfirmationId" />
    </div>
    <script type="text/javascript" src="scripts/HotelRoom.js?v=1.7"></script>
    <script type="text/javascript" src="scripts/B2CCustomerLogin.js"></script>
    <script src="scripts/Booking.js?V=1.8"></script>

    <script src="https://www.paypalobjects.com/api/checkout.js"></script>
    <script>
        var Total = $("#Total_Amount").val();
        paypal.Button.render({
            //env: 'sandbox',
            env: 'production', // Optional: specify 'sandbox' environment
            client: {
                //sandbox: 'AbnH0yJx-bjsnaBwYmg2_aMG5P3vQ56f5YiJ9PSVXd9l5B8Ej9uGg-TbCcVqzvGDfy2QW2sGtnnOxtbA',
                production: 'AWbBKK_cxfyLPZVf9v5UJYmiJ6EnZ8-FoE0QKUTyO0mLCOfplrOmkJVv8aiPsuQkQ0taytnR5oK9bcYL'
            },

            // Show the buyer a 'Pay Now' button in the checkout flow
            // commit: true,

            // payment() is called when the button is clicked
            payment: function (data, actions) {
                // Make a call to the REST API to set up the payment

                return actions.payment.create({

                    transactions: [{

                        amount: {
                            total: Total,
                            currency: 'MYR'
                        }
                    }],
                    redirect_urls: {
                        return_url: 'https://vacaaay.com/BookingConfirm.aspx',
                        cancel_url: 'https://vacaaay.com'
                    }
                });
            },

            // onAuthorize() is called when the buyer approves the payment
            onAuthorize: function (data, actions) {
                return actions.payment.execute()
                  .then(function () {
                      ProceedConfirmBooking();
                      window.alert('Thank you Booking.');


                      // Make a call to the REST API to execute the payment
                      //actions.redirect();
                  });

                if (error === 'INSTRUMENT_DECLINED') {
                    actions.restart();
                }
            },
            onError: function (err) {
                // Show an error page here, when an error occurs
            },

            onCancel: function (data, actions) {
                // Show a cancel page or return to cart
            }
        }, '#paypal-button');
    </script>
    <script type="text/javascript">
        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }

        function hasWhiteSpace(s) {
            if (s.indexOf(' ') >= 0)
                return s.replace(/ /g, '+');
            else
                return s;
        }

        $("#hndBookingStatus").val("Vouchered")
        $(document).ready(function () {
            var param = getParameterByName('code');
            var param1 = getParameterByName('id');
            var param2 = getParameterByName('Supplier');
            var param4 = getParameterByName('roomdescriptionId');
            var param5 = getParameterByName('RoomP');
            var Params6 = getParameterByName('_noRooms');
            var Params7 = getParameterByName('CutID');
            param5 = hasWhiteSpace(param5);
            $('#Supplier').val(param2);
            $('#RoomDescriptionId').val(param4);
            $("#aBack").append("<a href=\'b2chotel.aspx?success?1' class=\"active\">Back to Search List</a>");
            HotelBooking(param, param1, param2, param4, param5, Params6, Params7);
        });
        function CancellationPolicy(obj, amount, fdate, tdate, bdate) {
            var data = 'Charges - ' + amount + ' INR applicable if cancelled between ' + fdate + ' and ' + tdate + '.No Charges before ' + bdate + ' hrs';
            $('#Policy').html(data);
            $(obj).attr('data-toggle', 'modal');
        }
        function CheckLoginUser() {
            var reg = new RegExp('[0-9]$');
            var regPan = new RegExp('^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$');
            var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
            $("#Amount1").addClass(Currency);
            $("#Amount2").addClass(Currency);
            $("#Amount3").addClass(Currency);
            $("#Amount4").addClass(Currency);
            $("#Amount5").addClass(Currency);
            $("#Amount6").addClass(Currency);
            // ValidationsForAmount();
            IsValid = true;
            var regex = new RegExp("^[a-zA-Z0-9]+$");
            if (IsValid) {
                var bValue = true;
                var cValue = true;
                var dValue = true;
                var eValue = true;
                var fValue = true;
                //var gValue = false;
                $('.txtName').each(function () {
                    if ($(this).val() == "")
                        bValue = false;
                    if (/\d/.test($(this).val()))
                        cValue = false;
                    //if (/\s/.test($(this).val()))
                    //    dValue = false;
                    //if (!regex.test($(this).val()))
                    //    eValue = false;
                    if ($(this).val().length < 2 || $(this).val().length > 25)
                        fValue = false;
                    //if ($(this).val() == "")
                    //    gValue = true;
                });
                $('.txtAge').each(function () {
                    if ($(this).val() == "")
                        bValue = false;
                    else if (!$.isNumeric($(this).val()))
                        bValue = false;
                    else if (parseInt($(this).val()) <= 0 || parseInt($(this).val()) >= 13)
                        bValue = false;
                });
                if (bValue == false) {
                    alert("Please fill out the name of all passengers properly with valid information.");
                }
                else if (cValue == false) {
                    alert("Name must not be numeric.");
                }
                else if (dValue == false) {
                    alert("No white spaces allowed.");
                }
                else if (eValue == false) {
                    alert("No special characters allowed.");
                }
                else if (fValue == false) {
                    alert("Name should contain minimum 2 characters and maximum 25.");
                }
                else if ($("#txt_AgentEmail").val() == "") {
                    bValue = false;
                    alert("Please Insert   email.");
                }
                else if ($("#txt_CustomerMobileNumber").val() == "") {
                    bValue = false;
                    alert("Please Insert   Contact No.");
                }
                else if (chkTermsAndCondition.checked == false) {
                    alert('Please accept the terms & conditions.');
                }
                else if ($("#txt_AgentEmail").val() != "") {
                    if (!(pattern.test($("#txt_AgentEmail").val()))) {
                        bValid = false;
                        alert("Please Insert  valid email.");
                    }
                }

                if (bValue == true && cValue == true && eValue == true && fValue == true && chkTermsAndCondition.checked) {
                    $.ajax({
                        type: "POST",
                        url: "HotelHandler.asmx/HotelConfirmSession",
                        contentType: 'application/json',
                        data: ConfirmationData(),
                        datatype: "json",
                        success: function (response) {
                            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                            if (result.retCode == 1) {
                                $("#PreviewModalRegisteration").modal('show');
                                //if (result.IsLogedIn == true)
                                //    $("#ConfirmModal").modal('show');
                                //else
                                //    $("#PreviewModalRegisteration").modal('show');
                            }


                        },
                        error: function (xhr, error, statusCode) {
                            $('#SpnMessege').text('Something Went Wrong');
                            $('#ModelMessege').modal('show')
                            // alert('Something Went Wrong');
                        }
                    });
                }


            }
        }
        function LoginUser() {
            $.ajax({
                type: "POST",
                url: "../HotelHandler.asmx/GetAvaiCredit",
                contentType: 'application/json',
                data: ConfirmationData(),
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.retCode == 1) {
                        debugger;
                        $("#ConfirmationId").val(result.ConfirmationId)
                        $("#order_id").val(result.ConfirmationId);
                        var Url = "http://Vacaaay.com/WaitBooking.aspx?ConfirmationId=" + result.ConfirmationId;
                        $("#redirect_url").val(Url);
                        Sholess()
                        BookingDetails();

                    }
                    else if (result.retCode == 2) {
                        alert(result.Message, "BookingDetails", []);
                    }
                    else {

                        var Massage = "At This Time We Are Unable To Process Your Request Please Contact Admin..";
                        alert(Massage);
                        window.location.href = "Default.aspx";
                    }

                },
                error: function (xhr, error, statusCode) {
                    $('#SpnMessege').text(xhr.responseText);
                    $('#ModelMessege').modal('show')
                }
            });
        }
        function Sholess() {
            var minimized_elements = $('.note');

            minimized_elements.each(function () {
                var t = $(this).text();
                if (t.length < 100) return;

                $(this).html(
                    t.slice(0, 100) + '<span>... </span><a href="#" class="more red"> Read More>> </a>' +
                    '<span style="display:none;">' + t.slice(100, t.length).replace('Less', "") + ' <a href="#" class="less"> << Less </a></span>'
                );
            });

            $('a.more', minimized_elements).click(function (event) {
                event.preventDefault();
                $(this).hide().prev().hide();
                $(this).next().show();
            });

            $('a.less', minimized_elements).click(function (event) {
                event.preventDefault();
                $(this).parent().hide().prev().show().prev().show();
            });
        }
    </script>

    <script>
        $('.btn-close').click(function (e) {
            e.preventDefault();
            $(this).parent().parent().parent().fadeOut();
        });
        $('.btn-minimize').click(function (e) {
            e.preventDefault();
            var $target = $(this).parent().parent().next('.box-content');
            if ($target.is(':visible')) $('i', $(this)).removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
            else $('i', $(this)).removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
            $target.slideToggle();
        });
        $('.btn-setting').click(function (e) {
            e.preventDefault();
            $('#myModal').modal('show');
        });
        function Cancel() {
            $("#ConformModal").modal("hide")
        }
        function close() {

            $("#AlertModal").modal("hide")
        }
        function Ok(Message, Method, arg) {
            debugger;
            $("#btnOk").css("display", "")
            var id = [];
            if (arg != null) {
                for (var i = 0; i < arg.length; i++) {
                    id.push('"' + arg[i] + '"')
                }
            }

            $("#ConformMessage").html(Message);
            if (arg == null)
                document.getElementById("btnOk").setAttribute("onclick", Method + "()")
            else
                document.getElementById("btnOk").setAttribute("onclick", Method + "(" + id + ")")
            $("#ConformModal").modal("show")
        }
        function Success(Message) {
            $("#AlertMessage").html(Message);
            //$("#btnOk").css("display", "none")
            //$("#Cancel").val("Close")
            $("#AlertModal").modal("show")
        }
    </script>

    <!-- Javascript -->
    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.noconflict.js"></script>
    <script type="text/javascript" src="js/modernizr.2.7.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="js/jquery-ui.1.10.4.min.js"></script>

    <!-- Twitter Bootstrap -->
    <%--<script type="text/javascript" src="js/bootstrap.js"></script>--%>

    <!-- load revolution slider scripts -->
    <script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>

    <!-- load BXSlider scripts -->
    <script type="text/javascript" src="components/jquery.bxslider/jquery.bxslider.min.js"></script>

    <!-- load FlexSlider scripts -->
    <script type="text/javascript" src="components/flexslider/jquery.flexslider-min.js"></script>

    <!-- Google Map Api -->
    <script src="http://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>

    <script type="text/javascript" src="js/calendar.js"></script>

    <!-- parallax -->
    <script type="text/javascript" src="js/jquery.stellar.min.js"></script>

    <!-- waypoint -->
    <script type="text/javascript" src="js/waypoints.min.js"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="js/theme-scripts.js"></script>
    <script type="text/javascript" src="js/booking.js"></script>

    <script type="text/javascript">
    
    </script>
</body>
</html>
