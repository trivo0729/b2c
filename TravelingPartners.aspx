﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="TravelingPartners.aspx.cs" Inherits="CUTUK.TravelingPartners" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-title-container">
        <div class="container">
            <div class="page-title pull-left">
                <h2 class="entry-title">Traveling Partners</h2>
            </div>
            <ul class="breadcrumbs pull-right">
                <li><a href="Default.aspx">HOME</a></li>
                <li class="active">Traveling Partners</li>
            </ul>
        </div>
    </div>
    <section id="content">
        <div class="container">
            <div id="main">
                <div class="items-container isotope row image-box style9">
                    <div class="iso-item col-xs-12 col-sms-3 col-sm-3 col-md-3 filter-all filter-island filter-beach">
                        <article class="box">
                            <figure>
                                <a class="hover-effect" title="" href="#">
                                    <img width="370" height="190" alt="" src="images/indigo.jpg" /></a>
                            </figure>
                        </article>
                    </div>
                    <div class="iso-item col-xs-12 col-sms-3 col-sm-3 col-md-3 filter-all filter-island filter-beach">
                        <article class="box">
                            <figure>
                                <a class="hover-effect" title="" href="#">
                                    <img width="370" height="190" alt="" src="images/Spicejet_logo-2.jpg" /></a>
                            </figure>
                        </article>
                    </div>
                    <div class="iso-item col-xs-12 col-sms-3 col-sm-3 col-md-3 filter-all filter-beach filter-ocean">
                        <article class="box">
                            <figure>
                                <a class="hover-effect" title="" href="#">
                                    <img width="370" height="190" alt="" src="images/flysmart.jpg" /></a>
                            </figure>
                        </article>
                    </div>
                    <div class="iso-item col-xs-12 col-sms-3 col-sm-3 col-md-3 filter-all filter-countries">
                        <article class="box">
                            <figure>
                                <a class="hover-effect" title="" href="#">
                                    <img width="370" height="190" alt="" src="images/air-asia-logo.jpg" /></a>
                            </figure>
                        </article>
                    </div>

                </div>
            </div>
        </div>
    </section>
</asp:Content>
