﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FlightInvoice.aspx.cs" Inherits="CUTUK.FlightInvoice" %>

<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>View Invoice</title>
    <style type="text/css">
        @page {
            size: A3 portrait;
            margin: 0.5cm;
        }

        @media print {
            .page {
                page-break-after: avoid;
            }
        }
    </style>
    <!-- Picker -->
    <meta charset="utf-8" />
    <meta name="keywords" content=" " />
    <meta name="description" content="" />
    <meta name="author" content="SoapTheme" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />

    <!-- Theme Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="css/animate.min.css" />
    <link rel="stylesheet" href="css/responsive.css" />

    <!-- Current Page Styles -->
    <link rel="stylesheet" type="text/css" href="components/revolution_slider/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/revolution_slider/css/style.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/jquery.bxslider/jquery.bxslider.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/flexslider/flexslider.css" media="screen" />
    <link href="css/style.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/custom.css" />
    <script src="js/jquery-2.1.3.min.js"></script>
    <script src="js/modal.js"></script>


    <script type="text/javascript">
        var ReservationID;
        var Status;
        $(document).ready(function () {
            debugger;
            ReservationID = GetQueryStringParamsForAgentRegistrationUpdate('ReservationId');
            var Uid = GetQueryStringParamsForAgentRegistrationUpdate('Uid');
            Status = GetQueryStringParamsForAgentRegistrationUpdate('Status');
            //$("#reservationId").text(GetQueryStringParamsForAgentRegistrationUpdate('ReservationId').replace(/%20/g, ' '));
            //InvoicePrint(ReservationID, Uid);
            //document.getElementById("btn_Cancel").setAttribute("onclick", "OpenCancellationPopup('" + ReservationID + "', '" + Status + "')");
            document.getElementById("btn_VoucherPDF").setAttribute("onclick", "GetPDFInvoice('" + ReservationID + "', '" + Uid + "','" + Status + "')");
            if (Status == "Cancelled") {
                document.getElementById("btn_Cancel").setAttribute("style", "Display:none");
            }
        });


        function ProceedToCancellation() {
            //var Supplier = GetQueryStringParamsForAgentRegistrationUpdate('Supplier')
            //debugger;
            //if ($("#btnCancelBooking").val() == "Cancel Booking") {
            //    if ($("#hndIsCancelable").val() == "0") {

            //        $('#AgencyBookingCancelModal').modal('hide')
            //        $('#SpnMessege').text("Sorry! You cannot cancel this booking.")
            //        $('#ModelMessege').modal('show')


            //        // alert("Sorry! You cannot cancel this booking.")
            //    }
            //    else {
            //        var data = {
            //            ReservationID: $("#hndReservatonID").val(),
            //            ReferenceCode: $("#hndReferenceCode").val(),
            //            CancellationAmount: $("#hndCancellationAmount").val(),
            //            BookingStatus: $("#hndStatus").val(),
            //            Remark: $("#txtRemark").val(),
            //            TotalFare: $("#hdn_TotalFare").val(),
            //            ServiceCharge: $("#hdn_ServiceCharge").val(),
            //            Total: $("#hdn_Total").val(),
            //            Type: Supplier
            //        }
            //        $("#dlgLoader").css("display", "initial");
            //        $.ajax({
            //            type: "POST",
            //            url: "../HotelHandler.asmx/HotelCancelBooking",
            //            data: JSON.stringify(data),//'{"ReservationID":"' + $("#hndReservatonID").val() + '","ReferenceCode":"' + $("#hndReferenceCode").val() + '","CancellationAmount":"' + $("#hndCancellationAmount").val() + '","Remark":"' + $("#txtRemark").val() + '"}',
            //            contentType: "application/json; charset=utf-8",
            //            datatype: "json",
            //            success: function (response) {
            //                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            //                if (result.retCode == 1) {
            //                    alert("Your booking has been cancelled. An Email has been sent with Cancelation Detail.");
            //                    $("#AgencyBookingCancelModal").modal('hide');
            //                    location.reload();
            //                    //$.ajax({
            //                    //    type: "POST",
            //                    //    url: "../EmailHandler.asmx/CancellationEmail",
            //                    //    data: '{"ReservationID":"' + ReserID + '","totalamount":"' + totalamount + '","checkin":"' + checkin + '","checkout":"' + checkout + '","bookingdate":"' + bookingdate + '","HotelName":"' + HotelName + '","PassengerName":"' + PassengerName + '","City":"' + City + '"}',
            //                    //    contentType: "application/json; charset=utf-8",
            //                    //    datatype: "json",
            //                    //    success: function (response) {
            //                    //        var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            //                    //        if (result.Session == 0) {
            //                    //            alert("Some error occured, Please try again in some time.");

            //                    //        }
            //                    //        if (result.retCode == 1) {
            //                    //            //alert("Email has been sent successfully.");


            //                    //        }
            //                    //        else if (result.retCode == 0) {
            //                    //            alert("Sorry Please Try Later.");


            //                    //        }

            //                    //    },
            //                    //    error: function () {
            //                    //        alert('Something Went Wrong');
            //                    //    }

            //                    //});

            //                    ///// email ends here...


            //                }
            //                else if (result.retCode == 0) {
            //                    $('#AgencyBookingCancelModal').modal('hide')
            //                    $('#SpnMessege').text(result.Message);
            //                    $('#ModelMessege').modal('show')

            //                    //alert(result.Message);


            //                }
            //            },
            //            error: function () {
            //                $('#AgencyBookingCancelModal').modal('hide')
            //                $('#SpnMessege').text('Something Went Wrong');
            //                $('#ModelMessege').modal('show')
            //                // alert("something went wrong");
            //            },
            //            complete: function () {
            //                $("#dlgLoader").css("display", "none");
            //            }
            //        });
            //    }
            //} else if ($("#btnCancelBooking").val() == "Confirm Booking") {
            //    debugger;
            //    if ($("#hndIsConfirmable").val() == "0") {
            //        $('#AgencyBookingCancelModal').modal('hide')
            //        $('#SpnMessege').text("Sorry! You cannot confirm this booking.")
            //        $('#ModelMessege').modal('show')
            //        // alert("Sorry! You cannot confirm this booking.")
            //    }
            //    else {
            //        var data = {
            //            ReservationID: $("#hndReservatonID").val()
            //        }
            //        $("#dlgLoader").css("display", "initial");
            //        $.ajax({
            //            type: "POST",
            //            url: "../HotelHandler.asmx/ConfirmHoldBooking",
            //            data: JSON.stringify(data),
            //            contentType: "application/json; charset=utf-8",
            //            datatype: "json",
            //            success: function (response) {
            //                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            //                if (result.retCode == 1) {
            //                    $('#SpnMessege').text("Your booking has been confirmed.Confirmation email has been sent.!");
            //                    $('#ModelMessege').modal('show')
            //                    //  alert("Your booking has been confirmed.Confirmation email has been sent.!");
            //                    $("#AgencyBookingCancelModal").modal('hide');
            //                    location.reload();

            //                    //here mail goes for hold relaese
            //                    debugger;
            //                    $.ajax({
            //                        type: "POST",
            //                        url: "../EmailHandler.asmx/HoldReleaseEmail",
            //                        data: '{"ReservationID":"' + ReserID + '","totalamount":"' + totalamount + '","checkin":"' + checkin + '","checkout":"' + checkout + '","bookingdate":"' + bookingdate + '","HotelName":"' + HotelName + '","PassengerName":"' + PassengerName + '","City":"' + City + '"}',
            //                        contentType: "application/json; charset=utf-8",
            //                        datatype: "json",
            //                        success: function (response) {
            //                            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            //                            if (result.Session == 0) {
            //                                $('#SpnMessege').text("Some error occured, Please try again in some time.");
            //                                $('#ModelMessege').modal('show')
            //                                // alert("Some error occured, Please try again in some time.");

            //                            }
            //                            if (result.retCode == 1) {
            //                                //alert("Email has been sent successfully.");


            //                            }
            //                            else if (result.retCode == 0) {
            //                                $('#AgencyBookingCancelModal').modal('hide')
            //                                $('#SpnMessege').text("Sorry Please Try Later.");
            //                                $('#ModelMessege').modal('show')
            //                                // alert("Sorry Please Try Later.");


            //                            }

            //                        },
            //                        error: function () {
            //                            $('#AgencyBookingCancelModal').modal('hide')
            //                            $('#SpnMessege').text('Something Went Wrong');
            //                            $('#ModelMessege').modal('show')
            //                            //alert('Something Went Wrong');
            //                        }

            //                    });
            //                    // Mail Ends Here....
            //                }
            //                else if (result.retCode == 0) {
            //                    $('#AgencyBookingCancelModal').modal('hide')
            //                    $('#SpnMessege').text(result.Message);
            //                    //   $('#ModelMessege').modal('show')
            //                    alert(result.Message);
            //                }

            //                else if (result.retCode == 2) {
            //                    //alert(result.Message);
            //                    $('#AgencyBookingCancelModal').modal('hide')
            //                    $("#dspAlertMessage").html("Your Balance is Insufficient to confirm this booking.");
            //                    $("#dspAlertMessage").css("display", "block");
            //                    //$("#AgencyBookingCancelModal").modal('hide');
            //                }
            //            },
            //            error: function () {
            //                $('#AgencyBookingCancelModal').modal('hide')
            //                $('#SpnMessege').text("something went wrong during hold confirmation");
            //                $('#ModelMessege').modal('show')
            //                // alert("something went wrong during hold confirmation");
            //            },
            //            complete: function () {
            //                $("#dlgLoader").css("display", "none");
            //            }
            //        });
            //    }
            //}
        }

        function GetQueryStringParamsForAgentRegistrationUpdate(sParam) {
            var sPageURL = window.location.search.substring(1);
            var sURLVariables = sPageURL.split('&');
            for (var i = 0; i < sURLVariables.length; i++) {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] == sParam) {
                    return sParameterName[1];
                }
            }
        }


    </script>

</head>

<body>

    <div style="margin-left: 40%;" id="BtnDiv">
        <input type="button" class="button btn-small orange" value="Email" style="cursor: pointer" title="Invoice" data-toggle="modal" data-target="#InvoiceModal" />
        <input type="button" class="button btn-small orange" value="Download" id="btn_VoucherPDF" />
        <%--<input type="button" class="button btn-small orange" value="Cancel" id="btn_Cancel" />--%>
    </div>
    <br />
    <div id="maincontainer" runat="server" class="print-portrait-a4" style="font-family: 'Segoe UI', Tahoma, sans-serif; margin: 0px 40px 0px 40px; width: auto; border: 2px solid gray;">
    </div>

    <div class="modal fade" id="ModelMessege" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="width: 40%; padding-top: 15%">

            <div class="modal-content">
                <div class="modal-header" style="border-bottom: none">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>

                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="frow2">
                            <div>
                                <center><span id="SpnMessege"></span></center>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade bs-example-modal-lg" id="AgencyBookingCancelModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 700px;">
                <div class="modal-header" style="border-bottom: 1px solid #fff">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel3" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Booking Details</b></div>
                    <div class="frow2">
                        <table>
                            <tr>
                                <td><span class="dark bold">Hotel: </span></td>
                                <td style="padding-left: 15px;"><span class="dark" id="dspHotelName"></span></td>
                                <td style="padding-left: 20px;"><span class="dark bold">Check-In: </span></td>
                                <td style="padding-left: 15px;"><span class="dark" id="dspCheckin"></span></td>
                                <td style="padding-left: 20px;"><span class="dark bold">Check-Out: </span></td>
                                <td style="padding-left: 15px;"><span class="dark" id="dspCheckout"></span></td>
                            </tr>
                            <tr>
                                <td><span class="dark bold">Passenger: </span></td>
                                <td style="padding-left: 15px;"><span class="dark" id="dspPasssengerName"></span></td>
                                <td style="padding-left: 20px;"><span class="dark bold">Location: </span></td>
                                <td style="padding-left: 15px;"><span class="dark" id="dspLocation"></span></td>
                                <td style="padding-left: 20px;"><span class="dark bold">Nights: </span></td>
                                <td style="padding-left: 15px;"><span class="dark" id="dspNights"></span></td>
                            </tr>
                            <tr>
                                <td><span class="dark bold">Booking ID: </span></td>
                                <td style="padding-left: 15px;"><span class="dark" id="dspBookingID"></span></td>
                                <td style="padding-left: 20px;"><span class="dark bold">Booking Date: </span></td>
                                <td style="padding-left: 15px;"><span class="dark" id="dspBookingDate"></span></td>
                                <td style="padding-left: 20px;"><span class="dark bold">Amount: </span></td>
                                <td style="padding-left: 15px;"><span class="dark" id="dspBookingAmount"></span></td>
                            </tr>
                        </table>
                        <div class="clearfix"></div>
                        <br>
                    </div>
                    <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Cancellation Policy</b></div>
                    <div class="frow2">
                        <div id="dspCancellationPolicy">
                        </div>
                        <div class="clearfix"></div>
                        <br>
                    </div>

                    <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;" id="divRemark1"><b>Remark</b></div>
                    <div class="frow2" id="divRemark2">
                        <textarea rows="4" cols="140" id="txtRemark" class="form-control"></textarea>
                        <div class="clearfix"></div>
                        <br>
                    </div>
                    <div class="alert alert-warning" id="dspAlertMessage" style="display: none"></div>


                    <div class="frow2" style="text-align: center;">
                        <img style="-webkit-user-select: none; margin-top: 10px; display: none;" id="dlgLoader" src="../images/loader.gif"><br />
                        <input type="button" value="Cancel Booking" onclick="ProceedToCancellation(); return false" id="btnCancelBooking" class="btn-search4 margtop20" style="float: none;" />
                        <input type="hidden" id="hndReferenceCode" />
                        <input type="hidden" id="hndCancellationAmount" />
                        <input type="hidden" id="hndReservatonID" />
                        <input type="hidden" id="hndIsCancelable" />
                        <input type="hidden" id="hndStatus" />
                        <input type="hidden" id="hndIsConfirmable" />
                        <input type="hidden" id="hdn_TotalFare" />
                        <input type="hidden" id="hdn_ServiceCharge" />
                        <input type="hidden" id="hdn_Total" />
                        <input type="hidden" id="hdn_Source" />
                    </div>



                </div>

            </div>
        </div>
    </div>

    <div class="modal fade bs-example-modal-lg" id="InvoiceModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width: 60%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel3" style="text-align: left">Send Invoice</h4>
                </div>
                <div class="modal-body">

                    <form class="cruise-booking-form">
                        <div class="person-information">
                            <div class="row">
                                <div class="form-group col-sm-8 col-md-8">
                                    <input type="text" id="txt_sendInvoice" class="form-control" value="" placeholder="Enter Your Email" />
                                </div>
                                <div class="form-group col-sm-4 col-md-4">
                                    <button type="button" class="button btn-small orange" id="btn_sendInvoice" onclick="MailInvoice()">Send Invoice</button>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>



    <meta charset="utf-8" />
    <meta name="keywords" content=" " />
    <meta name="description" content="" />
    <meta name="author" content="SoapTheme" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <script src="scripts/FlightInvoice.js"></script>
    <!-- Theme Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="css/animate.min.css" />
    <link rel="stylesheet" href="css/responsive.css" />

    <!-- Current Page Styles -->
    <link rel="stylesheet" type="text/css" href="components/revolution_slider/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/revolution_slider/css/style.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/jquery.bxslider/jquery.bxslider.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/flexslider/flexslider.css" media="screen" />
    <link href="css/style.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/custom.css" />
    <script src="js/jquery-2.1.3.min.js"></script>
    <script src="js/modal.js"></script>

</body>
</html>
