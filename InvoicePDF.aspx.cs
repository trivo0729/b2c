﻿
using CUT.BL;
using CUTUK.DataLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CUTUK
{
    public partial class InvoicePDF : System.Web.UI.Page
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            CUT.DataLayer.GlobalDefault objGlobalDefaults = (CUT.DataLayer.GlobalDefault)Session["LoginUser"];
            if (objGlobalDefaults != null)
            {
                string CurrencyClass = "";
                string CurrencyImage = "";
                string ReservationID = Request.QueryString["ReservationID"];
                Int64 Uid = objGlobalDefaults.sid;
                //string Uid = Request.QueryString["Uid"];
                //Uid = Convert.ToInt64(Uid);
                string Night = Request.QueryString["NoOfDays"];
                DataSet ds = new DataSet();
                DataTable dtHotelReservation, dtBookedPassenger, dtBookedRoom, dtBookingTransactions, dtAgentDetail, dtHotelAdd;
                string AllPassengers = "";

                SqlParameter[] SQLParams = new SqlParameter[2];
                SQLParams[0] = new SqlParameter("@ReservationID", ReservationID);
                SQLParams[1] = new SqlParameter("@sid", Uid);
                DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("Proc_BookingDetails", out ds, SQLParams);

                dtHotelReservation = ds.Tables[0];
                dtBookedPassenger = ds.Tables[1];
                dtBookedRoom = ds.Tables[2];
                dtBookingTransactions = ds.Tables[3];
                dtAgentDetail = ds.Tables[4];
                if (objGlobalDefaults.UserType == "Agent")
                {
                    CurrencyClass = (HttpContext.Current.Session["CurrencyClass"]).ToString();
                    switch (CurrencyClass)
                    {
                        case "Currency-AED":
                            CurrencyImage = "PAED";

                            break;
                        case "Currency-SAR":
                            CurrencyImage = "PSAR";
                            break;
                        case "fa fa-eur":
                            CurrencyImage = "PEUR;";
                            break;
                        case "fa fa-gbp":
                            CurrencyImage = "PGBP";
                            break;
                        case "fa fa-dollar":
                            CurrencyImage = "PUSD";
                            break;
                        case "fa fa-inr":
                            CurrencyImage = "PINR";
                            break;
                    }
                }
                else
                {
                    CurrencyClass = dtAgentDetail.Rows[0]["CurrencyCode"].ToString();
                    switch (CurrencyClass)
                    {
                        case "AED":
                            CurrencyImage = "PAED";

                            break;
                        case "SAR":
                            CurrencyImage = "PSAR";
                            break;
                        case "EUR":
                            CurrencyImage = "PEUR;";
                            break;
                        case "GBP":
                            CurrencyImage = "PGBP";
                            break;
                        case "USD":
                            CurrencyImage = "PUSD";
                            break;
                        case "INR":
                            CurrencyImage = "PINR";
                            break;
                    }

                }

                StringBuilder sb = new StringBuilder();

                string invoice = "";
                sb.Append(EmailManager.GetInvoice(ReservationID, Convert.ToInt64(Uid), out invoice));

                HttpContext context = System.Web.HttpContext.Current;
                var htmlContent = String.Format(sb.ToString());
                NReco.PdfGenerator.HtmlToPdfConverter pdfConverter = new NReco.PdfGenerator.HtmlToPdfConverter();
                pdfConverter.Size = NReco.PdfGenerator.PageSize.A3;
                pdfConverter.PdfToolPath = ConfigurationManager.AppSettings["rootPath"];
                //pdfConverter.PdfToolPath = @"D:\Kashif\ClickUrTrip\CUT\CUT\Agent";
                //pdfConverter.PdfToolPath = "C:\\inetpub\\wwwroot\\httpdocs\\Agent";
                var pdfBytes = pdfConverter.GeneratePdf(htmlContent);
                context.Response.ContentType = "application/pdf";
                context.Response.BinaryWrite(pdfBytes);
            }
        }



        private static string[] ones = { "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen", };

        private static string[] tens = { "Zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

        private static string[] thous = { "Hundred,", "Thousand,", "Million,", "Billion,", "Trillion,", "Quadrillion," };

        public static string ToWords(decimal number, string CurrencyName)
        {
            if (number < 0)
                return "negative " + ToWords(Math.Abs(number), CurrencyName);

            int intPortion = (int)number;
            int decPortion = (int)((number - intPortion) * (decimal)100);

            if (CurrencyName == "Currency-AED")
            {
                return string.Format("{0} AED and {1} Fils", ToWords(intPortion), ToWords(decPortion));
            }
            else if (CurrencyName == "Currency-SAR")
            {
                return string.Format("{0} SAR and {1} Halalah", ToWords(intPortion), ToWords(decPortion));
            }
            else if (CurrencyName == "fa fa-eur")
            {
                return string.Format("{0} Euro and {1} 	Cent", ToWords(intPortion), ToWords(decPortion));
            }
            else if (CurrencyName == "fa fa-gbp")
            {
                return string.Format("{0} Great Britain Pounds and {1} Penny", ToWords(intPortion), ToWords(decPortion));
            }
            else if (CurrencyName == "fa fa-dollar")
            {
                return string.Format("{0} Dollar and {1} Cent", ToWords(intPortion), ToWords(decPortion));
            }
            else
                return string.Format("{0} Rupees and {1} Paise", ToWords(intPortion), ToWords(decPortion));
            //return string.Format("{0} Rupees and {1} Paise", ToWords(intPortion), ToWords(decPortion));        //orig line without any conditions
        }

        private static string ToWords(int number, string appendScale = "")
        {
            string numString = "";
            if (number < 100)
            {
                if (number < 20)
                    numString = ones[number];
                else
                {
                    numString = tens[number / 10];
                    if ((number % 10) > 0)
                        numString += "-" + ones[number % 10];
                }
            }
            else
            {
                int pow = 0;
                string powStr = "";

                if (number < 1000) // number is between 100 and 1000
                {
                    pow = 100;
                    powStr = thous[0];
                }
                else // find the scale of the number
                {
                    int log = (int)Math.Log(number, 1000);
                    pow = (int)Math.Pow(1000, log);
                    powStr = thous[log];
                }

                numString = string.Format("{0} {1}", ToWords(number / pow, powStr), ToWords(number % pow)).Trim();
            }

            return string.Format("{0} {1}", numString, appendScale).Trim();
        }


        private static string[] _ones =
        {
                "zero",
                "one",
                "two",
                "three",
                "four",
                "five",
                "six",
                "seven",
                "eight",
                "nine"
         };

        private string[] _teens =
        {
        "ten",
        "eleven",
        "twelve",
        "thirteen",
        "fourteen",
        "fifteen",
        "sixteen",
        "seventeen",
        "eighteen",
        "nineteen"
        };

        private string[] _tens =
        {
        "",
        "ten",
        "twenty",
        "thirty",
        "forty",
        "fifty",
        "sixty",
        "seventy",
        "eighty",
        "ninety"
        };

        // US Nnumbering`:

        private string[] _thousands = { "", "thousand", "million", "billion", "trillion", "quadrillion" };

        public string ConvertToNum(decimal value)
        {
            string digits, temp;
            bool showThousands = false;
            bool allZeros = true;
            StringBuilder builder = new StringBuilder();
            // Convert integer portion of value to string
            digits = ((long)value).ToString();
            // Traverse characters in reverse order
            for (int i = digits.Length - 1; i >= 0; i--)
            {
                int ndigit = (int)(digits[i] - '0');
                int column = (digits.Length - (i + 1));

                // Determine if ones, tens, or hundreds column
                switch (column % 3)
                {
                    case 0:        // Ones position
                        showThousands = true;
                        if (i == 0)
                        {
                            // First digit in number (last in loop)
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else if (digits[i - 1] == '1')
                        {
                            // This digit is part of "teen" value
                            temp = String.Format("{0} ", _teens[ndigit]);
                            // Skip tens position
                            i--;
                        }
                        else if (ndigit != 0)
                        {
                            // Any non-zero digit
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else
                        {
                            // This digit is zero. If digit in tens and hundreds
                            // column are also zero, don't show "thousands"
                            temp = String.Empty;
                            // Test for non-zero digit in this grouping
                            if (digits[i - 1] != '0' || (i > 1 && digits[i - 2] != '0'))
                                showThousands = true;
                            else
                                showThousands = false;
                        }

                        // Show "thousands" if non-zero in grouping
                        if (showThousands)
                        {
                            if (column > 0)
                            {
                                temp = String.Format("{0}{1}{2}",
                                temp,
                                _thousands[column / 3],
                                    //allZeros ? " " : ", ");
                                allZeros ? " " : " ");
                            }
                            // Indicate non-zero digit encountered
                            allZeros = false;
                        }
                        builder.Insert(0, temp);
                        break;

                    case 1:        // Tens column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0}{1}",
                            _tens[ndigit],
                            (digits[i + 1] != '0') ? "-" : " ");
                            builder.Insert(0, temp);
                        }
                        break;

                    case 2:        // Hundreds column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0} hundred ", _ones[ndigit]);
                            builder.Insert(0, temp);
                        }
                        break;
                }
            }

            // Append fractional portion/cents
            builder.AppendFormat(" DOLLARS and {0:00} / 100", (value - (long)value) * 100);//Replace Dollars with paisa if you are using indian currencry

            // Capitalize first letter
            return String.Format("{0}{1}",
            Char.ToUpper(builder[0]),
            builder.ToString(1, builder.Length - 1));
        }
    }
}