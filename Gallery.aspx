﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Gallery.aspx.cs" Inherits="CUTUK.Gallery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-title-container">
        <div class="container">
            <div class="page-title pull-left">
                <h2 class="entry-title">Gallery</h2>
            </div>
            <ul class="breadcrumbs pull-right">
                <li><a href="#">HOME</a></li>
                <li class="active">Gallery</li>
            </ul>
        </div>
    </div>
    <section id="content">
        <div class="container">
            <div id="main">
                <div class="tour-packages row add-clearfix image-box">
                    <div class="col-sm-6 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/gallery/g1.jpg" alt=""></a>

                            </figure>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/gallery/g2.jpeg" alt=""></a>

                            </figure>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/gallery/g3.jpeg" alt=""></a>

                            </figure>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/gallery/g4.jpg" alt=""></a>

                            </figure>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/gallery/g5.jpg" alt=""></a>

                            </figure>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/gallery/g6.jpg" alt=""></a>

                            </figure>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/gallery/g7.jpg" alt=""></a>

                            </figure>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/gallery/g8.jpg" alt=""></a>

                            </figure>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/gallery/g9.jpg" alt=""></a>

                            </figure>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/gallery/g10.jpg" alt=""></a>

                            </figure>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/gallery/g11.jpg" alt=""></a>

                            </figure>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/gallery/g12.jpg" alt=""></a>

                            </figure>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/gallery/g13.jpg" alt=""></a>

                            </figure>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/gallery/g14.jpg" alt=""></a>

                            </figure>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/gallery/g15.jpg" alt=""></a>

                            </figure>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/gallery/g16.jpg" alt=""></a>

                            </figure>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/gallery/h3.jpg" alt=""></a>

                            </figure>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/gallery/Germany1.jpg" alt="" /></a>

                            </figure>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/gallery/Germany2.jpg" alt="" /></a>

                            </figure>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/gallery/Germany3.jpg" alt="" /></a>

                            </figure>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
