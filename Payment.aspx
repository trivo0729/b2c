﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Payment.aspx.cs" Inherits="CUTUK.Payment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="scripts/Transfer.js"></script>
    <script>
        window.onload = function () {
            var d = new Date().getTime();
            document.getElementById("tid").value = d;
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-title-container">
        <div class="container">
            <div class="page-title pull-left">
                <h2 class="entry-title">Payment</h2>
            </div>
            <ul class="breadcrumbs pull-right">
                <li><a href="#">HOME</a></li>
                <li class="active">Payment</li>
            </ul>
        </div>
    </div>

    <section id="content" class="gray-area">
        <div class="container shortcode">

            <div class="row image-box style3">
                <div class="col-md-12">
                    <article class="box">
                        <figure class="col-md-5">
                            <a href="#" title="" class="middle-block middle-block-auto-height" style="position: relative; height: 149px;">
                                <img class="middle-item" src="images/canarabank-payment.png" alt="" width="476" height="318" style="position: absolute; top: 50%; margin-top: -163px; left: 50%; margin-left: -244px;">
                            </a>
                        </figure>
                        <div class="details col-md-offset-5 text-justify">
                            <h4 class="box-title">UNISAFE TOURS Bank Details</h4>
                            <br />
                            <hr />
                            <p>
                                <strong>Bank Details To Transfer :</strong><br />
                                <strong>Account Name:</strong>&nbsp; UNISAFE TOURS<br />
                                <strong>Account Number :</strong>&nbsp;&nbsp;1629201011565<br />
                                <strong>Branch Address :</strong>&nbsp; VIBHAV NAGAR AGRA (Uttar Pradesh)<br />
                                <strong>Bank Name :</strong>&nbsp; Canara Bank<br />
                                <strong>IFSC CODE :</strong>&nbsp; – CNRB0001629<br />
                                <strong>SWIFT CODE :</strong>&nbsp; – CNRBINBBAOB<br />
                                <strong>MICR CODE :</strong>&nbsp; – 28201501
                            </p>
                            <%--<a class="button btn-small dull-blue" onclick="PayOnline()">Pay Online</a>--%>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade bs-example-modal-lg" id="PaymentModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <form method="post" name="customerData" action="ccavRequestHandler.aspx">
                <input type="hidden" name="tid" id="tid" />
                <input type="hidden" name="amount" id="Amount" />
                <input type="hidden" name="order_id" id="order_id" />
                <input type="hidden" name="merchant_id" id="merchant_id" value="174467" />
                <input type="hidden" name="currency" id="currency" value="INR" />
                <input type="hidden" name="redirect_url" value="Default.aspx" id="redirect_url" />
                <input type="hidden" name="cancel_url" value="ccavResponseHandler.aspx" id="cancel_url" />
                <div class="modal-content">
                    <div class="modal-header" style="border-bottom: 1px solid #fff">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div id="main" class="col-sms-12 col-sm-12 col-md-12">
                            <div class="booking-section travelo-box">
                                <div class="card-information">
                                    <div class="form-group row">
                                        <div class="col-sm-6 col-md-6">
                                            <label>Full Name :</label>
                                            <div class="selector">
                                                <input type="text" id="Name" name="billing_name" class="input-text full-width" value="" placeholder="Enter Your Name" />
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6">
                                            <label>Address :</label>
                                            <input type="text" id="Address" name="billing_address" class="input-text full-width" value="" placeholder="Enter Your Address" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-6 col-md-6">
                                            <label>Email :</label>
                                            <input type="text" id="Email" name="billing_email" class="input-text full-width" placeholder="Enter Your Email" />
                                        </div>
                                        <div class="col-sm-6 col-md-6">
                                            <label>Mobile :</label>
                                            <input type="text" id="Mobile" name="billing_tel" class="input-text full-width" placeholder="Enter Your Mobile No" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-6 col-md-6">
                                            <label>Amount :</label>
                                            <input type="text" id="txt_Amount" class="input-text full-width" value="" placeholder="Enter Amount" />
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group row" style="float: right">
                                    <div class="col-sm-6 col-md-6">
                                        <button id="PayAmount" type="button" class="button btn-medium sky-blue1" onclick="Pay()">Submit</button>
                                        <button id="Btn_CCAvenue" type="submit" style="display:none" class="button btn-medium sky-blue1">Pay</button>
                                    </div>
                                </div>
                                <%--<div class="form-group row" id="PayDiv" style="display: none">
                                    <div class="col-sm-6 col-md-6">
                                        <p>Click Pay Button to Continue</p>
                                    </div>
                                    <div class="col-sm-6 col-md-6">
                                        <button id="Btn_CCAvenue" type="submit" class="button btn-medium sky-blue1">Pay</button>
                                    </div>

                                </div>--%>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
            <br />
            <br />
            <br />
            <br />
        </div>
    </div>
</asp:Content>
