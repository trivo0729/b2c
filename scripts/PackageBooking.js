﻿var nID = "0";

$(function () {

    if (location.href.indexOf('?') != -1) {
        CategoryName = GetQueryStringParams('CatName');
        Id = GetQueryStringParams('Id');
        nID = GetQueryStringParams('Id');
        GetPackage(Id);
        GetPackPrice(Id, CategoryName);
        GetCategory(CategoryName);
    }
});


function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}



var Package = new Array();

function GetPackage(Id) {

    $.ajax({
        url: "PackageHandler.asmx/GetPackage",
        type: "post",
        data: '{"Id":"' + Id + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 0) {

                alert("No packages found");
            }
            else if (result.retCode == 1) {
                Package = result.Arr;
                htmlbuilder();
            }
        },
        error: function () {
            alert('Errror in getting Product details, Please try again!');
        }
    });
}

var CategoryID = "";

function GetCategory(CategoryName) {
    if (CategoryName == "STANDARD") {
        CategoryID = "1";
    }
    else if (CategoryName == "DELUXE") {
        CategoryID = "2";
    }
    else if (CategoryName == "PREMIUM") {
        CategoryID = "3";
    }
    else if (CategoryName == "LUXURY") {
        CategoryID = "4";
    }

}


var PackagePrice = new Array();

function GetPackPrice(Id, CategoryName) {
    $.ajax({
        url: "PackageHandler.asmx/GetPackagePrice",
        type: "post",
        data: '{"Id":"' + Id + '" , "CategoryName":"' + CategoryName + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 0) {

                alert("No packages found");
            }
            else if (result.retCode == 1) {
                PackagePrice = result.Arr;
            }
        },
        error: function () {
            alert('Errror');
        }
    });
}

function htmlbuilder() {
    try {
        var html = "";
        $("#Right").empty();
        html += '<h4>Booking Details</h4>'
        html += '                <hr /><br />'
        html += '                <article class="tour-detail">'
        html += '                   <h6><b> Package Name : </b> ' + Package[0].sPackageName + '</h6>'
        html += '                <hr /><br />'
        html += '                   <h6><b> Package Category : </b> ' + CategoryName + '</h6>'
        html += '                    <div class="details">'
        html += '                        <div class="icon-box style11 full-width">'
        html += '                            <div class="icon-wrapper">'
        html += '                                <i class="soap-icon-departure"></i>'
        html += '                            </div>'
        html += '                            <dl class="details">'
        html += '                                <dt class="skin-color">Location</dt>'
        var City = Package[0].sPackageDestination.split("|");
        html += '                                <dd>' + City[0] + '</dd>'
        html += '                            </dl>'
        html += '                        </div>'
        html += '                         <div class="icon-box style11 full-width">'
        html += '                            <div class="icon-wrapper">'
        html += '                                <i class="soap-icon-clock"></i>'
        html += '                            </div>'
        html += '                            <dl class="details">'
        html += '                                <dt class="skin-color">Duration</dt>'
        html += '                                <dd>' + Package[0].nDuration + ' Days</dd>'
        html += '                            </dl>'
        html += '                        </div>'
        html += '                        <div class="icon-box style11 full-width">'
        html += '                            <div class="icon-wrapper">'
        html += '                                <i class="soap-icon-calendar"></i>'
        html += '                            </div>'
        html += '                            <dl class="details">'
        html += '                                <dt class="skin-color">From </dt>'
        var From = Package[0].dValidityFrom.split(" ");
        html += '                                <dd>' + From[0] + '</dd>'
        html += '                            </dl>'
        html += '                        </div>'
        html += '                        <div class="icon-box style11 full-width">'
        html += '                            <div class="icon-wrapper">'
        html += '                                <i class="soap-icon-calendar"></i>'
        html += '                            </div>'
        html += '                            <dl class="details">'
        html += '                                <dt class="skin-color">Up To</dt>'
        var To = Package[0].dValidityTo.split(" ");
        html += '                                <dd>' + To[0] + '</dd>'
        html += '                            </dl>'
        html += '                        </div>'
        html += '                    </div>'
        html += '                </article>'
        $("#Right").html(html)
    } catch (e) {

    }
}

var Name, MobileNo, Email, noAdults, noChild, TarvelDate, AddOnReq, ChildAges = "0";
var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

function SendMail() {
    var from = Package[0].dValidityFrom.split(' ');
    var To = Package[0].dValidityTo.split(' ');
    var bValid = true;
    Name = $("#txt_name").val()
    MobileNo = $("#txt_phone").val()
    Email = $("#txt_email").val()
    noAdults = $("#Select_Adults").val()
    noChild = $("#Select_CHILD").val()
    TarvelDate = $("#txt_date").val()
    AddOnReq = $("#txt_Remark").val()
    bValid = ValidateMail();
    if (bValid == true) {
        var data = {
            nID: nID,
            Name: Name,
            MobileNo: MobileNo,
            Email: Email,
            noAdults: noAdults,
            noChild: noChild,
            noInfant: "",
            ChildAges: ChildAges,
            TarvelDate: TarvelDate,
            AddOnReq: AddOnReq,
            PackageName: Package[0].sPackageName,
            StartDate: from[0],
            TillDate: To[0],
            AdultPrice: PackagePrice[0].dSingleAdult,
            ChildBedsPrice: PackagePrice[0].dKidWBed,
            ChildWBed: PackagePrice[0].dKidWOBed,
            noOfDays: Package[0].nDuration,
            CategoryID: CategoryID,
            CategoryType: CategoryName,
        }
        var Jason = JSON.stringify(data)
        $.ajax({
            type: "POST",
            url: "PackageHandler.asmx/BookPackage",
            data: Jason,
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    alert("Thanks For Booking, We Will Update you when it is Conform by Administrator..", null)
                    window.location.href = "Default.aspx";
                }
            },
            error: function () {
                alert("An error occured", null)
            }
        });
    }
}
function ValidateMail() {
    if (Name == "") {
        $("#txt_name").focus();
        alert("Please Insert  Passenger Name", "txt_name")
        return false;
    }
    if (MobileNo == "") {
        $("#txt_name").focus();
        alert("Please Insert  Mobile No", "txt_phone")
        return false;
    }
    if (Email == "") {
        $("#txt_email").focus();
        alert("Please Insert  MailId", "txt_email")
        return false;
    }
    if (!emailReg.test(Email)) {
        $("#txt_email").focus();
        alert("Please Insert Valid MailId", "txt_email")
        return false;
    }
    if (noChild != "0") {
        if (noChild == "1") {
            ChildAges = $("#Select_AgeChildSecond1").val()
        }
        else if (noChild == "2") {
            ChildAges = $("#Select_AgeChildSecond1").val() + "," + $("#Select_AgeChildSecond2").val()

        }
        return true
    }
    if (TarvelDate == "") {
        $("#txt_date").focus();
        alert("Please Insert Tarvel Date", "txt_date")
        return false;
    }
    if (AddOnReq == "") {
        $("#txt_Remark").focus();
        alert("Please Insert Add Request", "txt_Remark")
        return false;
    }
    return true

}


function NoChild(Val) {
    if (Val == 0) {
        $("#div_FirstChild").css("display", "none")
        $("#div_SecondChild").css("display", "none")
        $("#Childs").atrr("class", "col-md-12")
    }
    else if (Val == 1) {
        $("#div_FirstChild").css("display", "")
        $("#div_SecondChild").css("display", "none")
    }
    else if (Val == 2) {
        $("#div_FirstChild").css("display", "")
        $("#div_SecondChild").css("display", "")
    }
}


function CheckUserLogin() {
    debugger;
    $.ajax({
        url: "DefaultHandler.asmx/CheckSession",
        type: "post",
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                SendMail();
            }
            else if (result.retCode == 0) {
                $("#RegisterationModal").modal('show');
            }
        },
    });
}

function LoginPackageBooking() {
    debugger;
    var sUserName = document.getElementById("txtUserName").value;
    var sPassword = document.getElementById("txtPassword").value;
    var bValid = true;

    if (sUserName == "") {
        bValid = false;
        alert("User Name/email cannot be blank");
        document.getElementById('txtUserName').focus();
        return false;
    }
    else if (!emailReg.test(sUserName)) {
        bValid = false;
        alert("The email address you have entered is invalid");
        document.getElementById('txtUserName').focus();
        return false;
    }
    if (sPassword == "") {
        bValid = false;
        alert("Password cannot be blank");
        document.getElementById('txtPassword').focus();
        // $("#txt_UserPassword").focus();
        return false;
    }
    if (bValid == true) {
        $.ajax({
            type: "POST",
            url: "DefaultHandler.asmx/UserLogin",
            data: '{"sUserName":"' + sUserName + '","sPassword":"' + sPassword + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.Session == 0) {
                    window.location.href = "Default.aspx";
                    return false;
                }
                else if (result.retCode == 1) {
                    $("#RegisterationModal").modal('hide');
                    SendMail();
                }
                else {
                    alert('Username/Password did not match.');
                }
            },
            error: function () {
            }
        });
    }
}
//................Validation.......................//
var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

function ClearAll() {
    $("#txt_UserId").value('');
    $("#txt_UserPassword").value('');
}

function ValidatePackageRegistration() {
    $("#lbl_UserName").css("display", "none");
    $("#lbl_Password").css("display", "none");
    $("#lbl_ConfirmPassword").css("display", "none");
    $("#lbl_Mobile").css("display", "none");
    $("#lbl_Phone").css("display", "none");
    $("#lbl_Address").css("display", "none");
    $("#lbl_City").css("display", "none");
    $("#lbl_PinCode").css("display", "none");

    $("#lbl_UserName").html("* This field is required.");
    $("#lbl_Password").html("* This field is required.");
    $("#lbl_ConfirmPassword").html("* This field is required.");
    $("#lbl_Mobile").html("* This field is required.");
    $("#lbl_Phone").html("* This field is required.");
    $("#lbl_Address").html("* This field is required.");
    $("#lbl_City").html("* This field is required.");
    $("#lbl_PinCode").html("* This field is required.");
    debugger;
    var reg = new RegExp('[0-9]$');

    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);

    var bValid = true;

    var UserName = $("#txt_UserName").val();

    var Address = $("#txt_Address").val();
    var CityId = $("#selCity").val();
    var Email = $("#txt_Email").val();
    var Password = $("#txt_Password").val();
    var ConfirmPassword = $("#txt_ConfirmPassword").val();
    var Contact2 = $("#txt_Phone").val();
    var Contact1 = $("#txt_Mobile").val();
    if (UserName == "") {
        bValid = false;
        $("#lbl_UserName").css("display", "");
    }
    else {
        if (!(pattern.test(UserName))) {
            bValid = false;
            $("#lbl_UserName").html("* Wrong email format.");
            $("#lbl_UserName").css("display", "");
        }
    }
    if (Password == "") {
        bValid = false;
        $("#lbl_Password").css("display", "");
    }
    if (ConfirmPassword == "") {
        bValid = false;
        $("#lbl_ConfirmPassword").html("* This field is required.");
        $("#lbl_ConfirmPassword").css("display", "");
    }
    else if (Password != ConfirmPassword) {
        bValid = false;
        $("#lbl_ConfirmPassword").html("Confirm Password did not matched!");
        $("#lbl_ConfirmPassword").css("display", "");
    }
    if (Address == "") {
        bValid = false;
        $("#lbl_Address").css("display", "");
    }
    if (Address != "" && reg.test(Address)) {
        bValid = false;
        $("#lbl_Address").html("* Address must not be numeric at end.");
        $("#lbl_Address").css("display", "");
    }
    if (CityId == "-") {
        bValid = false;
        $("#lbl_City").css("display", "");
    }
    if (Contact1 == "") {
        bValid = false;
        $("#lbl_Mobile").css("display", "");
    }

    else {
        if (!(reg.test(Contact1))) {
            bValid = false;
            $("#lbl_Mobile").html("* Mobile no must be numeric.");
            $("#lbl_Mobile").css("display", "");
        }
    }
    if (Contact2 == "") {
        Contact2 = '0';
    }
    else {
        if (!(reg.test(Contact2))) {
            bValid = false;
            $("#lbl_Phone").html("* Phone no must be numeric.");
            $("#lbl_Phone").css("display", "");
        }
    }
    if (bValid == true) {
        debugger;
        var data = {
            UserName: UserName,
            Password: Password,
            Contact1: Contact1,
            Contact2: Contact2,
            Address: Address,
            CityId: CityId,
        };
        $.ajax({
            type: "POST",
            url: "B2CCustomerLoginHandler.asmx/UserRegistration",
            //data: '{"sB2C_Id":"' + sB2C_Id + '","sUserName":"' + sUserName + '","sEmail":"' + sEmail + '","nMobile":"' + nMobile + '","nPhone":"' + nPhone + '","sAddress":"' + sAddress + '","sCity":"' + sCity + '","nPinCode":"' + nPinCode + '"}',
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {

                    alert("Registration Successfully");
                    $("#RegisterationModal").modal('hide');
                    ClearRegistrationForFlight();
                    SendMail();
                }
                else if (result.retCode == 2) {
                    alert("Username already registered. Please register with other Username.");
                }
                else if (result.retCode == -1) {
                    alert("Registeration UnSuccessfull.");
                }

                else {
                    alert("Something Went Wrong");
                    window.location.reload();
                }
            }
        });
    }
}


function ClearPackageRegistration() {
    $("#PreviewModalRegisteration").modal('hide');
    $('#txt_UserName').val("");
    $('#txt_Password').val("");
    $('#txt_ConfirmPassword').val("");
    $('#txt_Mobile').val("");
    $('#txt_Phone').val("");
    $('#txt_Address').val("");
    $('#selCity option').val("-");
    $('#txt_PinCode').val("");

    $("#lbl_UserName").css("display", "none");
    $("#lbl_Password").css("display", "none");
    $("#lbl_ConfirmPassword").css("display", "none");
    $("#lbl_Mobile").css("display", "none");
    $("#lbl_Phone").css("display", "none");
    $("#lbl_Address").css("display", "none");
    $("#lbl_City").css("display", "none");
    $("#lbl_PinCode").css("display", "none");

    $("#lbl_UserName").html("* This field is required.");
    $("#lbl_Password").html("* This field is required.");
    $("#lbl_ConfirmPassword").html("* This field is required.");
    $("#lbl_Mobile").html("* This field is required.");
    $("#lbl_Phone").html("* This field is required.");
    $("#lbl_Address").html("* This field is required.");
    $("#lbl_City").html("* This field is required.");
    $("#lbl_PinCode").html("* This field is required.");


    //for login clearing

    $('#txtUserName').val("");
    $('#txtPassword').val("");

    $("#lbl_txtUserName").css("display", "none");
    $("#lbl_txtPassword").css("display", "none");

    $("#lbl_txtUserName").html("* This field is required.");
    $("#lbl_txtPassword").html("* This field is required.");
    //Clear();
}