﻿

var hiddensid;
var ReservationId;
var Latitude;
var Longitude;
var sid;
var UID;
var Ammount;
var arrHotelReservation = new Array();
var arrBookedPassenger = new Array();
var arrBookedRoom = new Array();
var arrBookingTransactions = new Array();
var arrAgentDetails = new Array();
var Night;
var Status;
var HSupplier;


//---------------------------Invoice-------------------------------------------
function InvoicePrint(ReservationID, Uid) {
    debugger;

    $("#tblCreditDetails tbody tr").remove();
    $.ajax({
        type: "POST",
        url: "HotelBookingHandler.asmx/BookingDetails",
        data: '{"ReservationID":"' + ReservationID + '","UID":"' + Uid + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrHotelReservation = result.HotelReservation;
                arrBookedPassenger = result.BookedPassenger;
                arrBookedRoom = result.BookedRoom;
                arrBookingTransactions = result.BookingTransactions;
                arrAgentDetails = result.AgentDetails;
                debugger;
                var strCin = arrHotelReservation[0].CheckIn;
                var ret1 = strCin.split(" ");
                var CheckInDate = ret1[0];




                var strCout = arrHotelReservation[0].CheckOut;
                var ret2 = strCout.split(" ");
                var CheckOutDate = ret2[0];


                var Reservation = arrHotelReservation[0].ReservationDate;
                var ret3 = Reservation.split(" ");
                var ReservationDateBooking = ret3[0];


                var Nights = (arrHotelReservation[0].NoOfDays < 10) ? '0' + arrHotelReservation[0].NoOfDays : '' + arrHotelReservation[0].NoOfDays;

                $('#spnInvoice').text(arrHotelReservation[0].InvoiceID);
                $('#spnVoucher').text(arrHotelReservation[0].VoucherID);
                $('#spnChkIn').text(CheckInDate);
                $('#spnChkOut').text(CheckOutDate);
                $('#spnHotelName').text(arrHotelReservation[0].HotelName);
                $('#spnNights').text(Nights);
                $('#spnStatus').text(arrHotelReservation[0].Status);
                $('#spnDate').text(ReservationDateBooking);
                $('#spnAgentRef').text(arrHotelReservation[0].AgentRef);
                $('#spnHotelDestination').text(arrHotelReservation[0].City);



                $('#spnName').text(arrAgentDetails[0].AgencyName);
                $('#spnAdd').text(arrAgentDetails[0].Address);
                $('#spnCity').text(arrAgentDetails[0].Description);
                $('#spnCountry').text(arrAgentDetails[0].Countryname);
                $('#spnPhone').text(arrAgentDetails[0].phone);
                $('#spnemail').text(arrAgentDetails[0].email);


                var Night = arrHotelReservation[0].NoOfDays;
                var SalesTax = arrBookingTransactions[0].SeviceTax;
                SalesTax = parseFloat(SalesTax).toFixed(2);
                Ammount = arrBookingTransactions[0].BookingAmtWithTax;
                Ammount = parseFloat(Ammount).toFixed(2);


                // Convert numbers to words
                // copyright 25th July 2006, by Stephen Chapman http://javascript.about.com
                // permission to use this Javascript on your web page is granted
                // provided that all of the code (including this copyright notice) is
                // used exactly as shown (you can change the numbering system if you wish)
                var words;
                // American Numbering System
                var th = ['', 'Thousand,', 'Million,', 'Billion,', 'Trillion,'];
                // uncomment this line for English Number System
                // var th = ['','thousand','million', 'milliard','billion'];

                var dg = ['Zero', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine']; var tn = ['Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen']; var tw = ['Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety']; function toWords(s) { s = s.toString(); s = s.replace(/[\, ]/g, ''); if (s != parseFloat(s)) return 'not a number'; var x = s.indexOf('.'); if (x == -1) x = s.length; if (x > 15) return 'too big'; var n = s.split(''); var str = ''; var sk = 0; for (var i = 0; i < x; i++) { if ((x - i) % 3 == 2) { if (n[i] == '1') { str += tn[Number(n[i + 1])] + ' '; i++; sk = 1; } else if (n[i] != 0) { str += tw[n[i] - 2] + ' '; sk = 1; } } else if (n[i] != 0) { str += dg[n[i]] + ' '; if ((x - i) % 3 == 0) str += 'hundred '; sk = 1; } if ((x - i) % 3 == 1) { if (sk) str += th[(x - i - 1) / 3] + ' '; sk = 0; } } if (x != s.length) { var y = s.length; str += 'point '; for (var i = x + 1; i < y; i++) str += dg[n[i]] + ' '; } return str.replace(/\s+/g, ' '); }

                words = toWords(Ammount);

                var tRow = '';
                tRow += '<tr>' +
                    '<td style="background-color: #35C2F1; color: white; font-size: 15px; border: none;  font-weight: 700; text-align:left; padding-left:10px ">' +
                    '<span>No.</span>' +
                    '</td>' +
                    '<td style="width:170px; height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: left">' +
                    '<span>Room Type</span>' +
                    '</td>' +
                    '<td style="height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none;  font-weight: 700; text-align:center">' +
                    '<span>Board</span>' +
                    '</td>' +
                    '<td style="height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: center">' +
                    '<span>Rooms</span>' +
                    '</td>' +
                    '<td style="height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: center;">' +
                    '<span>Nights</span>' +
                    '</td>' +
                    '<td style="height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: center;">' +
                    '<span>Rate</span>' +
                    '</td>' +
                    '<td style="width: 100px; height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: center;">' +
                    '<span>Total</span>' +
                    '</td>' +
                    '</tr>';
                for (var i = 0; i < arrBookedRoom.length; i++) {

                    tRow += '<tr style="border: none; background-color: #E6E7E9; padding: 15px 0px 0px 10px; text-align: left; color: #57585A">';
                    tRow += '<td align="center" style="width:30px; border: none; text-align: left; padding-left: 15px;">' + (i + 1) + '</td>';
                    tRow += '<td style="border: none">' + arrBookedRoom[i].RoomType + '</td>';
                    tRow += '<td align="center" style="border: none; padding-bottom: 8px; padding-top: 8px;">' + arrBookedRoom[i].BoardText + '</td>';
                    tRow += '<td align="center" style="border: none; padding-bottom: 8px; padding-top: 8px;">' + arrBookedRoom[i].RoomNumber + '</td>';
                    tRow += '<td align="center" style="border: none; padding-bottom: 8px; padding-top: 8px;" >' + Night + '</td>';
                    tRow += '<td align="center" style="border: none; padding-bottom: 8px; padding-top: 8px;">' + numberWithCommas((parseFloat(arrBookedRoom[i].RoomAmount / Night).toFixed(2))) + '</td>';
                    tRow += '<td align="center" style="border: none; padding-bottom: 8px; padding-top: 8px;">' + numberWithCommas((parseFloat(arrBookedRoom[i].RoomAmount).toFixed(2))) + '</td>';

                    //tRow += '<td align="center" style="border: none; padding-bottom: 8px; padding-top: 8px;">' + numberWithCommas((parseFloat(arrBookedRoom[i].RoomAmount).toFixed(2))) + '</td>';
                    //tRow += '<td align="center" style="border: none; padding-bottom: 8px; padding-top: 8px;">' + numberWithCommas((Night) * (parseFloat(arrBookedRoom[i].RoomAmount).toFixed(2))) + '</td>';
                    tRow += '</tr>';

                }
                tRow += '<tr style="border: none; background-color: #E6E7E9; text-align: right">' +
                        '<td align="right" colspan="6" style="border: none; width: 20px; padding-right: 25px; color: #57585A; ">' +
                        '<span><b> Service Tax (1.4%)</b></span>' +
                        '</td>' +
                        '<td style="border: none; width: 20px; text-align: center; color: #57585A"><b>' + numberWithCommas(SalesTax) + '</b></td>' +
                        '</tr>';

                tRow += '<tr border="1" style="border-spacing:0px">' +
                       '<td colspan="5" align="left" style="height: 35px;  background-color: #00AEEF; color: white; font-size: 15px; padding-left: 10px;  border-width: 3px 0px 0px 0px; border-top-color: #E6DCDC;">' +
                       '<b> In Words:</b> <span>: ' + words + '</span>' +
                       '</td>' +
                       '<td align="center" style="height: 35px; background-color: #27B4E8; color: white; font-size: 18px; padding-left: 10px; font-weight: 700; border-width: 3px 0px 0px 0px; border-top-color: #E6DCDC;">' +
                       '<span>Total Amount</span>' +
                       '</td>' +
                      '<td align="center" style="height: 35px; background-color: #35C2F1; color: white; font-size: 18px; padding-left: 10px; font-weight: 700; border-width: 3px 0px 0px 0px; border-top-color: #E6DCDC;">' +
                      '<span>₹' + numberWithCommas(Ammount) + ' </span>' +
                      '</td>' +
                      '</tr>';
                $("#tbl_Rate tbody").append(tRow);
                $("#tbl_Rate tbody").html(tRow);
                //--------------Cancellation Table----------------


                var TRow = '';
                TRow = '<tr style="border: none">' +

                '<td style="background-color: #35C2F1; color: white; font-size: 15px; border: none;  font-weight: 700; text-align:left; padding-left:10px ">' +
                    '<span>No.</span>' +
                '</td>' +
                '<td style="width:190px; height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: left">' +
                    '<span>Room Type</span>' +
                '</td>' +

                '<td align="center" style="height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; padding-left: 10px; font-weight: 700; ">' +
                    '<span>Rooms</span>' +
                '</td>' +
                '<td align="center" style="height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; padding-left: 10px; font-weight: 700; ">' +
                    '<span>Cancellation After</span>' +
                '</td>' +
                '<td align="center" style="height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; padding-left: 10px; font-weight: 700; ">' +
                    '<span>Charge/Unit</span>' +
                '</td>' +
                '<td align="center" style="height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; padding-left: 10px; font-weight: 700; ">' +
                    '<span>Total Charge</span>' +
                '</td>' +
            '</tr>';
                var CancellationAmt = "";
                for (var i = 0; i < arrBookedRoom.length; i++) {


                    //CancellationAmt = arrBookedRoom[0].CanAmtWithTax;
                    CancellationAmt = arrBookedRoom[i].CancellationAmount;

                    if (CancellationAmt.indexOf('|') >= 0) {
                        var CanAmt = CancellationAmt.split('|');
                        CanAmt[0];
                        CanAmt[1];

                        //CancellationAmt = numberWithCommas((parseFloat(CanAmt[0]).toFixed(2))) + '|' + numberWithCommas((parseFloat(CanAmt[1]).toFixed(2)));
                        //var CancellationAmtWNight = numberWithCommas(((parseFloat(CanAmt[0]).toFixed(2)) * Night)) + '|' + numberWithCommas(((parseFloat(CanAmt[1]).toFixed(2)) * Night));
                        CancellationAmt = numberWithCommas(((parseFloat(CanAmt[0])) / Night).toFixed(2)) + '|' + numberWithCommas(((parseFloat(CanAmt[1])) / Night).toFixed(2));
                        var CancellationAmtWNight = numberWithCommas(((parseFloat(CanAmt[0]).toFixed(2)))) + '|' + numberWithCommas(((parseFloat(CanAmt[1]).toFixed(2))));
                    }
                    else {
                        //CancellationAmt = (arrBookedRoom[0].CanAmtWithTax);

                        //CancellationAmt = (arrBookedRoom[0].CancellationAmount);
                        //CancellationAmtWNight = numberWithCommas((parseFloat(CancellationAmt).toFixed(2) * Night));
                        CancellationAmt = numberWithCommas(((arrBookedRoom[0].CancellationAmount) / Night).toFixed(2));
                        CancellationAmtWNight = numberWithCommas((parseFloat(CancellationAmt).toFixed(2)));
                    }

                    TRow += '<tr style="border: none; background-color: #E6E7E9; text-align: center; color: #57585A">';
                    TRow += '<td align="center" style="width:30px; border: none; text-align: left; padding-left: 15px;">' + (i + 1) + ' </td>';
                    TRow += '<td style="border: none;text-align: left"><span> ' + arrBookedRoom[i].RoomType + '</span> </td>';
                    TRow += '<td style="border: none; padding-bottom: 8px; padding-top: 8px; ">  <span>' + arrBookedRoom[i].RoomNumber + ' </span></td>';
                    TRow += '<td style="border: none; padding-bottom: 8px; padding-top: 8px; "> <span>' + arrBookedRoom[i].SupplierNoChargeDate + '</span> </td>';
                    TRow += '<td style="border: none; padding-bottom: 8px; padding-top: 8px; "><span>₹' + CancellationAmt + '</span></td>';
                    TRow += '<td style="border: none; padding-bottom: 8px; padding-top: 8px; "> <span> ₹' + CancellationAmtWNight + '</span></td>';
                    TRow += '</tr>';


                }
                TRow += '<tr style="border: none; background-color: #E6E7E9; text-align: left;">' +
                       '<td colspan="7" style="border: none; width: 20px; padding: 0px 15px 15px 25px; color: #ECA236; ">' +
                       '*Dates & timing will calculated based on local timing </td>' +
                       '</tr>';

                $("#tbl_Cancellation tbody").append(TRow);
                $("#tbl_Cancellation tbody").html(TRow);






            }
        },
        error: function () {
            $('#SpnMessege').text("An error occured while loading credit information")
            $('#ModelMessege').modal('show')
            //alert("An error occured while loading credit information")
        }
    });

}

function GetPrintInvoice(ReservationID, Uid, Status, Type) {
    debugger;
    if (Status == 'Vouchered' || Status == 'Cancelled') {



        if (Type == "HotelBeds") {
            Type="A"
        }

        if (Type == "MGH") {
            Type = "B"
        }

        if (Type == "DoTW") {
            Type = "C"
        }

        if (Type == "GRN") {
            Type = "E"
        }
        if (Type == "GTA") {
            Type = "F"
        }
        if (Type == "TBO") {
            Type = "G"
        }


        //var win = window.open('ViewInvoice.aspx?ReservationId=' + ReservationID + '&Uid=' + Uid + '&Status=' + Status, '_blank');
        var win = window.open('ViewInvoice.aspx?ReservationId=' + ReservationID + '&Uid=' + Uid + '&Status=' + Status + '&Supplier=' + Type, '_blank');
        //window.open('../Agent/Invoice.html?ReservationId=' + ReservationID + '&Status=' + Status + '&Uid=' + Uid + '&Type=' + Type, 'tester', 'left=50000,top=50000,width=800,height=600');
   }
    else {
        $('#SpnMessege').text('Please Confirm Your Booking to get Invoice !!')
        $('#ModelMessege').modal('show')
        //  alert('Please Confirm Your Booking!')
    }
}

function InvoiceDetails(Resevationid, Uid, Nights, status) {
    debugger;
    if (status == 'Booking') {
        $('#SpnMessege').text('Please Confirm Your Booking! ')
        $('#ModelMessege').modal('show')
        // alert('Please Confirm Your Booking! ')
    }
    UID = Uid;
    ReservationId = Resevationid;
    Status = status;
    Night = (Nights < 10) ? '0' + Nights : '' + Nights;
}

function MailInvoice() {
    debugger;
    ReservationId = GetQueryStringParamsForAgentRegistrationUpdate('ReservationId');
    Status = GetQueryStringParamsForAgentRegistrationUpdate('Status');
    if (Status == 'Vouchered' || Status == 'Cancelled') {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);

        var bValid = true;
        var sEmail = $("#txt_sendInvoice").val();
        if (sEmail == "") {
            bValid = false;
            $("#lbl_txt_sendInvoice").css("display", "");
        }
        else {
            if (!(pattern.test(sEmail))) {
                bValid = false;
                $("#lbl_txt_sendInvoice").html("* Wrong email format.");
                $("#lbl_txt_sendInvoice").css("display", "");
            }
        }


        if (bValid == true) {
            $.ajax({
                type: "POST",
                url: "DefaultHandler.asmx/SendInvoice",
                data: '{"sEmail":"' + sEmail + '","ReservationId":"' + ReservationId + '","UID":"' + UID + '","Night":"' + Night + '"}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result != true) {
                        $('#SpnMessege').text("Some error occured, Please try again in some time.");
                        $('#ModelMessege').modal('show')
                        return false;
                    }
                    if (result == true) {
                        $('#SpnMessege').text("Email has been sent successfully.");
                        $('#InvoiceModal').modal('hide')
                        $('#txt_sendInvoice').val('')                    
                        $('#ModelMessege').modal('show')    
                        return false;
                    }
                    else if (result.retCode == 0) {
                        $('#SpnMessege').text("Sorry Please Try Later.");
                        $('#ModelMessege').modal('show')
                        return false;
                    }
                    else if (result.retCode == 2) {
                        $('#SpnMessege').text("Sorry, No account found with this email id.");
                        $('#ModelMessege').modal('show')
                        return false;
                    }
                },
                error: function () {
                    $('#SpnMessege').text('Something Went Wrong');
                    $('#ModelMessege').modal('show')
                    // alert('Something Went Wrong');
                }

            });
        }
    }
    else {
        $('#SpnMessege').text('Please Confirm Your Booking!!!!!');
        $('#ModelMessege').modal('show')
        // alert('Please Confirm Your Booking!!!!!');
        //window.location.href = "Invoice.aspx";
    }
}

function MailActInvoice() {
    debugger;
    ReservationId = GetQueryStringParamsForAgentActivityUpdates('ReservationId');
    Status = GetQueryStringParamsForAgentActivityUpdates('Status');
    if (Status == 'Vouchered' || Status == 'Cancelled') {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);

        var bValid = true;
        var sEmail = $("#txt_sendInvoice").val();
        if (sEmail == "") {
            bValid = false;
            $("#lbl_txt_sendInvoice").css("display", "");
        }
        else {
            if (!(pattern.test(sEmail))) {
                bValid = false;
                $("#lbl_txt_sendInvoice").html("* Wrong email format.");
                $("#lbl_txt_sendInvoice").css("display", "");
            }
        }


        if (bValid == true) {
            $.ajax({
                type: "POST",
                url: "../EmailHandler.asmx/SendActInvoice",
                data: '{"sEmail":"' + sEmail + '","ReservationId":"' + ReservationId + '","UID":"' + UID + '"}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.Session == 0) {
                        $('#SpnMessege').text("Some error occured, Please try again in some time.");
                        $('#ModelMessege').modal('show')
                        // alert("Some error occured, Please try again in some time.");
                        return false;
                    }
                    if (result.retCode == 1) {
                        $('#SpnMessege').text("Email has been sent successfully.");
                        $('#InvoiceModal').modal('hide')
                        $('#txt_sendInvoice').val('')
                        $('#ModelMessege').modal('show')
                        // alert("Email has been sent successfully.");
                        //window.location.href = "Invoice.aspx";
                        //                    window.location.href = "../CUT/Default.aspx";
                        return false;
                    }
                    else if (result.retCode == 0) {
                        $('#SpnMessege').text("Sorry Please Try Later.");
                        $('#ModelMessege').modal('show')
                        // alert("Sorry Please Try Later.");
                        //window.location.href = "Invoice.aspx";
                        //                    window.location.href = "../CUT/Default.aspx";
                        return false;
                    }
                    else if (result.retCode == 2) {
                        $('#SpnMessege').text("Sorry, No account found with this email id.");
                        $('#ModelMessege').modal('show')
                        //alert("Sorry, No account found with this email id.");
                        //window.location.href = "Invoice.aspx";
                        //                    window.location.href = "../CUT/Default.aspx";
                        return false;
                    }
                },
                error: function () {
                    $('#SpnMessege').text('Something Went Wrong');
                    $('#ModelMessege').modal('show')
                    // alert('Something Went Wrong');
                }

            });
        }
    }
    else {
        $('#SpnMessege').text('Please Confirm Your Booking!!!!!');
        $('#ModelMessege').modal('show')
        // alert('Please Confirm Your Booking!!!!!');
        //window.location.href = "Invoice.aspx";
    }
}

function GetPDFInvoice(ReservationID, Uid, Status) {
    debugger;
    var win = window.open('InvoicePDF.aspx?ReservationID=' + ReservationID + '&Uid=' + Uid + '&Status=' + Status + '', '_blank');
}
//---------------------------Activty Invoice-------------------------------------------

function GetPDFActInvoice(ReservationID, Uid) {
    debugger;
    var win = window.open('ActivityInvoicePDF.aspx?ReservationID=' + ReservationID + '&Uid=' + Uid);
}

//---------------------------Activty Voucher-------------------------------------------

function GetPDFActVoucher(ReservationID, Uid) {
    debugger;
    var win = window.open('ActivityVoucherPDF.aspx?ReservationID=' + ReservationID + '&Uid=' + Uid);
}

//---------------------------Voucher-------------------------------------------

function GetPrintVoucher(ReservationID, Uid, Latitude, Longitude, Status, Type) {
    debugger;
    if (Status == 'Vouchered' || Status == 'Hold') {

        var win = window.open('ViewVoucher.aspx?ReservationId=' + ReservationID + '&Uid=' + Uid + '&Status=' + Status + '', '_blank');
        //window.open('../Agent/Voucher.html?ReservationId=' + ReservationID + '&Status=' + Status + '&Uid=' + Uid + '&Latitude=' + Latitude + '&Longitutude=' + Longitude + '&Type=' + Type, 'tester', 'left=50000,top=50000,width=800,height=600');
    }
    else if (Status == 'Booking') {

        $('#SpnMessege').text('Please Confirm Your Booking!')
        $('#ModelMessege').modal('show')
        // alert('Please Confirm Your Booking!')
    }
    else {
        $('#SpnMessege').text('You cannot get voucher for cancelled booking!')
        $('#ModelMessege').modal('show')
        //  alert('You cannot get voucher for cancelled booking!')
    }

}

function VoucherPrint(ReservationID, Uid) {
    debugger;
    var arrHotelReservation = new Array();
    var arrBookedPassenger = new Array();
    var arrBookedRoom = new Array();
    var arrBookingTransactions = new Array();
    var arrAgentDetails = new Array();
    var arrHotelAdd = new Array();

    //var ReservationID;
    $("#tblCreditDetails tbody tr").remove();
    $.ajax({
        type: "POST",
        url: "HotelBookingHandler.asmx/BookingDetails",
        data: '{"ReservationID":"' + ReservationID + '","UID":"' + Uid + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrHotelReservation = result.HotelReservation;
                arrBookedPassenger = result.BookedPassenger;
                arrBookedRoom = result.BookedRoom;
                arrBookingTransactions = result.BookingTransactions;
                arrAgentDetails = result.AgentDetails;
                arrHotelAdd = result.HotelAdd;

                debugger;
                var lat = ''
                var long = ''

                lat = arrHotelReservation[0].LatitudeMGH;
                long = arrHotelReservation[0].LongitudeMGH;

                if (arrHotelReservation[0].LatitudeMGH != true) {
                    lat = arrHotelReservation[0].Latitude;
                    long = arrHotelReservation[0].Longitude;

                }
                if (arrHotelReservation[0].Latitude != true || arrHotelReservation[0].LatitudeMGH != true) {
                    lat = arrHotelAdd[0].LatitudeMGH;
                    long = arrHotelAdd[0].LongitudeMGH;

                }


                var strCin = arrHotelReservation[0].CheckIn;
                var ret1 = strCin.split(" ");
                var CheckInDate = ret1[0];
                var countrycity = arrHotelReservation[0].City
                var arrCountryCity = countrycity.split(',');
                var Country = "";

                if (arrHotelReservation[0].Source == "DoTW") {
                    Country = arrHotelAdd[0].Country;
                }
                else {
                    Country = arrCountryCity[1];
                }




                var strCout = arrHotelReservation[0].CheckOut;
                var ret2 = strCout.split(" ");
                var CheckOutDate = ret2[0];


                var Reservation = arrHotelReservation[0].ReservationDate;
                var ret3 = Reservation.split(" ");
                var ReservationDateBooking = ret3[0];

                var Nights = (arrHotelReservation[0].NoOfDays < 10) ? '0' + arrHotelReservation[0].NoOfDays : '' + arrHotelReservation[0].NoOfDays;

                var agentcode = arrAgentDetails[0].Agentuniquecode;

                $('#spnVoucher').text(arrHotelReservation[0].VoucherID);
                $('#spnChkIn').text(CheckInDate);
                $('#spnChkOut').text(CheckOutDate);
                $('#spnHotelName').text(arrHotelReservation[0].HotelName);
                $('#spnNights').text(Nights);
                $('#spnDate').text(ReservationDateBooking);
                $('#spnAgentRef').text(arrHotelReservation[0].AgentRef);

                setTimeout(function () {
                    GetComments(ReservationID);
                }, 1500);


                var RefNo = (arrHotelReservation[0].ReferenceCode) + "-" + (arrHotelReservation[0].ReservationID)


                $('#Supplier').text(arrHotelReservation[0].Source);
                $('#spnSupplierRef').text(RefNo);
                $('#spnfileNo').text(arrHotelReservation[0].HotelBookingData);



                //for hotel detail
                $('#spnAdd1').text(arrHotelAdd[0].Address);
                $('#spnPostalCode').text(arrHotelAdd[0].PostalCode);

                $('#spnCity1').text(arrHotelAdd[0].City);
                $('#spnCountry1').text(Country);


                if (arrHotelReservation[0].Source == "DoTW" || arrHotelReservation[0].Source == "MGH") {
                    $('#spnPhone1').text(arrHotelAdd[0].ContactPhone);
                }
                else {
                    $('#spnPhone1').text(arrHotelAdd[0].Number_);
                }



                //for agent detail
                $('#spnAdd').text(arrAgentDetails[0].Address);
                $('#spnCity').text(arrAgentDetails[0].Description);
                $('#spnCounty').text(arrAgentDetails[0].Countryname);
                $('#spnAgenPhone').text(arrAgentDetails[0].phone);
                $('#spnAgentEmail').text(arrAgentDetails[0].email);

                $('#AgentLog').append('<img src="../AgencyLogos/' + agentcode + '.png" alt="Logo not found" height="auto" width="auto"  />');
                $('#Map').append('<img src="https://maps.googleapis.com/maps/api/staticmap?zoom=15&size=400x250&sensor=false&maptype=roadmap&markers=color:red|' + lat + ',' + long + '" alt="Map not found"  />');


                //$('#spnemail').text(arrAgentDetails[0].email);
                var tRow = '';
                var PassengerName = "";
                for (var i = 0; i < arrBookedRoom.length; i++) {
                    debugger;

                    for (var j = 0; j < arrBookedPassenger.length; j++) {

                        if (arrBookedRoom[i].RoomCode == arrBookedPassenger[j].RoomCode) {

                            if ((j + 1) != arrBookedPassenger.length) {
                                PassengerName += arrBookedPassenger[j].Name + " " + arrBookedPassenger[j].LastName + ", ";
                            }
                            else {
                                PassengerName += arrBookedPassenger[j].Name + " " + arrBookedPassenger[j].LastName + "";
                            }


                        }
                    }

                    tRow += '<tr style="border-spacing: 0px;">';
                    tRow += '<td style="border-left: none; border-right: none;border-top-width: 3px; border-top-color: white; background-color: #00AEEF; color: white; font-size: 18px;  padding-left:10px; font-weight:700">' + "Room No " + +(i + 1) + '</td>';
                    tRow += '<td colspan="7" style="border-left: none; border-right: none; border-top-width: 3px; border-top-color: white; height: 35px; background-color: #35C2F1; color: white; font-size: 18px;  padding-left: 10px; font-weight: 700; ">';

                    tRow += 'Guest Name :&nbsp;&nbsp;&nbsp; <span> ' + PassengerName + '</span></td>';
                    tRow += '</tr>';

                    tRow += '<tr style="border: none; text-align: center; color: #57585A; font-weight:600">';
                    tRow += '<td align="left" style="border: none; padding-top: 15px; padding: 15px 0px 0px 10px; ">Room Type</td>';
                    tRow += '<td style="border: none; padding-top: 15px"> Board </td>';
                    tRow += '<td style="border: none; padding-top: 15px">Total Rooms</td>';
                    tRow += '<td style="border: none; padding-top: 15px"> Adults </td>';
                    tRow += '<td style="border: none; padding-top: 15px">Child </td>';
                    tRow += '<td style="border: none; padding-top: 15px">Child Age</td>';
                    tRow += '<td align="left" style="border: none; padding-top: 15px">Remark</td>';
                    tRow += '</tr>';
                    tRow += '<tr style="color: #B0B0B0; border: none; text-align: center; color: #57585A;">';
                    tRow += '<td align="left" style="border: none; padding:12px 0px 15px 10px; "> ' + arrBookedRoom[i].RoomType + '</td>';
                    tRow += '<td style="border: none; padding-top: 12px; padding-bottom: 15px; ">' + arrBookedRoom[i].BoardText + '</td>';
                    tRow += '<td style="border: none; padding-top: 12px; padding-bottom: 15px; ">' + arrBookedRoom[i].TotalRooms + '</td>';
                    tRow += '<td style="border: none; padding-top: 12px; padding-bottom: 15px; ">' + arrBookedRoom[i].Adults + '</td>';
                    tRow += '<td style="border: none; padding-top: 12px; padding-bottom: 15px; ">' + arrBookedRoom[i].Child + '</td>';
                    tRow += '<td style="border: none; padding-top: 12px; padding-bottom: 15px; ">' + arrBookedRoom[i].ChildAge + '</td>';
                    tRow += '<td align="left" style="border: none; padding-top: 12px; padding-bottom: 15px; ">' + arrBookedRoom[i].Remark + '</td>';
                    tRow += '</tr>';

                }


                $("#tbl_RoomDetails tbody").append(tRow);
                $("#tbl_RoomDetails tbody").html(tRow);

            }
        },
        error: function () {
            $('#SpnMessege').text("An error occured while loading credit information")
            $('#ModelMessege').modal('show')
            //  alert("An error occured while loading credit information")
        }
    });

}

function VoucherDetails(ReservationID, Uid, latitude, longitude, Nights, status, Supp) {
    debugger;
    if (status == 'Booking') {
        $('#SpnMessege').text('Please Confirm Your Booking! ')
        $('#ModelMessege').modal('show')
        // alert('Please Confirm Your Booking! ')
    }
    else if (status == 'Cancelled') {
        $('#SpnMessege').text('You cannot get voucher for cancelled booking!')
        $('#ModelMessege').modal('show')
        // alert('You cannot get voucher for cancelled booking!')
    }

    UID = Uid;
    ReservationId = ReservationID;
    Latitude = latitude;
    Longitude = longitude;
    Night = (Nights < 10) ? '0' + Nights : '' + Nights;
    Status = status;
    HSupplier = Supp;

}

function MailVoucher() {
    debugger;
    ReservationId = GetQueryStringParamsForAgentRegistrationUpdate('ReservationId');
    Status = GetQueryStringParamsForAgentRegistrationUpdate('Status');
    if (Status == 'Vouchered') {

        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);

        var bValid = true;
        var sEmail = $("#txt_sendVoucher").val();
        if (sEmail == "") {
            alert("Please Enter Your Email");
            bValid = false;
        }
        else {
            if (!(pattern.test(sEmail))) {
                alert("Wrong email format.");
                bValid = false;
            }
        }

        if (bValid == true) {
            $.ajax({
                type: "POST",
                url: "DefaultHandler.asmx/SendVoucher",
                data: '{"sEmail":"' + sEmail + '","ReservationId":"' + ReservationId + '","UID":"' + UID + '","Latitude":"' + Latitude + '","Longitude":"' + Longitude + '","Night":"' + Night + '","Supp":"' + HSupplier + '"}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result != true) {
                        $('#SpnMessege').text("Some error occured, Please try again in some time.");
                        $('#ModelMessege').modal('show')
                        return false;
                    }
                    if (result == true) {
                        $('#SpnMessege').text("Email has been sent successfully.");
                        $('#VoucherModal').modal('hide')
                        $('#txt_sendVoucher').val('')
                        $('#ModelMessege').modal('show')
                        return false;
                    }
                    else if (result.retCode == 0) {
                        $('#SpnMessege').text("Sorry Please Try Later.");
                        $('#ModelMessege').modal('show')
                        return false;
                    }
                    else if (result.retCode == 2) {
                        $('#SpnMessege').text("Sorry, No account found with this email id.");
                        $('#ModelMessege').modal('show')
                        return false;
                    }
                },
                error: function () {
                    $('#SpnMessege').text('Something Went Wrong');
                    $('#ModelMessege').modal('show')
                }

            });
        }
    }
    else {
        $('#SpnMessege').text('Please Confirm Your Booking!!!!');
        $('#ModelMessege').modal('show')
    }
}

function MailActVoucher() {
    debugger;
    ReservationId = GetQueryStringParamsForAgentActivityUpdates('ReservationId');
    Status = GetQueryStringParamsForAgentActivityUpdates('Status');
    if (Status == 'Vouchered') {

        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);

        var bValid = true;
        var sEmail = $("#txt_sendVoucher").val();
        if (sEmail == "") {
            bValid = false;
            $("#lbl_txt_sendVoucher").css("display", "");
        }
        else {
            if (!(pattern.test(sEmail))) {
                bValid = false;
                $("#lbl_txt_sendVoucher").html("* Wrong email format.");
                $("#lbl_txt_sendVoucher").css("display", "");
            }
        }

        if (bValid == true) {
            $.ajax({
                type: "POST",
                url: "../EmailHandler.asmx/SendActVoucher",
                data: '{"sEmail":"' + sEmail + '","ReservationId":"' + ReservationId + '","UID":"' + UID + '"}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.Session == 0) {
                        $('#SpnMessege').text("Some error occured, Please try again in some time.");
                        $('#ModelMessege').modal('show')
                        // alert("Some error occured, Please try again in some time.");
                        return false;
                    }
                    if (result.retCode == 1) {
                        $('#SpnMessege').text("Email has been sent successfully.");
                        $('#VoucherModal').modal('hide')
                        $('#txt_sendVoucher').val('')
                        $('#ModelMessege').modal('show')
                        // alert("Email has been sent successfully.");
                        //window.location.href = "Invoice.aspx";
                        //                    window.location.href = "../CUT/Default.aspx";
                        return false;
                    }
                    else if (result.retCode == 0) {
                        $('#SpnMessege').text("Sorry Please Try Later.");
                        $('#ModelMessege').modal('show')
                        // alert("Sorry Please Try Later.");
                        //window.location.href = "Invoice.aspx";
                        //                    window.location.href = "../CUT/Default.aspx";
                        return false;
                    }
                    else if (result.retCode == 2) {
                        $('#SpnMessege').text("Sorry, No account found with this email id.");
                        $('#ModelMessege').modal('show')
                        // alert("Sorry, No account found with this email id.");
                        //window.location.href = "Invoice.aspx";
                        //                    window.location.href = "../CUT/Default.aspx";
                        return false;
                    }
                },
                error: function () {
                    $('#SpnMessege').text('Something Went Wrong');
                    $('#ModelMessege').modal('show')
                    // alert('Something Went Wrong');
                }

            });
        }
    }
    else {
        $('#SpnMessege').text('Please Confirm Your Booking!!!!');
        $('#ModelMessege').modal('show')
        // alert('Please Confirm Your Booking!!!!');
        //window.location.href = "Invoice.aspx";
    }
}


function GetPDFVoucher(ReservationID, Uid, Status) {
    debugger;
    var win = window.open('VoucherPDF.aspx?ReservationID=' + ReservationID + '&Uid=' + Uid + '&Status=' + Status + '', '_blank');
}

function GetComments(ReservationId) {
    var arrHotelReservationDetails = new Array();
    $.ajax({
        method: 'POST',
        url: 'HotelBookingHandler.asmx/GetComments',
        data: { ReservationID: ReservationId },
        //contentType: "application/json",
        datatype: 'xml',
        success: function (data) {


            debugger;
            //var obj = JSON.parse(response);
            var jQueryXml = $(data);
            //var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            //if (result.retCode == 1) {
            var test = data.lastChild.innerHTML;
            //var test2 = test.replace("{","").replace("Session","").replace(":","").replace("1","").replace(",","").replace("retCode","").replace(":","").replace("1","").replace(",","").replace("ReservationDetails","").replace(":","").replace("[","").replace("terms","").replace(":","");
            $("#spnComment").html(test);

            $("#spnComment").val(jQueryXml.find('terms').text());

            // }
        },
        error: function () {
            $('#SpnMessege').text("something went wrong");
            $('#ModelMessege').modal('show')
            // alert("something went wrong");
        }
    });
}


//---------------------Export to Excel----------------------------------------------

function ExportBooking(Document) {
    debugger;

    Type = $('#Sel_Type').val();
    From = $('#datepicker3').val();
    To = $('#datepicker4').val();
    Passenger = $('#txt_PassengerName').val();
    BookingDt = $('#datepicker5').val();
    RefNo = $('#txt_RefNo').val();
    HotelName = $('#txt_Hotel').val();
    Destination = $('#txt_Destination').val();
    if (Type == "All" && From == "" && To == "" && Passenger == "" && BookingDt == "" && RefNo == "" && HotelName == "" && Destination == "") {
        SearchBit = false;
    }

    if (SearchBit == false) {
        window.location.href = "ExportToExcelHandler.ashx?datatable=BookingList&Type=All&Document=" + Document;
    }
    else {
        window.location.href = "ExportToExcelHandler.ashx?datatable=BookingList&Type=Search&Document=" + Document;
    }



}

function numberWithCommas(x) {


    x = x.toString();
    var afterPoint = '';
    if (x.indexOf('.') > 0)
        afterPoint = x.substring(x.indexOf('.'), x.length);
    x = Math.floor(x);
    x = x.toString();
    var lastThree = x.substring(x.length - 3);
    var otherNumbers = x.substring(0, x.length - 3);
    if (otherNumbers != '')
        lastThree = ',' + lastThree;
    var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
    return res;



    // var parts = n.toString().split(".");
    // return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
}

function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}