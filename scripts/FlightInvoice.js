﻿function MailInvoice() {
    debugger;
    ReservationId = GetQueryStringParamsForAgentRegistrationUpdate('ReservationId');
    Status = GetQueryStringParamsForAgentRegistrationUpdate('Status');
    if (Status == 'Tiketed' || Status == 'Cancelled') {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);

        var bValid = true;
        var sEmail = $("#txt_sendInvoice").val();
        if (sEmail == "") {
            bValid = false;
            $("#lbl_txt_sendInvoice").css("display", "");
        }
        else {
            if (!(pattern.test(sEmail))) {
                bValid = false;
                $("#lbl_txt_sendInvoice").html("* Wrong email format.");
                $("#lbl_txt_sendInvoice").css("display", "");
            }
        }

        if (bValid == true) {
            $.ajax({
                type: "POST",
                url: "DefaultHandler.asmx/SendFlightInvoice",
                data: '{"sEmail":"' + sEmail + '","ReservationId":"' + ReservationId + '"}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result != true) {
                        $('#SpnMessege').text("Some error occured, Please try again in some time.");
                        $('#ModelMessege').modal('show')
                        return false;
                    }
                    if (result == true) {
                        $('#SpnMessege').text("Email has been sent successfully.");
                        $('#InvoiceModal').modal('hide')
                        $('#txt_sendInvoice').val('')
                        $('#ModelMessege').modal('show')
                        return false;
                    }
                    else if (result.retCode == 0) {
                        $('#SpnMessege').text("Sorry Please Try Later.");
                        $('#ModelMessege').modal('show')
                        return false;
                    }
                    else if (result.retCode == 2) {
                        $('#SpnMessege').text("Sorry, No account found with this email id.");
                        $('#ModelMessege').modal('show')
                        return false;
                    }
                },
                error: function () {
                    $('#SpnMessege').text('Something Went Wrong');
                    $('#ModelMessege').modal('show')
                    // alert('Something Went Wrong');
                }

            });
        }
    }
    else {
        $('#SpnMessege').text('Please Confirm Your Booking!!!!!');
        $('#ModelMessege').modal('show')
        // alert('Please Confirm Your Booking!!!!!');
        //window.location.href = "Invoice.aspx";
    }
}

function GetPDFInvoice(ReservationID, Status) {
    debugger;
    var win = window.open('FlightPDF.aspx?ReservationID=' + ReservationID + '&Status=' + Status + '', '_blank');
}