﻿function GenrateFilter() {
    /*Supplier Airlines*/
    var html = '';
    if (arrSearch.Filter.arrInbounds.length != 0) {

        html += '<div class="tab-container style1">'
        html += '<ul class="tabs full-width">'
        html += '<li class="active"><a href="#TabOut" data-toggle="tab">OutBound Airline</a></li>'
        html += '<li><a href="#TabIn" data-toggle="tab">Inbound Airline</a></li>'
        html += '</ul>'

        /*TAB Contents*/
        html += '<div class="tab-content">'
        html += '<div class="tab-pane fade in active" id="TabOut">'
        html += GetSegmentsFilter(arrSearch.Filter.arrOutBounds, "Out");
        html += '</div>'

        html += '<div class="tab-pane fade" id="TabIn">'
        html += GetSegmentsFilter(arrSearch.Filter.arrInbounds, "In");
        html += '</div>'
        html += '</div>'
        html += '</div>'

    }
    else {
        html += GetSegmentsFilter(arrSearch.Filter.arrOutBounds, "Out");
    }
    $("#m_Supplier").append(html)
    //$("#Slider1_Out").slider({ from: 100, to: 5000, step: 5, smooth: true, round: 0, dimension: "&nbsp;درهم ", skin: "round" })
}

function GetSegmentsFilter(arrFligts, Type) {
    var html = ''
    var arrCounts = $.grep(arrFligts[0].Airlines, function (p) { return p.Count })
                .map(function (p) { return p.Count });
    if (arrSearch.Filter.arrInbounds.length != 0) {
        html += '<article class="detailed-logo" style="padding:0px;">'
    }
    else {
        html += '<article class="detailed-logo">'
    }

    html += '<div class="details">'
    html += '<h4 class="search-results-title"><i class="soap-icon-search"></i> <b> ' + arrCounts.reduce(function (a, b) { return a + b; }, 0) + ' </b>Flights found</h4>'
    html += '<h5 class="search-results-title">From <b>' + arrFligts[0].Destination[0] + '</b>  To <b>' + arrFligts[0].OriginAirport[0] + '</b> </h5>'
    html += '</div>'
    html += '</article>'

    html += '<div class="toggle-container filters-container">'
    html += '<!--Departure -->'
    html += '<div class="panel style1 arrow-right">'
    html += '<h4 class="panel-title">'
    html += '<a data-toggle="collapse" href="#collapse4_' + Type + '" class="" aria-expanded="true">Departure time</a>'
    html += '</h4>'
    html += '<div id="collapse4_' + Type + '" class="panel-collapse collapse in">'
    html += '<div class="panel-content">'
    for (var i = 0; i < arrFligts[0].DepartureTime.length; i++) {
        html += '<div class="">'
        html += '<input type="checkbox" class="DepartureTime_' + Type + '" value="' + arrFligts[0].DepartureTime[i].value + '" onchange="GenrateFilters(\'' + Type + '\')" checked> ' + arrFligts[0].DepartureTime[i].Type + ' ( ' + arrFligts[0].DepartureTime[i].Counts + ' )'
        html += '</div>'
    }
    html += '</div>'
    html += '</div>'
    html += '</div>'
    html += '<!-- End of Departure -->'
    /*Departure Details*/

    html += '<!--LayOvers -->'
    html += '<div class="panel style1 arrow-right">'
    html += '<h4 class="panel-title">'
    html += '<a data-toggle="collapse" href="#LayOvers_' + Type + '" class="" aria-expanded="true">Layover</a>'
    html += '</h4>'
    html += '<div id="LayOvers_' + Type + '" class="panel-collapse collapse in">'
    html += '<div class="panel-content">'
    for (var i = 0; i < arrFligts[0].Layover.length; i++) {
        html += '<div>'
        html += '<input type="checkbox" class="Layover_' + Type + '" value="' + arrFligts[0].Layover[i] + '" onchange="GenrateFilters(\'' + Type + '\')" checked> ' + arrFligts[0].Layover[i]
        html += '</div>'
    }
    html += '</div>'
    html += '</div>'
    html += '</div>'
    html += '<!-- End of LayOvers -->'
    /*LayOvers Details*/


    html += '<!--Refund Type -->'
    html += '<div class="panel style1 arrow-right" style="display:none">'
    html += '<h4 class="panel-title">'
    html += '<a data-toggle="collapse" href="#Refund_' + Type + '" class="" aria-expanded="true">Refund Type</a>'
    html += '</h4>'
    html += '<div id="Refund_' + Type + '" class="panel-collapse collapse in">'
    html += '<div class="panel-content">'
    for (var i = 0; i < arrFligts[0].FareType.length; i++) {
        html += '<div class="">'
        html += '<input type="checkbox" class="FareType_' + Type + '" value="' + arrFligts[0].FareType[i].value + '" checked onchange="GenrateFilters(\'' + Type + '\')"> ' + arrFligts[0].FareType[i].Type + ' ( ' + arrFligts[0].FareType[i].Counts + ')'
        html += '</div>'
    }
    html += '</div>'
    html += '</div>'
    html += '</div>'
    html += '<!-- End of LayOvers -->'
    /*LayOvers Details*/

    html += '<!--  Airlines -->'
    html += '<div class="panel style1 arrow-right">'
    html += '<h4 class="panel-title">'
    html += '<a data-toggle="collapse" href="#Airlines_' + Type + '" class="" aria-expanded="true">Airlines</a>'
    html += '</h4>'
    html += '<div id="Airlines_' + Type + '" class="panel-collapse collapse in">'
    html += '<div class="panel-content">'
    for (var i = 0; i < arrFligts[0].Airlines.length; i++) {
        html += '<div class="">'
        html += '<input type="checkbox" class="Airlines_' + Type + '" value="' + arrFligts[0].Airlines[i].Name + '" onchange="GenrateFilters(\'' + Type + '\')" checked> ' + arrFligts[0].Airlines[i].Name + ' ( ' + arrFligts[0].Airlines[i].Count + ')'
        html += '</div>'
    }
    html += '</div>'
    html += '</div>'
    html += '</div>'
    html += '<!-- End of Airlines -->                                                                                                                '

    html += '</div>'

    return html;
}
function GenrateFilters(Type) {
    var filter = new Array();
    var ListFareType = new Array();
    $(".FareType_" + Type + "").each(function () {
        if ($(this).is(":checked")) {
            ListFareType.push({
                Name: $(this)[0].value,
                Counts: 0,
                value: $(this)[0].value
            });
        }
    });


    var Layover = new Array();
    $(".Layover_" + Type + "").each(function () {
        if ($(this).is(":checked")) {
            Layover.push($(this)[0].value);
        }
    });

    var arrAirlines = new Array();
    $(".Airlines_" + Type + "").each(function () {
        if ($(this).is(":checked")) {
            arrAirlines.push({
                Name: $(this)[0].value,
                Code: $(this)[0].value,
                Count: 0
            });
        }
    });

    var arrDepart = new Array();
    $(".DepartureTime_" + Type + "").each(function () {
        if ($(this).is(":checked")) {
            arrDepart.push({
                Name: $(this)[0].value,
                Counts: 0,
                value: $(this)[0].value
            });
        }
    });

    var arrStops = new Array();
    $(".Stops_" + Type + "").each(function () {
        if ($(this).is(":checked")) {
            arrStops.push({
                Name: $(this)[0].value,
                Counts: 0,
                value: $(this)[0].value
            });
        }
    });

    filter = {
        OrderByDepart: 0,
        OrderByArrival: 0,
        OrderByDuration: 0,
        Orderby: $("#txt_Orderby" + Type).val(),
        OrderByPrice: $("#txt_FilterBy" + Type).val(),
        MinPrice: 0,
        MaxPrice: 0,
        FareType: ListFareType,
        Airlines: arrAirlines,
        DepartureTime: arrDepart,
        arrStops: arrStops,
        Layover: Layover,
        Type: Type

    };

    $.ajax({
        type: "POST",
        url: "../handler/FlightHandler.asmx/Filter",
        data: JSON.stringify({ ObjSearch: filter }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrFlights = result.arrFligts.FlightList;
                $("#spn_Count" + Type).text(arrFlights[0].length + " ")
                if (arrSearch.JourneyType == "2" && arrSearch.IsDomastic)
                    FilterDomestic(Type, arrFlights[0])
                else
                    GenrateHtml();

                if (filter.Orderby == 0)
                    $("#txt_Orderby" + Type).val(1);
                else
                    $("#txt_Orderby" + Type).val(0);
            }
            else if (result.retCode == 0) {
                $('#SpnMessege').text(result.errormsg);
                $('#ModelMessege').modal('show')
            }
        },
        error: function () {
            $('#AgencyBookingCancelModal').modal('hide')
            $('#SpnMessege').text("something went wrong");
            $('#ModelMessege').modal('show')
        },
        complete: function () {
            $("#dlgLoader").css("display", "none");
        }
    });
}

function FilterDomestic(Type, arrFlights) {
    $("#div_" + Type).empty();
    html = "";
    if (Type == "Out") {
        html += OrderByFilterRound(Type);
        html += '<div class="flight-list listing-style3 flight">'
        var arrDeparture = arrFlights;
        for (var i = 0; i < arrDeparture.length; i++) {
            var Class = "1";
            if (i % 2 === 0) { Class = "1" }
            else { Class = "2" }
            html += htmlforRound(arrDeparture[i].Segment, Class + ' Departure', arrDeparture[i].Logo, arrDeparture[i].FareTip.replace('↵', '\n'), arrDeparture[i].Fare, arrDeparture[i].Remark, arrDeparture[i].ResultIndex, arrDeparture[i].Refunadble)
        }
        html += '</div>'

    }
    else {
        html += OrderByFilterRound(Type);
        html += '<div class="flight-list listing-style3 flight">'
        var arrArrival = arrFlights
        for (var j = 0; j < arrArrival.length; j++) {
            var Class = "2";
            if (j % 2 === 0) { Class = "2" }
            else { Class = "1" }
            html += htmlforRound(arrArrival[j].Segment, Class + ' Arival', arrArrival[j].Logo, arrArrival[j].FareTip.replace('↵', '\n'), arrArrival[j].Fare, arrArrival[j].Remark, arrArrival[j].ResultIndex, arrArrival[j].Refunadble)
        }
        html += '</div>'
    }
    $("#div_" + Type).append(html)
    //GetSelection()
}