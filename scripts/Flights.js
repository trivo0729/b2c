﻿$(function () {
    GetSearch();
});
var arrFlights = new Array();
var arrSearch = new Array();
function GetSearch() {
    $.ajax({
        type: "POST",
        url: "../handler/FlightHandler.asmx/Getflight",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrSearch = result.arrFligts;
                arrFlights = result.arrFligts.FlightList;
                GenrateFilter();
                GenrateHtml()
                tpj("#datepicker_Return").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "dd-mm-yy",
                    minDate: "dateToday",
                });

                tpj("#dt_CalendarMonthYear").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "dd-mm-yy",
                    minDate: "dateToday",
                });
                $('#sel_Airlines').change(function () {
                    console.log($(this).val());
                }).multipleSelect({
                    width: '100%'
                });


            }
            else if (result.retCode == 0) {
                //$('#AgencyBookingCancelModal').modal('hide')
                //$('#SpnMessege').text(result.Message);
                //$('#ModelMessege').modal('show')
                alert(result.Message)
            }
        },
        error: function () {
            //$('#AgencyBookingCancelModal').modal('hide')
            //$('#SpnMessege').text("something went wrong");
            //$('#ModelMessege').modal('show')
            alert("something went wrong");
        },
        complete: function () {
            $("#dlgLoader").css("display", "none");
        }
    });
}

function GenrateHtml() {
    var html = '';

    $("#div_Airline").empty();
    html += '<div class="row">'
    if ((arrSearch.JourneyType == "1" || arrSearch.JourneyType == "3") || ((arrSearch.JourneyType == "2" || arrSearch.JourneyType == "5") && arrSearch.IsDomastic == false)) {
        html += '<div class="col-sm-12">'
        html += OrderByFilter('Out');
        html += '<div class="flight-list listing-style3 flight">'
        //html += ModifySearch();

        for (var i = 0; i < arrFlights[0].length; i++) {
            var Class = "1";
            if (i % 2 === 0) { Class = "1" }
            else { Class = "2" }
            html += htmlbuilder(arrFlights[0][i].Segment, Class, arrFlights[0][i].Logo, arrFlights[0][i].FareTip.replace('↵', '\n'), arrFlights[0][i].Fare, arrFlights[0][i].Remark, arrFlights[0][i].ResultIndex, arrFlights[0][i].Refunadble)
        }
        html += '<div>'
        html += '<div>'

    }

        /*One way && RoundWay Internation*/ /*One Way && Round WAY International*/

    else if ((arrSearch.JourneyType == "2" || arrSearch.JourneyType == "5") && arrSearch.IsDomastic) {

        if (arrSearch.FlightList.length != 1) {
            /*Departure*/
            //html += ModifySearch();
            html += '<div class="col-sm-6" id="div_Out">'
            html += OrderByFilterRound('Out');
            html += '<div class="flight-list listing-style3 flight">'

            var arrDeparture = arrSearch.FlightList[0];
            for (var i = 0; i < arrDeparture.length; i++) {
                var Class = "Departure";
                //if (i % 2 === 0) { Class = "1" }
                //else { Class = "2" }
                html += htmlforRound(arrDeparture[i].Segment, Class, arrDeparture[i].Logo, arrDeparture[i].FareTip.replace('?', '\n'), arrDeparture[i].Fare, arrDeparture[i].Remark, arrDeparture[i].ResultIndex, arrDeparture[i].Refunadble)
            }
            html += '</div>'
            html += '</div>'

            /*Arrival*/
            html += '<div class="col-sm-6" id="div_In">'
            html += OrderByFilterRound('In');
            html += '<div class="flight-list listing-style3 flight">'
            var arrArrival = arrSearch.FlightList[1]
            for (var j = 0; j < arrArrival.length; j++) {
                var Class = "Arival";
                //if (j % 2 === 0) { Class = "2" }
                //else { Class = "1" }
                html += htmlforRound(arrArrival[j].Segment, Class, arrArrival[j].Logo, arrArrival[j].FareTip.replace('?', '\n'), arrArrival[j].Fare, arrArrival[j].Remark, arrArrival[j].ResultIndex, arrArrival[j].Refunadble)
            }
            html += '</div>'
            html += '</div>'
        }

        else {
            html += '<div class="col-sm-12">'
            html += OrderByFilter('Out');
            html += '<div class="flight-list listing-style3 flight">'
            //html += ModifySearch();

            for (var i = 0; i < arrFlights[0].length; i++) {
                var Class = "1";
                if (i % 2 === 0) { Class = "1" }
                else { Class = "2" }
                html += htmlbuilder(arrFlights[0][i].Segment, Class, arrFlights[0][i].Logo, arrFlights[0][i].FareTip.replace('↵', '\n'), arrFlights[0][i].Fare, arrFlights[0][i].Remark, arrFlights[0][i].ResultIndex, arrFlights[0][i].Refunadble)
            }
            html += '<div>'
            html += '<div>'
        }

    }

    else {
        html += '<div class="col-sm-12">'
        html += '<div class="flight-list listing-style3 flight">'
        //html += ModifySearch();
        for (var i = 0; i < arrFlights[0].length; i++) {

            var Class = "1";
            if (i % 2 === 0) { Class = "1" }
            else { Class = "2" }
            html += GenrateAdvanceSegments(arrFlights[0][i].Segment, Class, arrFlights[0][i].Logo, arrFlights[0][i].Remark, arrFlights[0][i].ResultIndex, arrFlights[0][i].Refunadble)
        }
        html += '<div>'
        html += '<div>'
    }


    html += '<div>'

    $("#div_Airline").append(html);
    $('[data-toggle="tooltip"]').tooltip();
    GetSelection();
    GetCityForModify();
}

function htmlbuilder(arrSegment, Class, Logo, FareTip, TotalFare, Remark, ResultIndex, Refunadble) {
    var html = '';

    html += '<div class="travelo-box">'
    for (var s = 0; s < arrSegment.length; s++) {
        var Baggage = '';
        for (var i = 0; i < arrSegment[s].length; i++) {
            Baggage = arrSegment[s][i].Baggage + ' , ' + arrSegment[s][i].Airline.AirlineCode;
            var minutes = parseInt(arrSegment[s][i].Duration);
            // calculate
            var hours = Math.floor(minutes / 60);
            minutes = minutes % 60;
            if (i == 0) {
                html += '<article class="box" style="padding: 0px; margin-bottom: 0px">'
                html += '<figure class="col-xs-3 col-sm-2">'
                html += '<span>'
                html += GetLogo(Logo)
                html += '<br/>'
                html += '' + arrSegment[s][i].Airline.AirlineName + ''
                html += '<br/>'
                html += '' + arrSegment[s][i].Airline.AirlineCode + '-' + arrSegment[s][i].Airline.FlightNumber + '' + arrSegment[s][i].Airline.FareClass + ''
                html += '</span>'
                html += '</figure>'
                html += '<div class="details col-xs-9 col-sm-10">'
                html += '<div class="details-wrapper">'
                html += '<div class="first-row">'
                if (s == 0) {
                    html += '<div>'
                    html += '<h5 class="box-title">' + arrSegment[s][i].DepartureFrom.CityName + '(' + arrSegment[s][i].DepartureFrom.CityCode + '),' + arrSegment[s][i].DepartureFrom.CountryName + '<i class="soap-icon-longarrow-right"></i>' + arrSegment[s][i].ArrivalFrom.CityName + '<b>(' + arrSegment[s][i].ArrivalFrom.CityCode + ')</b>,' + arrSegment[s][i].ArrivalFrom.CountryName + '</h5>'
                    html += '</div>'
                    html += '<div>'
                    html += '<span data-placement="left" data-toggle="tooltip" class="price" data-container="body" data-original-title="' + FareTip + '" class="price" style="font-size: 16px;">RM ' + TotalFare + '</span>'
                    html += '</div>'
                }
                else {
                    html += '<div style="border-right: 0px">'
                    html += '<h5 class="box-title">' + arrSegment[s][i].DepartureFrom.CityName + '(' + arrSegment[s][i].DepartureFrom.CityCode + '),' + arrSegment[s][i].DepartureFrom.CountryName + '<i class="soap-icon-longarrow-right"></i>' + arrSegment[s][i].ArrivalFrom.CityName + '<b>(' + arrSegment[s][i].ArrivalFrom.CityCode + ')</b>,' + arrSegment[s][i].ArrivalFrom.CountryName + '</h5>'
                    html += '</div>'
                }
                html += '</div>'

                html += '<div class="second-row">'
                if (s == 0) {
                    html += '<div class="time">'
                    html += '<div class="take-off col-sm-3">'
                    html += '<div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>'
                    //html += getTwentyFourHourTime(moment(arrSegment[s][i].DepartureAt).format("LT"));
                    html += '<div style="font-size:18px"> ' + moment(arrSegment[s][i].DepartureAt).format("LT") + ''
                    html += '</div>'
                    html += '</div>'
                    html += '<div class="landing col-sm-3">'
                    html += '<div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>'
                    //html += getTwentyFourHourTime(moment(arrSegment[s][i].ArrivalTime).format("LT"));
                    html += '<div style="font-size:18px"> ' + moment(arrSegment[s][i].ArrivalTime).format("LT") + ''
                    html += '</div>'
                    html += '</div>'
                    html += '<div class="total-time col-sm-3">'
                    html += '<div class="icon"><i class="soap-icon-clock yellow-color"></i></div>'
                    html += '<div>' + hours + 'h ' + minutes + ' Min'
                    html += '</div>'
                    html += '</div>'
                    html += '<div class="col-sm-3">'
                    //html += '<div class="icon"><i class="soap-icon-card  yellow-color"></i></div>'
                    html += '<div>'
                    html += '<span class="skin-color" style="cursor:pointer;" onclick="GetFareRule(\'' + ResultIndex + '\')">Fare Rules</span><br />'
                    if (Refunadble) {
                        html += '<b>'
                        html += '<small style="color:#7db921"><b> Refundable </b></small>'
                        html += '</b>'
                    }

                    else {
                        html += '<b>'
                        html += '<small style="color:#ff3300"><b> Non Refundable </b></small>'
                        html += '</b>'
                    }
                    html += '<br />'
                    html += '<small><b style="color:#ff3300">' + arrSegment[s][i].noSeatAvailable + 'Left</b></small>'
                    html += '</div>'
                    html += '</div>'
                    html += '</div>'
                    html += '<div class="action">'
                    html += '<a href="#" class="button btn-small sky-blue1" onclick="BookTicket(\'' + ResultIndex + '\')">Book</a>'
                    html += '</div>'
                }

                else {
                    html += '<div class="time" style="border-right: 0px">'
                    html += '<div class="take-off col-sm-3">'
                    html += '<div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>'
                    //html += getTwentyFourHourTime(moment(arrSegment[s][i].DepartureAt).format("LT"));
                    html += '<div style="font-size:18px"> ' + moment(arrSegment[s][i].DepartureAt).format("LT") + ''
                    html += '</div>'
                    html += '</div>'
                    html += '<div class="landing col-sm-3">'
                    html += '<div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>'
                    //html += getTwentyFourHourTime(moment(arrSegment[s][i].ArrivalTime).format("LT"));
                    html += '<div style="font-size:18px"> ' + moment(arrSegment[s][i].ArrivalTime).format("LT") + ''
                    html += '</div>'
                    html += '</div>'
                    html += '<div class="total-time col-sm-3">'
                    html += '<div class="icon"><i class="soap-icon-clock yellow-color"></i></div>'
                    html += '<div>' + hours + 'h ' + minutes + ' Min'
                    html += '</div>'
                    html += '</div>'
                    html += '<div class="col-sm-3">'
                    //html += '<div class="icon"><i class="soap-icon-card  yellow-color"></i></div>'
                    html += '<div>'
                    html += '<span class="skin-color" style="cursor:pointer;" onclick="GetFareRule(\'' + ResultIndex + '\')">Fare Rules</span><br />'
                    if (Refunadble) {
                        html += '<b>'
                        html += '<small style="color:#7db921"><b> Refundable </b></small>'
                        html += '</b>'
                    }

                    else {
                        html += '<b>'
                        html += '<small style="color:#ff3300"><b> Non Refundable </b></small>'
                        html += '</b>'
                    }
                    html += '<br />'
                    html += '<small><b style="color:#ff3300">' + arrSegment[s][i].noSeatAvailable + 'Left</b></small>'
                    html += '</div>'
                    html += '</div>'
                    html += '</div>'
                }


                html += '</div>'
                html += '</div>'
                html += '</div>'
                html += '</article>'
                //html += '<div class="row">'
                //html += '<div class="col-xs-12">'
                //html += '<div>Remark :'
                //html += '<span class="skin-color"> ' + Remark + '</span>'
                //html += '</div>'
                //html += '</div>'
                //html += '</div>'
                html += '<div class="row">'
                html += '<div class="col-xs-12">'
                html += '<div>Baggage  :'
                html += '<span class="skin-color"> ' + Baggage + '</span>'
                html += '</div>'
                html += '</div>'
                html += '</div>'
            }

            else {
                html += '<br/>'
                html += '<article class="box" style="padding: 0px; margin-bottom: 0px">'
                html += '<figure class="col-xs-3 col-sm-2">'
                html += '<span>'
                html += GetLogo(Logo)
                html += '<br/>'
                html += '' + arrSegment[s][i].Airline.AirlineName + ''
                html += '<br/>'
                html += '' + arrSegment[s][i].Airline.AirlineCode + '-' + arrSegment[s][i].Airline.FlightNumber + '' + arrSegment[s][i].Airline.FareClass + ''
                html += '</span>'
                html += '</figure>'
                html += '<div class="details col-xs-9 col-sm-10">'
                html += '<div class="details-wrapper">'
                html += '<div class="first-row">'
                html += '<div style="border-right: 0px">'
                html += '<h5 class="box-title">' + arrSegment[s][i].DepartureFrom.CityName + '(' + arrSegment[s][i].DepartureFrom.CityCode + '),' + arrSegment[s][i].DepartureFrom.CountryName + '<i class="soap-icon-longarrow-right"></i>' + arrSegment[s][i].ArrivalFrom.CityName + '<b>(' + arrSegment[s][i].ArrivalFrom.CityCode + ')</b>,' + arrSegment[s][i].ArrivalFrom.CountryName + '</h5>'
                //html += '<a class="button btn-mini stop">' + Flights[i].NoStop + ' STOP</a>'
                html += '</div>'
                html += '</div>'
                html += '<div class="second-row">'

                html += '<div class="time" style="border-right: 0px">'
                html += '<div class="take-off col-sm-3">'
                html += '<div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>'
                //html += getTwentyFourHourTime(moment(arrSegment[s][i].DepartureAt).format("LT"));
                html += '<div style="font-size:18px"> ' + moment(arrSegment[s][i].DepartureAt).format("LT") + ''
                html += '</div>'
                html += '</div>'
                html += '<div class="landing col-sm-3">'
                html += '<div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>'
                //html += getTwentyFourHourTime(moment(arrSegment[s][i].ArrivalTime).format("LT"));
                html += '<div style="font-size:18px"> ' + moment(arrSegment[s][i].ArrivalTime).format("LT") + ''
                html += '</div>'
                html += '</div>'
                html += '<div class="total-time col-sm-3">'
                html += '<div class="icon"><i class="soap-icon-clock yellow-color"></i></div>'
                html += '<div>' + hours + 'h ' + minutes + ' Min'
                html += '</div>'
                html += '</div>'
                html += '<div class="col-sm-3">'
                //html += '<div class="icon"><i class="soap-icon-card  yellow-color"></i></div>'
                html += '<div>'
                html += '<span class="skin-color" style="cursor:pointer;" onclick="GetFareRule(\'' + ResultIndex + '\')">Fare Rules</span><br />'
                if (Refunadble) {
                    html += '<b>'
                    html += '<small style="color:#7db921"><b> Refundable </b></small>'
                    html += '</b>'
                }

                else {
                    html += '<b>'
                    html += '<small style="color:#ff3300"><b> Non Refundable </b></small>'
                    html += '</b>'
                }
                html += '<br />'
                html += '<small><b style="color:#ff3300">' + arrSegment[s][i].noSeatAvailable + 'Left</b></small>'
                html += '</div>'
                html += '</div>'
                html += '</div>'

                html += '</div>'
                html += '</div>'
                html += '</div>'
                html += '</article>'
                //html += '<div class="row">'
                //html += '<div class="col-xs-12">'
                //html += '<div>Remark :'
                //html += '<span class="skin-color"> ' + Remark + '</span>'
                //html += '</div>'
                //html += '</div>'
                //html += '</div>'
                html += '<div class="row">'
                html += '<div class="col-xs-12">'
                html += '<div>Baggage  :'
                html += '<span class="skin-color"> ' + Baggage + '</span>'
                html += '</div>'
                html += '</div>'
                html += '</div>'
            }
        }
    }
    html += '</div>'
    return html;
}


function getTwentyFourHourTime(amPmString) {
    var html = '';
    var d = new Date("1/1/2013 " + amPmString);
    return html = '<div style="font-size:18px"> ' + d.getHours() + ':' + d.getMinutes() + '';
}

function TwentyFourHourTime(amPmString) {
    var html = '';
    var d = new Date("1/1/2013 " + amPmString);
    return html = '<div style="padding: 0px;font-size:18px"> ' + d.getHours() + ':' + d.getMinutes() + '';
}

function BindSearch() {

    if (arrSearch.JourneyType != "3") {
        var DepDest = $.grep(arrCity, function (p) { return p.City_Code == arrSearch.ObjSearch.Segments[0].Origin; })
                        .map(function (p) { return p.City_Name; });
        var ArrivDest = $.grep(arrCity, function (p) { return p.City_Code == arrSearch.ObjSearch.Segments[0].Destination; })
                               .map(function (p) { return p.City_Name; });
        $("#origin0").val(DepDest);
        $("#hdoriginCode0").val(arrSearch.ObjSearch.Segments[0].Origin);
        $("#destination0").val(ArrivDest);
        $("#hddestCode0").val(arrSearch.ObjSearch.Segments[0].Destination);
    }

    var Dat = arrSearch.ObjSearch.Segments[0].PreferredArrivalTime;
    var Date = Dat.split('T');
    var tdate = moment(Date[0]).format('DD-MM-YYYY');


    $("#sel_Adults").val(arrSearch.ObjSearch.AdultCount);
    $("#sel_Child").val(arrSearch.ObjSearch.ChildCount);
    $("#sel_Infant").val(arrSearch.ObjSearch.InfantCount);
    $("#sel_Class").val(arrSearch.ObjSearch.Segments[0].FlightCabinClass);
    if (arrSearch.ObjSearch.OneStopFlight == "true") {
        $("#chkNonStop").prop("checked");
    }

    if (arrSearch.JourneyType == "1") {
        $("#radio01").attr('checked', true);
        OneWayRadioFunc()
        $("#dte_OnwardDateId0").val(tdate);
    }
    if (arrSearch.JourneyType == "2") {
        $("#radio02").attr('checked', true);
        RoundWayRadioFunc();
        var TDat = arrSearch.ObjSearch.Segments[0].PreferredArrivalTime;
        var TDate = TDat.split('T');
        var RDat = arrSearch.ObjSearch.Segments[1].PreferredArrivalTime;
        var RDate = RDat.split('T');
        var RDatee = moment(RDate[0]).format('DD-MM-YYYY');
        var TDatee = moment(TDate[0]).format('DD-MM-YYYY');
        $("#dte_OnwardDateId0").val(TDatee);
        $("#datepicker_Return").val(RDatee);
    }
    if (arrSearch.JourneyType == "3") {
        $("#radio03").attr('checked', true);
        MultiWayRadioFunc()
        for (var i = 0; i < arrSearch.ObjSearch.Segments.length; i++) {
            var TDat = arrSearch.ObjSearch.Segments[i].PreferredArrivalTime;
            var TDate = TDat.split('T');
            var TDatee = moment(TDate[0]).format('DD-MM-YYYY');
            $("#dte_OnwardDateId" + i + "").val(TDatee);
            var DepDest = $.grep(arrCity, function (p) { return p.City_Code == arrSearch.ObjSearch.Segments[i].Origin; })
                        .map(function (p) { return p.City_Name; });
            var ArrivDest = $.grep(arrCity, function (p) { return p.City_Code == arrSearch.ObjSearch.Segments[i].Destination; })
                                   .map(function (p) { return p.City_Name; });
            $("#origin" + i + "").val(DepDest);
            $("#hdoriginCode" + i + "").val(arrSearch.ObjSearch.Segments[0].Origin);
            $("#destination" + i + "").val(ArrivDest);
            $("#hddestCode" + i + "").val(arrSearch.ObjSearch.Segments[0].Destination);
        }
    }
}

function ModifySearch() {
    var html = "";
    html += '<div class="search-box-wrapper style2" style="margin-top: 0px; display: none" id="SearchDive">                                                                                                                                                                                      '
    html += '                        <div class="search-box">                                                                                                                                                                             '
    html += '                                                                                                                                                                                                                             '
    html += '                            <div class="search-tab-content">                                                                                                                                                                 '
    html += '                                <div class="tab-pane fade active in" id="hotels-tab">                                                                                                                                        '
    html += '                                    <form>                                                                                                                                                                                   '
    html += '                                        <div class="row">                                                                                                                                                                    '
    html += '                                            <div class="col-sm-4">                                                                                                                                                           '
    html += '                                                <h5 style="color: black; margin-top: 0px">Book Domestic &amp; International Flight Tickets</h5>                                                                              '
    html += '                                            </div>                                                                                                                                                                           '
    html += '                                            <div>                                                                                                                                                                            '
    html += '                                                <div class="col-sm-1">                                                                                                                                                       '
    html += '                                                    <input type="radio" name="JourneyType" id="radio01" value="O" class="JourneyType" title="One Way" onclick="OneWayRadioFunc()" checked />                              '
    html += '                                                    <label for="radio01">                                                                                                                                                    '
    html += '                                                        <span></span>                                                                                                                                                        '
    html += '                                                        <span style="color: black">One Way</span>                                                                                                                            '
    html += '                                                    </label>                                                                                                                                                                 '
    html += '                                                </div>                                                                                                                                                                       '
    html += '                                                <div class="col-sm-2">                                                                                                                                                       '
    html += '                                                    <input type="radio" name="JourneyType" id="radio02" value="R" class="JourneyType" title="Round Way" onclick="RoundWayRadioFunc()" />                                     '
    html += '                                                    <label for="radio02">                                                                                                                                                    '
    html += '                                                        <span></span>                                                                                                                                                        '
    html += '                                                        <span style="color: black">Round Way</span>                                                                                                                          '
    html += '                                                    </label>                                                                                                                                                                 '
    html += '                                                </div>                                                                                                                                                                       '
    html += '                                                <div class="col-sm-3">                                                                                                                                                       '
    html += '                                                    <input type="radio" name="JourneyType" id="radio03" value="M" class="JourneyType" title="Multiple Destinations" onclick="MultiWayRadioFunc()" />                         '
    html += '                                                    <label for="radio03">                                                                                                                                                    '
    html += '                                                        <span></span>                                                                                                                                                        '
    html += '                                                        <span style="color: black">Multiple Destinations</span>                                                                                                              '
    html += '                                                    </label>                                                                                                                                                                 '
    html += '                                                </div>                                                                                                                                                                       '
    html += '                                                <div class="col-sm-2" style="display: none">                                                                                                                                                       '
    html += '                                                    <input type="radio" name="JourneyType" id="radio04" value="M" class="JourneyType" title="Multi Way" onclick="CalendarFareRadioFunc()" />                                 '
    html += '                                                    <label for="radio04">                                                                                                                                                    '
    html += '                                                        <span></span>                                                                                                                                                        '
    html += '                                                        <span style="color: black">Calendar Fare</span>                                                                                                                      '
    html += '                                                    </label>                                                                                                                                                                 '
    html += '                                                </div>                                                                                                                                                                       '
    html += '                                                <div class="col-sm-2" style="display: none">                                                                                                                                                       '
    html += '                                                    <input type="radio" name="JourneyType" id="radioAdvance" value="M" class="JourneyType" title="Multi Way" onclick="OneWayRadioFunc()" />                                  '
    html += '                                                    <label for="radioAdvance">                                                                                                                                               '
    html += '                                                        <span></span>                                                                                                                                                        '
    html += '                                                        <span style="color: black">Advance Search</span>                                                                                                                     '
    html += '                                                    </label>                                                                                                                                                                 '
    html += '                                                </div>                                                                                                                                                                       '
    html += '                                                <div class="col-sm-2" style="float: left">                                                                                                                                   '
    html += '                                                    <input type="checkbox" name="NonStop" id="chkNonStop" value="NonStop" checked="" class="JourneyType" title="Non Stop Flights" />                                         '
    html += '                                                    <label for="chkNonStop">                                                                                                                                                 '
    html += '                                                        <span></span>                                                                                                                                                        '
    html += '                                                        <span style="color: black">Non Stop Flights</span>                                                                                                                   '
    html += '                                                    </label>                                                                                                                                                                 '
    html += '                                                </div>                                                                                                                                                                       '
    html += '                                            </div>                                                                                                                                                                           '
    html += '                                        </div>                                                                                                                                                                               '
    html += '                                                                                                                                                                                                                             '
    html += '                                        <div class="row" style="display: none">                                                                                                                                              '
    html += '                                            <div class="col-sm-4">                                                                                                                                                           '
    html += '                                            </div>                                                                                                                                                                           '
    html += '                                            <div>                                                                                                                                                                            '
    html += '                                                <div class="col-sm-2" style="display: none">                                                                                                                                 '
    html += '                                                    <input type="radio" name="RoundWay" id="rdb_Normal" value="O" style="display: none" class="RoundWay" title="Normal Return" checked="" style="display: inline-block;" />  '
    html += '                                                    <label for="rdb_Normal" class="RoundWay" style="display: none">                                                                                                          '
    html += '                                                        <span></span>                                                                                                                                                        '
    html += '                                                        <span style="color: black">Normal Return</span>                                                                                                                      '
    html += '                                                    </label>                                                                                                                                                                 '
    html += '                                                </div>                                                                                                                                                                       '
    html += '                                                <div class="col-sm-2" style="display: none">                                                                                                                                 '
    html += '                                                    <input type="radio" name="RoundWay" id="rdb_SP" value="R" style="display: none" class="RoundWay" title="Special Return" style="display: inline-block;" />                '
    html += '                                                    <label for="rdb_SP" class="RoundWay" style="display: none">                                                                                                              '
    html += '                                                        <span></span>                                                                                                                                                        '
    html += '                                                        <span style="color: black">LCC Special Returns</span>                                                                                                                '
    html += '                                                    </label>                                                                                                                                                                 '
    html += '                                                </div>                                                                                                                                                                       '
    html += '                                                <div class="col-sm-2" style="display: none">                                                                                                                                 '
    html += '                                                    <input type="radio" name="RoundWay" id="rdb_GDS" value="M" style="display: none" class="RoundWay" title="Special GDS Return" style="display: inline-block;" />           '
    html += '                                                    <label for="rdb_GDS" class="RoundWay" style="display: none">                                                                                                             '
    html += '                                                        <span></span>                                                                                                                                                        '
    html += '                                                        <span style="color: black">GDS Special Returns</span>                                                                                                                '
    html += '                                                    </label>                                                                                                                                                                 '
    html += '                                                </div>                                                                                                                                                                       '
    html += '                                            </div>                                                                                                                                                                           '
    html += '                                        </div>                                                                                                                                                                               '
    html += '                                                                                                                                                                                                                             '
    html += '                                        <div id="GeneralSearch">                                                                                                                                                             '
    html += '                                            <div class="row">                                                                                                                                                                '
    html += '                                                <div class="col-md-8" id="fromToOnwardDateDiv">                                                                                                                              '
    html += '                                                    <div class="form-group row"></div>                                                                                                                                       '
    html += '                                                </div>                                                                                                                                                                       '
    html += '                                                                                                                                                                                                                             '
    html += '                                                <div class="col-md-4" style="display: none" id="returnDateDivId">                                                                                                            '
    html += '                                                    <br />                                                                                                                                                                   '
    html += '                                                    <div class="form-group row">                                                                                                                                             '
    html += '                                                        <span class="text-left" style="color: #838383">Return Date</span>                                                                                                    '
    html += '                                                        <div class="datepicker-wrap">                                                                                                                                        '
    html += '                                                            <input id="datepicker_Return" type="text" name="date_from" class="form-control" placeholder="return On" />                                                       '
    html += '                                                        </div>                                                                                                                                                               '
    html += '                                                    </div>                                                                                                                                                                   '
    html += '                                                                                                                                                                                                                             '
    html += '                                                </div>                                                                                                                                                                       '
    html += '                                            </div>                                                                                                                                                                           '
    html += '                                            <br />                                                                                                                                                                           '
    html += '                                            <div>                                                                                                                                                                            '
    html += '                                                <div id="addDestinationDivId" class="row white" style="float: right; padding-right: 300px; display: none; margin-bottom: 20px;">                                             '
    html += '                                                    <img src="../images/Bond/plus.png" id="plus" alt="plus" onclick="addDestination()" title="Add Destination" style="width: 25px" />                                        '
    html += '                                                </div>                                                                                                                                                                       '
    html += '                                                <div id="removeLastDestnDivId" class="row white" style="float: right; padding-right: 300px; display: none; margin-bottom: 20px;">                                            '
    html += '                                                                                                                                                                                                                             '
    html += '                                                    <img src="../images/Bond/minus.png" id="minus" alt="minus" onclick="removeLastDestn()" title="Remove Destination" style="width: 30px" />                                 '
    html += '                                                </div>                                                                                                                                                                       '
    html += '                                            </div>                                                                                                                                                                           '
    html += '                                            <div class="row">                                                                                                                                                                '
    html += '                                                <div class="col-md-6">                                                                                                                                                       '
    html += '                                                    <div class="form-group row">                                                                                                                                             '
    html += '                                                        <div class="col-md-6">                                                                                                                                               '
    html += '                                                            <span class="text-left" style="color: #838383">Airlines</span>                                                                                                   '
    html += '                                                            <select id="sel_Airlines" class="form-control" multiple="multiple">                                                                                              '
    html += '                                                                <option value="SG" selected="selected">Spice Jet</option>                                                                                                    '
    html += '                                                                <option value="6E" selected="selected">Indigo</option>                                                                                                       '
    html += '                                                                <option value="G8" selected="selected">Go Air</option>                                                                                                       '
    html += '                                                                <option value="G9" selected="selected">Air Arabia</option>                                                                                                   '
    html += '                                                                <option value="FZ" selected="selected">Fly Dubai</option>                                                                                                    '
    html += '                                                                <option value="IX" selected="selected">Air India Express</option>                                                                                            '
    html += '                                                                <option value="AK" selected="selected">Air Asia</option>                                                                                                     '
    html += '                                                                <option value="LB" selected="selected">Air Costa</option>                                                                                                    '
    html += '                                                                <option value="UK" selected="selected">Air Vistara</option>                                                                                                  '
    html += '                                                                <option value="UK" selected="selected">Air Vistara</option>                                                                                                  '
    html += '                                                                <option value="AI" selected="selected">Air India</option>                                                                                                    '
    html += '                                                                <option value="9W" selected="selected">Jet Airways</option>                                                                                                  '
    html += '                                                                <option value="S2" selected="selected">JetLite</option>                                                                                                      '
    html += '                                                            </select>                                                                                                                                                        '
    html += '                                                        </div>                                                                                                                                                               '
    html += '                                                        <div class="col-md-6">                                                                                                                                               '
    html += '                                                            <span class="text-left" style="color: #838383">Class</span>                                                                                                      '
    html += '                                                            <select class="form-control" id="sel_Class">                                                                                                                     '
    html += '                                                                <option selected="selected" value="1">All</option>                                                                                                           '
    html += '                                                                <option value="2">Economy</option>                                                                                                                           '
    html += '                                                                <option value="3">Premium Economy</option>                                                                                                                   '
    html += '                                                                <option value="4">Business</option>                                                                                                                          '
    html += '                                                                <option value="5">Premium Business</option>                                                                                                                  '
    html += '                                                                <option value="6">First</option>                                                                                                                             '
    html += '                                                            </select>                                                                                                                                                        '
    html += '                                                        </div>                                                                                                                                                               '
    html += '                                                    </div>                                                                                                                                                                   '
    html += '                                                </div>                                                                                                                                                                       '
    html += '                                            </div>                                                                                                                                                                           '
    html += '                                            <div class="row">                                                                                                                                                                '
    html += '                                                <div class="col-md-6">                                                                                                                                                       '
    html += '                                                    <div class="form-group row">                                                                                                                                             '
    html += '                                                        <div class="col-md-4">                                                                                                                                               '
    html += '                                                            <span class="text-left" style="color: #838383">Adults</span>                                                                                                     '
    html += '                                                            <select class="form-control" id="sel_Adults">                                                                                                                    '
    html += '                                                                <option selected="selected" value="1">01</option>                                                                                                            '
    html += '                                                                <option value="2">02</option>                                                                                                                                '
    html += '                                                                <option value="3">03</option>                                                                                                                                '
    html += '                                                                <option value="4">04</option>                                                                                                                                '
    html += '                                                                <option value="">05</option>                                                                                                                                 '
    html += '                                                                <option value="1">06</option>                                                                                                                                '
    html += '                                                                <option value="2">07</option>                                                                                                                                '
    html += '                                                                <option value="3">08</option>                                                                                                                                '
    html += '                                                                <option value="4">09</option>                                                                                                                                '
    html += '                                                            </select>                                                                                                                                                        '
    html += '                                                        </div>                                                                                                                                                               '
    html += '                                                        <div class="col-md-4">                                                                                                                                               '
    html += '                                                            <span class="text-left" style="color: #838383">Kids</span>                                                                                                       '
    html += '                                                            <select class="form-control" id="sel_Child">                                                                                                                     '
    html += '                                                                <option selected="selected" value="0">00</option>                                                                                                            '
    html += '                                                                <option value="1">01</option>                                                                                                                                '
    html += '                                                                <option value="2">02</option>                                                                                                                                '
    html += '                                                                <option value="3">03</option>                                                                                                                                '
    html += '                                                                <option value="4">04</option>                                                                                                                                '
    html += '                                                                <option value="">05</option>                                                                                                                                 '
    html += '                                                                <option value="1">06</option>                                                                                                                                '
    html += '                                                                <option value="2">07</option>                                                                                                                                '
    html += '                                                                <option value="3">08</option>                                                                                                                                '
    html += '                                                                <option value="4">09</option>                                                                                                                                '
    html += '                                                            </select>                                                                                                                                                        '
    html += '                                                        </div>                                                                                                                                                               '
    html += '                                                        <div class="col-md-4">                                                                                                                                               '
    html += '                                                            <span class="text-left" style="color: #838383">Infants</span>                                                                                                    '
    html += '                                                            <select class="form-control" id="sel_Infant">                                                                                                                    '
    html += '                                                                <option selected="selected" value="0">00</option>                                                                                                            '
    html += '                                                                <option value="1">01</option>                                                                                                                                '
    html += '                                                                <option value="2">02</option>                                                                                                                                '
    html += '                                                                <option value="3">03</option>                                                                                                                                '
    html += '                                                                <option value="4">04</option>                                                                                                                                '
    html += '                                                                <option value="">05</option>                                                                                                                                 '
    html += '                                                                <option value="1">06</option>                                                                                                                                '
    html += '                                                                <option value="2">07</option>                                                                                                                                '
    html += '                                                                <option value="3">08</option>                                                                                                                                '
    html += '                                                                <option value="4">09</option>                                                                                                                                '
    html += '                                                            </select>                                                                                                                                                        '
    html += '                                                        </div>                                                                                                                                                               '
    html += '                                                    </div>                                                                                                                                                                   '
    html += '                                                </div>                                                                                                                                                                       '
    html += '                                                <div class="col-md-6">                                                                                                                                                       '
    html += '                                                    <div class="form-group row">                                                                                                                                             '
    html += '                                                        <div class="col-xs-3">                                                                                                                                               '
    html += '                                                        </div>                                                                                                                                                               '
    html += '                                                        <div class="col-xs-6 pull-right">                                                                                                                                    '
    html += '                                                            <input type="button" class="button btn-medium sky-blue1" value="Search" onclick="SearchFlight()" />                                                              '
    html += '                                                        </div>                                                                                                                                                               '
    html += '                                                        <br />                                                                                                                                                               '
    html += '                                                    </div>                                                                                                                                                                   '
    html += '                                                </div>                                                                                                                                                                       '
    html += '                                            </div>                                                                                                                                                                           '
    html += '                                        </div>                                                                                                                                                                               '
    html += '                                                                                                                                                                                                                             '
    html += '                                        <div id="CalendarSearch" style="display: none">                                                                                                                                      '
    html += '                                            <br />                                                                                                                                                                           '
    html += '                                            <div class="row">                                                                                                                                                                '
    html += '                                                <div class="col-md-3">                                                                                                                                                       '
    html += '                                                    <span class="text-left" style="color: #838383">From</span>                                                                                                               '
    html += '                                                    <br />                                                                                                                                                                   '
    html += '                                                    <input type="text" id="CalenderFrom" class="form-control" />                                                                                                             '
    html += '                                                    <input type="hidden" id="hdCalenderFromCode" />                                                                                                                          '
    html += '                                                </div>                                                                                                                                                                       '
    html += '                                                <div class="col-md-3">                                                                                                                                                       '
    html += '                                                    <span class="text-left" style="color: #838383">To</span>                                                                                                                 '
    html += '                                                    <br />                                                                                                                                                                   '
    html += '                                                    <input type="text" id="CalenderTo" class="form-control" />                                                                                                               '
    html += '                                                    <input type="hidden" id="hdCalenderToCode" />                                                                                                                            '
    html += '                                                </div>                                                                                                                                                                       '
    html += '                                                <div class="col-md-3">                                                                                                                                                       '
    html += '                                                    <span class="text-left" style="color: #838383">Month & Year</span>                                                                                                       '
    html += '                                                    <div class="datepicker-wrap">                                                                                                                                            '
    html += '                                                        <input id="dt_CalendarMonthYear" type="text" name="date_from" class="form-control" />                                                                                '
    html += '                                                    </div>                                                                                                                                                                   '
    html += '                                                </div>                                                                                                                                                                       '
    html += '                                            </div>                                                                                                                                                                           '
    html += '                                            <br />                                                                                                                                                                           '
    html += '                                            <div class="row">                                                                                                                                                                '
    html += '                                                <div class="col-md-3">                                                                                                                                                       '
    html += '                                                    <span class="text-left" style="color: #838383">Preferred Carrier</span>                                                                                                  '
    html += '                                                    <select class="form-control" id="PreferredCarrier">                                                                                                                      '
    html += '                                                        <option value="0">select</option>                                                                                                                                    '
    html += '                                                        <option value="SG">Spice Jet</option>                                                                                                                                '
    html += '                                                        <option value="6E">Indigo</option>                                                                                                                                   '
    html += '                                                        <option value="G8">Go Air</option>                                                                                                                                   '
    html += '                                                        <option value="G9">Air Arabia</option>                                                                                                                               '
    html += '                                                        <option value="FZ">Fly Dubai</option>                                                                                                                                '
    html += '                                                        <option value="IX">Air India Express</option>                                                                                                                        '
    html += '                                                        <option value="AK">Air Asia</option>                                                                                                                                 '
    html += '                                                        <option value="LB">Air Costa</option>                                                                                                                                '
    html += '                                                        <option value="GDS">GDS</option>                                                                                                                                     '
    html += '                                                    </select>                                                                                                                                                                '
    html += '                                                </div>                                                                                                                                                                       '
    html += '                                                <div class="col-sm-2">                                                                                                                                                       '
    html += '                                                    <span class="text-left" style="color: #838383">Class</span>                                                                                                              '
    html += '                                                    <select class="form-control" id="sel_CalendarClass">                                                                                                                     '
    html += '                                                        <option value="1">All</option>                                                                                                                                       '
    html += '                                                        <option value="2">Economy</option>                                                                                                                                   '
    html += '                                                        <option value="3">Premium Economy</option>                                                                                                                           '
    html += '                                                        <option value="4">Business</option>                                                                                                                                  '
    html += '                                                        <option value="5">Premium Business</option>                                                                                                                          '
    html += '                                                        <option value="6">First</option>                                                                                                                                     '
    html += '                                                    </select>                                                                                                                                                                '
    html += '                                                </div>                                                                                                                                                                       '
    html += '                                            </div>                                                                                                                                                                           '
    html += '                                            <br />                                                                                                                                                                           '
    html += '                                            <input type="button" class="button btn-medium sky-blue1" id="btn_CalendarSearch" style="float: right" value="Search" onclick="CalendarSearchFlight()" />                         '
    html += '                                            <br />                                                                                                                                                                           '
    html += '                                        </div>                                                                                                                                                                               '
    html += '                                    </form>                                                                                                                                                                                  '
    html += '                                </div>                                                                                                                                                                                       '
    html += '                            </div>                                                                                                                                                                                           '
    html += '                        </div>                                                                                                                                                                                               '
    html += '                    </div>                                                                                                                                                                                                   '
    html += '                    <br/>                                                                                                                                                                                                   '
    return html;
}


function GenrateAdvanceSegments(arrSegment, Class, Logo, Remark, ResultIndex, Refunadble) {
    var html = '';
    html += '<div class="travelo-box">'
    for (var s = 0; s < arrSegment.length; s++) {
        var Baggage = '';
        for (var i = 0; i < arrSegment[s].length; i++) {
            Baggage = arrSegment[s][i].Baggage + ' , ' + arrSegment[s][i].Airline.AirlineCode;
            var minutes = parseInt(arrSegment[s][i].Duration);
            // calculate
            var hours = Math.floor(minutes / 60);
            minutes = minutes % 60;
            if (i == 0) {

                html += '<article class="box" style="padding: 0px; margin-bottom: 0px">'
                html += '<figure class="col-xs-3 col-sm-2">'
                html += '<span>'
                html += GetLogo(Logo)
                html += '<br/>'
                html += '' + arrSegment[s][i].Airline.AirlineName + ' (' + arrSegment[s][i].Airline.AirlineCode + '-' + arrSegment[s][i].Airline.FlightNumber + '' + arrSegment[s][i].Airline.FareClass + ')' + ''
                html += '<br/>'
                html += '' + arrSegment[s][i].Airline.AirlineName + '(' + arrSegment[s][i].Airline.OperatingCarrier + '-' + arrSegment[s][i].Airline.FlightNumber + ')' + ''
                html += '</span>'
                html += '</figure>'
                html += '<div class="details col-xs-9 col-sm-10">'
                html += '<div class="details-wrapper">'
                html += '<div class="first-row">'
                html += '<div style="border-right: 0px">'
                html += '<h5 class="box-title">' + arrSegment[s][i].DepartureFrom.CityName + '(' + arrSegment[s][i].DepartureFrom.CityCode + '),' + arrSegment[s][i].DepartureFrom.CountryName + '<span class="farrow"></span>' + arrSegment[s][i].ArrivalFrom.CityName + '<b>(' + arrSegment[s][i].ArrivalFrom.CityCode + ')</b>,' + arrSegment[s][i].ArrivalFrom.CountryName + '</h5>'
                html += '</div>'
                html += '</div>'
                html += '<div class="second-row">'
                html += '<div class="time" style="border-right: 0px">'
                html += '<div class="take-off col-sm-2">'
                html += '<div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>'
                html += getTwentyFourHourTime(moment(arrSegment[s][i].DepartureAt).format("LT"));
                //html += '<div>' + moment(arrSegment[s][i].DepartureAt).format("LT") + ''
                html += '</div>'
                html += '</div>'
                html += '<div class="landing col-sm-2">'
                html += '<div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>'
                html += getTwentyFourHourTime(moment(arrSegment[s][i].ArrivalTime).format("LT"));
                //html += '<div>' + moment(arrSegment[s][i].ArrivalTime).format("LT") + ''
                html += '</div>'
                html += '</div>'
                html += '<div class="total-time col-sm-2">'
                html += '<div class="icon"><i class="soap-icon-clock yellow-color"></i></div>'
                html += '<div>' + hours + 'h ' + minutes + ' Min</div>'
                html += '</div>'
                html += '<div class="col-sm-2">'
                html += '<div style="padding-left: 0px">'
                html += '<span class="skin-color" style="cursor:pointer;" onclick="GetFareRule(\'' + ResultIndex + '\')">Fare Rules</span><br />'
                if (Refunadble)
                    html += '<b><small style="color: #7db921"><b>Refundable </b></small></b>'
                else
                    html += '<b><small style="color:#ff3300"><b> Non Refundable </b></small></b>'
                html += '</div>'
                html += '</div>'
                html += '<div class="col-sm-2">'
                html += '<div style="padding-left: 0px">'
                html += '<small><b style="color: #ff3300">' + arrSegment[s][i].noSeatAvailable + 'Left</b></small>'
                html += '</div>'
                html += '</div>'
                html += '<div class="col-sm-2">'
                html += '<div style="padding-left: 0px">'
                if (s == 0)
                    html += '<div class="price" style="font-size: 16px;" id="spn_Total' + ResultIndex + '"></div><br/>'
                html += '<a href="#" class="button btn-small sky-blue1" id="btn_' + ResultIndex + '" type="button" onclick="GetPriceRBD(\'' + ResultIndex + '\')" style="display:none">Get Fare</a>'
                html += '</div>'
                html += '</div>'
                html += '</div>'
                html += '</div>'
                html += '</div>'
                html += '</div>'
                html += '</article>'

                html += '<br />'
                html += '<div class="row">'
                html += '<div class="col-xs-12">'
                html += '<div>Select Class :</div>'
                html += '<div>'
                for (var a = 0; a < arrSegment[s][i].Availability.length; a++) {
                    html += '<a class="button btn-mini silver Fare_' + ResultIndex + i + '" onclick="SetClass(\'' + ResultIndex + '\',\'' + arrSegment[s][i].Availability[a].Class + '\',\'' + arrSegment[s].length + '\',\'' + i + '\',this)" >' + arrSegment[s][i].Availability[a].Class + arrSegment[s][i].Availability[a].Seats + '</a>&nbsp;'
                }
                html += '</div>'
                html += '</div>'
                html += '</div>'
            }

            else {
                html += '<br />'
                html += '<br />'
                html += '<article class="box" style="padding: 0px; margin-bottom: 0px">'
                html += '<figure class="col-xs-3 col-sm-2">'
                html += '<span>'
                html += GetLogo(Logo)
                html += '<br/>'
                html += '' + arrSegment[s][i].Airline.AirlineName + ' (' + arrSegment[s][i].Airline.AirlineCode + '-' + arrSegment[s][i].Airline.FlightNumber + '' + arrSegment[s][i].Airline.FareClass + ')' + ''
                html += '<br/>'
                html += '' + arrSegment[s][i].Airline.AirlineName + '(' + arrSegment[s][i].Airline.OperatingCarrier + '-' + arrSegment[s][i].Airline.FlightNumber + ')' + ''
                html += '</span>'
                html += '</figure>'
                html += '<div class="details col-xs-9 col-sm-10">'
                html += '<div class="details-wrapper">'
                html += '<div class="first-row">'
                html += '<div style="border-right: 0px">'
                html += '<h5 class="box-title">' + arrSegment[s][i].DepartureFrom.CityName + '(' + arrSegment[s][i].DepartureFrom.CityCode + '),' + arrSegment[s][i].DepartureFrom.CountryName + '<span class="farrow"></span>' + arrSegment[s][i].ArrivalFrom.CityName + '<b>(' + arrSegment[s][i].ArrivalFrom.CityCode + ')</b>,' + arrSegment[s][i].ArrivalFrom.CountryName + '</h5>'
                html += '</div>'
                html += '</div>'
                html += '<div class="second-row">'
                html += '<div class="time" style="border-right: 0px">'
                html += '<div class="take-off col-sm-2">'
                html += '<div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>'
                html += getTwentyFourHourTime(moment(arrSegment[s][i].DepartureAt).format("LT"));
                //html += '<div>' + moment(arrSegment[s][i].DepartureAt).format("LT") + ''
                html += '</div>'
                html += '</div>'
                html += '<div class="landing col-sm-2">'
                html += '<div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>'
                html += getTwentyFourHourTime(moment(arrSegment[s][i].ArrivalTime).format("LT"));
                //html += '<div>' + moment(arrSegment[s][i].ArrivalTime).format("LT") + ''
                html += '</div>'
                html += '</div>'
                html += '<div class="total-time col-sm-2">'
                html += '<div class="icon"><i class="soap-icon-clock yellow-color"></i></div>'
                html += '<div>' + hours + 'h ' + minutes + ' Min</div>'
                html += '</div>'
                html += '<div class="col-sm-2">'
                html += '<div style="padding-left: 0px">'
                html += '<span class="skin-color" style="cursor:pointer;" onclick="GetFareRule(\'' + ResultIndex + '\')">Fare Rules</span><br />'
                if (Refunadble)
                    html += '<b><small style="color: #7db921"><b>Refundable </b></small></b>'
                else
                    html += '<b><small style="color:#ff3300"><b> Non Refundable </b></small></b>'
                html += '</div>'
                html += '</div>'
                html += '<div class="col-sm-2">'
                html += '<div style="padding-left: 0px">'
                html += '<small><b style="color: #ff3300">' + arrSegment[s][i].noSeatAvailable + 'Left</b></small>'
                html += '</div>'
                html += '</div>'
                html += '<div class="col-sm-2">'
                html += '<div style="padding-left: 0px">'
                //html += '<a href="#" class="button btn-small sky-blue1" id="btn_' + ResultIndex + '" type="button" onclick="GetPriceRBD(\'' + ResultIndex + '\')" style="display:none">Get Fare</a>'
                html += '</div>'
                html += '</div>'
                html += '</div>'
                html += '</div>'
                html += '</div>'
                html += '</div>'
                html += '</article>'
                html += '<br />'
                html += '<div class="row">'
                html += '<div class="col-xs-12">'
                html += '<div>Select Class :</div>'
                html += '<div>'
                for (var a = 0; a < arrSegment[s][i].Availability.length; a++) {
                    html += '<a class="button btn-mini silver Fare_' + ResultIndex + i + '" onclick="SetClass(\'' + ResultIndex + '\',\'' + arrSegment[s][i].Availability[a].Class + '\',\'' + arrSegment[s].length + '\',\'' + i + '\',this)" >' + arrSegment[s][i].Availability[a].Class + arrSegment[s][i].Availability[a].Seats + '</a>&nbsp;'
                }
                html += '</div>'
                html += '</div>'
                html += '</div>'
            }

        }
    }
    html += '</div>'

    return html;
}


function htmlforRound(arrSegment, Class, Logo, FareTip, TotalFare, Remark, ResultIndex, Refunadble) {
    var html = '';
    html += '<div class="travelo-box ' + Class + '" id="' + ResultIndex + '" onmouseover="chgbordercolor(' + ResultIndex + ')" onmouseout="chgbordercolor1(' + ResultIndex + ')">'
    for (var s = 0; s < arrSegment.length; s++) {
        var Baggage = '';
        for (var i = 0; i < arrSegment[s].length; i++) {
            Baggage = arrSegment[s][i].Baggage + ' , ' + arrSegment[s][i].Airline.AirlineCode;
            var minutes = parseInt(arrSegment[s][i].Duration);
            // calculate
            var hours = Math.floor(minutes / 60);
            minutes = minutes % 60;
            if (i == 0) {
                html += '<article class="box" style="padding: 0px; margin-bottom: 0px">'
                html += '<figure class="col-xs-3 col-sm-2">'
                html += '<span>'
                html += GetLogo(Logo)
                html += '<br/>'
                html += '' + arrSegment[s][i].Airline.AirlineName + ''
                html += '<br/>'
                html += '' + arrSegment[s][i].Airline.AirlineCode + '-' + arrSegment[s][i].Airline.FlightNumber + '' + arrSegment[s][i].Airline.FareClass + ''
                html += '</figure>'
                html += '<div class="details col-xs-9 col-sm-10">'
                html += '<div class="details-wrapper">'
                html += '<div class="first-row">'
                html += '<div>'
                html += '<h5 class="box-title" style="float: none">' + arrSegment[s][i].DepartureFrom.CityName + '(' + arrSegment[s][i].DepartureFrom.CityCode + '),' + arrSegment[s][i].DepartureFrom.CountryName + '<i class="soap-icon-longarrow-right"></i>' + arrSegment[s][i].ArrivalFrom.CityName + '<b>(' + arrSegment[s][i].ArrivalFrom.CityCode + ')</b>,' + arrSegment[s][i].ArrivalFrom.CountryName + '</h5>'
                html += '</div>'
                html += '<div><span data-placement="left" data-toggle="tooltip" class="price" data-container="body" data-original-title="' + FareTip + '" style="font-size: 16px;">RM ' + TotalFare + '</span></div>'
                html += '</div>'
                html += '<div class="second-row">'
                html += '<div class="time">'
                html += '<div class="take-off col-sm-3">'
                html += '<div class="icon" style="float: none"><i class="soap-icon-plane-right yellow-color"></i></div>'
                //html += TwentyFourHourTime(moment(arrSegment[s][i].DepartureAt).format("LT"));
                html += '<div style="padding: 0px">' + moment(arrSegment[s][i].DepartureAt).format("LT") + ''
                html += '</div>'
                html += '</div>'
                html += '<div class="landing col-sm-4">'
                html += '<div class="icon" style="float: none"><i class="soap-icon-plane-right yellow-color"></i></div>'
                //html += TwentyFourHourTime(moment(arrSegment[s][i].ArrivalTime).format("LT"));
                html += '<div style="padding: 0px">' + moment(arrSegment[s][i].ArrivalTime).format("LT") + ''
                html += '</div>'
                html += '</div>'
                html += '<div class="total-time col-sm-3">'
                html += '<div class="icon" style="float: none"><i class="soap-icon-clock yellow-color"></i></div>'
                html += '<div style="padding: 0px">' + hours + 'h ' + minutes + ' Min</div>'
                html += '</div>'
                html += '<div class="col-sm-2">'
                html += '<div style="padding: 0px">'
                html += '<small style="color: #ff3300">' + arrSegment[s][i].noSeatAvailable + ' left</small>'
                html += '</div>'
                html += '</div>'
                html += '</div>'
                html += '<div class="action">'
                html += '<input type="radio" name="' + Class + '" class="' + Class + '" id="' + ResultIndex + '"/><br/>'
                html += '<span class="skin-color" style="cursor:pointer;" onclick="GetFareRule(\'' + ResultIndex + '\')">Fare Rules</span><br />'
                if (Refunadble) {
                    html += '<b>'
                    html += '<small style="color:#7db921"><b> Refundable </b></small>'
                    html += '</b>'
                }

                else {
                    html += '<b>'
                    html += '<small style="color:#ff3300"><b> Non Refundable </b></small>'
                    html += '</b>'
                }
                html += '</div>'
                html += '</div>'
                html += '</div>'
                html += '</div>'
                html += '</article>'
                html += '<br />'
                //html += '<div class="row">'
                //html += '<div class="col-xs-10 col-sm-12">'
                //html += '<div>Remark :<span class="skin-color">' + Remark + '</span></div>'
                //html += '</div>'
                //html += '</div>'
                html += '<div class="row">'
                html += '<div class="col-xs-10 col-sm-12">'
                html += '<div>Baggage  :<span class="skin-color">' + Baggage + '</span></div>'
                html += '</div>'
                html += '</div>'
            }

            else {
                html += '<br/>'
                html += '<article class="box" style="padding: 0px; margin-bottom: 0px">'
                html += '<figure class="col-xs-3 col-sm-2">'
                html += '<span>'
                html += GetLogo(Logo)
                html += '<br/>'
                html += '' + arrSegment[s][i].Airline.AirlineName + ''
                html += '<br/>'
                html += '' + arrSegment[s][i].Airline.AirlineCode + '-' + arrSegment[s][i].Airline.FlightNumber + '' + arrSegment[s][i].Airline.FareClass + ''
                html += '</figure>'
                html += '<div class="details col-xs-9 col-sm-10">'
                html += '<div class="details-wrapper">'
                html += '<div class="first-row">'
                html += '<div style="border-right: 0px; text-align:left"">'
                html += '<h5 class="box-title" style="float: none;>' + arrSegment[s][i].DepartureFrom.CityName + '(' + arrSegment[s][i].DepartureFrom.CityCode + '),' + arrSegment[s][i].DepartureFrom.CountryName + '<i class="soap-icon-longarrow-right"></i>' + arrSegment[s][i].ArrivalFrom.CityName + '<b>(' + arrSegment[s][i].ArrivalFrom.CityCode + ')</b>,' + arrSegment[s][i].ArrivalFrom.CountryName + '</h5>'
                html += '</div>'
                html += '</div>'
                html += '<div class="second-row">'
                html += '<div class="time" style="border-right: 0px">'
                html += '<div class="take-off col-sm-2">'
                html += '<div class="icon" style="float: none"><i class="soap-icon-plane-right yellow-color"></i></div>'
                //html += TwentyFourHourTime(moment(arrSegment[s][i].DepartureAt).format("LT"));
                html += '<div style="padding: 0px">' + moment(arrSegment[s][i].DepartureAt).format("LT") + ''
                html += '</div>'
                html += '</div>'
                html += '<div class="landing col-sm-3">'
                html += '<div class="icon" style="float: none"><i class="soap-icon-plane-right yellow-color"></i></div>'
                //html += TwentyFourHourTime(moment(arrSegment[s][i].ArrivalTime).format("LT"));
                html += '<div style="padding: 0px">' + moment(arrSegment[s][i].ArrivalTime).format("LT") + ''
                html += '</div>'
                html += '</div>'
                html += '<div class="total-time col-sm-2">'
                html += '<div class="icon" style="float: none"><i class="soap-icon-clock yellow-color"></i></div>'
                html += '<div style="padding: 0px">' + hours + 'h ' + minutes + ' Min</div>'
                html += '</div>'
                html += '<div class="col-sm-2">'
                html += '<div style="padding: 0px">'
                html += '<small style="color: #ff3300">' + arrSegment[s][i].noSeatAvailable + ' left</small>'
                html += '</div>'
                html += '</div>'
                html += '<div class="col-sm-3">'
                html += '<div style="padding: 0px">'
                html += '<span class="skin-color" style="cursor:pointer;" onclick="GetFareRule(\'' + ResultIndex + '\')">Fare Rules</span><br />'
                if (Refunadble) {
                    html += '<b>'
                    html += '<small style="color:#7db921"><b> Refundable </b></small>'
                    html += '</b>'
                }

                else {
                    html += '<b>'
                    html += '<small style="color:#ff3300"><b> Non Refundable </b></small>'
                    html += '</b>'
                }
                html += '</div>'
                html += '</div>'
                html += '</div>'

                html += '</div>'
                html += '</div>'
                html += '</div>'
                html += '</article>'
                //html += '<div class="row">'
                //html += '<div class="col-xs-10 col-sm-12">'
                //html += '<div>Remark :<span class="skin-color">' + Remark + '</span></div>'
                //html += '</div>'
                //html += '</div>'
                html += '<div class="row">'
                html += '<div class="col-xs-10 col-sm-12">'
                html += '<div>Baggage  :<span class="skin-color">' + Baggage + '</span></div>'
                html += '</div>'
                html += '</div>'
            }
        }
    }
    html += '</div>'
    return html;
}

function chgbordercolor(id) {
    $(id).css("border", "2px solid #30ddb6");
}
function chgbordercolor1(id) {
    $(id).css("border", "");
}

function GetLogo(AirlinCode) {
    var ext = ".gif";
    if (AirlinCode == "UK")
        ext = ".png";
    var img = '<img src="../images/AirlineLogo/' + AirlinCode + ext + '" alt="">';
    return img;
}/*Logo*/

function LogoForModal(Logo) {
    var ext = ".gif";
    if (Logo == "UK")
        ext = ".png";
    var img = '<img src="images/AirlineLogo/' + Logo + ext + '" style="height:51px ; width:65px" />';
    return img;
}

function GetFareRule(ResultIndex) {
    $("#AlertMessage").empty();
    $.ajax({
        type: "POST",
        url: "../handler/FlightHandler.asmx/GetFareRule",
        data: '{"ResultIndex":"' + ResultIndex + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var arrRules = result.arrRules;
                $("#AlertModal .modal-dialog").css("width", "80%")
                Success(arrRules[0].FareRuleDetail);
            }
            else if (result.retCode == 0) {
                $('#AgencyBookingCancelModal').modal('hide')
                $('#SpnMessege').text(result.Message);
                $('#ModelMessege').modal('show')
            }
        },
        error: function () {
            $('#AgencyBookingCancelModal').modal('hide')
            $('#SpnMessege').text("something went wrong");
            $('#ModelMessege').modal('show')
        },
        complete: function () {
            $("#dlgLoader").css("display", "none");
        }
    });
}

function OrderByFilter(Type) {
    var html = '';
    html += '<div class="sort-by-section clearfix box">'
    html += '<ul class="sort-bar clearfix block-sm">'
    html += '<li class="sort-by-price"><a class="sort-by-container" href="#"onclick="SortBy(\'' + Type + '\',4,this)" title="Sort By Price"><span>Price </span></a></li>'
    html += '<label id="lblCategory" style="display: none">0</label>'
    html += '<li class="sort-by-Depart"><a class="sort-by-container" href="#" onclick="SortBy(\'' + Type + '\',1,this)" title="Sort By Departure Time"><span>Depart </span></a></li>'
    html += '<input type="hidden" id="txt_FilterBy' + Type + '" value="0">'
    html += '<input type="hidden" id="txt_Orderby' + Type + '" value="0">'
    html += '<li class="sort-by-Arrival"><a class="sort-by-container" href="#" onclick="SortBy(\'' + Type + '\',2,this)" title="Sort By Arrival Time"><span>Arrival </span></a></li>'
    html += '<li class="sort-by-Time"><a class="sort-by-container" href="#" onclick="SortBy(\'' + Type + '\',3,this)" title="Sort By Duration"><span>Time</span></a></li>'
    html += '</ul>'
    html += '<div class="col-md-2" style="margin-top: 15px;">'
    html += '<button type="button" style="background-color: #f5f5f5; color: #9e9e9e" class="full-width" id="btn_Show" onclick="Show();">Modify Search</button>'
    html += '<button type="button" style="background-color: #f5f5f5; color: #9e9e9e; display: none" class="full-width" id="btn_hide" onclick="Hide();">Modify Search</button>'
    html += '</div>'
    html += '</div>'
    return html;
}

function OrderByFilterRound(Type) {
    var html = '';

    html += '<div class="sort-by-section clearfix box" style="padding: 1px 8px;">'
    html += '<ul class="sort-bar clearfix block-sm">'
    html += '<li class="sort-by-price" style="padding: 15px 2px;"><a class="sort-by-container" href="#"onclick="SortBy(\'' + Type + '\',4,this)" title="Sort By Price"><span>Price </span></a></li>'
    html += '<label id="lblCategory" style="display: none">0</label>'
    html += '<li class="sort-by-Depart"><a class="sort-by-container" href="#" onclick="SortBy(\'' + Type + '\',1,this)" title="Sort By Departure Time"><span>Depart </span></a></li>'
    html += '<input type="hidden" id="txt_FilterBy' + Type + '" value="0">'
    html += '<li class="sort-by-Arrival"><a class="sort-by-container" href="#" onclick="SortBy(\'' + Type + '\',2,this)" title="Sort By Arrival Time"><span>Arrival </span></a></li>'
    html += '<li class="sort-by-Time"><a class="sort-by-container" href="#" onclick="SortBy(\'' + Type + '\',3,this)" title="Sort By Duration"><span>Time</span></a></li>'
    html += '</ul>'
    if (Type == "Out") {
        //html += '<div class="col-md-2" style="margin-top: 15px;">'
        //html += '<button type="button" style="background-color: #f5f5f5; color: #9e9e9e" class="full-width" id="btn_Show" onclick="Show();">Modify Search</button>'
        //html += '<button type="button" style="background-color: #f5f5f5; color: #9e9e9e; display: none" class="full-width" id="btn_hide" onclick="Hide();">Modify Search</button>'
        //html += '</div>'
        $("#btn_Modify").show();
    }
    html += '</div>'
    return html;
}



var ResultIndex = new Array();
function GetFareQuote() {
    $.ajax({
        type: "POST",
        url: "../handler/FlightHandler.asmx/GetFarQuote",
        data: JSON.stringify({ ReultIndex: ResultIndex }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Booking(ResultIndex)
            }
            else if (result.retCode == 0) {
                $("#exampleModal2").modal("hide");
                if (result.errormsg != "Your Fare not Found.Please select another Fare.")
                    Ok(result.errormsg, "Booking", [ResultIndex])
                else
                    Success(result.errormsg);
            }
        },
        error: function () {
            $('#AgencyBookingCancelModal').modal('hide')
            $('#SpnMessege').text("something went wrong");
            $('#ModelMessege').modal('show')
            // alert("something went wrong");
        },
        complete: function () {
            $("#dlgLoader").css("display", "none");
        }
    });
}

function Booking(FlightID) {
    window.location.href = "FlightBooking.aspx?FlightID=" + FlightID;
}

var DepartID, ArrivalID;
function GetSelection() {
    $(".Arival").click(function () {
        ArrivalID = $(this)[0].id;
        GetDomesticPrice()
        $(".Arival").removeClass("unselected");
        $(".Arival").removeClass("selected");
        $(this).addClass("selected").siblings().addClass("unselected");
    });
    $(".Departure").click(function () {
        DepartID = $(this)[0].id;
        GetDomesticPrice()
        $(".Departure").removeClass("unselected");
        $(".Departure").removeClass("selected");
        $(this).addClass("selected").siblings().addClass("unselected");
    });
}






function GetDomesticPrice() {
    ResultIndex = [];
    if (DepartID != undefined && ArrivalID != undefined) {
        $("#Div_domestic").empty();
        $("#exampleModal2").modal("show");
        var html = '';
        //html += ' <span class="dark">2 Tickets: Roundtrip</span>'
        html += '<div class="row">'
        var objDSegment = $.grep(arrSearch.FlightList[0], function (p) { return p.ResultIndex == DepartID })
                .map(function (p) { return p });
        var objASegment = $.grep(arrSearch.FlightList[1], function (p) { return p.ResultIndex == ArrivalID })
               .map(function (p) { return p });
        var CityDeaprt = objDSegment[0].Segment[0][0].DepartureFrom.CityName + ' ' + objDSegment[0].Segment[0][0].DepartureFrom.CountryName;
        var CityRDpart = (objASegment[0].Segment[0][0].DepartureFrom.CityName + ' ' + objASegment[0].Segment[0][0].DepartureFrom.CountryName)
        html += GetDomsticReturn(CityDeaprt,
                        CityRDpart,
                        moment(objDSegment[0].Segment[0][0].DepartureAt).format("LT"),
                        moment(objDSegment[0].Segment[0][0].ArrivalTime).format("LT"), objDSegment[0].Segment[0][0].Duration, objDSegment[0].Fare, objDSegment[0].Logo) /*One Way Ticket*/
        html += GetDomsticReturn(CityRDpart,
                        CityDeaprt,
                        moment(objASegment[0].Segment[0][0].DepartureAt).format("LT"),
                        moment(objASegment[0].Segment[0][0].ArrivalTime).format("LT"), objASegment[0].Segment[0][0].Duration, objASegment[0].Fare, objASegment[0].Logo)/*Return Way Ticket*/
        //$("#spn_Return").text(CityDeaprt + " ⇋ " + CityRDpart)
        html += '<div class="col-sm-2">'
        html += '<div class="row">'
        html += '    <div class="col-sm-12">'
        html += '        <span style="color: green; font-size: 18px">RM ' + (parseFloat(objDSegment[0].Fare.replace(",", "")) + parseFloat(objASegment[0].Fare.replace(",", ""))) + '</span>'
        html += '    </div>'
        html += '</div>'
        html += '<div class="row">'
        html += '    <div class="col-sm-12">'
        html += '        <br />'
        html += '        <input type="button" onclick ="GetFareQuote()" class="button btn-small sky-blue1" value="Book" />'
        html += '    </div>'
        html += '</div>'
        html += '</div>'
        html += '</div>'
        ResultIndex.push(DepartID)
        ResultIndex.push(ArrivalID)
        $("#Div_domestic").append(html)
    }
}

function GetDomsticReturn(Origin, Destination, DepartTime, Arrival, Duration, Fare, Logo) {
    var html = '';
    html += ' <div class="col-sm-5" style="border-right: 1px dashed #c2c2c2;">                            '
    html += '                            <div class="row">                                                '
    html += '                                <div class="col-sm-8">                                      '
    html += '                                    <h4 class="title">' + Origin + ' To ' + Destination + '</h4>   '
    html += '                                </div>                                                       '
    html += '                                <div class="col-sm-4">                                      '
    html += '<span style="color: green; font-size: 18px">RM ' + Fare + '</span>'
    //html += '                                    <h4 class="title"><i class="fa fa-inr"></i> ' + Fare + '</h4>   '
    html += '                                </div>   '
    html += '                            </div>                                                           '
    html += '                            <div class="row">                                                '
    html += '                                <div class="col-sm-3">                                       '
    html += LogoForModal(Logo)
    //html += '                                    <img src="images/AirlineLogo/' + Logo + '.png" style="height:51px;width:65px" />                  '
    html += '                                </div>                                                       '
    html += '                                <div class="col-sm-3">                                       '
    html += '                                    <h5 class="title">' + DepartTime + ' <small>Departure</small></h4>  '
    html += '                                </div>                                                       '
    html += '                                <div class="col-sm-3">                                       '
    html += '                                    <h5 class="title">' + Duration + ' Min <small>Duration</small></h4>    '
    html += '                                </div>                                                       '
    html += '                                <div class="col-sm-3">                                       '
    html += '                                    <h5 class="title">' + Arrival + ' <small>Arival</small></h4>    '
    html += '                                </div>                                                       '
    html += '                            </div>                                                           '
    html += '                        </div>'
    //$("#Div_domestic").append(html)
    return html;
}

function BookTicket(ID) {
    ResultIndex = [];
    ResultIndex.push(ID)
    GetFareQuote()
}

function SortBy(Type, By, FilterBy) {
    //$(".Filter" + 1 + Type).removeClass("fblueline");
    //$(".Filter" + 2 + Type).removeClass("fblueline");
    //$(".Filter" + 3 + Type).removeClass("fblueline");
    //$(".Filter" + 4 + Type).removeClass("fblueline");
    $("#txt_FilterBy" + Type).val(By);
    GenrateFilters(Type);
    //$(".Filter" + By + Type).addClass("fblueline")
}

var arrFares = []
function GetPriceRBD(ResultIndex) {
    var Data = {
        ResultIndex: ResultIndex,
        FareClass: arrFares
    }
    $.ajax({
        type: "POST",
        url: "../handler/FlightHandler.asmx/GetSegments",
        data: JSON.stringify(Data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                $("#spn_Total" + ResultIndex).append(result.sTotal)
                $('[data-toggle="tooltip"]').tooltip();
            }
            else if (result.retCode == 0) {
                $('#AgencyBookingCancelModal').modal('hide')
                $('#SpnMessege').text(result.errorMsg);
                $('#ModelMessege').modal('show')
            }
        },
        error: function () {
            $('#AgencyBookingCancelModal').modal('hide')
            $('#SpnMessege').text("something went wrong");
            $('#ModelMessege').modal('show')
        },
        complete: function () {
            $("#dlgLoader").css("display", "none");
        }
    });
}

var ListFares = new Array();
function SetClass(ResultIndex, arrFare, TotalSegment, selected, ClassItem) {
    arrFares[selected] = arrFare;
    $(".Fare_" + ResultIndex + selected).removeClass("red")
    $(ClassItem).addClass("red")
    $("#spn_Total" + ResultIndex).empty();
    if (parseInt(TotalSegment) == arrFares.length) {
        $("#btn_" + ResultIndex).text("Book");
        document.getElementById("btn_" + ResultIndex).setAttribute("onclick", "BookTicket(\"" + ResultIndex + "\")")
        $("#btn_" + ResultIndex).show()
        GetPriceRBD(ResultIndex)
        //$("#btn_" + ResultIndex).text("Get Fare");
        //document.getElementById("btn_" + ResultIndex).setAttribute("onclick", "GetPriceRBD(\"" + ResultIndex + "\")")
    }
}

function Show() {
    $("#SearchDive").show();
    $("#btn_hide").show();
    $("#btn_Show").hide();

}

function Hide() {
    $("#SearchDive").hide();
    $("#btn_hide").hide();
    $("#btn_Show").show();
}

function ShowR() {
    $("#SearchDive").show();
    $("#btn_hider").show();
    $("#btn_Showr").hide();

}

function HideR() {
    $("#SearchDive").hide();
    $("#btn_hider").hide();
    $("#btn_Showr").show();
}

var arrCity = new Array();
function GetCityForModify() {
    var sndcountry = "";
    $.ajax({
        type: "POST",
        url: "../Handler/FlightHandler.asmx/GetselCityList",
        data: "{'sndcountry':'" + sndcountry + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var ddlRequest = "";
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCity = result.Arr;
            }
            BindSearch();
        },
        error: function () {
        }
    });
}

//function SetClass(ResultIndex, arrFare, TotalSegment, selected, ClassItem) {
//    arrFares[selected] = arrFare;
//    $(".Fare_" + ResultIndex + selected).removeClass("red")
//    $(ClassItem).addClass("red")
//    if (parseInt(TotalSegment) == arrFares.length) {
//        $("#btn_" + ResultIndex).show()
//    }
//}

