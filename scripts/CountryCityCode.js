﻿$(document).ready(function () {
    //$('#selCountry').change(function () {
    //    //var sndcountry = $('#selCountry').val();
        
    //});
    //GetCity("GB");

    GetCity("India");

});
var arrCountry = new Array();
var arrCity = new Array();
var arrCityCode = new Array();
function GetCountry() {
    $.ajax({
        type: "POST",
        url: "../DefaultHandler.asmx/GetCountry",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCountry = result.Country;
                if (arrCountry.length > 0) {
                    $("#selCountry").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any Country</option>';
                    for (i = 0; i < arrCountry.length; i++) {
                        ddlRequest += '<option value="' + arrCountry[i].Country + '">' + arrCountry[i].Countryname + '</option>';
                    }
                    $("#selCountry").append(ddlRequest);
                }
            }
        },
        error: function () {
        }
    });
}

function GetCity(reccountry) {
    $.ajax({
        type: "POST",
        url: "DefaultHandler.asmx/GetCity",
        data: '{"country":"' + reccountry + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCity = result.City;
                if (arrCity.length > 0) {
                    $("#selCity").empty();
                    $("#sel_City").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any City</option>';
                    for (i = 0; i < arrCity.length; i++) {
                        ddlRequest += '<option value="' + arrCity[i].Code + '">' + arrCity[i].Description + '</option>';
                    }
                    $("#selCity").append(ddlRequest);
                    
                    $("#sel_City").append(ddlRequest);
                }
            }
            if (result.retCode == 0) {
                $("#selCity").empty();
                $("#sel_City").empty();
            }
        },
        error: function () {
        }
    });
}