﻿$(document).ready(function () {
    $('#Select_Rooms').change(function () {
        if ($("#Select_Rooms option:selected").val() == 1) {
            $('#spn_RoomType2').css("display", "none");
            $('#spn_RoomType3').css("display", "none");
            $('#spn_RoomType4').css("display", "none");
            $('#Select_Adults2').css("display", "none");
            $('#Select_Adults3').css("display", "none");
            $('#Select_Adults4').css("display", "none");
            $('#Select_Children2').css("display", "none");
            $('#Select_Children3').css("display", "none");
            $('#Select_Children4').css("display", "none");
            $('#Select_AgeChildFirst2').css("display", "none");
            $('#Select_AgeChildFirst3').css("display", "none");
            $('#Select_AgeChildFirst4').css("display", "none");
            $('#Select_AgeChildSecond2').css("display", "none");
            $('#Select_AgeChildSecond3').css("display", "none");
            $('#Select_AgeChildSecond4').css("display", "none");

            $("#Span_RoomType2").css("display", "none");
            $("#Span_RoomType3").css("display", "none");
            $("#Span_RoomType4").css("display", "none");

            $("#Span_Adults2").css("display", "none");
            $("#Span_Adults3").css("display", "none");
            $("#Span_Adults4").css("display", "none");

            $("#Span_Children2").css("display", "none");
            $("#Span_Children3").css("display", "none");
            $("#Span_Children4").css("display", "none");

            $('#Select_AgeChildFirst2').prop('selectedIndex', 0);
            $('#Select_AgeChildFirst3').prop('selectedIndex', 0);
            $('#Select_AgeChildFirst4').prop('selectedIndex', 0);
            $('#Select_AgeChildSecond2').prop('selectedIndex', 0);
            $('#Select_AgeChildSecond3').prop('selectedIndex', 0);
            $('#Select_AgeChildSecond4').prop('selectedIndex', 0);

            $('#Select_Adults2').prop('selectedIndex', 1);
            $('#Select_Adults3').prop('selectedIndex', 1);
            $('#Select_Adults4').prop('selectedIndex', 1);
            $('#Select_Children2').prop('selectedIndex', 0);
            $('#Select_Children3').prop('selectedIndex', 0);
            $('#Select_Children4').prop('selectedIndex', 0);
            $("#Span_AgeChildFirst2").css("display", "none");
            $("#Span_AgeChildFirst3").css("display", "none");
            $("#Span_AgeChildFirst4").css("display", "none");
            $("#Span_AgeChildSecond2").css("display", "none");
            $("#Span_AgeChildSecond3").css("display", "none");
            $("#Span_AgeChildSecond4").css("display", "none");
            $('#trroom2').css("display", "none");
            $('#trroom3').css("display", "none");
            $('#trroom4').css("display", "none");
        }
        if ($("#Select_Rooms option:selected").val() == 2) {
            $('#spn_RoomType2').css("display", "");
            $('#spn_RoomType3').css("display", "none");
            $('#spn_RoomType4').css("display", "none");
            $('#Select_Adults2').css("display", "");
            $('#Select_Adults3').css("display", "none");
            $('#Select_Adults4').css("display", "none");
            $('#Select_Children2').css("display", "");
            $('#Select_Children3').css("display", "none");
            $('#Select_Children4').css("display", "none");
            $('#Select_AgeChildFirst3').css("display", "none");
            $('#Select_AgeChildFirst4').css("display", "none");
            $('#Select_AgeChildSecond3').css("display", "none");
            $('#Select_AgeChildSecond4').css("display", "none");

            $("#Span_RoomType2").css("display", "");
            $("#Span_RoomType3").css("display", "none");
            $("#Span_RoomType4").css("display", "none");

            $("#Span_Adults2").css("display", "");
            $("#Span_Adults3").css("display", "none");
            $("#Span_Adults4").css("display", "none");

            $("#Span_Children2").css("display", "");
            $("#Span_Children3").css("display", "none");
            $("#Span_Children4").css("display", "none");

            $('#Select_AgeChildFirst3').prop('selectedIndex', 0);
            $('#Select_AgeChildFirst4').prop('selectedIndex', 0);
            $('#Select_AgeChildSecond3').prop('selectedIndex', 0);
            $('#Select_AgeChildSecond4').prop('selectedIndex', 0);

            $('#Select_Adults3').prop('selectedIndex', 1);
            $('#Select_Adults4').prop('selectedIndex', 1);
            $('#Select_Children3').prop('selectedIndex', 0);
            $('#Select_Children4').prop('selectedIndex', 0);
            $("#Span_AgeChildFirst3").css("display", "none");
            $("#Span_AgeChildFirst4").css("display", "none");
            $("#Span_AgeChildSecond3").css("display", "none");
            $("#Span_AgeChildSecond4").css("display", "none");
            $('#trroom2').css("display", "");
            $('#trroom3').css("display", "none");
            $('#trroom4').css("display", "none");
        }
        if ($("#Select_Rooms option:selected").val() == 3) {
            $('#spn_RoomType2').css("display", "");
            $('#spn_RoomType3').css("display", "");
            $('#spn_RoomType4').css("display", "none");
            $('#Select_Adults2').css("display", "");
            $('#Select_Adults3').css("display", "");
            $('#Select_Adults4').css("display", "none");
            $('#Select_Children2').css("display", "");
            $('#Select_Children3').css("display", "");
            $('#Select_Children4').css("display", "none");
            $('#Select_AgeChildFirst4').css("display", "none");
            $('#Select_AgeChildSecond4').css("display", "none");

            $("#Span_RoomType2").css("display", "");
            $("#Span_RoomType3").css("display", "");
            $("#Span_RoomType4").css("display", "none");

            $("#Span_Adults2").css("display", "");
            $("#Span_Adults3").css("display", "");
            $("#Span_Adults4").css("display", "none");

            $("#Span_Children2").css("display", "");
            $("#Span_Children3").css("display", "");
            $("#Span_Children4").css("display", "none");

            $('#Select_AgeChildFirst4').prop('selectedIndex', 0);
            $('#Select_AgeChildSecond4').prop('selectedIndex', 0);

            $('#Select_Adults4').prop('selectedIndex', 1);
            $('#Select_Children4').prop('selectedIndex', 0);
            $("#Span_AgeChildFirst4").css("display", "none");
            $("#Span_AgeChildSecond4").css("display", "none");
            $('#trroom2').css("display", "");
            $('#trroom3').css("display", "");
            $('#trroom4').css("display", "none");
        }
        if ($("#Select_Rooms option:selected").val() == 4) {
            $('#spn_RoomType2').css("display", "");
            $('#spn_RoomType3').css("display", "");
            $('#spn_RoomType4').css("display", "");
            $("#Span_RoomType2").css("display", "");
            $("#Span_RoomType3").css("display", "");
            $("#Span_RoomType4").css("display", "");
            $("#Span_Children2").css("display", "");
            $("#Span_Children3").css("display", "");
            $("#Span_Children4").css("display", "");
            $("#Span_Adults2").css("display", "");
            $("#Span_Adults3").css("display", "");
            $("#Span_Adults4").css("display", "");
            $('#Select_Adults2').css("display", "");
            $('#Select_Adults3').css("display", "");
            $('#Select_Adults4').css("display", "");
            $('#Select_Children2').css("display", "");
            $('#Select_Children3').css("display", "");
            $('#Select_Children4').css("display", "");
            $('#trroom2').css("display", "");
            $('#trroom3').css("display", "");
            $('#trroom4').css("display", "");
        }
    });
    $('#Select_Children1').change(function () {
        if ($("#Select_Children1 option:selected").val() == 0) {
            $('#Select_AgeChildFirst1').prop('selectedIndex', 0);
            $('#Span_AgeChildFirst1').prop('selectedIndex', 0);
            $('#Select_AgeChildSecond1').prop('selectedIndex', 0);
            $('#Span_AgeChildSecond1').prop('selectedIndex', 0);
            $("#Select_AgeChildFirst1").css("display", "none");
            $("#Span_AgeChildFirst1").css("display", "none");
            $("#Select_AgeChildSecond1").css("display", "none");
            $("#Span_AgeChildSecond1").css("display", "none");
        }
        if ($("#Select_Children1 option:selected").val() == 1) {
            $('#Select_AgeChildSecond1').prop('selectedIndex', 0);
            $('#Span_AgeChildSecond1').prop('selectedIndex', 0);
            $("#Select_AgeChildFirst1").css("display", "");
            $("#Span_AgeChildFirst1").css("display", "");
            $("#Select_AgeChildSecond1").css("display", "none");
            $("#Span_AgeChildSecond1").css("display", "none");
        }
        if ($("#Select_Children1 option:selected").val() == 2) {
            $("#Select_AgeChildFirst1").css("display", "");
            $("#Span_AgeChildFirst1").css("display", "");
            $("#Select_AgeChildSecond1").css("display", "");
            $("#Span_AgeChildSecond1").css("display", "");
        }
    });
    $('#Select_Children2').change(function () {
        if ($("#Select_Children2 option:selected").val() == 0) {
            $('#Select_AgeChildFirst2').prop('selectedIndex', 0);
            $('#Span_AgeChildFirst2').prop('selectedIndex', 0);
            $('#Select_AgeChildSecond2').prop('selectedIndex', 0);
            $('#Span_AgeChildSecond2').prop('selectedIndex', 0);
            $("#Select_AgeChildFirst2").css("display", "none");
            $("#Span_AgeChildFirst2").css("display", "none");
            $("#Select_AgeChildSecond2").css("display", "none");
            $("#Span_AgeChildSecond2").css("display", "none");
        }
        if ($("#Select_Children2 option:selected").val() == 1) {
            $('#Select_AgeChildSecond2').prop('selectedIndex', 0);
            $('#Span_AgeChildSecond2').prop('selectedIndex', 0);
            $("#Select_AgeChildFirst2").css("display", "");
            $("#Span_AgeChildFirst2").css("display", "");
            $("#Select_AgeChildSecond2").css("display", "none");
            $("#Span_AgeChildSecond2").css("display", "none");
        }
        if ($("#Select_Children2 option:selected").val() == 2) {
            $("#Select_AgeChildFirst2").css("display", "");
            $("#Span_AgeChildFirst2").css("display", "");
            $("#Select_AgeChildSecond2").css("display", "");
            $("#Span_AgeChildSecond2").css("display", "");
        }
    });
    $('#Select_Children3').change(function () {
        if ($("#Select_Children3 option:selected").val() == 0) {
            $('#Select_AgeChildFirst3').prop('selectedIndex', 0);
            $('#Span_AgeChildFirst3').prop('selectedIndex', 0);
            $('#Select_AgeChildSecond3').prop('selectedIndex', 0);
            $('#Span_AgeChildSecond3').prop('selectedIndex', 0);
            $("#Select_AgeChildFirst3").css("display", "none");
            $("#Span_AgeChildFirst3").css("display", "none");
            $("#Select_AgeChildSecond3").css("display", "none");
            $("#Span_AgeChildSecond3").css("display", "none");
        }
        if ($("#Select_Children3 option:selected").val() == 1) {
            $('#Select_AgeChildSecond3').prop('selectedIndex', 0);
            $('#Span_AgeChildSecond3').prop('selectedIndex', 0);
            $("#Select_AgeChildFirst3").css("display", "");
            $("#Span_AgeChildFirst3").css("display", "");
            $("#Select_AgeChildSecond3").css("display", "none");
            $("#Span_AgeChildSecond3").css("display", "none");
        }
        if ($("#Select_Children3 option:selected").val() == 2) {
            $("#Select_AgeChildFirst3").css("display", "");
            $("#Span_AgeChildFirst3").css("display", "");
            $("#Select_AgeChildSecond3").css("display", "");
            $("#Span_AgeChildSecond3").css("display", "");
        }
        //if ($("#Select_Children3 option:selected").val() == 0) {
        //    $('#Select_AgeChildFirst3').prop('selectedIndex', 0);
        //    $('#Select_AgeChildSecond3').prop('selectedIndex', 0);
        //    $("#Select_AgeChildFirst3").css("display", "none");
        //    $("#Select_AgeChildSecond3").css("display", "none");
        //}
        //if ($("#Select_Children3 option:selected").val() == 1) {
        //    $('#Select_AgeChildSecond3').prop('selectedIndex', 0);
        //    $("#Select_AgeChildFirst3").css("display", "");
        //    $("#Select_AgeChildSecond3").css("display", "none");
        //}
        //if ($("#Select_Children3 option:selected").val() == 2) {
        //    $("#Select_AgeChildFirst3").css("display", "");
        //    $("#Select_AgeChildSecond3").css("display", "");
        //}
    });
    $('#Select_Children4').change(function () {
        if ($("#Select_Children4 option:selected").val() == 0) {
            $('#Select_AgeChildFirst4').prop('selectedIndex', 0);
            $('#Span_AgeChildFirst4').prop('selectedIndex', 0);
            $('#Select_AgeChildSecond4').prop('selectedIndex', 0);
            $('#Span_AgeChildSecond4').prop('selectedIndex', 0);
            $("#Select_AgeChildFirst4").css("display", "none");
            $("#Span_AgeChildFirst4").css("display", "none");
            $("#Select_AgeChildSecond4").css("display", "none");
            $("#Span_AgeChildSecond4").css("display", "none");
        }
        if ($("#Select_Children4 option:selected").val() == 1) {
            $('#Select_AgeChildSecond4').prop('selectedIndex', 0);
            $('#Span_AgeChildSecond4').prop('selectedIndex', 0);
            $("#Select_AgeChildFirst4").css("display", "");
            $("#Span_AgeChildFirst4").css("display", "");
            $("#Select_AgeChildSecond4").css("display", "none");
            $("#Span_AgeChildSecond4").css("display", "none");
        }
        if ($("#Select_Children4 option:selected").val() == 2) {
            $("#Select_AgeChildFirst4").css("display", "");
            $("#Span_AgeChildFirst4").css("display", "");
            $("#Select_AgeChildSecond4").css("display", "");
            $("#Span_AgeChildSecond4").css("display", "");
        }
        //if ($("#Select_Children4 option:selected").val() == 0) {
        //    $('#Select_AgeChildFirst4').prop('selectedIndex', 0);
        //    $('#Select_AgeChildSecond4').prop('selectedIndex', 0);
        //    $("#Select_AgeChildFirst4").css("display", "none");
        //    $("#Select_AgeChildSecond4").css("display", "none");
        //}
        //if ($("#Select_Children4 option:selected").val() == 1) {
        //    $('#Select_AgeChildSecond4').prop('selectedIndex', 0);
        //    $("#Select_AgeChildFirst4").css("display", "");
        //    $("#Select_AgeChildSecond4").css("display", "none");
        //}
        //if ($("#Select_Children4 option:selected").val() == 2) {
        //    $("#Select_AgeChildFirst4").css("display", "");
        //    $("#Select_AgeChildSecond4").css("display", "");
        //}
    });
    $('#atoggle').click(function () {
        debugger;
        if ($('#trhidden').css('display') == 'none') {
            $("#trhidden").css("display", "");
        }
        else
            $("#trhidden").css("display", "none");
    });
});

function Redirect() {
    var sLocation;
    var dCheckIn;
    var dCheckOut;
    var nTotalNights;
    var sStarRating;
    var sHotelName;
    var sNationality;
    var nNumberOfRooms;
    var sRoomType = new Array();
    var nAdults = new Array();
    var nChildrens = new Array();
    var nAgeOfChild1 = new Array();
    var nAgeOfChild2 = new Array();
    if ($('#txtCity').val() == "") {
        alert("Please tell us where do you want to go!");
        $('#txtCity').focus();
    }
    else {
        sLocation = $('#txtCity').val();
        nLocationId = $('#hdnDCode').val();
        dCheckIn = $('#datepicker_HotelCheckin').val();
        dCheckOut = $('#datepicker_HotelCheckout').val();
        nTotalNights = $('#Select_TotalNights').val();
        sStarRating = $('#Select_StarRating').val();
        sHotelName = $('#txt_HotelName').val();;
        sNationality = $('#Select_Nationality').val();
        nNumberOfRooms = $('#Select_Rooms').val();

        for (var i = 0; i < nNumberOfRooms; i++) {
            sRoomType[i] = $('#spn_RoomType' + (i + 1)).val();
            nAdults[i] = $('#Select_Adults' + (i + 1)).val();
            nChildrens[i] = $('#Select_Children' + (i + 1)).val();
            if (nChildrens[i] == 0) {
                nAgeOfChild1[i] = '0';
                nAgeOfChild2[i] = '0';
            }
            else if (nChildrens[i] == 1) {
                nAgeOfChild1[i] = $('#Select_AgeChildFirst' + (i + 1)).val();
                nAgeOfChild2[i] = '0';
            }
            else if (nChildrens[i] == 2) {
                nAgeOfChild1[i] = $('#Select_AgeChildFirst' + (i + 1)).val();
                nAgeOfChild2[i] = $('#Select_AgeChildSecond' + (i + 1)).val();
            }
        }

        var dataToPass = {
            sLocation: sLocation,
            nLocationId:nLocationId,
            CheckIn: dCheckIn,
            CheckOut: dCheckOut,
            nTotalNights: nTotalNights,
            sStarRating: sStarRating,
            sHotelName: sHotelName,
            sNationality: sNationality,
            nNoOfRooms: nNumberOfRooms,
            sRoomType: sRoomType,
            nAdults: nAdults,
            nChildrens: nChildrens,
            nAgeChild1: nAgeOfChild1,
            nAgeChild2: nAgeOfChild2
        };
        var jsonText = JSON.stringify(dataToPass);

        $.ajax({
            type: "POST",
            url: "../MGHHandler.asmx/Search",
            data: jsonText,
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    alert("success!")
                }
                if (result.retCode == 0) {
                    alert("Something went wrong! Please try again.")
                }
            },
            error: function () {
                alert("An error occured while adding log! Please try again");
            }
        });
    }
}

var arrNationalityCOR = new Array();
function GetNationality() {
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/GetNationalityCOR",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrNationalityCOR = result.NationalityMaster;
                if (arrNationalityCOR.length > 0) {
                    $("#Select_Nationality").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select A Nationality</option>';
                    for (i = 0; i < arrNationalityCOR.length; i++) {
                        ddlRequest += '<option value="' + arrNationalityCOR[i].Nationality + '">' + arrNationalityCOR[i].Country + '</option>';
                    }
                    $("#Select_Nationality").append(ddlRequest);
                }
            }
        },
        error: function () {
        }
    });
}

function GetHotelCategory() {
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/GetHotelCategory",
        data: "{'code':'ENG'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var arrHotelCategory = result;
            if (arrHotelCategory.length > 0) {
                $("#Select_StarRating").empty();
                var ddlRequest = '';
                for (i = 0; i < arrHotelCategory.length; i++) {
                    if (i == 0)
                        ddlRequest = '<option selected="selected" value="' + arrHotelCategory[i].id + '">' + arrHotelCategory[i].value + '</option>';
                    else
                        ddlRequest += '<option value="' + arrHotelCategory[i].id + '">' + arrHotelCategory[i].value + '</option>';
                }
                $("#Select_StarRating").append(ddlRequest);
            }
        },
        error: function () {
        }
    });
}


//function Redirect() {
//    //AddHotelSearchLog();
//    var destination = $('#hdnDCode').val();
//    var fdate = $('#datepicker_HotelCheckin').val();
//    var tdate = $('#datepicker_HotelCheckout').val();
//    var hotel = $('#hdnHCode').val();
//    var rating = $('#Select_StarRating').val();
//    var HName = $('#txt_HotelName').val();
//    var DName = $('#txtCity').val();
//    var room = $('#Select_Rooms').val();
//    $.ajax({
//        type: "POST",
//        url: "../MGHHandler.asmx/Search",
//        data: '{"destination":"' + destination + '","checkin":"' + fdate + '","checkout":"' + tdate + '"}',
//        contentType: "application/json; charset=utf-8",
//        datatype: "json",
//        success: function (response) {
//            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
//            var arrHotelCategory = result;
//            if (arrHotelCategory.length > 0) {
//                $("#Select_StarRating").empty();
//                var ddlRequest = '';
//                for (i = 0; i < arrHotelCategory.length; i++) {
//                    if (i == 0)
//                        ddlRequest = '<option selected="selected" value="' + arrHotelCategory[i].id + '">' + arrHotelCategory[i].value + '</option>';
//                    else
//                        ddlRequest += '<option value="' + arrHotelCategory[i].id + '">' + arrHotelCategory[i].value + '</option>';
//                }
//                $("#Select_StarRating").append(ddlRequest);
//            }
//        },
//        error: function () {
//        }
//    });
//}

//function Redirect() {
//    //AddHotelSearchLog();
//    var destination = $('#hdnDCode').val();
//    var fdate = $('#datepicker_HotelCheckin').val();
//    var tdate = $('#datepicker_HotelCheckout').val();
//    var hotel = $('#hdnHCode').val();
//    var rating = $('#Select_StarRating').val();
//    var HName = $('#txt_HotelName').val();
//    var DName = $('#txtCity').val();
//    var room = $('#Select_Rooms').val();
//    if (destination == "") {
//        alert("enter your city name!!");
//        return false;
//    }
//    else {
//        var roomcount = parseInt(room);
//        var occupancy = '';
//        for (var i = 0; i < roomcount; i++) {
//            if (i == 0) {
//                occupancy = Room1();
//            }
//            else if (i == 1) {
//                occupancy = occupancy + '$' + Room2();
//            }
//            else if (i == 2) {
//                occupancy = occupancy + '$' + Room3();
//            }
//            else if (i == 3) {
//                occupancy = occupancy + '$' + Room4();
//            }
//        }
//        $.ajax({
//            type: "POST",
//            url: "../MGHHandler.asmx/Search",
//            data: '{"rooms":"' + roomcount + '","destination":"' + destination + '","checkin":"' + fdate + '","checkout":"' + tdate + '","occupancy":"' + occupancy + '"}',
//            contentType: "application/json; charset=utf-8",
//            datatype: "json",
//            success: function (response) {
//                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
//                var arrHotelCategory = result;
//                if (arrHotelCategory.length > 0) {
//                    $("#Select_StarRating").empty();
//                    var ddlRequest = '';
//                    for (i = 0; i < arrHotelCategory.length; i++) {
//                        if (i == 0)
//                            ddlRequest = '<option selected="selected" value="' + arrHotelCategory[i].id + '">' + arrHotelCategory[i].value + '</option>';
//                        else
//                            ddlRequest += '<option value="' + arrHotelCategory[i].id + '">' + arrHotelCategory[i].value + '</option>';
//                    }
//                    $("#Select_StarRating").append(ddlRequest);
//                }
//            },
//            error: function () {
//            }
//        });
//        //var session = destination + '_' + DName + '_' + fdate + '_' + tdate + '_' + room + '_' + occupancy + '_' + hotel + '_' + HName + '_' + rating;
//        //window.location.href = "waitforresponse.aspx?session=" + session;
//    }
//}

//function Room1() {
//    var adult = $('#Select_Adults1').val();
//    var child = $('#Select_Children1').val();
//    var age = 0;
//    if (parseInt(child) > 0) {
//        age = $('#Select_AgeChildFirst1').val();
//        if (parseInt(child) == 2) {
//            age = age + '^' + $('#Select_AgeChildSecond1').val();
//        }
//    }
//    return adult + '|' + child + '^' + age;
//}
//function Room2() {
//    var adult = $('#Select_Adults2').val();
//    var child = $('#Select_Children2').val();
//    var age = 0;
//    if (parseInt(child) > 0) {
//        age = $('#Select_AgeChildFirst2').val();
//        if (parseInt(child) == 2) {
//            age = age + '^' + $('#Select_AgeChildSecond2').val();
//        }
//    }
//    return adult + '|' + child + '^' + age;
//}
//function Room3() {
//    var adult = $('#Select_Adults3').val();
//    var child = $('#Select_Children3').val();
//    var age = 0;
//    if (parseInt(child) > 0) {
//        age = $('#Select_AgeChildFirst3').val();
//        if (parseInt(child) == 2) {
//            age = age + '^' + $('#Select_AgeChildSecond3').val();
//        }
//    }
//    return adult + '|' + child + '^' + age;
//}
//function Room4() {
//    var adult = $('#Select_Adults4').val();
//    var child = $('#Select_Children4').val();
//    var age = 0;
//    if (parseInt(child) > 0) {
//        age = $('#Select_AgeChildFirst4').val();
//        if (parseInt(child) == 2) {
//            age = age + '^' + $('#Select_AgeChildSecond4').val();
//        }
//    }
//    return adult + '|' + child + '^' + age;
//}