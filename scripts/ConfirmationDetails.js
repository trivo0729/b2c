﻿
var resId;
var ResId;
var Uid;
var uId;
var Latitude;
var Longitude;
var status;
var Supplier;

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
$(document).ready(function () {
    resId = getParameterByName('ResID');
    //if (resId.indexOf('|') > -1) {
    //    ResId = resId.split('|');
    //    resId = ResId[0];
    //}
    //ResId = "";
    status = getParameterByName('Status');
    //$("#reservationId").text(resId);
    if (status == "Vouchered") {
        $("#spComplete").text("Your booking is compelete");
        $("#spAck").text("Thank you for booking with us");
    }
    else if (status == "Booking") {
        $("#spComplete").text("Your booking is holded successfully");
        $("#spAck").text("Please confirm it as soon as possible");
    }

    ConfirmationDetails();





});

var arrHotelReservation = new Array();
var arrUid = new Array();
function ConfirmationDetails() {
    debugger;
    var m = resId;
    var Data = { ReservationID: resId }
    $.ajax({
        type: "POST",
        url: "DefaultHandler.asmx/ConfirmationDetails",
        data: '{"ReservationID":"' + resId + '"}',
        //data:JSON.stringify(Data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrHotelReservation = result.ReservationDetails;
                BookingDetails();
                arrUid = arrHotelReservation[0].Uid.split("|");
                Uid = arrUid[0];
                if (resId.indexOf('|') > -1) {
                    Latitude = arrHotelReservation[0].LatitudeMGH;
                    Longitude = arrHotelReservation[0].LongitudeMGH;
                }
                else {
                    Latitude = arrHotelReservation[0].Latitude;
                    Longitude = arrHotelReservation[0].Longitude;
                }
                $("#reservationId").text(arrHotelReservation[0].InvoiceID);
                //Uid = arrHotelReservation[0].Uid;
                //Latitude = arrHotelReservation[0].Latitude;
                //Longitude = arrHotelReservation[0].Longitude;
                Supplier = arrHotelReservation[0].Source;
                
            }
        },
        error: function () {
            alert("An error occured while loading information")
        }
    });

}

function BookingDetails() {

    try {

        var html = '';
        html += '<dt>Booking number: </dt>'
        html += '<dd>' + arrHotelReservation[0].InvoiceID + '</dd>'
        html += '<dt>Passenger Name:</dt>'
        html += '<dd>' + arrHotelReservation[0].bookingname + '</dd>'
        html += '<dt>E-mail address:</dt>'
        html += '<dd>' + arrHotelReservation[0].AgentEmail + '</dd>'
        html += '<dt>Hotel Name:</dt>'
        html += '<dd>' + arrHotelReservation[0].HotelName + '</dd>'
        html += '<dt>Address:</dt>'
        html += '<dd>' + arrHotelReservation[0].City + '</dd>'
        html += '<dt>Check In:</dt>'
        html += '<dd>' + arrHotelReservation[0].CheckIn + '</dd>'
        html += '<dt>Check Out:</dt>'
        html += '<dd>' + arrHotelReservation[0].CheckOut + '</dd>'
        html += '<dt>Reservation Date:</dt>'
        html += '<dd>' + arrHotelReservation[0].ReservationDate + '</dd>'

        var Time = arrHotelReservation[0].ReservationTime.replace("-",":")

        html += '<dt>Reservation Time:</dt>'
        html += '<dd>' + Time + '</dd>'
        html += '<dt>Adults:</dt>'
        html += '<dd>' + arrHotelReservation[0].NoOfAdults + '</dd>'
        html += '<dt>Nights:</dt>'
        html += '<dd>' + arrHotelReservation[0].NoOfDays + '</dd>'
        html += '<dt>Total Fare:</dt>'
        html += '<dd><i class="fa fa-inr"></i> ' + arrHotelReservation[0].TotalFare + '</dd>'

        $("#bookingdetails").append(html);
    }

    catch (ex) {
        alert(ex.message)
    }


}

function ConfirmationInvoice() {

    GetPrintInvoice(resId, Uid, status, Supplier);
    //if (status == "Booking") {
    //    alert("You can not get Invoice.Please Confirm Your Booking!")
    //}

    //else {

    //    window.open('../Agent/Invoice.html?ReservationId=' + resId + '&Uid=' + Uid, 'GetInvoice', 'left=50000,top=50000,width=800,height=600');
    //}
}

function ConfirmationVoucher() {

    GetPrintVoucher(resId, Uid, status, Supplier)

    //if (status == "Booking") {
    //    alert("You can not get Voucher.Please Confirm Your Booking!")
    //}
    //else {
    //    window.open('../Agent/Voucher.html?ReservationId=' + resId + '&Uid=' + Uid + '&Latitude=' + Latitude + '&Longitutude=' + Longitude, 'GetVoucher', 'left=50000,top=50000,width=800,height=600');
    //}
}

function GetPrintVoucher(ReservationID, Uid, Status, Type) {
    debugger;
    //if (Status == 'Vouchered') {
    if (Status == 'CONFIRMED' || Status == 'Vouchered') {
        var win = window.open('ViewVoucher.aspx?ReservationId=' + ReservationID + '&Uid=' + Uid + '&Status=' + Status + '', '_blank');
        //window.open('../Agent/Voucher.html?ReservationId=' + ReservationID + '&Status=' + Status + '&Uid=' + Uid + '&Latitude=' + Latitude + '&Longitutude=' + Longitude + '&Type=' + Type, 'tester', 'left=50000,top=50000,width=800,height=600');
    }
    else if (Status == 'Booking') {

        $('#SpnMessege').text('Please Confirm Your Booking!')
        $('#ModelMessege').modal('show')
        // alert('Please Confirm Your Booking!')
    }
    else {
        $('#SpnMessege').text('You cannot get voucher for cancelled booking!')
        $('#ModelMessege').modal('show')
        //  alert('You cannot get voucher for cancelled booking!')
    }

}


