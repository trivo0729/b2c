﻿$(document).ready(function () {
    $('input[type=checkbox]').attr("disabled", true);
    $('#selCompanyType').change(function () {
        if ($('#selCompanyType').val() == 'Other') {
            $('#txtOtherCompanyType').css('display', '');
        }
        else
            $('#txtOtherCompanyType').css('display', 'none');
        if ($('#selCompanyType').val() == 'TourOperator' || $('#selCompanyType').val() == 'TravelAgency') {
            $('input[type=checkbox]').attr("disabled", false);
            $('input[type=checkbox]').attr("checked", false);
            //$('.chkMember').each(function () {
            //    this.enabled = true;
            //});
        }
        else {
            $('input[type=checkbox]').attr("disabled", true);
            $('input[type=checkbox]').attr("checked", false);
            $('#txtOtherMemberType').css('display', 'none');
            //$('.chkMember').each(function () {
            //    this.enabled = false;
            //});
        }
        $("#lbl_CompanyType").css("display", "none");
        $("#lbl_CompanyTypeText").css("display", "none");
    });
    $('#selReferType').change(function () {
        if ($('#selReferType').val() == 'Other') {
            $('#txtOtherReferType').css('display', '');
        }
        else
            $('#txtOtherReferType').css('display', 'none');
        $("#lbl_selReferType").css("display", "none");
        $("#lbl_selReferTypeText").css("display", "none");
    });
    $('#chkOtherMemberType').on('click', function () {
        if ($(this).is(':checked')) {
            $('#txtOtherMemberType').css('display', '');
            $("#lbl_OtherMemberType").css("display", "none");
        } else {
            $('#txtOtherMemberType').css('display', 'none');
            $("#lbl_OtherMemberType").css("display", "none");
        }
    });

    GetCountry();

    $('#Select_Country').change(function () {
        if ($('#Select_Country option:selected').val() == 'IN') {
            $('#txt_PAN').attr("disabled", false);
            $('#txt_ReferenceNumber').attr("disabled", false);
        }
        else {
            $('#txt_PAN').attr("disabled", true);
            $('#txt_ReferenceNumber').attr("disabled", true);
        }
    });
    $('#Select_IATA').change(function () {
        $("#lbl_IATA").css("display", "none");
        $('#txt_IATA').val('');
        if ($('#Select_IATA option:selected').val() == '1') {
            $('#txt_IATA').attr("disabled", false);
        }
        else {
            $('#txt_IATA').attr("disabled", true);
        }
    });
    //$('percent').keyup(function () {

    //    // initialize the sum (total price) to zero
    //    var sum = 0;

    //    // we use jQuery each() to loop through all the textbox with 'price' class
    //    // and compute the sum for each loop
    //    $('percent').each(function () {
    //        sum += Number($(this).val());
    //    });

    //    // set the computed value to 'totalPrice' textbox
    //    $('#lblPercent').val(sum);

    //});
});
var reg = new RegExp('[0-9]$');
var regPan = new RegExp('^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$');
var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
function Submit() {
    $('#FranchiseeRequestForm label').css("display", "none");
    $('#FranchiseeRequestForm label').html("* This field is required");
    var bValid = true;
    var CompanyName = $('#txtCompanyName').val();
    var CompanyType = $('#selCompanyType option:selected').val();
    var IATA;

    //..............
    var sAddress = $("#txt_Address").val();
    var bIATAStatus = $("#Select_IATA").val();
    var nIATA = $("#txt_IATA").val();
    var sCity = $("#Select_City").val();
    var sCountry = $("#Select_Country").val();
    var nPinCode = $("#txt_PinCode").val();
    var sEmail = $("#txt_Email").val();
    var nPhone = $("#txt_Phone").val();
    var nMobile = $("#txt_Mobile").val();
    var nFax = $("#txt_Fax").val();
    var sUsertype = $("#Select_Usertype").val();
    var sServiceTaxNumber = $("#txt_ReferenceNumber").val();
    var nPAN = $("#txt_PAN").val();
    var sWebsite = $("#txt_Website").val();
    var Currency = $("#SelCurrency").val();
    var sRemarks = $("#txt_Remarks").val();
    //..............

    if ($("#chkIATA").is(':checked') == true) {
        //IATA = $("#chkIATA").val();
        IATA = true;
    }
    else
        IATA = false;
    var TAAI;
    if ($("#chkTAAI").is(':checked') == true) {
        //TAAI = $("#chkTAAI").val();
        TAAI = true;
    }
    else
        TAAI = false;
    var TAFI;
    if ($("#chkTAFI").is(':checked') == true) {
        //TAFI = $("#chkTAFI").val();
        TAFI = true;
    }
    else
        TAFI = false;
    var Other;
    var OtherAssociation;
    if ($("#chkOtherMemberType").is(':checked') == true) {
        Other = true;
        OtherAssociation = $("#txtOtherMemberType").val();
    }
    else {
        Other = false;
        OtherAssociation = $("#txtOtherMemberType").val();
    }
    var YearlyTurnover = $("#txtYearlyTurnover").val();
    var DomesticTicketing = $("#txtDomesticTicketing").val();
    var InternationalTicketing = $("#txtInternationalTicketing").val();
    var DomesticTourPackages = $("#txtDomesticTourPackages").val();
    var InternationalTourPackages = $("#txtInternationalTourPackages").val();
    var HajjUmrahTourPackages = $("#txtHajjUmrahTourPackages").val();
    var OtherServices = $("#txtOtherServices").val();
    var firstname = $('#txtFirstName').val();
    var lastname = $('#txtLastName').val();
    var designation = $('#txtDesignation').val();
    var PublicityType = $('#selReferType option:selected').val();
    if (CompanyType == 'Other') {
        CompanyType = $('#txtOtherCompanyType').val();
    }
    if (PublicityType == 'Other') {
        PublicityType = $('#txtOtherReferType').val();
    }
    if (firstname == "") {
        bValid = false;
        $("#lbl_FirstName").css("display", "");
        //Success("Please enter your first name");
    }
    else if (firstname != "" && reg.test(firstname)) {
        bValid = false;
        $("#lbl_FirstName").html("* Designation must not be numeric at end.");
        $("#lbl_FirstName").css("display", "");
        //Success("Please enter your first name");
    }
    if (lastname == "") {
        bValid = false;
        $("#lbl_LastName").css("display", "");
        //Success("Please enter your last name");
    }
    else if (lastname != "" && reg.test(lastname)) {
        bValid = false;
        $("#lbl_LastName").html("* Designation must not be numeric at end.");
        $("#lbl_LastName").css("display", "");
        //Success("Please enter your first name");
    }
    if (designation == "") {
        bValid = false;
        $("#lbl_Designation").css("display", "");
        //Success("Please enter your designation");
    }
    else if (designation != "" && reg.test(designation)) {
        bValid = false;
        $("#lbl_Designation").html("* Designation must not be numeric at end.");
        $("#lbl_Designation").css("display", "");
        //Success("Please enter your first name");
    }
    if (CompanyName == "") {
        bValid = false;
        $("#lbl_CompanyName").css("display", "");
    }
    else if (CompanyName != "" && reg.test(CompanyName)) {
        bValid = false;
        $("#lbl_CompanyName").html("* Designation must not be numeric at end.");
        $("#lbl_CompanyName").css("display", "");
        //Success("Please enter your first name");
    }
    if (CompanyType == "-") {
        bValid = false;
        $("#lbl_CompanyType").css("display", "");
        $("#lbl_CompanyTypeText").css("display", "none");
    }
    else if ($('#selCompanyType option:selected').val() == "Other" && CompanyType == "") {
        bValid = false;
        $("#lbl_CompanyType").css("display", "none");
        $("#lbl_CompanyTypeText").css("display", "");
    }
    else if ($('#selCompanyType option:selected').val() == "Other" && CompanyType != "" && reg.test(CompanyType)) {
        bValid = false;
        $("#lbl_CompanyType").css("display", "none");
        $("#lbl_CompanyTypeText").html("* Designation must not be numeric at end.");
        $("#lbl_CompanyTypeText").css("display", "");
    }
    if ($("#chkOtherMemberType").is(':checked') == true && OtherAssociation == "") {
        bValid = false;
        $("#lbl_OtherMemberType").css("display", "");
    }
    else if ($("#chkOtherMemberType").is(':checked') == true && OtherAssociation != "" && reg.test(OtherAssociation)) {
        bValid = false;
        $("#lbl_OtherMemberType").html("* Designation must not be numeric at end.");
        $("#lbl_OtherMemberType").css("display", "");
    }
    if (YearlyTurnover == "") {
        bValid = false;
        $("#lbl_YearlyTurnover").css("display", "");
    }
    else if (YearlyTurnover != "" && !(reg.test(YearlyTurnover))) {
        bValid = false;
        $("#lbl_YearlyTurnover").html("*Numbers only");
        $("#lbl_YearlyTurnover").css("display", "");
    }
    if (PublicityType == "-") {
        bValid = false;
        $("#lbl_selReferType").css("display", "");
        $("#lbl_selReferTypeText").css("display", "none");
    }
    else if ($('#selReferType option:selected').val() == "Other" && PublicityType == "") {
        bValid = false;
        $("#lbl_selReferType").css("display", "none");
        $("#lbl_selReferTypeText").css("display", "");
    }
    else if ($('#selReferType option:selected').val() == "Other" && PublicityType != "" && reg.test(PublicityType)) {
        bValid = false;
        $("#lbl_selReferType").css("display", "none");
        $("#lbl_selReferTypeText").html("* Designation must not be numeric at end.");
        $("#lbl_selReferTypeText").css("display", "");
    }
    //...............
    if (sEmail == "") {
        bValid = false;
        $("#lbl_Email").css("display", "");
    }
    else {
        if (!(pattern.test(sEmail))) {
            bValid = false;
            $("#lbl_Email").html("* Wrong email format.");
            $("#lbl_Email").css("display", "");
        }
    }
    if (sAddress == "") {
        bValid = false;
        $("#lbl_Address").css("display", "");
    }
    if (sAddress != "" && reg.test(sAddress)) {
        bValid = false;
        $("#lbl_Address").html("* Address must not be numeric at end.");
        $("#lbl_Address").css("display", "");
    }
    if (bIATAStatus == "0") {
        bValid = false;
        $("#lbl_Select_IATA").css("display", "");
    }
    if (bIATAStatus == "1" && nIATA == "") {
        bValid = false;
        $("#lbl_IATA").css("display", "");
    }
    if (bIATAStatus == "1" && nIATA != "" && !reg.test(nIATA)) {
        bValid = false;
        $("#lbl_IATA").html("* IATA no. must be numeric and valid.");
        $("#lbl_IATA").css("display", "");
    }
    if (sCity == "-") {
        bValid = false;
        $("#lbl_City").css("display", "");
    }
    if (sCountry == "-") {
        bValid = false;
        $("#lbl_Country").css("display", "");
    }
    if (nMobile == "") {
        bValid = false;
        $("#lbl_Mobile").css("display", "");
    }
    else {
        if (!(reg.test(nMobile))) {
            bValid = false;
            $("#lbl_Mobile").html("* Mobile no. must be numeric.");
            $("#lbl_Mobile").css("display", "");
        }
    }
    if (nFax == "") {
        nFax = '0';
    }
    else {
        if (!(reg.test(nFax))) {
            bValid = false;
            $("#lbl_Fax").html("* Fax no. must be numeric.");
            $("#lbl_Fax").css("display", "");
        }
    }
    if (nPhone == "") {
        nPhone = '0';
    }
    else {
        if (!(reg.test(nPhone))) {
            bValid = false;
            $("#lbl_Phone").html("* Phone no must be numeric.");
            $("#lbl_Phone").css("display", "");
        }
    }
    if (nPinCode == "") {
        nPinCode = '0';
    }
    else {
        if (!(reg.test(nPinCode))) {
            bValid = false;
            $("#lbl_PinCode").html("* Pincode must be numeric.");
            $("#lbl_PinCode").css("display", "");
        }
    }
    if (nPAN == "") {
        nPAN = '0';
    }
    else {
        if (!(regPan.test(nPAN))) {
            bValid = false;
            $("#lbl_PAN").html("* PAN no must be correct.");
            $("#lbl_PAN").css("display", "");
        }
    }
    if (Currency == "0") {
        bValid = false;
        $("#lbl_Currency").css("display", "");
    }
    //...............
    if (bValid == true) {
        $.ajax({
            type: "POST",
            url: "RequestHandler.asmx/CheckForUniqueAgent",
            data: '{"sEmail":"' + sEmail + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.Session == 0) {
                    window.location.href = "Default.aspx";
                    //                    window.location.href = "../CUT/Default.aspx";
                    return false;
                }
                if (result.retCode == 1) {
                    Success("This email/username is already taken, Please choose another.")
                    //alert("This email/username is already taken, Please choose another.");
                    $("#txt_Email").css("border-color", "brown");
                    $("#txt_Email").focus();
                    //$("#txt_Mobile").css("border-color", "brown");
                    return false;
                }
                else if (result.retCode == 0) {
                    Success("Their is some error in processing your request, Please try again")
                    //alert("Their is some error in processing your request, Please try again");
                    return false;
                }
                else if (result.retCode == 2) {
                    $.ajax({
                        type: "POST",
                        url: "RequestHandler.asmx/FranchiseeRequest",
                        data: '{"CompanyName":"' + CompanyName + '","CompanyType":"' + CompanyType + '","IATA":"' + IATA + '","TAAI":"' + TAAI + '","TAFI":"' + TAFI + '","Other":"' + Other + '","OtherAssociation":"' + OtherAssociation + '","YearlyTurnover":"' + YearlyTurnover + '","DomesticTicketing":"' + DomesticTicketing + '","InternationalTicketing":"' + InternationalTicketing + '","DomesticTourPackages":"' + DomesticTourPackages + '","InternationalTourPackages":"' + InternationalTourPackages + '","HajjUmrahTourPackages":"' + HajjUmrahTourPackages + '","OtherServices":"' + OtherServices + '","firstname":"' + firstname + '","lastname":"' + lastname + '","designation":"' + designation + '","PublicityType":"' + PublicityType + '","sEmail":"' + sEmail + '","sAddress":"' + sAddress + '","sCity":"' + sCity + '","nPinCode":"' + nPinCode + '","nIATA":"' + nIATA + '","nPAN":"' + nPAN + '","nPhone":"' + nPhone + '","nMobile":"' + nMobile + '","nFax":"' + nFax + '","sServiceTaxNumber":"' + sServiceTaxNumber + '","sWebsite":"' + sWebsite + '","sRemarks":"' + sRemarks + '","Currency":"' + Currency + '"}',
                        contentType: "application/json; charset=utf-8",
                        datatype: "json",
                        success: function (response) {
                            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                            if (result.Session == 0) {
                                window.location.href = "Default.aspx";
                                //                                window.location.href = "../CUT/Default.aspx";
                                return false;
                            }
                            if (result.retCode == 1) {
                                Ok("Your Request submitted successfully.", "Reditect", null)
                                return false;
                            }
                            else if (result.retCode == 0) {
                                Success("Something went wrong! Please contact administrator.");
                            }
                        },
                        error: function () {
                            Success("An error occured during registration! Please contact administrator.");
                        }
                    });
                }
            },
            error: function () {
                Success("An error occured during registration! Please contact administrator.");
            }
        });
    }
}
function Reditect() {
    window.location.href = "Default.aspx";
}

function TotalPercentage() {
    //  $('#spnPercent').text('');
    var DomTick = $('#txtDomesticTicketing').val();
    var IntTick = $('#txtInternationalTicketing').val();
    var DomTour = $('#txtDomesticTourPackages').val();
    var IntTour = $('#txtInternationalTourPackages').val();
    var HUTour = $('#txtHajjUmrahTourPackages').val();
    var OtherService = $('#txtOtherServices').val();
    if (DomTick == '' || DomTick == null) {
        DomTick = 0;
    }
    if (IntTick == '' || IntTick == null) {
        IntTick = 0;
    }
    if (DomTour == '' || DomTour == null) {
        DomTour = 0;
    }
    if (IntTour == '' || IntTour == null) {
        IntTour = 0;
    }
    if (HUTour == '' || HUTour == null) {
        HUTour = 0;
    }
    if (OtherService == '' || OtherService == null) {
        OtherService = 0;
    }
    var total = Number(DomTick) + Number(IntTick) + Number(DomTour) + Number(IntTour) + Number(HUTour) + Number(OtherService);
    if (total > 100) {
        Success("Total activities percentage cannot exceed 100%");
        //$('#txtOtherServices').val('');
        $('#txtOtherServices').focus();
    }
    else if (reg.test(total)) {
        $('#spnPercent').text(total);
    }
    else
        Success('Entered text is not a number!');
    // this(parent).focus();
    if (!reg.test(DomTick)) {
        $('#txtDomesticTicketing').focus();
    }
    if (!reg.test(IntTick)) {
        $('#txtInternationalTicketing').focus();
    }
    if (!reg.test(DomTour)) {
        $('#txtDomesticTourPackages').focus();
    }
    if (!reg.test(IntTour)) {
        $('#txtInternationalTourPackages').focus();
    }
    if (!reg.test(HUTour)) {
        $('#txtHajjUmrahTourPackages').focus();
    }
    if (!reg.test(OtherService)) {
        $('#txtOtherServices').focus();
    }
    //$('#txtDomesticTicketing').val('');
    //$('#txtInternationalTicketing').val('');
    //$('#txtDomesticTourPackages').val('');
    //$('#txtInternationalTourPackages').val('');
    //$('#txtHajjUmrahTourPackages').val('');
    //$('#txtOtherServices').val('');
}