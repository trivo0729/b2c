﻿$(document).ready(function () {
    GetAllPackages();
});

function GetCategory(id) {
    if (id == 1) {
        return "Standard";
    }
    else if (id == 2) {
        return "Deluxe";
    }
    else if (id == 3) {
        return "Premium";
    }
    else if (id == 4) {
        return "Luxury";
    }
    else {
        return "No Category";
    }
}

function GetPackageType(id) {
    if (id == 1) {
        return "Holidays";
    }
    else if (id == 2) {
        return "Umrah";
    }
    else if (id == 3) {
        return "Hajj";
    }
    else if (id == 4) {
        return "Honeymoon";
    }
    else if (id == 5) {
        return "Summer";
    }
    else if (id == 6) {
        return "Adventure";
    }
    else if (id == 7) {
        return "Deluxe";
    }
    else if (id == 8) {
        return "Business";
    }
    else if (id == 9) {
        return "Premium";
    }
    else if (id == 10) {
        return "Wildlife";
    }
    else if (id == 11) {
        return "Weekend";

    }
    else if (id == 12) {
        return "New Year";
    }
    else {
        return "No Theme";
    }
}

function GetAllPackages() {
    debugger;
    $.ajax({
        url: "PackageHandler.asmx/GetAllPackages",
        type: "post",
        data: {},
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.Session == 0) {
                window.location.href = "index.htm";
            }
            if (result.retCode == 0) {
                alert("No packages found");
            }
            else if (result.retCode == 1) {
                debugger;
                List_Packages = result.Arr;
                var html = '';
                for (var i = 0; i < List_Packages.length; i++) {
                    var sCategory = List_Packages[i].sPackageCategory.split(',');
                    var sCategoryRow = '';
                    for (var sCate = 0; sCate < sCategory.length; sCate++) {
                        if (sCategory[sCate] != "") {
                            sCategoryRow += '<input type=button class="button btn-mini sky-blue2" value="' + GetCategory(sCategory[sCate]) + '" />&nbsp;';
                        }
                    }
                    var sTheme = List_Packages[i].sPackageThemes.split(',');
                    var sThemeRow = '';
                    for (var sThem = 0; sThem < sTheme.length; sThem++) {
                        if (sTheme[sThem] != "") {

                            sThemeRow += '<input type=button class="button btn-mini dull-blue" value="' + GetPackageType(sTheme[sThem]) + '"/>&nbsp;';
                        }
                    }
                    var Img = List_Packages[i].ImageArray.split("^_^")[0];
                    var PackageId = List_Packages[i].nID;
                    var City = List_Packages[i].sPackageDestination.split('|')

                    var Url = 'http://admin.Vacaaay.com/ImagesFolder/' + PackageId + '/' + Img

                    html += ' <article class="box">'
                    html += '    <figure class="col-sm-5 col-md-4">'
                    html += '        <a title="">'
                    html += '            <img width="270" height="180" alt="" src="' + Url + '"></a>'
                    html += '    </figure>'
                    html += '    <div class="details col-sm-7 col-md-8">'
                    html += '        <div>'
                    html += '            <div>'
                    html += '                <h4 class="box-title">Package Name: ' + List_Packages[i].sPackageName + '<small>Place : <i class="soap-icon-departure yellow-color"></i>' + City + '</small></h4>'
                    html += '            </div>'
                    html += '            <div>'
                    html += '<span class="price">'
                    GetPrice(List_Packages[i].nID);
                    html += '<small>Avg/Person</small><i class="fa fa-inr"></i><span id="' + List_Packages[i].nID + '"></span>'
                    html += '</span>'
                    html += '            </div>'
                    html += '        </div>'
                    html += '        <div>'
                    html += '<Span><b>Category :</b> ' + sCategoryRow + '  <b>Theme :</b> ' + sThemeRow + '<br/> <br/><b>Desciption:</b> ' + List_Packages[i].sPackageDescription + '</span>'
                    html += '            <div>'
                    html += '                <span>Total Duration <br/> ' + List_Packages[i].nDuration + ' Days</span>'
                    html += '                <a  type="button" class="button btn-small silver" title="" href="#" onclick="PackageImages(' + List_Packages[i].nID + ')">Details</a>'
                    html += '            </div>'
                    html += '        </div>'
                    html += '    </div>'
                    html += '</article>'
                    html += '<hr />'
                    html += '<br />'
                }
                $("#div_package").html(html);
            }
        },
        error: function () {
            alert('Error in getting package!');
        }
    });
}



function PackageImages(nID) {
    
    $(location).attr('href', '/PackageDetails.aspx?' + btoa('nID=' + nID));
}



function GetPrice(Id) {

    var data = { Id: Id };
    $.ajax({
        type: "POST",
        url: "PackageHandler.asmx/GetPrice",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var Arr = result.Arr;
                $("#" + Id + "").text(Arr.dSingleAdult)
            }
            else {
                alert("An error occured !!!");
            }
        },
        error: function () {
            alert("An error occured !!!");
        }
    });


}