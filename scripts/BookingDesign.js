﻿

function GenerateRooms() {

    try {
        $("#Rooms").empty();

        var html = '';
        html += '<h2>Who is traveling?</h2>'
        html += '<hr />'
        html += '<p>Leading guest should be min 18 year or older & should carry valid passport / photo ID.</p>'
        html += '<br />'
        var r = 1;
        for (var i = 0; i < RoomsDetails.length; i++) {
            html += '<div class="row">'
            html += '<div class="col-md-2" style="margin-top: 4%">'
            html += '<h3 style="color: #ff3300">Room ' + RoomsDetails[i].RoomNo + '</h3>'
            html += '</div>'
            html += '<div class="col-md-10" id="tblTravelers' + r + '">'
            for (var a = 0; a < RoomsDetails[i].AdultCout; a++) {

                html += '<div class="form-group row" name="adult_' + RoomsDetails[i].ROOMID + '_' + r + '">'
                html += '<div class="col-sm-3 col-md-3">'
                html += '<select name="Select_Gender" id="SelAGender_' + RoomsDetails[i].RoomNo + '_' + (a + 1) + '" class="form-control">'
                html += '<option value="Mr" selected="selected">Mr</option>'
                html += '<option value="Mrs">Mrs</option>'
                html += '<option value="Miss">Miss</option>'
                html += '</select>'
                html += '</div>'
                html += '<div class="col-sm-4 col-md-4">'
                html += '<input class="form-control txtName" type="text" placeholder="First Name" id="txtFName_' + RoomsDetails[i].RoomNo + '_' + (a + 1) + '" required="required" />'
                html += '</div>'
                html += '<div class="col-sm-4 col-md-4">'
                html += '<input class="form-control txtName" type="text" placeholder="Last Name" id="txtLName_' + RoomsDetails[i].RoomNo + '_' + (a + 1) + '" required="required" />'
                html += '</div>'
                if (a == 0) {
                    html += '<div class="col-sm-1 col-md-1">'
                    html += '<a class="skin-color" style="cursor: pointer;" onclick="CopyPassengerName(' + RoomsDetails[i].RoomNo + ',' + (a + 1) + ')">Copy</a>'
                    html += '</div>'
                }
                html += '</div>'

            }

            for (var c = 0; c < RoomsDetails[i].ChilldCount ; c++) {

                var ChildAges = new Array();

                ChildAges = RoomsDetails[i].ChildAges.split(",")

                html += '<div class="form-group row" name="child_' + RoomsDetails[i].ROOMID + '_' + r + '">'
                html += '<div class="col-sm-3 col-md-3">'
                html += '<select name="Select_Gender" id="SelCGender_' + RoomsDetails[i].RoomNo + '_' + (c + 1) + '" class="form-control">'
                html += '<option value="Master" selected="selected">Master</option>'
                html += '<option value="Miss">Miss</option>'
                html += '</select>'
                html += '</div>'
                html += '<div class="col-sm-4 col-md-4">'
                html += '<input class="form-control txtName" type="text" placeholder="Name" name="txtName" id="txtCName_' + RoomsDetails[i].RoomNo + '_' + (c + 1) + '" required="required" />'
                html += '</div>'
                html += '<div class="col-sm-4 col-md-4">'
                html += '<input class="form-control txtAge" type="text" placeholder="Age"  name="txtAge"  readonly="readonly" value="' + ChildAges[c] + '" id="txtCAge_' + RoomsDetails[i].RoomNo + '_' + (c + 1) + '" required="required" />'
                html += '</div>'
                html += '</div>'

            }
            html += '</div>'
            html += '</div>'

            //Start cancellation policy//
            html += '<div class="col-md-12">'
            html += '<div class="alert alert-general">'
            html += '<ul class="circle">'
            for (var Canc = 0; Canc < RoomsDetails[i].Cancelation.length; Canc++) {

                if (Canc != 0) {
                    var NoteDesign = RoomsDetails[i].Cancelation[Canc].toString().replace('<i class="fa fa-gbp"></i>', 'RM ')
                    html += '<li style="color:#c09853">' + NoteDesign + '</li>'
                }
                else {
                    html += '<li style="color:#c09853">' + RoomsDetails[i].Cancelation[Canc] + '</li>'
                }
            }
            html += '<span style="color:#c09853;float: right">Date and time is calculated based on Indian Standard time.</span><br/>'
            html += '</ul>'
            html += '</div>'
            html += '</div>'
            //End cancellation policy//
            html += '<br />'

            //Start Tarrif Note//
            if (RoomsDetails[i].TarrifNote != null && RoomsDetails[i].TarrifNote != "") {
                html += '<div class="col-md-12">'
                html += '<div class="alert alert-help">'
                html += '<ul>'
                html += '<li style="color:#838383">' + RoomsDetails[i].TarrifNote + '</li>'
                html += '</ul>'
                html += '</div>'
                html += '</div>'
                html += '<br />'
            }
            //End Tarrif Note//
            r++;
        }
        $("#Rooms").append(html);

    }

    catch (ex) {
        alert(ex.message)
    }

}

function GetCurrentHotel() {

    try {
        $("#SessionDetails").empty();
        var html = '';
        html += '<article class="image-box hotel listing-style1">'
        html += '<div class="travel-title">'
        html += '<h5 class="box-title" style="color: #ff3300">' + HotelDetails.HotelName + '</h5>'
        if (HotelDetails.Address == null) {

            html += '<p><small><b></b></small></p>'
        }
        else {
            html += '<p><small><b>' + HotelDetails.Address + '</b></small></p>'
        }
        html += '<hr>'
        html += '</div>'
        html += '<div class="details">'
        html += '<div class="constant-column-3 timing clearfix">'
        html += '<div class="check-in">'
        html += '<label>Check in</label>'
        html += '<span>' + HotelDetails.Checkin + '</span>'
        html += '</div>'
        html += '<div class="duration text-center">'
        html += '<i class="soap-icon-clock"></i>'
        html += '<span>' + HotelDetails.NoDays + ' Nights</span>'
        html += '</div>'
        html += '<div class="check-out">'
        html += '<label>Check out</label>'
        html += '<span>' + HotelDetails.CheckOut + '</span>'
        html += '</div>'
        html += '</div>'
        html += '</div>'
        html += '</article>'
        $("#SessionDetails").append(html);
    }

    catch (ex) {
        alert(ex.message)
    }

}



function TotalPayable() {
    try {
        $("#Total").empty();
        var html = '';
        html += '<div class="booking-details travelo-box">'
        html += '<div class="row">'
        html += '<div class="col-md-12">'
        html += '<div class="form-group row">'
        html += '<div class="col-sm-6 col-md-6">'
        html += '<h4 style="color:#333">Total Payable :</h4>'
        html += '</div>'
        html += '<div class="col-sm-6 col-md-6" style="text-align: end">'
        html += '<h4>'
        html += '<span style="color:#ff5a00" data-toggle="tooltip" data-placement="left" data-container="body" data-original-title="' + DisplayRequest[0].PayableTooltip + '">RM ' + DisplayRequest[0].TotalPayable + '</span>'
        html += '</h4>'
        html += '</div>'
        html += '</div>'
        html += '</div>'
        html += '</div>'
        html += '</div>'
        $("#Total").append(html);
        $('[data-toggle="tooltip"]').tooltip();
    }

    catch (ex) {
        alert(ex.message)
    }
}

function Confirmation() {

    try {

        var html = '';
        html += '<div class="booking-details travelo-box" style="border: 1px solid #cccccc;">'
        html += '<div class="row">'
        html += '<div class="col-md-12">'
        html += '<div class="form-group row">'
        html += '<div class="col-sm-6 col-md-6">'
        html += '<h5 class="box-title" style="color: #ff3300">' + HotelDetails.HotelName + '</h5>'
        if (HotelDetails.Address == null) {

            html += '<p><small><b></b></small></p>'
        }
        else {
            html += '<p><small><b>' + HotelDetails.Address + '</b></small></p>'
        }
        html += '</div>'
        html += '<div class="col-sm-3 col-md-3">'
        html += '<label>CheckIn :</label>'
        html += '<span> ' + HotelDetails.Checkin + '</span>'
        html += '<br/>'
        html += '<label>CheckOut :</label>'
        html += '<span> ' + HotelDetails.CheckOut + '</span>'
        html += '</div>'
        html += ' <div class="col-sm-3 col-md-3">'
        html += '<label>No of Nights :</label>'
        html += '<span> ' + HotelDetails.NoDays + '</span>'
        html += '<br/>'
        html += '<label>No of Rooms :</label>'
        html += '<span> ' + RoomsDetails.length + '</span>'
        html += '</div>'
        html += '</div>'
        html += '</div>'
        html += '</div>'
        html += '</div>'
        $(".ConfirmationDetails").append(html);
        ConfirmationPax();
    }

    catch (ex) {
        alert(ex.message)
    }

}



function GenerateRigthRooms() {

    try {
        $("#RigthRooms").empty();

        var html = '';
        var r = 0;

        for (var i = 0; i < RateBrackupDetails.length; i++) {
            html += '<div class="booking-details travelo-box">'
            html += '<div class="row">'
            html += '<div class="col-md-12">'
            html += '<div class="form-group row">'
            html += '<div class="col-sm-4 col-md-4">'
            html += '<h4 style="color: #ff3300">Room ' + (r + 1) + '</h4>'
            html += '</div>'
            html += '<div class="col-sm-8 col-md-8" style="text-align:end">'
            if (Occupancy[r].ChildCount != 0) {

                html += '<span>' + Occupancy[r].AdultCount + ' Adult, ' + Occupancy[r].ChildCount + ' Child , (' + Occupancy[r].ChildAges + 'yrs)</span>'
            }
            else {
                html += '<span>' + Occupancy[r].AdultCount + ' Adults</span>'
            }
            html += '</div>'
            html += '</div>'
            html += '</div>'
            html += '</div>'
            html += '<dl class="other-details">'
            html += '<dt class="feature" style="color: #2d3e52">' + RateBrackupDetails[i].RoomName + '</dt>'
            html += '<dd class="value">' + RateBrackupDetails[i].MealPlan + '</dd>'
            html += '</dl>'
            html += '<hr>'
            html += '<div class="row">'
            html += '<div class="col-md-12">'
            html += '<div class="form-group row">'
            html += '<div class="col-sm-8 col-md-8 toggle-container box" style="margin-bottom:0px">'
            html += '<div class="panel style1">'
            html += '<h6 class="panel-title">'
            html += '<a class="collapsed" href="#tgg' + (r + 1) + '" data-toggle="collapse" aria-expanded="false">Rate Breakup</a>'
            html += '</h6>'
            html += '<div class="panel-collapse collapse" id="tgg' + (r + 1) + '" aria-expanded="false" style="height: 0px;">'
            html += '<div class="panel-content">'
            for (var j = 0; j < RateBrackupDetails[i].NigtRate.length; j++) {

                html += '<div class="row">'
                html += '<div class="col-md-12">'
                html += '<div class="form-group row">'
                html += '<div class="col-sm-6 col-md-6" style="color: #337ab7">' + RateBrackupDetails[i].NigtRate[j].Date + '<br />'
                html += '</div>'
                html += '<div class="col-sm-6 col-md-6" style="color: #337ab7">'
                html += '<span style="color: #337ab7" data-toggle="tooltip" data-placement="left" data-container="body" data-original-title="' + RateBrackupDetails[i].NigtRate[j].NightRateTip + '">RM ' + RateBrackupDetails[i].NigtRate[j].NightRate + '</span><br/>'
                html += '</div>'
                html += '</div>'
                html += '</div>'
                html += '</div>'
            }
            html += '</div>'
            html += '</div>'
            html += '</div>'
            html += '</div>'
            html += '<div class="col-sm-4 col-md-4" style="text-align:end">'
            html += '<h5>'
            html += '<span><b>Room ' + (r + 1) + ' Total</b></span><br><br>'
            html += '<span data-toggle="tooltip" data-placement="left" data-container="body" data-original-title="' + RateBrackupDetails[i].CurencyTip + '">RM ' + RateBrackupDetails[i].Curency + '</span><br>'
            html += '</h5>'
            html += '</div>'
            html += '</div>'
            html += '</div>'
            html += '</div>'
            html += '</div>'

            r++;
        }
        $("#RigthRooms").append(html);
        $('[data-toggle="tooltip"]').tooltip();
    }

    catch (ex) {
        alert(ex.message)
    }
}

function ConfirmationPax() {

    try {
        //$("#RigthRooms").empty();

        var html = '';
        var r = 0;

        for (var i = 0; i < RateBrackupDetails.length; i++) {
            html += '<div class="booking-details travelo-box" style="border: 1px solid #cccccc;">'
            html += '<div class="row">'
            html += '<div class="col-md-12">'
            html += '<div class="form-group row">'
            html += '<div class="col-sm-7 col-md-7">'
            html += '<label style="color: #ff3300">Room ' + (r + 1) + ':</label>'
            if (Occupancy[r].ChildCount != 0) {

                html += '<span> ' + Occupancy[r].AdultCount + ' Adult, ' + Occupancy[r].ChildCount + ' Child , (' + Occupancy[r].ChildAges + 'yrs)</span>'
            }
            else {
                html += '<span> ' + Occupancy[r].AdultCount + ' Adults</span>'
            }
            html += '<br/>'
            html += '<span>'
            html += '<ul class="circle"  id="roomPax_' + (r + 1) + '">'
            html += '</ul>'
            html += '</span>'
            html += '</div>'
            html += '<div class="col-sm-5 col-md-5" style="background-color:floralwhite">'
            html += '<span>' + RateBrackupDetails[i].RoomName + '</span>'
            html += '<br/>'
            html += '<span>(' + RateBrackupDetails[i].MealPlan + ')</span>'
            html += '<br/>'
            html += '<label style="color: #ff3300"> <b>Room ' + (r + 1) + ':</b></label>'
            html += '<span style="color:#ff3300" data-toggle="tooltip" data-placement="left" data-container="body" data-original-title="' + RateBrackupDetails[i].CurencyTip + '">RM <b> ' + RateBrackupDetails[i].Curency + ' </b></span><br>'
            html += '</div>'
            html += '</div>'
            html += '<hr/>'
            //Start cancellation policy//
            html += '<div class="form-group row">'
            html += '<div class="col-sm-7 col-md-7" style="background-color: #fcf8e3">'
            html += '<ul class="circle">'
            for (var Canc = 0; Canc < RoomsDetails[i].Cancelation.length; Canc++) {

                html += '<li style="color:#c09853">' + RoomsDetails[i].Cancelation[Canc] + '</li>'
            }
            html += '</ul>'
            html += '</div>'
            //End cancellation policy//

            if (RoomsDetails[i].TarrifNote != null && RoomsDetails[i].TarrifNote != "") {
                html += '<div class="col-sm-5 col-md-5" style="background-color: #f2dede">'
                html += '<ul>'
                html += '<li style="color:#b94a48">' + RoomsDetails[i].TarrifNote + '</li>'
                html += '</ul>'
                html += '</div>'
            }
            html += '</div>'
            html += '</div>'
            html += '</div>'
            html += '</div>'
            r++;
        }
        $(".ConfirmationDetails").append(html);
        $('[data-toggle="tooltip"]').tooltip();
    }

    catch (ex) {
        alert(ex.message)
    }

}