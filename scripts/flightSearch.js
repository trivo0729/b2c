﻿
var Search = new Array();
var arrFlights = new Array();
arrFlights.push({ ID: "SG", Value: "Spice Jet", Type: ["N", "L"] },
                { ID: "6E", Value: "Indigo", Type: ["N", "L"] },
                { ID: "G8", Value: "Go Air", Type: ["N", "L"] },
                { ID: "G9", Value: "Air Arabia", Type: ["N"] },
                { ID: "FZ", Value: "Fly Dubai", Type: ["N"] },
                { ID: "IX", Value: "Air India Express", Type: ["N"] },
                { ID: "AK", Value: "Air Asia", Type: ["N"] },
                { ID: "LB", Value: "Air Costa", Type: ["N"] },
                { ID: "UK", Value: "Air Vistara", Type: ["N", "G"] },
                { ID: "AI", Value: "Air India", Type: ["G"] },
                { ID: "9W", Value: "Jet Airways", Type: ["G"] },
                { ID: "S2", Value: "JetLite", Type: ["G"] }
               );
$(function () {
    $(".JourneyType").click(function () {
        debugger;
        if (this.value == "O") {
            BindAirlines("N")
        }
        else if (this.value == "R") {
            $(".RoundWay").click(function () {
                if (rdb_Normal.checked)
                    BindAirlines("N")
                else if (rdb_SP.checked)
                    BindAirlines("L")
                else if (rdb_GDS.checked)
                    BindAirlines("G")
            });
        }
        else {
            BindAirlines("N")
        }
    });
})
function BindAirlines(sType) {
    $('#sel_Airlines').empty();
    var opt = '';
    for (var i = 0; i < arrFlights.length; i++) {
        var Type = $.grep(arrFlights[i].Type, function (p) { return p == sType; })
                             .map(function (p) { return p; });
        if (Type != undefined && Type.length != 0)
            opt += ' <option value="' + arrFlights[i].ID + '" selected="selected">' + arrFlights[i].Value + '</option>'
    }
    $('#sel_Airlines').append(opt);
    $('#sel_Airlines').change(function () {
        console.log($(this).val());
    }).multipleSelect({
        width: '100%'
    });
}

function SearchFlight() {
    var AdultCount = $("#sel_Adults").val();
    var ChildCount = $("#sel_Child").val();
    var InfantCount = $("#sel_Infant").val();
    var PreferredAirlines = new Array();
    if ($("#sel_Airlines").val() == null)
        PreferredAirlines = null;
    else {
        debugger
        for (var i = 0; i < $("#sel_Airlines").val().length; i++) {
            PreferredAirlines.push($("#sel_Airlines").val()[i]);
        }
        PreferredAirlines = null;
    }

    var Class = $("#sel_Class").val();
    var Sources = new Array();
    var Segment = new Array();
    var JourneyType = "1";
    if (radio03.checked)
        JourneyType = "3";
    if (radio02.checked) {
        JourneyType = "2";
    }
    if (radioAdvance.checked) {
        JourneyType = "4";
    }

    if (Validate()) {
        for (var i = 0; i <= inz; i++) {
            var spurce = {
                Destination: GetCityCode($("#destination" + i).val()),
                FlightCabinClass: $("#sel_Class").val(),
                Origin: GetCityCode($("#origin" + i).val()),
                PreferredArrivalTime: $("#dte_OnwardDateId" + i).val(),
                PreferredDepartureTime: $("#dte_OnwardDateId" + i).val(),
            };
            Segment.push(spurce)
            if (JourneyType == "2") {
                var spurce = {
                    Destination: GetCityCode($("#origin" + i).val()),
                    FlightCabinClass: $("#sel_Class").val(),
                    Origin: GetCityCode($("#destination" + i).val()),
                    PreferredArrivalTime: $("#datepicker_Return").val(),
                    PreferredDepartureTime: $("#datepicker_Return").val(),
                };
                Segment.push(spurce)
                if (rdb_SP.checked)
                    JourneyType = '5';
                break;
            }
        }
        if ($("#sel_Airlines").val() == null)
            Sources = null;
        else {
            //Sources.push($("#sel_Airlines").val())
            for (var i = 0; i < $("#sel_Airlines").val().length; i++) {
                Sources.push($("#sel_Airlines").val()[i]);
            }
        }

        var NonStop = "true", Direct = "false";
        if (chkNonStop.checked) {
            NonStop = "false";
            Direct = "true"
        }
        Search = {
            Origin: GetCityCode($("#origin0").val()),
            Destination: GetCityCode($("#destination0").val()),
            AdultCount: AdultCount,
            ChildCount: ChildCount,
            InfantCount: InfantCount,
            DirectFlight: Direct,
            OneStopFlight: NonStop,
            PreferredAirlines: PreferredAirlines,
            Segments: Segment,
            Sources: Sources,
            TokenId: "",
            JourneyType: JourneyType,
        }
        window.location.href = "waitforFlights.aspx?Search=" + JSON.stringify(Search);
    }
}

function GetCityCode(value) {
    return value.split('(')[1].split(')')[0];
}




function CalendarSearchFlight() {
    if ($("#CalenderFrom").val() == "") {
        alert("Please enter from.");
        return false;
    }
    if ($("#CalenderTo").val() == "") {
        alert("Please enter to.");
        return false;
    }
    if ($("#dt_CalendarMonthYear").val() == "") {
        alert("Please select month & year.");
        return false;
    }

    var JourneyType = "1";
    var Class = $("#sel_CalendarClass").val();
    var PreferredDepartureTime = $("#dt_CalendarMonthYear").val();
    var Sources = new Array();
    var Segment = new Array();
    var spurce = {
        Destination: GetCityCode($("#CalenderTo").val()),
        FlightCabinClass: $("#sel_CalendarClass").val(),
        Origin: GetCityCode($("#CalenderFrom").val()),
        PreferredDepartureTime: PreferredDepartureTime,
    };
    Segment.push(spurce)

    var PreferredAirlines = new Array();
    if ($("#PreferredCarrier option:selected").val() == "0")
        PreferredAirlines = null;
    else {
        PreferredAirlines.push($("#PreferredCarrier option:selected").val());
    }

    if ($("#PreferredCarrier").val() == "0")
        Sources = null;
    else {
        Sources.push($("#PreferredCarrier option:selected").val())
    }

    Search = {
        Origin: GetCityCode($("#CalenderFrom").val()),
        Destination: GetCityCode($("#CalenderTo").val()),
        PreferredAirlines: PreferredAirlines,
        Segments: Segment,
        Sources: Sources,
        TokenId: "",
        JourneyType: JourneyType,
    }

    $.ajax({
        type: "POST",
        url: "handler/FlightHandler.asmx/CalendarSerchFlight",
        data: JSON.stringify({ ObjCalendarSearch: Search }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                window.location.href = "CalendarSearch.aspx?Date=" + Segment[0].PreferredDepartureTime;
            }
            if (result.retCode == 0) {
                alert(result.error);
            }
        },
        error: function () {
            alert("Somethings went wrong")
        }
    });
}

function Validate() {
    var bValid = true;
    var JourneyType = "1";
    if (radio03.checked)
        JourneyType = "3";
    if (radio02.checked) {
        JourneyType = "2";
    }
    for (var i = 0; i <= inz; i++) {
        $("#lbl_Errordestination" + i).text("")
        if ($("#destination" + i).val() == "") {
            $("#lbl_Errordestination" + i).text("*Please Add destination.")
            bValid = false;
        }
        else if (!$("#destination" + i).val().includes("(")) {
            $("#lbl_Errordestination" + i).text("*Please Select destination from given option.")
            bValid = false;
        }
        $("#lbl_Errorsel_Class").text("")
        if ($("#sel_Class").val() == "") {
            $("#lbl_Errorsel_Class").text("*Please Add Cabin Class.")
            bValid = false;
        }
        $("#lbl_Errororigin" + i).text("")
        if ($("#origin" + i).val() == "") {
            $("#lbl_Errororigin" + i).text("*Please Select Origin.")
            bValid = false;
        }
        else if (!$("#origin" + i).val().includes("(")) {
            $("#lbl_Errororigin" + i).text("*Please Select Origin from given option.")
            bValid = false;
        }
        $("#lbl_ErrorOnwardDateId" + i).text("")
        if ($("#dte_OnwardDateId" + i).val() == "") {
            $("#lbl_ErrorOnwardDateId" + i).text("*Please Select Date.")
            bValid = false;
        }
        if (JourneyType == "2") {
            $("#lbl_Errordatepicker_Return").text("")
            if ($("#datepicker_Return").val() == "") {
                $("#lbl_Errordatepicker_Return").text("*Please Select Return Date.")
                bValid = false;
            }
            break;
        }

    }
    return bValid;
}