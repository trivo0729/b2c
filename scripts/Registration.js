﻿$(document).ready(function () {
    $(function () {
        debugger;
        $('[data-toggle="popover"]').popover()
    })
    $('#btn_RegiterAgent').attr("disabled", false);
    //$('#chkTermsAndConditions').change(function () {
    //    if ($('#chkTermsAndConditions').is(':checked')) {
    //        $('#btn_RegiterAgent').attr("disabled", false);
    //    }
    //    else
    //        $('#btn_RegiterAgent').attr("disabled", true);
    //    $("#tbl_AgentRegistration tr td label").css("display", "none");
    //    $("#tbl_AgentRegistration tr td label").html("* This field is required");
    //});
    $('#Select_Country').change(function () {
        if ($('#Select_Country option:selected').val() == 'IN') {
            $('#txt_PAN').attr("disabled", false);
            $('#txt_ReferenceNumber').attr("disabled", false);
        }
        else {
            $('#txt_PAN').attr("disabled", true);
            $('#txt_ReferenceNumber').attr("disabled", true);
        }
    });
    $('#Select_IATA').change(function () {
        $("#lbl_IATA").css("display", "none");
        $('#txt_IATA').val('');
        if ($('#Select_IATA option:selected').val() == '1') {
            $('#txt_IATA').attr("disabled", false);
        }
        else {
            $('#txt_IATA').attr("disabled", true);
        }
    });
});
var hiddenID;
function ValidateLogin() {
    debugger;
    var reg = new RegExp('[0-9]$');
    var regPan = new RegExp('^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$');
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);

    $("#formAgent label").css("display", "none");
    $("#formAgent label").html("* This field is required");
    //$("#tbl_AgentRegistration tr td label").css("display", "none");
    //$("#tbl_AgentRegistration tr td label").html("* This field is required");
    //$("#txt_Email").css("border-color", "#ebebeb");
    //$("#txt_Mobile").css("border-color", "#ebebeb");
    var bValid = true;
    var sAgencyName = $("#txt_AgencyName").val();
    var sFirstName = $("#txt_FirstName").val();
    var sLastName = $("#txt_LastName").val();
    var sDesignation = $("#txt_Designation").val();
    var sAddress = $("#txt_Address").val();
    var bIATAStatus = $("#Select_IATA").val();
    var nIATA = $("#txt_IATA").val();
    var sCity = $("#Select_City").val();
    var sCountry = $("#Select_Country").val();
    var nPinCode = $("#txt_PinCode").val();
    var sEmail = $("#txt_Email").val();
    //var sPassword = $("#txt_Password").val();
    //var sConfirmPassword = $("#txt_ConfirmPassword").val();
    var nPhone = $("#txt_Phone").val();
    var nMobile = $("#txt_Mobile").val();
    var nFax = $("#txt_Fax").val();
    //var sUsertype = $("#Select_Usertype").val();
    var sServiceTaxNumber = $("#txt_ReferenceNumber").val();
    var nPAN = $("#txt_PAN").val();
    var sWebsite = $("#txt_Website").val();
    var sReferral = $("#SelReferral").val();
    var sRemarks = $("#txt_Remarks").val();
    var sCurrency = $("#SelCurrency").val();
    //if (sCountry == "IN") {
    //    if (nPAN == "") {
    //        bValid = false;
    //        $("#lbl_PAN").css("display", "");
    //    }
    //}
    if (sAgencyName == "") {
        bValid = false;
        $("#lbl_AgencyName").css("display", "");
    }
    if (sFirstName == "") {
        bValid = false;
        $("#lbl_FirstName").css("display", "");
    }
    if (sLastName == "") {
        bValid = false;
        $("#lbl_LastName").css("display", "");
    }
    if (sDesignation == "") {
        bValid = false;
        $("#lbl_Designation").css("display", "");
    }
    if (sEmail == "") {
        bValid = false;
        $("#lbl_Email").css("display", "");
    }
    else {
        if (!(pattern.test(sEmail))) {
            bValid = false;
            $("#lbl_Email").html("* Wrong email format.");
            $("#lbl_Email").css("display", "");
        }
    }

    if (sAddress == "") {
        bValid = false;
        $("#lbl_Address").css("display", "");
    }
    if (sAddress != "" && reg.test(sAddress)) {
        bValid = false;
        $("#lbl_Address").html("* Address must not be numeric at end.");
        $("#lbl_Address").css("display", "");
    }
    if (bIATAStatus == "0") {
        bValid = false;
        $("#lbl_Select_IATA").css("display", "");
    }
    if (bIATAStatus == "1" && nIATA == "") {
        bValid = false;
        $("#lbl_IATA").css("display", "");
    }
    if (bIATAStatus == "1" && nIATA != "" && !reg.test(nIATA)) {
        bValid = false;
        $("#lbl_IATA").html("* IATA no. must be numeric and valid.");
        $("#lbl_IATA").css("display", "");
    }
    if (sCity == "-") {
        bValid = false;
        $("#lbl_City").css("display", "");
    }
    if (sCountry == "-") {
        bValid = false;
        $("#lbl_Country").css("display", "");
    }

    if (nMobile == "") {
        bValid = false;
        $("#lbl_Mobile").css("display", "");
    }

    else {
        if (!(reg.test(nMobile))) {
            bValid = false;
            $("#lbl_Mobile").html("* Mobile no must be numeric.");
            $("#lbl_Mobile").css("display", "");
        }
    }
    //if (sUsertype == "0") {
    //    bValid = false;
    //    $("#lbl_Usertype").css("display", "");
    //}

    if (nFax == "") {
        nFax = '0';
    }
    else {
        if (!(reg.test(nFax))) {
            bValid = false;
            $("#lbl_Fax").html("* Fax no. must be numeric.");
            $("#lbl_Fax").css("display", "");
        }
    }
    if (nPhone == "") {
        nPhone = '0';
    }
    else {
        if (!(reg.test(nPhone))) {
            bValid = false;
            $("#lbl_Phone").html("* Phone no must be numeric.");
            $("#lbl_Phone").css("display", "");
        }
    }
    if (nPinCode == "") {
        nPinCode = '0';
    }
    else {
        if (!(reg.test(nPinCode))) {
            bValid = false;
            $("#lbl_PinCode").html("* Pincode must be numeric.");
            $("#lbl_PinCode").css("display", "");
        }
    }
    if (nPAN == "") {
        nPAN = '0';
    }
    //else {
    //    if (!(regPan.test(nPAN))) {
    //        bValid = false;
    //        $("#lbl_PAN").html("* PAN no must be correct.");
    //        $("#lbl_PAN").css("display", "");
    //    }
    //}
    //if (sPassword != sConfirmPassword) {
    //    bValid = false;
    //    $("#lbl_ConfirmPassword").html("Password & Confirm password<br /> must be same");
    //    $("#lbl_ConfirmPassword").css("display", "");
    //}
    if (sCurrency == "0") {
        bValid = false;
        $("#lbl_Currency").css("display", "");
    }
    if (bValid == false) {
        Success("Please Fill out all necessary Information.")
        bValid == false;
    }
    if (bValid == true) {
        debugger;
        if (($('#chkTermsAndConditions').is(':checked'))) {
            $.ajax({
                type: "POST",
                url: "DefaultHandler.asmx/CheckForUniqueAgent",
                data: '{"sEmail":"' + sEmail + '"}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.Session == 0) {
                        window.location.href = "Default.aspx";
                        //                    window.location.href = "../CUT/Default.aspx";
                        return false;
                    }
                    if (result.retCode == 1) {
                        Success("This email/mobile is already taken, Please choose another.");
                        $("#txt_Email").css("border-color", "brown");
                        $("#txt_Email").focus();
                        $("#txt_Mobile").css("border-color", "brown");
                        return false;
                    }
                    else if (result.retCode == 0) {
                        Success("Their is some error in processing your request, Please try again");
                        return false;
                    }

                    else if (result.retCode == 2) {
                        //$("#LoaderModal").modal('show');
                        $.ajax({
                            type: "POST",
                            url: "DefaultHandler.asmx/UserRegistration",
                            data: '{"sAgencyName":"' + sAgencyName + '","sEmail":"' + sEmail + '","sFirstName":"' + sFirstName + '","sLastName":"' + sLastName + '","sDesignation":"' + sDesignation + '","sAddress":"' + sAddress + '","sCity":"' + sCity + '","nPinCode":"' + nPinCode + '","nIATA":"' + nIATA + '","nPAN":"' + nPAN + '","nPhone":"' + nPhone + '","nMobile":"' + nMobile + '","nFax":"' + nFax + '","sServiceTaxNumber":"' + sServiceTaxNumber + '","sWebsite":"' + sWebsite + '","sReferral":"' + sReferral + '","sCurrency":"' + sCurrency + '","sRemarks":"' + sRemarks + '"}',
                            contentType: "application/json; charset=utf-8",
                            datatype: "json",
                            success: function (response) {
                                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                                if (result.Session == 0) {
                                    window.location.href = "Default.aspx";
                                    //                                window.location.href = "../CUT/Default.aspx";
                                    return false;
                                }
                                if (result.retCode == 1 && location.href.indexOf('?') != -1) {
                                    //$("#LoaderModal").modal('hide');
                                    Success("You have registered successfully. Now you can upload your logo. A mail containing activation link is sent to your registered Email-Id.","");
                                    if (location.href.indexOf('?') != -1) {
                                        DeleteAgent(hiddenUid)
                                    }
                                }

                                else if (result.retCode == 1 && location.href.indexOf('?') == -1) {
                                    Success("You have registered successfully. Now you can upload your logo. A mail containing activation link is sent to your registered Email-Id.");
                                   

                                }
                                else if (result.retCode == 0) {
                                    Success("Something went wrong! Please contact administrator.");
                                }
                            },
                            error: function () {
                                Success("An error occured during registration! Please contact administrator.");
                            }
                        });
                    }
                },
                error: function () {
                }
            });
        }
        else {
            Success("Please Accept terms and condition")
        }

    }
    function Redirect()
    {
        window.location.href = "Registration2.aspx";
    }


    //debugger;
    //if (bValid == true && ($('#chkTermsAndConditions').is(':checked'))) {
    //    $.ajax({
    //        type: "POST",
    //        url: "DefaultHandler.asmx/CheckForUniqueAgent",
    //        data: '{"sEmail":"' + sEmail + '"}',
    //        contentType: "application/json; charset=utf-8",
    //        datatype: "json",
    //        success: function (response) {
    //            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
    //            if (result.Session == 0) {
    //                window.location.href = "Default.aspx";
    //                //                    window.location.href = "../CUT/Default.aspx";
    //                return false;
    //            }
    //            if (result.retCode == 1) {
    //                Success("This email/mobile is already taken, Please choose another.");
    //                $("#txt_Email").css("border-color", "brown");
    //                $("#txt_Email").focus();
    //                $("#txt_Mobile").css("border-color", "brown");
    //                return false;
    //            }
    //            else if (result.retCode == 0) {
    //                Success("Their is some error in processing your request, Please try again");
    //                return false;
    //            }

    //            else if (result.retCode == 2) {
    //                //$("#LoaderModal").modal('show');
    //                $.ajax({
    //                    type: "POST",
    //                    url: "DefaultHandler.asmx/UserRegistration",
    //                    data: '{"sAgencyName":"' + sAgencyName + '","sEmail":"' + sEmail + '","sFirstName":"' + sFirstName + '","sLastName":"' + sLastName + '","sDesignation":"' + sDesignation + '","sAddress":"' + sAddress + '","sCity":"' + sCity + '","nPinCode":"' + nPinCode + '","nIATA":"' + nIATA + '","nPAN":"' + nPAN + '","nPhone":"' + nPhone + '","nMobile":"' + nMobile + '","nFax":"' + nFax + '","sServiceTaxNumber":"' + sServiceTaxNumber + '","sWebsite":"' + sWebsite + '","sReferral":"' + sReferral + '","sCurrency":"' + sCurrency + '","sRemarks":"' + sRemarks + '"}',
    //                    contentType: "application/json; charset=utf-8",
    //                    datatype: "json",
    //                    success: function (response) {
    //                        var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
    //                        if (result.Session == 0) {
    //                            window.location.href = "Default.aspx";
    //                            //                                window.location.href = "../CUT/Default.aspx";
    //                            return false;
    //                        }
    //                        if (result.retCode == 1 && location.href.indexOf('?') != -1) {
    //                            //$("#LoaderModal").modal('hide');
    //                            Success("You have registered successfully, A mail containing password has been sent to registered mail. Your account will be activated shortly. Now you can upload your logo.");
    //                            if (location.href.indexOf('?') != -1) {
    //                                DeleteAgent(hiddenUid)
    //                            }
    //                        }

    //                        else if (result.retCode == 1 && location.href.indexOf('?') == -1) {
    //                            window.location.href = "Registration2.aspx";

    //                        }
    //                        else if (result.retCode == 0)
    //                        {
    //                            Success("Something went wrong! Please contact administrator.");
    //                        }
    //                    },
    //                    error: function () {
    //                        Success("An error occured during registration! Please contact administrator.");
    //                    }
    //                });
    //            }
    //        },
    //        error: function () {
    //        }
    //    });
    //}




}




function GetQueryStringParamsForAgentRegistrationUpdate(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}
var hiddenUid
$(document).ready(function () {
    if (location.href.indexOf('?') != -1) {
        hiddenUid = GetQueryStringParamsForAgentRegistrationUpdate('uid').replace("%20", " ");
        GetAgentDetails(hiddenUid)
    }


});
function GetAgentDetails(hiddenUid) {
    $.ajax({
        type: "POST",
        url: "DefaultHandler.asmx/GetAgentDetails",
        data: '{"sUserName":"' + hiddenUid + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var arrResult = result.tbl_Admintemp
                $('#txt_AgencyName').val(arrResult[0].AgencyName);
                $('#txt_Email').val(arrResult[0].Email);
                $('#txt_Mobile').val(arrResult[0].Mobile);
                $('#txt_FirstName').val(arrResult[0].ContactPerson);
                $('#txt_Address').val(arrResult[0].Address);
                $('#txt_PinCode').val(arrResult[0].pincode);
                $('#txt_Address').val(arrResult[0].Address);
            }
            if (result.retCode == 0) {
            }
        },
        error: function () {
            Success("An error occured while loading cities")
        }
    });
    //$('#selCity option:selected').val(tempcitycode);
}
function DeleteAgent(hiddenUid) {
    $.ajax({
        type: "POST",
        url: "DefaultHandler.asmx/DeleteAgent",
        data: '{"sUserName":"' + hiddenUid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                window.location.href = "Registration2.aspx";

            } else if (result.retCode == 0) {
                window.location.href = "Registration2.aspx";
            }
        },
        error: function () {
            Success('Error occured while processing your request! Please try again.');
        }
    });
}