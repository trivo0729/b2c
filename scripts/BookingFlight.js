﻿var arrPax = new Array();
var arrCountry = new Array();

function GenratePaxDetails() {
    var html = '';
    var PaxType = "";
    html += '<div class="person-information">'
    html += '<h3>Passengers</h3>'
    html += '<hr />'
    $("#div_Pax").append(html)
    for (var i = 0; i < arrPax.PaxDetails.length; i++) {
        html = '';
        html += '<div class="toggle-container box">                                                                     '
        html += '<div class="panel style2">                                                                             '
        html += '<h4 class="panel-title">                                                                               '
        html += '<a class="" href="#Passenger_' + (i) + '" data-toggle="collapse" aria-expanded="true">Passenger ' + (i + 1) + ': (' + arrPax.PaxDetails[i].PaxType.replace("AD", "Adult ").replace("CH", "Child ") + (i + 1) + ' )</a>'
        html += '</h4>                                                                                                  '
        html += '<div class="panel-collapse collapse in" id="Passenger_' + (i) + '" aria-expanded="true">'
        html += '<div class="panel-content">                                                                            '
        html += '<div class="form-group row">                                                                           '

        html += '<div class="col-sm-4 col-md-4">                                                                        '
        html += '<span style="font-size: 13px">First Name</span>' + RequiredFild(arrPax.PaxDetails[i].FirstName.IsRequired)
        html += '<input type="text" class="form-control " placeholder="" id="txt_Name' + i + '"><span  style="color:red" id="lbl_Name' + i + '"></span>'
        html += '<input type="hidden" id-"' + arrPax.PaxDetails[i].Type + '"  id-"txtPaxType' + i + '">'
        html += '</div>                                                                                                 '
        html += '<div class="col-sm-4 col-md-3">                                                                        '
        html += '<span style="font-size: 13px">Last Name</span>' + RequiredFild(arrPax.PaxDetails[i].LastName.IsRequired)
        html += '<input type="text" id="txt_LastName' + i + '" class="form-control " placeholder=""><span style="color:red" id="lbl_LastName' + i + '"></span>'
        html += '</div>                                                                                                 '
        html += '<div class="col-sm-2 col-md-3">                                                                        '
        html += '<span style="font-size: 13px">Date of birth</span>' + RequiredFild(arrPax.PaxDetails[i].DOB.IsRequired)
        html += '<div class="datepicker-wrap">                                                                          '
        html += '<input type="text" name="date_from" class="form-control" id="txt_DOB' + i + '" placeholder="dd-MM-yyyy"/><span  style="color:red" id="lbl_DOB' + i + '"></span>'
        html += '</div>                                                                                                 '
        html += '</div>                                                                                                 '
        html += '<div class="col-sm-2 col-md-2">                                                    '
        html += '<br />                                                                         '
        html += '<span>                                                                         '
        html += '<input type="radio" checked="" name="' + i + '" id="rdb_Male' + i + '" value="M">Male'
        html += '</span>                                                                        '
        html += '<br />                                                                         '
        html += '<span>                                                                         '
        html += '<input type="radio" name="' + i + '"  id="rdb_Female' + i + '" value="F">Female'
        html += '</span>                                                                        '
        html += '</div>                                                                             '
        html += '</div>                                                                                 '
        if (i == 0) {
            html += '<div class="form-group row">                                                           '
            html += '<div class="col-sm-4 col-md-4">                                                    '
            html += '<span style="font-size: 13px">Mobile :</span><span style="color:red">*</span>'
            html += '<input type="text" class="form-control " placeholder="" id="txt_MobileNo' + i + '"> <span  style="color:red" id="lbl_MobileNo' + i + '"></span>'
            html += '</div>                                                                             '
            html += '<div class="col-sm-4 col-md-4">                                                    '
            html += '<span style="font-size: 13px">Email:</span><span style="color:red">*</span>'
            html += '<input type="text" class="form-control " placeholder="" id="txt_Email' + i + '"> <span  style="color:red" id="lbl_Email' + i + '"></span>'
            html += '</div>'
            html += '</div>'
            html += '<div class="form-group row">'
            html += '<div class="col-sm-8 col-md-8">'
            html += '<span style="font-size: 13px">Address : </span><span style="color:red">*</span>'
            html += '<textarea id="txt_Address" rows="4" class="form-control"></textarea>'
            html += '<span  style="color:red" id="lbl_Address' + i + '"></span>'
            html += '                    </div>'
            html += '                    <div class="col-sm-4 col-md-4">'
            html += '                        <span style="font-size: 13px">Country </span></span><span style="color:red">*</span>'
            html += '                        <select class="form-control" id="sel_Country">'
            html += GetCountry()
            html += '                        </select><span  style="color:red" id="lbl_Country' + i + '"></span>'
            html += '                    </div>'
            html += '                    <div class="col-sm-4 col-md-4">'
            html += '                        <span style="font-size: 13px">City:</span><span style="color:red">*</span>'
            html += '<input type="text" class="form-control " placeholder="" id="txt_City">'
            html += '<span  style="color:red" id="lbl_City' + i + '"></span></div> '
            html += '                    </div>'
        }
        html += '                <div class="form-group row">                                                           '
        html += '                    <div class="col-sm-4 col-md-4">                                                    '
        html += '                        <span style="font-size: 13px">Passport No:</span>' + RequiredFild(arrPax.PaxDetails[i].Passport.IsRequired)
        html += '<input type="text"  class="form-control" placeholder="" id="txt_PassportNo' + i + '">'
        html += '<span  style="color:red" id="lbl_PassportNo' + i + '"></span>'
        html += '                    </div>                                                                             '
        html += '                    <div class="col-sm-4 col-md-4">                                                    '
        html += '                        <span style="font-size: 13px">Expiry date (if specified)</span>                '
        html += '                        <div class="datepicker-wrap">                                                  '
        html += '                            <input id="txt_PassportExp' + i + '" type="text" name="date_from" class="form-control" />'
        html += '                        </div>                                                                         '
        html += '                    </div>                                                                             '
        html += '                    <div class="col-sm-4 col-md-4">                                                    '
        html += '                        <span style="font-size: 13px">Nationality </span><span style="color:red">*</span>'
        html += '                        <select class="form-control" id="sel_Nationality' + i + '">                                 '
        html += GetCountry()
        html += '                        </select>                                                                      '
        html += '<span  style="color:red" id="lbl_Nationality' + i + '"></span></div> '
        html += '                    </div>                                                                             '


        html += '                <br />                                                                                 '
        html += '                <h4>Special Service Request</h4>                                                       '
        html += '                <hr />                                                                                 '
        html += '                <br />                                                                                 '

        html += GetSSR(i)
        html += '                </div>                                                                                 '
        html += '                </div>                                                                                 '
        html += '            </div>                                                                                     '
        html += '        </div>                                                                                         '
        html += '    </div>'
        html += '</div>'
        html += '<hr />'
        $("#div_Pax").append(html);
        var tpj = jQuery;
        tpj("#txt_DOB" + i).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "d-m-yy",
        });
        tpj("#txt_PassportExp" + i).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "d-m-yy",
        });
    }
    html = '';
    html += '<br />'
    html += '<div class="form-group row">'
    html += '    <div class="col-sm-8 col-md-18">'
    html += '    </div>'
    html += '    <div class="col-sm-4 col-md-4">'
    html += '        <a class="button btn-medium sky-blue1" onclick="CheckAvailcredit()">Proceed for booking</a>'
    html += '    </div>'
    html += '</div>'
    html += '<br />'
    html += '</div>'
    $("#div_Pax").append(html);
}

function RequiredFild(Validate) {
    var htmlreq = '';
    if (Validate)
        htmlreq += '<span style="color:red">*</span>'
    return htmlreq;
}
function GetCountry() {
    var html = '';
    for (var i = 0; i < arrCountry.length; i++) {
        html += '<option value="' + arrCountry[i].Country_Code + '">' + arrCountry[i].Country_name + '</option>'
    }
    return html;
} /*Pax Input Design*/

var arrPaxDetails = new Array();
function Book() {
    arrPaxDetails = new Array()
    if (Validate()) {
        for (var i = 0; i < arrPax.PaxDetails.length; i++) {
            var Title = "Mr", AddressLine = "", City = "", CountryCode = "", CountryName = "", email = ""; MobileNo = "";
            var isLeading = false;
            var Gender = "1"
            var Type = "1"
            if ($("#rdb_Female" + i).prop("checked") && arrPax.PaxDetails[i].PaxType != "CH" && arrPax.PaxDetails[i].PaxType != "In") {
                Title = "Mrs";
                Gender = 2;
                Type = "1";
            }
            else if (arrPax.PaxDetails[i].PaxType == "CH") {
                Title = "MASTER";
                Gender = 1;
                Type = "2"
            }
            else if (arrPax.PaxDetails[i].PaxType == "In") {
                Title = "MASTER";
                Gender = 1;
                Type = "3"
            }
            if (i == 0) {
                isLeading = true;
                AddressLine = $("#txt_Address").val();
                City = $("#txt_City").val();
                CountryCode = $("#sel_Country").val();
                CountryName = $("#sel_Country option:selected").text();
                email = $("#txt_Email0").val();
                MobileNo = $("#txt_MobileNo0").val();
            }
            var Baggage = [];
            for (var b = 0; b < $(".sel_Baggage" + i).length; b++) {
                Baggage.push(GetBaggageDetails($(".sel_Baggage" + i)[b].value, b));
            }
            var MealDynamic = [];
            for (var b = 0; b < $(".sel_DMeal" + i).length; b++) {
                MealDynamic.push(GetDMeal($(".sel_DMeal" + i)[b].value, b));
            }
            var Meal = new Array();
            for (var b = 0; b < $(".sel_Meal" + i).length; b++) {
                //Meal.push(GetMeal($(".sel_Meal" + i)[b].value, b));
                Meal = GetMeal($(".sel_Meal" + i)[b].value, b);
            }
            if ($(".sel_Meal" + i).length == 0)
                Meal = { Code: '', Description: '' }

            var Passenger = {
                Title: Title,
                FirstName: $("#txt_Name" + i).val(),
                LastName: $("#txt_LastName" + i).val(),
                PaxType: Type,
                DateOfBirth: $("#txt_DOB" + i).val(),
                Gender: Gender,
                PassportNo: $("#txt_PassportNo" + i).val(),
                PassportExpiry: $("#txt_PassportExp" + i).val(),
                AddressLine1: AddressLine,
                AddressLine2: "",
                City: City,
                CountryCode: CountryCode,
                CountryName: CountryName,
                Nationality: $("#sel_Nationality" + i + " option:selected").text(),
                ContactNo: MobileNo,
                Email: email,
                IsLeadPax: isLeading,
                Baggage: Baggage,
                MealDynamic: MealDynamic,
                Meal: Meal
            }
            arrPaxDetails.push(Passenger)
        }
        var arrBooking = new Array();
        Booking = {
            Passengers: arrPaxDetails,
            ResultIndexes: FlightId,
            EndUserIp: "",

        };
        $.ajax({
            type: "POST",
            url: "../handler/FlightHandler.asmx/Booking",
            data: JSON.stringify({ objFlightBooking: Booking }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    //$('#SpnMessege').text("Booking is Done your Flight Pnr no is :" + result.PNR);
                    var arrYourBooking = result.PNR;
                    var tr = '';
                    for (var i = 0; i < arrYourBooking.length; i++) {
                        tr += '<tr>'
                        if (arrYourBooking.Success)
                            tr += '<td class="green">Flight PNR: <td>' + arrYourBooking.BookingID;
                        else if (arrYourBooking.OnHold)
                            tr += '<td class="red">Flight PNR: <td>' + arrYourBooking.BookingID + ".Please Contact Administrator regarding this Booking.";
                        tr += '</tr>'
                    }
                    $('#SpnMessege').append(tr)
                    $('#ModelMessege').modal('show')
                    setTimeout(function () {
                        window.location.href = "Default.aspx";
                    }, 8000)
                }
                else if (result.retCode == 0) {
                    $('#SpnMessege').text(result.errormsg);
                    $('#ModelMessege').modal('show')
                    if (result.errormsg == "Your session (TraceId) is expired.") {
                        setTimeout(function () {
                            window.location.href = "Default.aspx";
                        }, 2000)
                    }

                }
            },
            error: function () {
                $('#AgencyBookingCancelModal').modal('hide')
                $('#SpnMessege').text("something went wrong");
                $('#ModelMessege').modal('show')
                // alert("something went wrong");
            },
            complete: function () {
                $("#dlgLoader").css("display", "none");
            }
        });
    }
} /*Booking*/

function Validate() {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    var bValid = true;
    for (var i = 0; i < arrPax.PaxDetails.length; i++) {
        $('#lbl_LastName' + i).hide()
        if ($("#txt_LastName" + i).val() == "" && arrPax.PaxDetails[i].LastName.IsRequired) {
            $('#lbl_LastName' + i).text("* Please add Last Name")
            $('#lbl_LastName' + i).show()
            bValid = false;
        }
        $('#lbl_Name' + i).hide()
        if ($("#txt_Name" + i).val() == "" && arrPax.PaxDetails[i].FirstName.IsRequired) {
            $('#lbl_Name' + i).text("* Please add First Name")
            $('#lbl_Name' + i).show()
            bValid = false;
        }
        $('#lbl_DOB' + i).hide()
        if ($("#txt_DOB" + i).val() == "" && arrPax.PaxDetails[i].DOB.IsRequired) {
            $('lbl_DOB' + i).text("* Please add DOB")
            $('lbl_DOB' + i).show()
            bValid = false;
        }
        $('#lbl_MobileNo0').hide()
        if (arrPax.PaxDetails[i].IsLeading) {
            if ($("#txt_MobileNo0").val() == "") {
                $('#lbl_MobileNo0').text("* Please add Mobile No")
                $('#lbl_MobileNo0').show()
                bValid = false;
            }
            $("#lbl_Email0").hide();
            if ($("#txt_Email0").val() == "") {
                bValid = false;
                $("#lbl_Email0").text("* Please add email.");
                $("#lbl_Email0").show();
            }
            else {
                if (!(pattern.test($("#txt_Email0").val()))) {
                    bValid = false;
                    $("#lbl_Email0").text("* Wrong email format.");
                    $("#lbl_Email0").show();
                }
            }
            $('lbl_City0').hide()
            if ($("#txt_City").val() == "") {
                $('#lbl_City0').text("* Please add City")
                $('#lbl_City0').show()
                bValid = false;
            }
            $('#lbl_Address0').hide()
            if ($("#txt_Address").val() == "") {
                $('#lbl_Address0').text("* Please add Address here")
                $('#lbl_Address0').show()
                bValid = false;

            }
        }

        $('#lbl_PassportNo' + i).hide()
        if ($("#txt_PassportNo" + i).val() == "" && arrPax.PaxDetails[i].Passport.IsRequired) {
            $('#lbl_PassportNo' + i).text("* Please add Passport No")
            $('#lbl_PassportNo' + i).show()
            bValid = false;
        }
    }
    return bValid;
}/*Booking Validation*/

var arrFlightDetails = new Array();
function GenrateFlightDetails() {
    var html = ""
    var ul = '';
    $("#Div_Trip").empty();
    if (arrFlightDetails.JourneyType == "1") {
        html += SegmentDesign(arrFlightDetails.FlightDetails[0].AirlineCode,
                arrFlightDetails.FlightDetails[0].FlightName,
                arrFlightDetails.FlightDetails[0].Origin.Airport.CityName + ',' + arrFlightDetails.FlightDetails[0].Origin.Airport.CountryName,
                arrFlightDetails.FlightDetails[0].Origin.DepTime,
                arrFlightDetails.FlightDetails[0].Destination.Airport.CityName + ',' + arrFlightDetails.FlightDetails[0].Destination.Airport.CountryName,
                arrFlightDetails.FlightDetails[0].Destination.ArrTime,
                arrFlightDetails.FlightDetails[0].Duration, 0
                )
    }
    else if (arrFlightDetails.JourneyType == "2") {
        if (arrFlightDetails.FlightDetails.length != 1) {
            html += '<div class="tab-container style1">'
            html += '<ul class="tabs full-width">'
            html += '<li class="active"><a href="#depart" data-toggle="tab" aria-expanded="true">Out Bound Ticket</a></li>'
            html += '<li class=""><a href="#Arrival" data-toggle="tab" aria-expanded="false">Inbound Ticket</a></li>'
            html += ' </ul>'
            html += '<div class="tab-content">'
            html += '<!-- Tab 1 -->'
            html += '<div class="tab-pane fade active in" id="depart" style="padding: 5px;">'
            html += SegmentDesign(arrFlightDetails.FlightDetails[0].AirlineCode,
                    arrFlightDetails.FlightDetails[0].FlightName,
                    arrFlightDetails.FlightDetails[0].Origin.Airport.CityName + ',' + arrFlightDetails.FlightDetails[0].Origin.Airport.CountryName,
                    arrFlightDetails.FlightDetails[0].Origin.DepTime,
                    arrFlightDetails.FlightDetails[0].Destination.Airport.CityName + ',' + arrFlightDetails.FlightDetails[0].Destination.Airport.CountryName,
                    arrFlightDetails.FlightDetails[0].Destination.ArrTime,
                    arrFlightDetails.FlightDetails[0].Duration, 0

                    )
            html += '</div>'
            html += '<!-- End of Tab 1 -->'
            html += '<!-- Tab 2 -->'
            html += '<div class="tab-pane fade" id="Arrival" style="padding: 5px;">'
            html += SegmentDesign(arrFlightDetails.FlightDetails[1].AirlineCode,
                   arrFlightDetails.FlightDetails[1].FlightName,
                   arrFlightDetails.FlightDetails[1].Origin.Airport.CityName + ',' + arrFlightDetails.FlightDetails[0].Origin.Airport.CountryName,
                   arrFlightDetails.FlightDetails[1].Origin.DepTime,
                   arrFlightDetails.FlightDetails[1].Destination.Airport.CityName + ',' + arrFlightDetails.FlightDetails[0].Destination.Airport.CountryName,
                   arrFlightDetails.FlightDetails[1].Destination.ArrTime,
                   arrFlightDetails.FlightDetails[1].Duration, 1
                   )
            html += '</div>'
            html += '<!-- End of Tab 2 -->'
            html += '</div>'
            html += '</div>'
        }

        else {
            html += '<div class="tab-container style1">'
            html += '<ul class="tabs full-width">'
            for (var i = 0; i < arrFlightDetails.FlightDetails.length; i++) {
                var Class = "";
                var expanded = "false";
                if (i == 0) {
                    Class = "active";
                    expanded = "true";
                }
                html += '<li class="' + Class + '"><a href="#tab' + i + '" data-toggle="tab" aria-expanded="' + expanded + '">' + arrFlightDetails.FlightDetails[i].Origin.Airport.CityName + '-'
                html += arrFlightDetails.FlightDetails[i].Destination.Airport.CityName
                html += '</a></li>'
            }
            ul += ' </ul>'
            html += '<div class="tab-content">'
            for (var i = 0; i < arrFlightDetails.FlightDetails.length; i++) {
                var Class = "";
                if (i == 0)
                    Class = "active in";
                html += '<!-- Tab ' + i + ' -->'
                html += '<div class="tab-pane fade ' + Class + '" id="tab' + i + '" style="padding: 5px;">'
                html += SegmentDesign(arrFlightDetails.FlightDetails[i].AirlineCode,
                   arrFlightDetails.FlightDetails[i].FlightName,
                   arrFlightDetails.FlightDetails[i].Origin.Airport.CityName + ',' + arrFlightDetails.FlightDetails[i].Origin.Airport.CountryName,
                   arrFlightDetails.FlightDetails[i].Origin.DepTime,
                   arrFlightDetails.FlightDetails[i].Destination.Airport.CityName + ',' + arrFlightDetails.FlightDetails[i].Destination.Airport.CountryName,
                   arrFlightDetails.FlightDetails[i].Destination.ArrTime,
                   arrFlightDetails.FlightDetails[i].Duration, 0
                   )
                html += '</div>'
            }
            html += '</div>'
            html += '</div>'
        }


    }
    else if (arrFlightDetails.JourneyType == "3") {
        html += '<div class="tab-container style1">'
        html += '<ul class="tabs full-width">'
        for (var i = 0; i < arrFlightDetails.FlightDetails.length; i++) {
            var Class = "";
            var expanded = "false";
            if (i == 0) {
                Class = "active";
                expanded = "true";
            }
            html += '<li class="' + Class + '"><a href="#tab' + i + '" data-toggle="tab" aria-expanded="' + expanded + '">' + arrFlightDetails.FlightDetails[i].Origin.Airport.CityName + '-'
            html += arrFlightDetails.FlightDetails[i].Destination.Airport.CityName
            html += '</a></li>'
        }
        ul += ' </ul>'
        html += '<div class="tab-content">'
        for (var i = 0; i < arrFlightDetails.FlightDetails.length; i++) {
            var Class = "";
            if (i == 0)
                Class = "active in";
            html += '<!-- Tab ' + i + ' -->'
            html += '<div class="tab-pane fade ' + Class + '" id="tab' + i + '" style="padding: 5px;">'
            html += SegmentDesign(arrFlightDetails.FlightDetails[i].AirlineCode,
               arrFlightDetails.FlightDetails[i].FlightName,
               arrFlightDetails.FlightDetails[i].Origin.Airport.CityName + ',' + arrFlightDetails.FlightDetails[i].Origin.Airport.CountryName,
               arrFlightDetails.FlightDetails[i].Origin.DepTime,
               arrFlightDetails.FlightDetails[i].Destination.Airport.CityName + ',' + arrFlightDetails.FlightDetails[i].Destination.Airport.CountryName,
               arrFlightDetails.FlightDetails[i].Destination.ArrTime,
               arrFlightDetails.FlightDetails[i].Duration, 0
               )
            html += '</div>'
        }
        html += '</div>'
        html += '</div>'
    }
    $("#Div_Trip").append(html)
    //$(".ConfirmationDetails").append(html)
    $('[data-toggle="tooltip"]').tooltip()


}/* Flight Details*/ var r = 0;

function SegmentDesign(AirlineCode, FlightName, Origin, DepTime, Destination, ArrTime, Duration, Id) {
    var html = '';
    html += '<h4>Trip Summary</h4>'
    html += '<hr />'
    html += '<article class="flight-booking-details">'
    html += '<figure class="clearfix">'
    html += '<a title="" href="#" class="middle-block" style="position: relative;">'
    html += '' + GetLogo(AirlineCode) + ''
    html += '</a>'
    html += '<div class="travel-title">'
    html += '<h5 class="box-title">' + Origin + ' To ' + Destination + '<br><br> <small>' + FlightName + ' (' + AirlineCode + ')</small></h5>'
    html += '</div>'
    html += '</figure>'
    html += '<div class="details">'
    html += '<div class="constant-column-3 timing clearfix">'
    html += '<div class="check-in">'
    html += '    <label>Departure</label>'
    html += '    <span>' + moment(DepTime).format("llll") + '</span>'
    html += '</div>'
    html += '<div class="duration text-center">'
    //html += '    <i class="soap-icon-clock"></i>'
    //html += '    <span>' + Duration + ' min</span>'
    html += '</div>'
    html += '<div class="check-out">'
    html += '    <label>Arrival </label>'
    html += '    <span>' + moment(ArrTime).format("llll") + '</span>'
    html += '</div>'
    html += '</div>'
    html += '<div class="row">'
    html += '<div class="col-md-12">'
    if (arrFlightDetails.JourneyType == "3" && r == 0) {
        html += GetBreckDown(Id)
        html += '<hr />'
    }

    if (arrFlightDetails.JourneyType == "1" || arrFlightDetails.JourneyType == "2" || arrFlightDetails.JourneyType == "5") {
        html += GetBreckDown(Id)
        html += '<hr />'
    }
    html += '<div class="form-group row">'
    html += '<br />'
    html += '<div class="col-sm-6 col-md-6">'
    html += '    <span style="font-size: 15px"><b>Trip Total:</b></span>'
    html += '</div>'
    html += '<div class="col-sm-6 col-md-6">'
    html += '<span class="price" data-toggle="tooltip" data-placement="left" data-container="body" data-original-title="' + arrFlightDetails.TotalPriceTip + '">RM ' + arrFlightDetails.TotalPrice + '</span>'
    html += '</div>'
    html += '</div>'
    html += '</div>'
    html += '</div>'
    html += '</div>'
    html += '</article>'
    r++;
    return html;

}

function GetLogo(AirlinCode) {
    var ext = ".gif";
    if (AirlinCode == "UK")
        ext = ".png";
    var img = '<img class="middle-item" alt="" src="images/AirlineLogo/' + AirlinCode + ext + '" style="position: absolute; top: 50%; margin-top: -37.5px; left: 50%; margin-left: -38px;">'
    return img;
}/*Logo*/

function GetSSR(s) {
    // $("#div_ssr").empty();
    var html = "";
    if (arrFlightDetails.SSR != null) {
        var Baggage = arrFlightDetails.SSR.Baggage;
        if (Baggage != undefined) {
            html += '                <div class="form-group row">'
            html += '                    <div class="col-sm-2 col-md-2"> '
            html += '                        <br />'
            html += '                        <span style="font-size: 13px">Baggage:</span><span class="red"></span>'
            html += '                    </div>'
            for (var i = 0; i < arrFlightDetails.SSR.Baggage.length; i++) {

                html += '                    <div class="col-sm-4 col-md-4">'
                html += '                        <span style="font-size: 13px"></span>(' + Baggage[i][0].Origin + '-' + Baggage[i][0].Destination + ')<span class="red">*</span>'
                html += '                        <select class="form-control sel_Baggage' + s + '">'
                for (var j = 0; j < Baggage[i].length; j++) {
                    var Price = ((Baggage[i][j].Price) * (0.057)).toFixed(2);
                    var Currency = "RM"
                    html += '<option value="' + Baggage[i][j].Code + '">' + Baggage[i][j].Weight + 'Kg (' + Currency + ' ' + Price + ')</option>'
                }
                html += '                        </select>'
                html += '                    </div>'
            }
            html += '                </div>'
        }
        var Meal = arrFlightDetails.SSR.MealDynamic;
        if (Meal != undefined) {
            html += '                <div class="form-group row">'
            html += '                    <div class="col-sm-2 col-md-2"> '
            html += '                        <br />'
            html += '                        <span style="font-size: 13px">Meal Preferences:</span><span class="red"></span>'
            html += '                    </div>'
            for (var i = 0; i < Meal.length; i++) {
                html += '                    <div class="col-sm-4 col-md-4">'
                html += '                        <span style="font-size: 13px"></span>(' + Meal[i][0].Origin + '-' + Meal[i][0].Destination + ')</span><span class="red">*</span>'
                html += '                        <select class="form-control sel_DMeal' + s + '">'
                for (var j = 0; j < Meal[i].length; j++) {
                    var Price = ((Meal[i][j].Price) * (0.057)).toFixed(2);
                    var Currency = "RM"
                    html += '<option value="' + Meal[i][j].Code + '">' + Meal[i][j].AirlineDescription + '(' + Currency + ' ' + Price + ')</option>'
                }
                html += '                        </select>'
                html += '                    </div>'
            }
            html += '                </div>'
        }

        var Meal = arrFlightDetails.SSR.Meal;
        if (Meal != undefined) {
            html += '                <div class="form-group row">'
            html += '                    <div class="col-sm-2 col-md-2"> '
            html += '                        <br />'
            html += '                        <span style="font-size: 13px">Meals:</span><span class="red"></span>'
            html += '                    </div>'
            html += '                    <div class="col-sm-4 col-md-4">'
            html += '                        <span style="font-size: 13px"></span><span class="red"></span>'
            html += '                        <select class="form-control sel_Meal' + s + '">'
            for (var i = 0; i < Meal.length; i++) {
                html += '<option value="' + Meal[i].Code + '">' + Meal[i].Description + '</option>'
            }
            html += '                        </select>'
            html += '                    </div>'
            html += '                </div>'
        }
    }
    return html;
    // $("#div_ssr").append(html);
}  /*SSR Design*/

function GetBaggageDetails(BaggageID) {
    var Baggage = new Array();
    var arrBaggage = $.grep(arrFlightDetails.SSR.Baggage[0], function (p) { return p.Code == BaggageID; })
                     .map(function (p) { return p; })[0];
    Baggage = {
        WayType: arrBaggage.WayType,
        Price: arrBaggage.Price,
        Code: BaggageID,
        Currency: arrBaggage.Currency,
        WayType: arrBaggage.WayType,
        Description: arrBaggage.Description,
        Weight: arrBaggage.Weight,
        Origin: arrBaggage.Origin,
        Destination: arrBaggage.Destination
    };
    return Baggage
}

function GetBreckDown(Idd) {
    var html = '';
    //if (arrFlightDetails.Breackdowns.length != 1) {
    //    html += '<ul id="tabs" class="nav navigation-tabs">'
    //    for (var i = 0; i < arrFlightDetails.Breackdowns.length; i++) {
    //        if (i == 0)
    //            html += ' <li class="active">';
    //        else
    //            html += ' <li>';
    //        html += '<a href="#Arrival" data-toggle="tab" data-original-title="" title="">Flight No ' + parseFloat(i + 1) + '</a>'
    //        html += '</li>'
    //    }
    //    html += ' </ul>'
    //}
    //for (var i = 0; i < arrFlightDetails.Breackdowns.length; i++) {

    for (var j = 0; j < arrFlightDetails.Breackdowns[Idd].length; j++) {

        html += '<div class="form-group row" style="margin-bottom: 0px">'
        html += '    <div class="col-sm-8 col-md-8 toggle-container box" style="margin-bottom: 0px">'
        html += '        <div class="panel style1">'
        html += '            <h6 class="panel-title"><a class="" href="#tgg_' + Idd + j + '" data-toggle="collapse" aria-expanded="true">Traveler ' + (Idd + 1) + '  (' + arrFlightDetails.Breackdowns[Idd][j].PassengerCount + ') ' + arrFlightDetails.Breackdowns[Idd][j].PassengerType + ' </a></h6>'
        html += '        </div>'
        html += '    </div>'
        html += '    <div class="col-sm-4 col-md-4" style="text-align: end">'
        html += '        <h5>'
        html += '            <br />'
        html += '            <span data-toggle="tooltip" data-placement="left" data-container="body" data-original-title="' + arrFlightDetails.Breackdowns[Idd][j].FareTip + '">RM ' + arrFlightDetails.Breackdowns[Idd][j].Fare + '</span><br>'
        html += '        </h5>'
        html += '    </div>'
        html += '</div>'
        html += '<div class="form-group row panel-collapse collapse in" id="tgg_' + Idd + j + '" aria-expanded="true" style="margin-bottom: 0px">'
        html += '    <div class="col-sm-7 col-md-7" style="color: #337ab7; text-align: end">'
        html += '        Flight<br>'
        html += '    </div>'
        html += '    <div class="col-sm-5 col-md-5" style="color: #337ab7; text-align: end">'
        html += '        <span style="color: #337ab7" data-toggle="tooltip" data-placement="left" data-container="body" data-original-title="' + arrFlightDetails.Breackdowns[Idd][j].FeeTip + '">RM ' + arrFlightDetails.Breackdowns[Idd][j].Fees + '</span><br>'
        html += '    </div>'
        html += '    <div class="col-sm-7 col-md-7" style="color: #337ab7; text-align: end">'
        html += '        Taxes & Fees<br>'
        html += '    </div>'
        html += '    <div class="col-sm-5 col-md-5" style="color: #337ab7; text-align: end">'
        html += '        <span style="color: #337ab7" data-toggle="tooltip" data-placement="left" data-container="body" data-original-title="' + arrFlightDetails.Breackdowns[Idd][j].TaxesTip + '">RM ' + arrFlightDetails.Breackdowns[Idd][j].Taxes + '</span><br>'
        html += '    </div>'
        html += '</div>'
    }
    //}
    return html;
}/*Price Breackups*/

function LogoForModal(AirlinCode) {
    var ext = ".gif";
    if (AirlinCode == "UK")
        ext = ".png";
    var img = '<img src="images/AirlineLogo/' + AirlinCode + ext + '" style="height:50px;">'
    return img;
}/*Logo*/

function GenerateModal() {
    var html = ""
    var ul = '';
    $("#Div_Modal").empty();
    if (arrFlightDetails.JourneyType == "1") {
        html += ModalDesign(arrFlightDetails.FlightDetails[0].AirlineCode,
                arrFlightDetails.FlightDetails[0].FlightName,
                arrFlightDetails.FlightDetails[0].Origin.Airport.CityName + ',' + arrFlightDetails.FlightDetails[0].Origin.Airport.CountryName,
                arrFlightDetails.FlightDetails[0].Origin.DepTime,
                arrFlightDetails.FlightDetails[0].Destination.Airport.CityName + ',' + arrFlightDetails.FlightDetails[0].Destination.Airport.CountryName,
                arrFlightDetails.FlightDetails[0].Destination.ArrTime,
                arrFlightDetails.FlightDetails[0].Duration, 0
                )
    }
    else if (arrFlightDetails.JourneyType == "2") {

        if (arrFlightDetails.FlightDetails.length != 1) {
            html += '<div class="tab-container style1">'
            html += '<ul class="tabs full-width">'
            html += '<li class="active"><a href="#departModal" data-toggle="tab" aria-expanded="true">Out Bound Ticket</a></li>'
            html += '<li class=""><a href="#ArrivalModal" data-toggle="tab" aria-expanded="false">Inbound Ticket</a></li>'
            html += ' </ul>'
            html += '<div class="tab-content">'
            html += '<!-- Tab 1 -->'
            html += '<div class="tab-pane fade active in" id="departModal" style="padding: 5px;">'
            html += ModalDesign(arrFlightDetails.FlightDetails[0].AirlineCode,
                    arrFlightDetails.FlightDetails[0].FlightName,
                    arrFlightDetails.FlightDetails[0].Origin.Airport.CityName + ',' + arrFlightDetails.FlightDetails[0].Origin.Airport.CountryName,
                    arrFlightDetails.FlightDetails[0].Origin.DepTime,
                    arrFlightDetails.FlightDetails[0].Destination.Airport.CityName + ',' + arrFlightDetails.FlightDetails[0].Destination.Airport.CountryName,
                    arrFlightDetails.FlightDetails[0].Destination.ArrTime,
                    arrFlightDetails.FlightDetails[0].Duration, 0

                    )
            html += '</div>'
            html += '<!-- End of Tab 1 -->'
            html += '<!-- Tab 2 -->'
            html += '<div class="tab-pane fade" id="ArrivalModal" style="padding: 5px;">'
            html += ModalDesign(arrFlightDetails.FlightDetails[1].AirlineCode,
                   arrFlightDetails.FlightDetails[1].FlightName,
                   arrFlightDetails.FlightDetails[1].Origin.Airport.CityName + ',' + arrFlightDetails.FlightDetails[0].Origin.Airport.CountryName,
                   arrFlightDetails.FlightDetails[1].Origin.DepTime,
                   arrFlightDetails.FlightDetails[1].Destination.Airport.CityName + ',' + arrFlightDetails.FlightDetails[0].Destination.Airport.CountryName,
                   arrFlightDetails.FlightDetails[1].Destination.ArrTime,
                   arrFlightDetails.FlightDetails[1].Duration, 1
                   )
            html += '</div>'
            html += '<!-- End of Tab 2 -->'
            html += '</div>'
            html += '</div>'
        }

        else {
            html += '<div class="tab-container style1">'
        html += '<ul class="tabs full-width">'
        for (var i = 0; i < arrFlightDetails.FlightDetails.length; i++) {
            var Class = "";
            var expanded = "false";
            if (i == 0) {
                Class = "active";
                expanded = "true";
            }
            html += '<li class="' + Class + '"><a href="#tabModal' + i + '" data-toggle="tab" aria-expanded="' + expanded + '">' + arrFlightDetails.FlightDetails[i].Origin.Airport.CityName + '-'
            html += arrFlightDetails.FlightDetails[i].Destination.Airport.CityName
            html += '</a></li>'
        }
        ul += ' </ul>'
        html += '<div class="tab-content">'
        for (var i = 0; i < arrFlightDetails.FlightDetails.length; i++) {
            var Class = "";
            if (i == 0)
                Class = "active in";
            html += '<!-- Tab ' + i + ' -->'
            html += '<div class="tab-pane fade ' + Class + '" id="tabModal' + i + '" style="padding: 5px;">'
            html += ModalDesign(arrFlightDetails.FlightDetails[i].AirlineCode,
               arrFlightDetails.FlightDetails[i].FlightName,
               arrFlightDetails.FlightDetails[i].Origin.Airport.CityName + ',' + arrFlightDetails.FlightDetails[i].Origin.Airport.CountryName,
               arrFlightDetails.FlightDetails[i].Origin.DepTime,
               arrFlightDetails.FlightDetails[i].Destination.Airport.CityName + ',' + arrFlightDetails.FlightDetails[i].Destination.Airport.CountryName,
               arrFlightDetails.FlightDetails[i].Destination.ArrTime,
               arrFlightDetails.FlightDetails[i].Duration, 0
               )
            html += '</div>'
        }
        html += '</div>'
        html += '</div>'
        }

    }
    else if (arrFlightDetails.JourneyType == "3") {
        html += '<div class="tab-container style1">'
        html += '<ul class="tabs full-width">'
        for (var i = 0; i < arrFlightDetails.FlightDetails.length; i++) {
            var Class = "";
            var expanded = "false";
            if (i == 0) {
                Class = "active";
                expanded = "true";
            }
            html += '<li class="' + Class + '"><a href="#tabModal' + i + '" data-toggle="tab" aria-expanded="' + expanded + '">' + arrFlightDetails.FlightDetails[i].Origin.Airport.CityName + '-'
            html += arrFlightDetails.FlightDetails[i].Destination.Airport.CityName
            html += '</a></li>'
        }
        ul += ' </ul>'
        html += '<div class="tab-content">'
        for (var i = 0; i < arrFlightDetails.FlightDetails.length; i++) {
            var Class = "";
            if (i == 0)
                Class = "active in";
            html += '<!-- Tab ' + i + ' -->'
            html += '<div class="tab-pane fade ' + Class + '" id="tabModal' + i + '" style="padding: 5px;">'
            html += ModalDesign(arrFlightDetails.FlightDetails[i].AirlineCode,
               arrFlightDetails.FlightDetails[i].FlightName,
               arrFlightDetails.FlightDetails[i].Origin.Airport.CityName + ',' + arrFlightDetails.FlightDetails[i].Origin.Airport.CountryName,
               arrFlightDetails.FlightDetails[i].Origin.DepTime,
               arrFlightDetails.FlightDetails[i].Destination.Airport.CityName + ',' + arrFlightDetails.FlightDetails[i].Destination.Airport.CountryName,
               arrFlightDetails.FlightDetails[i].Destination.ArrTime,
               arrFlightDetails.FlightDetails[i].Duration, 0
               )
            html += '</div>'
        }
        html += '</div>'
        html += '</div>'
    }
    $("#Div_Modal").append(html)
    $('[data-toggle="tooltip"]').tooltip()
}/* Flight Details*/ var n = 0;

function ModalDesign(AirlineCode, FlightName, Origin, DepTime, Destination, ArrTime, Duration, Id) {
    var html = '';
    html += '<div class="row" style="margin-bottom: 0px">'
    html += '<div class="col-md-2">'
    html += '' + LogoForModal(AirlineCode) + ''
    html += '<span>' + FlightName + '</span>'
    html += '</div>'
    html += '<div class="col-md-10">'
    html += '<div class="row">'
    html += '<div class="col-md-6">'
    html += '<span>Departure : ' + Origin + ''
    html += '<br />'
    html += '( ' + moment(DepTime).format("llll") + ' )</span>'
    //html += '<br />'
    //html += '<span>Duration :' + Duration + ' min</span>'
    html += '</div>'
    html += '<div class="col-md-6">'
    html += '<span>Arrival : ' + Destination + ''
    html += '<br />'
    html += '( ' + moment(ArrTime).format("llll") + ' )</span>'
    html += '</div>'
    html += '</div>'
    html += '</div>'
    html += '</div>'
    html += '<hr />'

    html += '<div class="row" style="margin-bottom: 0px">'

    if (arrFlightDetails.JourneyType == "3" && n == 0) {
        html += Accordions(Id)
    }

    if (arrFlightDetails.JourneyType == "1" || arrFlightDetails.JourneyType == "2" || arrFlightDetails.JourneyType == "5") {
        html += Accordions(Id)
    }

    html += '<div class="col-sm-5" id="Pax_Confirmation">'
    html += '</div>'
    html += '</div>'

    n++;
    return html;
}

function Accordions(l) {
    var html = '';
    for (var j = 0; j < arrFlightDetails.Breackdowns[l].length; j++) {
        html += '<div class="col-sm-7">'
        html += '<div class="toggle-container box"  style="border-right: 1px dashed #c2c2c2;">'
        html += '<div class="panel style1">'
        html += '<h4 class="panel-title">'
        html += '<a class="collapsed" href="#tg_' + l + j + '" data-toggle="collapse" aria-expanded="true" style="font-size: 12px;">Traveler ' + (l + 1) + '  (' + arrFlightDetails.Breackdowns[l][j].PassengerCount + ') ' + arrFlightDetails.Breackdowns[l][j].PassengerType + ' <b class="price" style="font-size: 14px;" data-toggle="tooltip" data-placement="left" data-container="body" data-original-title="' + arrFlightDetails.Breackdowns[l][j].FareTip + '">RM ' + arrFlightDetails.Breackdowns[l][j].Fare + '</b></a>'
        html += '</h4>'
        html += '<div class="panel-collapse collapse in" id="tg_' + l + j + '">'
        html += '<div class="panel-content">'
        html += '<p>Flight <span style="text-align: end" span style="color: #337ab7" data-toggle="tooltip" data-placement="left" data-container="body" data-original-title="' + arrFlightDetails.Breackdowns[l][j].FeeTip + '">RM ' + arrFlightDetails.Breackdowns[l][j].Fees + '</span></p>'
        html += '<p>Taxes & Fees <style="text-align: end" span style="color: #337ab7" data-toggle="tooltip" data-placement="left" data-container="body" data-original-title="' + arrFlightDetails.Breackdowns[l][j].TaxesTip + '">RM ' + arrFlightDetails.Breackdowns[l][j].Taxes + '</span></p>'
        html += '</div>'
        html += '</div>'
        html += '</div>'
        html += '</div>'
        html += '<div class="col-sm-12 col-md-12">'
        html += '<span style="font-size: 15px"><b>Trip Total:</b></span>&nbsp;&nbsp;&nbsp;<span class="price" data-toggle="tooltip" data-placement="left" data-container="body" data-original-title="' + arrFlightDetails.TotalPriceTip + '">RM ' + arrFlightDetails.TotalPrice + '</span>'
        //html += '<input type="hidden" name="amount" id="Amount" />'
        $("#Total_Amount").val(arrFlightDetails.TotalPrice);
        html += '</div>'
        html += '</div>'
    }
    return html;
}


function GetBaggageDetails(BaggageID, nSegment) {
    var Baggage = new Array();
    var arrBaggage = $.grep(arrFlightDetails.SSR.Baggage[nSegment], function (p) { return p.Code == BaggageID; })
                     .map(function (p) { return p; })[0];
    Baggage = {
        WayType: arrBaggage.WayType,
        Price: arrBaggage.Price,
        Code: BaggageID,
        Currency: arrBaggage.Currency,
        Description: arrBaggage.Description,
        Weight: arrBaggage.Weight,
        Origin: arrBaggage.Origin,
        Destination: arrBaggage.Destination
    };
    return Baggage
}

function GetDMeal(Code, nSegment) {
    var Meal = new Array();
    var arrMeal = $.grep(arrFlightDetails.SSR.MealDynamic[nSegment], function (p) { return p.Code == Code; })
                     .map(function (p) { return p; })[0];
    Meal = {
        WayType: arrMeal.WayType,
        Code: Code,
        Description: arrMeal.Description,
        AirlineDescription: arrMeal.AirlineDescription,
        Quantity: arrMeal.Quantity,
        Price: arrMeal.Price,
        Currency: arrMeal.Currency,
        Origin: arrMeal.Origin,
        Destination: arrMeal.Destination
    };
    return Meal
}

function CheckAvailcredit() {
    var FlightId = GetQueryStringParams("FlightID").split(',');
    if (Validate()) {
        $("#Pax_Confirmation").empty()
        $.ajax({
            type: "POST",
            url: "../handler/FlightHandler.asmx/CheckAvailCredit",
            data: JSON.stringify({ arrIndex: FlightId }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    var html = '';
                    html += '<span><b>Pax Details:</b></span>'
                    html += '<ul class="circle">'
                    for (var i = 0; i < arrPax.PaxDetails.length; i++) {
                        var Title = "Mr", AddressLine = "", City = "", CountryCode = "", CountryName = "", email = ""; MobileNo = "";
                        var isLeading = false;
                        var Gender = "1"
                        var Type = "1"
                        if ($("#rdb_Female" + i).prop("checked") && arrPax.PaxDetails[i].PaxType != "CH") {
                            Title = "Mrs";
                            Gender = 2;
                            Type = "1";
                        }
                        else if (arrPax.PaxDetails[i].PaxType == "CH") {
                            Title = "MASTER";
                            Gender = 1;
                            Type = "2"
                        }
                        html += '<li>' + Title + ' ' + $("#txt_Name" + i).val() + ' ' + $("#txt_LastName" + i).val(), +'</li>'
                    }
                    $("#Pax_Confirmation").append(html)
                    html += '</ul>'
                    //$('#ConfirmModal').modal('show')
                    CheckUserLogin();
                }
                else if (result.retCode == 0) {
                    $('#AgencyBookingCancelModal').modal('hide')
                    var Massage = "Unable To Process Your Request At This Time";
                    alert("Unable To Process Your Request At This Time");
                    //$('#SpnMessege').text(Massage);
                    //$('#ModelMessege').modal('show')
                }
            },
            error: function () {
                $('#AgencyBookingCancelModal').modal('hide')
                $('#SpnMessege').text("something went wrong");
                $('#ModelMessege').modal('show')
                // alert("something went wrong");
            },
            complete: function () {
                $("#dlgLoader").css("display", "none");
            }
        });
    }
}/*Check Avail credit*/

function CheckUserLogin() {
    debugger;
    $.ajax({
        url: "DefaultHandler.asmx/CheckSession",
        type: "post",
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                $('#ConfirmModal').modal('show');
            }
            else if (result.retCode == 0) {
                $("#RegisterationModal").modal('show');
            }
        },
    });
}

function LoginForFlightBooking() {
    debugger;
    var sUserName = document.getElementById("txtUserName").value;
    var sPassword = document.getElementById("txtPassword").value;
    var bValid = true;

    if (sUserName == "") {
        bValid = false;
        alert("User Name/email cannot be blank");
        document.getElementById('txtUserName').focus();
        return false;
    }
    else if (!emailReg.test(sUserName)) {
        bValid = false;
        alert("The email address you have entered is invalid");
        document.getElementById('txtUserName').focus();
        return false;
    }
    if (sPassword == "") {
        bValid = false;
        alert("Password cannot be blank");
        document.getElementById('txtPassword').focus();
        // $("#txt_UserPassword").focus();
        return false;
    }
    if (bValid == true) {
        $.ajax({
            type: "POST",
            url: "DefaultHandler.asmx/UserLogin",
            data: '{"sUserName":"' + sUserName + '","sPassword":"' + sPassword + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.Session == 0) {
                    window.location.href = "Default.aspx";
                    return false;
                }
                else if (result.retCode == 1) {
                    $("#RegisterationModal").modal('hide');
                    $('#ConfirmModal').modal('show');
                }
                else {
                    alert('Username/Password did not match.');
                }
            },
            error: function () {
            }
        });
    }
}
//................Validation.......................//
var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

function ClearAll() {
    $("#txt_UserId").value('');
    $("#txt_UserPassword").value('');
}

function ValidateRegistrationForFlight() {
    $("#lbl_UserName").css("display", "none");
    $("#lbl_Password").css("display", "none");
    $("#lbl_ConfirmPassword").css("display", "none");
    $("#lbl_Mobile").css("display", "none");
    $("#lbl_Phone").css("display", "none");
    $("#lbl_Address").css("display", "none");
    $("#lbl_City").css("display", "none");
    $("#lbl_PinCode").css("display", "none");

    $("#lbl_UserName").html("* This field is required.");
    $("#lbl_Password").html("* This field is required.");
    $("#lbl_ConfirmPassword").html("* This field is required.");
    $("#lbl_Mobile").html("* This field is required.");
    $("#lbl_Phone").html("* This field is required.");
    $("#lbl_Address").html("* This field is required.");
    $("#lbl_City").html("* This field is required.");
    $("#lbl_PinCode").html("* This field is required.");
    debugger;
    var reg = new RegExp('[0-9]$');

    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);

    var bValid = true;

    var UserName = $("#txt_UserName").val();

    var Address = $("#txt_Address").val();
    var CityId = $("#selCity").val();
    var Email = $("#txt_Email").val();
    var Password = $("#txt_Password").val();
    var ConfirmPassword = $("#txt_ConfirmPassword").val();
    var Contact2 = $("#txt_Phone").val();
    var Contact1 = $("#txt_Mobile").val();
    if (UserName == "") {
        bValid = false;
        $("#lbl_UserName").css("display", "");
    }
    else {
        if (!(pattern.test(UserName))) {
            bValid = false;
            $("#lbl_UserName").html("* Wrong email format.");
            $("#lbl_UserName").css("display", "");
        }
    }
    if (Password == "") {
        bValid = false;
        $("#lbl_Password").css("display", "");
    }
    if (ConfirmPassword == "") {
        bValid = false;
        $("#lbl_ConfirmPassword").html("* This field is required.");
        $("#lbl_ConfirmPassword").css("display", "");
    }
    else if (Password != ConfirmPassword) {
        bValid = false;
        $("#lbl_ConfirmPassword").html("Confirm Password did not matched!");
        $("#lbl_ConfirmPassword").css("display", "");
    }
    if (Address == "") {
        bValid = false;
        $("#lbl_Address").css("display", "");
    }
    if (Address != "" && reg.test(Address)) {
        bValid = false;
        $("#lbl_Address").html("* Address must not be numeric at end.");
        $("#lbl_Address").css("display", "");
    }
    if (CityId == "-") {
        bValid = false;
        $("#lbl_City").css("display", "");
    }
    if (Contact1 == "") {
        bValid = false;
        $("#lbl_Mobile").css("display", "");
    }

    else {
        if (!(reg.test(Contact1))) {
            bValid = false;
            $("#lbl_Mobile").html("* Mobile no must be numeric.");
            $("#lbl_Mobile").css("display", "");
        }
    }
    if (Contact2 == "") {
        Contact2 = '0';
    }
    else {
        if (!(reg.test(Contact2))) {
            bValid = false;
            $("#lbl_Phone").html("* Phone no must be numeric.");
            $("#lbl_Phone").css("display", "");
        }
    }
    if (bValid == true) {
        debugger;
        var data = {
            UserName: UserName,
            Password: Password,
            Contact1: Contact1,
            Contact2: Contact2,
            Address: Address,
            CityId: CityId,
        };
        $.ajax({
            type: "POST",
            url: "B2CCustomerLoginHandler.asmx/UserRegistration",
            //data: '{"sB2C_Id":"' + sB2C_Id + '","sUserName":"' + sUserName + '","sEmail":"' + sEmail + '","nMobile":"' + nMobile + '","nPhone":"' + nPhone + '","sAddress":"' + sAddress + '","sCity":"' + sCity + '","nPinCode":"' + nPinCode + '"}',
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {

                    alert("Registration Successfully");
                    $("#RegisterationModal").modal('hide');
                    ClearRegistrationForFlight();
                    $('#ConfirmModal').modal('show');
                }
                else if (result.retCode == 2) {
                    alert("Username already registered. Please register with other Username.");
                }
                else if (result.retCode == -1) {
                    alert("Registeration UnSuccessfull.");
                }

                else {
                    alert("Something Went Wrong");
                    window.location.reload();
                }
            }
        });
    }
}


function ClearRegistrationForFlight() {
    $("#PreviewModalRegisteration").modal('hide');
    $('#txt_UserName').val("");
    $('#txt_Password').val("");
    $('#txt_ConfirmPassword').val("");
    $('#txt_Mobile').val("");
    $('#txt_Phone').val("");
    $('#txt_Address').val("");
    $('#selCity option').val("-");
    $('#txt_PinCode').val("");

    $("#lbl_UserName").css("display", "none");
    $("#lbl_Password").css("display", "none");
    $("#lbl_ConfirmPassword").css("display", "none");
    $("#lbl_Mobile").css("display", "none");
    $("#lbl_Phone").css("display", "none");
    $("#lbl_Address").css("display", "none");
    $("#lbl_City").css("display", "none");
    $("#lbl_PinCode").css("display", "none");

    $("#lbl_UserName").html("* This field is required.");
    $("#lbl_Password").html("* This field is required.");
    $("#lbl_ConfirmPassword").html("* This field is required.");
    $("#lbl_Mobile").html("* This field is required.");
    $("#lbl_Phone").html("* This field is required.");
    $("#lbl_Address").html("* This field is required.");
    $("#lbl_City").html("* This field is required.");
    $("#lbl_PinCode").html("* This field is required.");


    //for login clearing

    $('#txtUserName').val("");
    $('#txtPassword').val("");

    $("#lbl_txtUserName").css("display", "none");
    $("#lbl_txtPassword").css("display", "none");

    $("#lbl_txtUserName").html("* This field is required.");
    $("#lbl_txtPassword").html("* This field is required.");
    //Clear();
}