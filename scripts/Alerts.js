﻿function Cancel() {
    $("#AlertModal").modal("hide")
}
function Ok(Message, Method, arg) {
    $("#btnOk").css("display", "")
    var id = [];
    if (arg != null) {
        for (var i = 0; i < arg.length; i++) {
            id.push('"' + arg[i] + '"')
        }
    }

    $("#AlertMessage").html(Message);
    if (arg == null)
        document.getElementById("btnOk").setAttribute("onclick", Method + "()")
    else
        document.getElementById("btnOk").setAttribute("onclick", Method + "(" + id + ")")
    $("#AlertModal").modal("show")
}
function Success(Message) {
    $("#AlertMessage").html(Message);
    $("#btnOk").css("display", "none")
    $("#Cancel").val("Close")
    $("#AlertModal").modal("show")
    return "";
}