﻿function numberWithCommas(x) {
    var sValue = x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    var retValue = sValue.split(".");
    return retValue[0];
}
var ReserID
var totalamount;
var checkin;
var checkout;
var bookingdate;
var CancellationPolicy;
var HotelName;
var PassengerName;
var City;
var TimeGap;
var h0=new Date();;
var h1 = new Date();;
var h2=new Date();;

function OpenCancellationPopup(ReservationID, Status) {
    debugger;
    ReserID = ReservationID;
    var arrCancellationDetails = new Array();
    var arrCancellationPolicy = new Array();
    var data = {
        ReservationID: ReservationID
    }
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/GetCancellationDetails",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                $("#divRemark1").css("display", "block");
                $("#divRemark2").css("display", "block");
                $("#btnCancelBooking").val("Cancel Booking");
                $("#dlgLoader").css("display", "none");
                $("#txtRemark").val("");
                $("#dspAlertMessage").css("display", "none");
                $("#hndCancellationAmount").val(result.CancellationAmount);
                $("#hndReservatonID").val(ReservationID);
                $("#hndIsCancelable").val(result.IsCancelable);
                $("#hndStatus").val(Status);

                $("#hdn_TotalFare").val(result.TotalFare);
                $("#hdn_ServiceCharge").val(result.ServiceCharge);
                $("#hdn_Total").val(result.Total);

                arrCancellationDetails = result.HotelReservation;
                arrCancellationPolicy = result.BookedRooms;
                $("#hndReferenceCode").val(arrCancellationDetails[0].ReferenceCode);
                if (result.IsCancelable == "0") {
                    $("#dspAlertMessage").html("You are not eligible to cancel this booking! It's already out of date.");
                    $("#dspAlertMessage").css("display", "block");
                }
                TimeGap = result.TimeGap;
                totalamount = arrCancellationDetails[0].TotalFare + arrCancellationDetails[0].ServiceCharge;
                checkin = arrCancellationDetails[0].CheckIn.split(" ");
                checkout = arrCancellationDetails[0].CheckOut.split(" ");
                bookingdate = arrCancellationDetails[0].ReservationDate.split(" ");
                CancellationPolicy = "";
                HotelName = arrCancellationDetails[0].HotelName;
                PassengerName = arrCancellationDetails[0].bookingname;
                City = arrCancellationDetails[0].City;
                if (HotelName.length > 17) {
                    HotelName = HotelName.substr(0, 17) + '..';
                }
                if (PassengerName.length > 18) {
                    PassengerName = PassengerName.substr(0, 18) + '..';
                }
                if (City.length > 18) {
                    City = City.substr(0, 18) + '..';
                }
                $("#dspHotelName").text(HotelName);
                $("#dspCheckin").text(checkin[0]);
                $("#dspCheckout").text(checkout[0]);
                $("#dspPasssengerName").text(PassengerName);
                $("#dspLocation").text(City);
                $("#dspNights").text(arrCancellationDetails[0].NoOfDays);
                $("#dspBookingID").text(ReservationID);
                $("#dspBookingDate").text(bookingdate);
                $("#dspBookingAmount").text(numberWithCommas(parseFloat(arrCancellationDetails[0].TotalFare)));
                //$("#dspBookingAmount").text(numberWithCommas(parseFloat(arrCancellationDetails[0].TotalFare) + parseFloat(arrCancellationDetails[0].ServiceCharge)));
                var arrAmount = new Array();
                var arrCanDate = new Array();
                for (var i = 0; i < arrCancellationPolicy.length; i++) {
                    var arTempAmount = new Array();
                    var arTempDate = new Array();
                    //arTempAmount = arrCancellationPolicy[i].CanAmtWithTax.split("|");
                    arTempAmount = arrCancellationPolicy[i].CancellationAmount.split("|");
                    arTempDate = arrCancellationPolicy[i].CutCancellationDate.split("|");
                    if (arTempAmount.length == 1) {
                        if (i == 0) {
                            //arrAmount[0] = parseFloat(arrCancellationPolicy[i].CanAmtWithTax);
                            arrAmount[0] = parseFloat(arrCancellationPolicy[i].CancellationAmount);
                            arrCanDate[0] = arrCancellationPolicy[i].CutCancellationDate;
                        }
                        else {
                            //arrAmount[0] = (parseFloat(arrAmount[0]) + parseFloat(arrCancellationPolicy[i].CanAmtWithTax));
                            arrAmount[0] = (parseFloat(arrAmount[0]) + parseFloat(arrCancellationPolicy[i].CancellationAmount));
                            arrCanDate[0] = arrCancellationPolicy[i].CutCancellationDate;
                        }
                    }
                    else if (arTempAmount.length == 2) {
                        if (i == 0) {
                            arrAmount[0] = parseFloat(arTempAmount[0]);
                            arrCanDate[0] = arTempDate[0];
                            arrAmount[1] = parseFloat(arTempAmount[1]);
                            arrCanDate[1] = arTempDate[1];
                        }
                        else {
                            arrAmount[0] = parseFloat(arrAmount[0]) + parseFloat(arTempAmount[0]);
                            arrCanDate[0] = arTempDate[0];
                            arrAmount[1] = parseFloat(arrAmount[1]) + parseFloat(arTempAmount[1]);
                            arrCanDate[1] = arTempDate[1];
                        }
                    }
                    else if (arTempAmount.length == 3) {
                        if (i == 0) {
                            arrAmount[0] = parseFloat(arTempAmount[0]);
                            arrCanDate[0] = arTempDate[0];
                            arrAmount[1] = parseFloat(arTempAmount[1]);
                            arrCanDate[1] = arTempDate[1];
                            arrAmount[2] = parseFloat(arTempAmount[2]);
                            arrCanDate[2] = arTempDate[2];
                        }
                        else {
                            arrAmount[0] = parseFloat(arrAmount[0]) + parseFloat(arTempAmount[0]);
                            arrCanDate[0] = arTempDate[0];
                            arrAmount[1] = parseFloat(arrAmount[1]) + parseFloat(arTempAmount[1]);
                            arrCanDate[1] = arTempDate[1];
                            arrAmount[2] = parseFloat(arrAmount[2]) + parseFloat(arTempAmount[2]);
                            arrCanDate[2] = arTempDate[2];
                        } 
                    }
                }
                if (Status == "Vouchered") {
                    for (var j = 0; j < arrAmount.length; j++)
                        CancellationPolicy += "In the event of cancellation after <b>" + arrCanDate[j] + "</b>, <b><i class=\"fa fa-inr\"></i> " + numberWithCommas(arrAmount[j].toFixed(2)) + "</b> plus Service Tax will be applicable.<br />";
                }
                else if (Status == "Booking")
                    CancellationPolicy = "No cancellation charges shall be applicable on holded booking.";
                $("#dspCancellationPolicy").html(CancellationPolicy);
                $("#AgencyBookingCancelModal").modal('show');
            }
            else if (result.retCode == 0) {
                $('#AgencyBookingCancelModal').modal('hide')
                $('#SpnMessege').text('Something Went Wrong');
                $('#ModelMessege').modal('show')
               // alert("error occured while getting cancellation details")
            }
        },
        error: function (xhr, status, error) {
            $('#AgencyBookingCancelModal').modal('hide')
            $('#SpnMessege').text("Error on cancellation popup:" + " " + xhr.readyState + " " + xhr.status);
            $('#ModelMessege').modal('show')
           // alert("Error on cancellation popup:" + " " + xhr.readyState + " " + xhr.status);
        }
    });
}

//function calculateTime(stringTime,TimeGap) {
//    var hoursToSubtract = TimeGap;
//    var oldTime = Date.parse('Jan 1, 2009 ' + stringTime);
//    var newTime = new Date(oldTime - 1000 * 60 * 60 * hoursToSubtract);
//    var hours = newTime.getHours();
//    var minutes = newTime.getMinutes();
//    var designation = "PM";
//    if ((hours == 0 || hours == 24) && minutes == 0)
//        designation = 'MIDNIGHT';
//    else if (hours == 12 && minutes == 0)
//        designation = 'NOON'
//    else if (hours < 12)
//        designation = 'AM';
//    else
//        hours -= 12;
//    return hours + ':' + minutes + ' ' + designation;
//}

function OpenConfirmationPopup(ReservationId, Status) {
    ReserID = ReservationId;
    var arrCancellationDetails1 = new Array();
    var arrCancellationPolicy1 = new Array();
    var data = {
        ReservationID: ReservationId
    }
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/GetCancellationDetails",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {

                $("#btnCancelBooking").val("Confirm Booking");

                $("#dlgLoader").css("display", "none");
                $("#divRemark1").css("display", "none");
                $("#divRemark2").css("display", "none");
                $("#dspAlertMessage").css("display", "none");
                $("#hndCancellationAmount").val(result.CancellationAmount);
                $("#hndReservatonID").val(ReservationId);
                $("#hndIsCancelable").val(result.IsCancelable);
                $("#hndIsConfirmable").val(result.IsConfirmable);
                $("#hndStatus").val(Status);
                arrCancellationDetails1 = result.HotelReservation;
                arrCancellationPolicy1 = result.BookedRooms;
                $("#hndReferenceCode").val(arrCancellationDetails1[0].ReferenceCode);
                //if (result.IsCancelable == "0") {
                //    //$("#btnCancelBooking").attr("disabled", "disabled");
                //    $("#dspAlertMessage").html("You are not eligible to cancel this booking! It's already out of date.");
                //    $("#dspAlertMessage").css("display", "block");
                //}

                totalamount = arrCancellationDetails1[0].TotalFare + arrCancellationDetails1[0].ServiceCharge;
                checkin = arrCancellationDetails1[0].CheckIn.split(" ");
                checkout = arrCancellationDetails1[0].CheckOut.split(" ");
                bookingdate = arrCancellationDetails1[0].ReservationDate.split(" ");
                CancellationPolicy = "";
                HotelName = arrCancellationDetails1[0].HotelName;
                PassengerName = arrCancellationDetails1[0].bookingname;
                City = arrCancellationDetails1[0].City;
                if (HotelName.length > 17) {
                    HotelName = HotelName.substr(0, 17) + '..';
                }
                if (PassengerName.length > 18) {
                    PassengerName = PassengerName.substr(0, 18) + '..';
                }
                if (City.length > 18) {
                    City = City.substr(0, 18) + '..';
                }
                $("#dspHotelName").text(HotelName);
                $("#dspCheckin").text(checkin[0]);
                $("#dspCheckout").text(checkout[0]);
                $("#dspPasssengerName").text(PassengerName);
                $("#dspLocation").text(City);
                $("#dspNights").text(arrCancellationDetails1[0].NoOfDays);
                $("#dspBookingID").text(ReservationId);
                $("#dspBookingDate").text(bookingdate);
                $("#dspBookingAmount").text(numberWithCommas(parseFloat(arrCancellationDetails1[0].TotalFare) + parseFloat(arrCancellationDetails1[0].ServiceCharge)));
                var arrAmount = new Array();
                var arrCanDate = new Array();
                for (var i = 0; i < arrCancellationPolicy1.length; i++) {
                    var arTempAmount = new Array();
                    var arTempDate = new Array();
                    //arTempAmount = arrCancellationPolicy1[i].CanAmtWithTax.split("|");
                    arTempAmount = arrCancellationPolicy1[i].CancellationAmount.split("|");
                    arTempDate = arrCancellationPolicy1[i].CutCancellationDate.split("|");
                    if (arTempAmount.length == 1) {
                        if (i == 0) {
                            //arrAmount[0] = parseFloat(arrCancellationPolicy1[i].CanAmtWithTax);
                            arrAmount[0] = parseFloat(arrCancellationPolicy1[i].CancellationAmount);
                            arrCanDate[0] = arrCancellationPolicy1[i].CutCancellationDate;
                        }
                        else {
                            //arrAmount[0] = (parseFloat(arrAmount[0]) + parseFloat(arrCancellationPolicy1[i].CanAmtWithTax));
                            arrAmount[0] = (parseFloat(arrAmount[0]) + parseFloat(arrCancellationPolicy1[i].CancellationAmount));
                            arrCanDate[0] = arrCancellationPolicy1[i].CutCancellationDate;
                        }
                    }
                    else if (arTempAmount.length == 2) {
                        if (i == 0) {
                            arrAmount[0] = parseFloat(arTempAmount[0]);
                            arrCanDate[0] = arTempDate[0];
                            arrAmount[1] = parseFloat(arTempAmount[1]);
                            arrCanDate[1] = arTempDate[1];
                        }
                        else {
                            arrAmount[0] = parseFloat(arrAmount[0]) + parseFloat(arTempAmount[0]);
                            arrCanDate[0] = arTempDate[0];
                            arrAmount[1] = parseFloat(arrAmount[1]) + parseFloat(arTempAmount[1]);
                            arrCanDate[1] = arTempDate[1];
                        }
                    }
                    else if (arTempAmount.length == 3) {
                        if (i == 0) {
                            arrAmount[0] = parseFloat(arTempAmount[0]);
                            arrCanDate[0] = arTempDate[0];
                            arrAmount[1] = parseFloat(arTempAmount[1]);
                            arrCanDate[1] = arTempDate[1];
                            arrAmount[2] = parseFloat(arTempAmount[2]);
                            arrCanDate[2] = arTempDate[2];
                        }
                        else {
                            arrAmount[0] = parseFloat(arrAmount[0]) + parseFloat(arTempAmount[0]);
                            arrCanDate[0] = arTempDate[0];
                            arrAmount[1] = parseFloat(arrAmount[1]) + parseFloat(arTempAmount[1]);
                            arrCanDate[1] = arTempDate[1];
                            arrAmount[2] = parseFloat(arrAmount[2]) + parseFloat(arTempAmount[2]);
                            arrCanDate[2] = arTempDate[2];
                        }
                    }
                }
                if (Status == "Booking") {
                    for (var j = 0; j < arrAmount.length; j++)
                        CancellationPolicy += "In the event of cancellation after <b>" + arrCanDate[j] + "</b>, <b><i class=\"fa fa-inr\"></i> " + numberWithCommas(arrAmount[j]) + "</b> will be applicable.<br />";
                }
                $("#dspCancellationPolicy").html(CancellationPolicy);
                $("#AgencyBookingCancelModal").modal('show');
            }
            else if (result.retCode == 0) {
                $('#AgencyBookingCancelModal').modal('hide')
                $('#SpnMessege').text("error occured while getting details on booking confirmation")
                $('#ModelMessege').modal('show')
               // alert("error occured while getting details on booking confirmation")
            }
        },
        error: function () {
            $('#AgencyBookingCancelModal').modal('hide')
            $('#SpnMessege').text("Error on confirmation popup");
            $('#ModelMessege').modal('show')
           // alert("Error on confirmation popup");
        }
    });
}


function ProceedToCancellation() {
    var Supplier = GetQueryStringParamsForAgentRegistrationUpdate('Supplier')
    debugger;
    if ($("#btnCancelBooking").val() == "Cancel Booking") {
        if ($("#hndIsCancelable").val() == "0") {
            $('#AgencyBookingCancelModal').modal('hide')
            $('#SpnMessege').text("Sorry! You cannot cancel this booking.")
            $('#ModelMessege').modal('show')
           // alert("Sorry! You cannot cancel this booking.")
        }
        else {
            var data = {
                ReservationID: $("#hndReservatonID").val(),
                ReferenceCode: $("#hndReferenceCode").val(),
                CancellationAmount: $("#hndCancellationAmount").val(),
                BookingStatus: $("#hndStatus").val(),
                Remark: $("#txtRemark").val(),
                TotalFare: $("#hdn_TotalFare").val(),
                ServiceCharge: $("#hdn_ServiceCharge").val(),
                Total: $("#hdn_Total").val(),
                Type: Supplier
            }
            $("#dlgLoader").css("display", "initial");
            $.ajax({
                type: "POST",
                url: "../HotelHandler.asmx/HotelCancelBooking",
                data: JSON.stringify(data),//'{"ReservationID":"' + $("#hndReservatonID").val() + '","ReferenceCode":"' + $("#hndReferenceCode").val() + '","CancellationAmount":"' + $("#hndCancellationAmount").val() + '","Remark":"' + $("#txtRemark").val() + '"}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.retCode == 1) {
                        $('#SpnMessege').text("Your booking has been cancelled. An Email has been sent with Cancelation Detail.");
                        $('#ModelMessege').modal('show')
                       // alert("Your booking has been cancelled. An Email has been sent with Cancelation Detail.");
                        $("#AgencyBookingCancelModal").modal('hide');
                        location.reload();
                        //$.ajax({
                        //    type: "POST",
                        //    url: "../EmailHandler.asmx/CancellationEmail",
                        //    data: '{"ReservationID":"' + ReserID + '","totalamount":"' + totalamount + '","checkin":"' + checkin + '","checkout":"' + checkout + '","bookingdate":"' + bookingdate + '","HotelName":"' + HotelName + '","PassengerName":"' + PassengerName + '","City":"' + City + '"}',
                        //    contentType: "application/json; charset=utf-8",
                        //    datatype: "json",
                        //    success: function (response) {
                        //        var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                        //        if (result.Session == 0) {
                        //            alert("Some error occured, Please try again in some time.");

                        //        }
                        //        if (result.retCode == 1) {
                        //            //alert("Email has been sent successfully.");


                        //        }
                        //        else if (result.retCode == 0) {
                        //            alert("Sorry Please Try Later.");


                        //        }

                        //    },
                        //    error: function () {
                        //        alert('Something Went Wrong');
                        //    }

                        //});

                        ///// email ends here...


                    }
                    else if (result.retCode == 0) {
                        $('#AgencyBookingCancelModal').modal('hide')
                        $('#SpnMessege').text(result.Message);
                        $('#ModelMessege').modal('show')
                       // alert(result.Message);


                    }
                },
                error: function () {
                    $('#AgencyBookingCancelModal').modal('hide')
                    $('#SpnMessege').text("something went wrong");
                    $('#ModelMessege').modal('show')
                   // alert("something went wrong");
                },
                complete: function () {
                    $("#dlgLoader").css("display", "none");
                }
            });
        }
    } else if ($("#btnCancelBooking").val() == "Confirm Booking") {
        debugger;
        if ($("#hndIsConfirmable").val() == "0") {
            $('#AgencyBookingCancelModal').modal('hide')
            $('#SpnMessege').text("Sorry! You cannot confirm this booking.")
            $('#ModelMessege').modal('show')
           // alert("Sorry! You cannot confirm this booking.")
        }
        else {
            var data = {
                ReservationID: $("#hndReservatonID").val()
            }
            $("#dlgLoader").css("display", "initial");
            $.ajax({
                type: "POST",
                url: "../HotelHandler.asmx/ConfirmHoldBooking",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.retCode == 1) {
                        $('#SpnMessege').text("Your booking has been confirmed.Confirmation email has been sent.!");
                        $('#ModelMessege').modal('show')
                        //alert("Your booking has been confirmed.Confirmation email has been sent.!");
                        $("#AgencyBookingCancelModal").modal('hide');
                        location.reload();

                        //here mail goes for hold relaese
                        debugger;
                        $.ajax({
                            type: "POST",
                            url: "../EmailHandler.asmx/HoldReleaseEmail",
                            data: '{"ReservationID":"' + ReserID + '","totalamount":"' + totalamount + '","checkin":"' + checkin + '","checkout":"' + checkout + '","bookingdate":"' + bookingdate + '","HotelName":"' + HotelName + '","PassengerName":"' + PassengerName + '","City":"' + City + '"}',
                            contentType: "application/json; charset=utf-8",
                            datatype: "json",
                            success: function (response) {
                                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                                if (result.Session == 0) {
                                    $('#AgencyBookingCancelModal').modal('hide')
                                    $('#SpnMessege').text("Some error occured, Please try again in some time.");
                                    $('#ModelMessege').modal('show')
                                   // alert("Some error occured, Please try again in some time.");

                                }
                                if (result.retCode == 1) {
                                    //alert("Email has been sent successfully.");


                                }
                                else if (result.retCode == 0) {
                                    $('#AgencyBookingCancelModal').modal('hide')
                                    $('#SpnMessege').text("Sorry Please Try Later.");
                                    $('#ModelMessege').modal('show')
                                   // alert("Sorry Please Try Later.");


                                }

                            },
                            error: function () {
                                $('#AgencyBookingCancelModal').modal('hide')
                                $('#SpnMessege').text('Something Went Wrong');
                                $('#ModelMessege').modal('show')
                                //alert('Something Went Wrong');
                            }

                        });
                        // Mail Ends Here....
                    }
                    else if (result.retCode == 0) {
                        $('#AgencyBookingCancelModal').modal('hide')
                        $('#SpnMessege').text(result.Message);
                        $('#ModelMessege').modal('show')
                      //  alert(result.Message);
                    }

                    else if (result.retCode == 2) {

                        //alert(result.Message);
                        $('#AgencyBookingCancelModal').modal('hide')
                        $("#dspAlertMessage").html("Your Balance is Insufficient to confirm this booking.");
                        $("#dspAlertMessage").css("display", "block");
                        //$("#AgencyBookingCancelModal").modal('hide');
                    }
                },
                error: function () {
                    $('#AgencyBookingCancelModal').modal('hide')
                    $('#SpnMessege').text("something went wrong during hold confirmation");
                    $('#ModelMessege').modal('show')
                   // alert("something went wrong during hold confirmation");
                },
                complete: function () {
                    $("#dlgLoader").css("display", "none");
                }
            });
        }
    }
}

//function ProceedToCancellation() {
//    var Supplier = GetQueryStringParams('Supplier')
//    debugger;
//    if ($("#btnCancelBooking").val() == "Cancel Booking") {
//        if ($("#hndIsCancelable").val() == "0") {
//            alert("Sorry! You cannot cancel this booking.")
//        }
//        else {
//            var data = {
//                ReservationID: $("#hndReservatonID").val(),
//                ReferenceCode: $("#hndReferenceCode").val(),
//                CancellationAmount: $("#hndCancellationAmount").val(),
//                BookingStatus: $("#hndStatus").val(),
//                Remark: $("#txtRemark").val(),
//                TotalFare: $("#hdn_TotalFare").val(),
//                ServiceCharge: $("#hdn_ServiceCharge").val(),
//                Total: $("#hdn_Total").val(),
//                Type: Supplier
//            }
//            $("#dlgLoader").css("display", "initial");
//            $.ajax({
//                type: "POST",
//                url: "../HotelHandler.asmx/HotelCancelBooking",
//                data: JSON.stringify(data),//'{"ReservationID":"' + $("#hndReservatonID").val() + '","ReferenceCode":"' + $("#hndReferenceCode").val() + '","CancellationAmount":"' + $("#hndCancellationAmount").val() + '","Remark":"' + $("#txtRemark").val() + '"}',
//                contentType: "application/json; charset=utf-8",
//                datatype: "json",
//                success: function (response) {
//                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
//                    if (result.retCode == 1) {
//                        alert("Your booking has been cancelled. An Email has been sent with Cancelation Detail.");
//                        $("#AgencyBookingCancelModal").modal('hide');
//                        location.reload();
//                        //$.ajax({
//                        //    type: "POST",
//                        //    url: "../EmailHandler.asmx/CancellationEmail",
//                        //    data: '{"ReservationID":"' + ReserID + '","totalamount":"' + totalamount + '","checkin":"' + checkin + '","checkout":"' + checkout + '","bookingdate":"' + bookingdate + '","HotelName":"' + HotelName + '","PassengerName":"' + PassengerName + '","City":"' + City + '"}',
//                        //    contentType: "application/json; charset=utf-8",
//                        //    datatype: "json",
//                        //    success: function (response) {
//                        //        var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
//                        //        if (result.Session == 0) {
//                        //            alert("Some error occured, Please try again in some time.");

//                        //        }
//                        //        if (result.retCode == 1) {
//                        //            //alert("Email has been sent successfully.");


//                        //        }
//                        //        else if (result.retCode == 0) {
//                        //            alert("Sorry Please Try Later.");


//                        //        }

//                        //    },
//                        //    error: function () {
//                        //        alert('Something Went Wrong');
//                        //    }

//                        //});

//                        ///// email ends here...


//                    }
//                    else if (result.retCode == 0) {

//                        alert(result.Message);


//                    }
//                },
//                error: function () {
//                    alert("something went wrong");
//                },
//                complete: function () {
//                    $("#dlgLoader").css("display", "none");
//                }
//            });
//        }
//    } else if ($("#btnCancelBooking").val() == "Confirm Booking") {
//        debugger;
//        if ($("#hndIsConfirmable").val() == "0") {
//            alert("Sorry! You cannot confirm this booking.")
//        }
//        else {
//            var data = {
//                ReservationID: $("#hndReservatonID").val()
//            }
//            $("#dlgLoader").css("display", "initial");
//            $.ajax({
//                type: "POST",
//                url: "../HotelHandler.asmx/ConfirmHoldBooking",
//                data: JSON.stringify(data),
//                contentType: "application/json; charset=utf-8",
//                datatype: "json",
//                success: function (response) {
//                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
//                    if (result.retCode == 1) {
//                        alert("Your booking has been confirmed.Confirmation email has been sent.!");
//                        $("#AgencyBookingCancelModal").modal('hide');
//                        location.reload();

//                        //here mail goes for hold relaese
//                        debugger;
//                        $.ajax({
//                            type: "POST",
//                            url: "../EmailHandler.asmx/HoldReleaseEmail",
//                            data: '{"ReservationID":"' + ReserID + '","totalamount":"' + totalamount + '","checkin":"' + checkin + '","checkout":"' + checkout + '","bookingdate":"' + bookingdate + '","HotelName":"' + HotelName + '","PassengerName":"' + PassengerName + '","City":"' + City + '"}',
//                            contentType: "application/json; charset=utf-8",
//                            datatype: "json",
//                            success: function (response) {
//                                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
//                                if (result.Session == 0) {
//                                    alert("Some error occured, Please try again in some time.");

//                                }
//                                if (result.retCode == 1) {
//                                    //alert("Email has been sent successfully.");


//                                }
//                                else if (result.retCode == 0) {
//                                    alert("Sorry Please Try Later.");


//                                }

//                            },
//                            error: function () {
//                                alert('Something Went Wrong');
//                            }

//                        });
//                        // Mail Ends Here....
//                    }
//                    else if (result.retCode == 0) {
//                        alert(result.Message);
//                    }

//                    else if (result.retCode == 2) {
//                        //alert(result.Message);
//                        $("#dspAlertMessage").html("Your Balance is Insufficient to confirm this booking.");
//                        $("#dspAlertMessage").css("display", "block");
//                        //$("#AgencyBookingCancelModal").modal('hide');
//                    }
//                },
//                error: function () {
//                    alert("something went wrong during hold confirmation");
//                },
//                complete: function () {
//                    $("#dlgLoader").css("display", "none");
//                }
//            });
//        }
//    }
//}

function CancelledBooking() {
    $('#AgencyBookingCancelModal').modal('hide')
    $('#SpnMessege').text("This booking is already cancelled.");
    $('#ModelMessege').modal('show')
   // alert("This booking is already cancelled.");
}

function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}