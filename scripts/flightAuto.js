﻿var tpj = jQuery;
var CurrencyClass = 'fa fa-inr';
var pageLoad = true;
var lockMultiwayRadio = false;
var inz = 0;
var dcount = 1;
var today1;
var minus = false;



tpj(function () {
    tpj("#datepicker_Return").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy",
        minDate: "dateToday",
    });

    tpj("#dt_CalendarMonthYear").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy",
        minDate: "dateToday",
    });
})
$(function () {
    //function to create div for entering origin city and destination city and onward date. 
    AddStops();
});

function OneWayRadioFunc() {
    var origin0 = $("#origin0").val();
    var destn0 = $("#destination0").val();
    $('#returnDateDivId').hide();
    $("#addDestinationDivId").hide();
    $("#removeLastDestnDivId").hide();
    $("#fromToOnwardDateDiv").empty();
    $(".RoundWay").hide();
    $('#GeneralSearch').show();
    $('#CalendarSearch').hide();
    inz = 0;
    dcount = 1;
    pageLoad = true;
    AddStops();
    $("#origin0").val(origin0);
    $("#destination0").val(destn0);
    lockMultiwayRadio = false;
    $("#dte_OnwardDateId0").val("");
}

function RoundWayRadioFunc() {
    var origin0 = $("#origin0").val();
    var destn0 = $("#destination0").val();
    $("#addDestinationDivId").hide();
    $("#removeLastDestnDivId").hide();
    $("#fromToOnwardDateDiv").empty();
    $('#GeneralSearch').show();
    $('#CalendarSearch').hide();
    $(".RoundWay").show();
    inz = 0;
    dcount = 1;
    pageLoad = true;
    AddStops();
    $('#returnDateDivId').show();
    $("#origin0").val(origin0);
    $("#destination0").val(destn0);
    lockMultiwayRadio = false;
}

function MultiWayRadioFunc() {
    if (lockMultiwayRadio == false) {
        lockMultiwayRadio = true;
        inz++;
        dcount++;
        AddStops();
        $("#swap").hide();
        $(".RoundWay").hide();
        $('#returnDateDivId').hide();
        $("#dte_OnwardDateId0").val("");
        $('#GeneralSearch').show();
        $('#CalendarSearch').hide();
    }
    else {
        return false;
    }
}

//function addDestination() {
//    inz++;
//    dcount++;
//    if (inz < 3) {
//        AddStops();
//    }
//    else {
//        alert("Maximum destination limit reached.");
//    }
//}


function CalendarFareRadioFunc() {
    $(".RoundWay").hide();
    $('#GeneralSearch').hide();
    $('#CalendarSearch').show();

    tpj("#CalenderFrom").autocomplete({                             //autocomplete for 'origin'
        source: function (request, response) {
            tpj.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "handler/FlightHandler.asmx/GetCityList",
                data: "{'name':'" + request.term + "'}",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    response(result);
                },
                error: function (result) {
                    alert("No Match");
                }
            });
        },
        minLength: 3,
        select: function (event, ui) {
            debugger;
            tpj('#hdCalenderFromCode').val(ui.item.id);
        }
    });

    tpj("#CalenderTo").autocomplete({                             //autocomplete for 'destination'
        source: function (request, response) {
            tpj.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "handler/FlightHandler.asmx/GetCityList",
                data: "{'name':'" + request.term + "'}",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    response(result);
                },
                error: function (result) {
                    alert("No Match");
                }
            });
        },
        minLength: 3,
        select: function (event, ui) {
            tpj('#hdCalenderToCode').val(ui.item.id);
        }
    });
}


function addDestination() {
    minus = true;
    inz = 2;
    dcount = 3;
    AddStops();
    minus = false;
}

function destnToNextOrigin(k) {
    var toOrigin = $("#destination" + k).val();
    k++;
    $("#origin" + k).val(toOrigin);
}

function swapCities() {
    var origin = $("#origin0").val();
    var destn = $("#destination0").val();
    $("#origin0").val(destn);
    $("#destination0").val(origin);
}

function removeLastDestn() {
    $("#row2").remove();
    $("#removeLastDestnDivId").hide();
    $("#addDestinationDivId").show();
}

function AddStops() {
    var div = '';
    for (var i = inz; i < dcount; i++) {
        var j = i - 1;

        if (pageLoad !== true) {
            if (minus !== true) {
                $("#addDestinationDivId").show();
            } else {
                $("#addDestinationDivId").hide();
                $("#removeLastDestnDivId").show();
            }
        }

        //div +='<div class="col-md-8">'
        div += '<div class="form-group row" id="row' + i + '" >'
        div += '</br>'
        div += '<div class="col-md-4">'
        div += '<span class="text-left" style="color: #838383">From</span>'
        div += '<input id="origin' + i + '" type="text" class="form-control" placeholder="Leaving From " />'
        div += '</div>'
        div += '<div class="col-md-4">'
        div += '<span class="text-left" style="color: #838383">To</span>'
        div += '<input id="destination' + i + '" type="text" class="form-control" onblur="destnToNextOrigin(' + i + ')" placeholder="Going To " />'
        div += '</div>'
        div += ' <div class="col-md-4">'
        div += '<span class="text-left" style="color: #838383">Onward Date</span>'
        div += '<div class="datepicker-wrap"><input type="text" id="dte_OnwardDateId' + i + '" name="date_from" class="form-control" placeholder="Onward Date" /></div>'
        div += '</div>'
        //div += '</div>'
        div += '</br>'
        $("#fromToOnwardDateDiv").append(div);

        if (pageLoad !== true) {
            var firstDestnValue = $("#destination" + j).val();
            $("#origin" + i).val(firstDestnValue);
        }
        if (pageLoad == true) {
            pageLoad = false;
        }

        tpj("#origin" + i).autocomplete({                             //autocomplete for 'origin'
            source: function (request, response) {
                tpj.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "handler/FlightHandler.asmx/GetCityList",
                    data: "{'name':'" + request.term + "'}",
                    dataType: "json",
                    success: function (data) {
                        var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                        response(result);
                    },
                    error: function (result) {
                        alert("No Match");
                    }
                });
            },
            minLength: 3,
            select: function (event, ui) {
                debugger;
                tpj('#hdoriginCode' + i).val(ui.item.id);
            }
        });

        tpj("#destination" + i).autocomplete({                             //autocomplete for 'destination'
            source: function (request, response) {
                tpj.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "handler/FlightHandler.asmx/GetCityList",
                    data: "{'name':'" + request.term + "'}",
                    dataType: "json",
                    success: function (data) {
                        var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                        response(result);
                    },
                    error: function (result) {
                        alert("No Match");
                    }
                });
            },
            minLength: 3,
            select: function (event, ui) {
                tpj('#hddestCode' + i).val(ui.item.id);
            }
        });

        tpj("#dte_OnwardDateId" + i).datepicker({
            changeMonth: true,
            changeYear: true,
            onSelect: insertDepartureDate,
            dateFormat: "dd-mm-yy",
            minDate: "dateToday",
        });
    }
}

//var prevAdultCount = [1];
//var prevChildrenCount = [0];
//var prevInfantCount = [0];

//function travellersLimitCheck() {
//    var adults = $("#sel_Adults").val();
//    var children = $("#sel_Child").val();
//    var infants = $("#sel_Infant").val();

//    adults = Number(adults);
//    children = Number(children);
//    infants = Number(infants);

//    prevAdultCount.push(adults);
//    prevChildrenCount.push(children);
//    prevInfantCount.push(infants);

//    if (adults < infants) {
//        var flag;
//        if (prevAdultCount[prevAdultCount.length - 1] !== prevAdultCount[prevAdultCount.length - 2]) {
//            for (var i = prevAdultCount.length - 1; i > 0 || i == 0; i--) {
//                if (infants < prevAdultCount[i] || infants == prevAdultCount[i]) {
//                    $("#sel_Adults").val(prevAdultCount[i]);
//                    break;
//                }
//            }
//            var a = prevAdultCount.pop();
//        } else {

//            for (var i = prevInfantCount.length - 1; i > 0 || i == 0; i--) {
//                if (adults > prevInfantCount[i] || adults == prevInfantCount[i]) {
//                    $("#sel_Infant").val(prevInfantCount[i]);
//                    break;
//                }
//            }
//            var b = prevInfantCount.pop();
//        }

//        var flag = true;
//        alert("Infants cannot be more than adults.");
//    } else {
//        var flag = false;
//    }

//    if (flag !== true) {
//        var totalTravellers = adults + children + infants;
//        if (totalTravellers > 9) {
//            if (prevAdultCount[prevAdultCount.length - 1] !== prevAdultCount[prevAdultCount.length - 2]) {
//                $("#sel_Adults").val(prevAdultCount[prevAdultCount.length - 2]);
//                var a = prevAdultCount.pop();
//            }
//            if (prevChildrenCount[prevChildrenCount.length - 1] !== prevChildrenCount[prevChildrenCount.length - 2]) {
//                $("#sel_Child").val(prevChildrenCount[prevChildrenCount.length - 2]);
//                var b = prevChildrenCount.pop();
//            }
//            if (prevInfantCount[prevInfantCount.length - 1] !== prevInfantCount[prevInfantCount.length - 2]) {
//                $("#sel_Infant").val(prevInfantCount[prevInfantCount.length - 2]);
//                var c = prevInfantCount.pop();
//            }
//            alert("Maximum travellers limit is 9.");
//        }
//    }
//}

function insertDepartureDate(value, date, inst) {
    debugger;
    if (radio02.checked) {
        var firstDate = new Date(tpj('#dte_OnwardDateId0').datepicker("getDate"));
        var dateAdjust = 1;
        var current_date = new Date();

        current_time = current_date.getTime();
        days = (firstDate.getTime() - current_time) / (1000 * 60 * 60 * 24);
        if (days < 0) {
            var add_day = 1;
        } else {
            var add_day = 2;
        }
        days = parseInt(days);
        tpj('#datepicker_Return').datepicker("option", "minDate", days + add_day);
        tpj('#datepicker_Return').datepicker("option", "maxDate", days + 365);
        dateAdjust = parseInt(dateAdjust);
        var secondDate = new Date(firstDate.getFullYear(), firstDate.getMonth(), firstDate.getDate() + dateAdjust);
        tpj('#datepicker_Return').datepicker('setDate', secondDate);
    }
    else if (radio03.checked) {
        var ID = parseInt(date.id.split('Id')[1]) + 1;
        var firstDate = new Date(tpj('#' + date.id).datepicker("getDate"));
        var dateAdjust = 1;
        var current_date = new Date();
        for (var i = ID ; i < inz; i++) {
            tpj('#dte_OnwardDateId' + i).val("");
        }
        current_time = current_date.getTime();
        days = (firstDate.getTime() - current_time) / (1000 * 60 * 60 * 24);
        if (days < 0) {
            var add_day = 1;
        } else {
            var add_day = 2;
        }
        days = parseInt(days);
        tpj('#dte_OnwardDateId' + ID).datepicker("option", "minDate", days + add_day);
        tpj('#dte_OnwardDateId' + ID).datepicker("option", "maxDate", days + 365);
        dateAdjust = parseInt(dateAdjust);
        var secondDate = new Date(firstDate.getFullYear(), firstDate.getMonth(), firstDate.getDate() + dateAdjust);
        tpj('#dte_OnwardDateId' + ID).datepicker('setDate', secondDate);
    }
}
