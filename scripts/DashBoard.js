﻿var hiddensid;
var arrAgentDetails = new Array();
$(function () {
    GetCity("IND");
    GetProfile();
    BookingList();
    PackageBookingList();
    GetFlightsBookings()
});

function Update_Click() {
    $("#lbl_txtMobileNumber").css("display", "none");
    $("#lbl_txtPhone").css("display", "none");
    $("#lbl_selCity").css("display", "none");
    $("#lbl_txtAddress").css("display", "none");


    $("#lbl_txtMobileNumber").html("* This field is required.");
    $("#lbl_txtPhone").html("* This field is required.");
    $("#lbl_selCity").html("* This field is required.");
    $("#lbl_txtAddress").html("* This field is required.");
    var reg = new RegExp('[0-9]$');
    var bValid = true;
    var Mobile = $('#txtMobileNumber').val();
    var Phone = $('#txtPhone').val();
    var city = $('#selCity option:selected').val();
    var Address = $('#txtAddress').val();
    if (Address == "") {
        bValid = false;
        $("#lbl_txtAddress").css("display", "");
    }
    if (Address != "" && reg.test(Address)) {
        bValid = false;
        $("#lbl_txtAddress").html("* Address must not be numeric at end.");
        $("#lbl_txtAddress").css("display", "");
    }
    if (city == "-") {
        bValid = false;
        $("#lbl_selCity").css("display", "");
    }
    if (Mobile == "") {
        bValid = false;
        $("#lbl_txtMobileNumber").css("display", "");
    }

    else {
        if (!(reg.test(Mobile))) {
            bValid = false;
            $("#lbl_txtMobileNumber").html("* Mobile no must be numeric.");
            $("#lbl_txtMobileNumber").css("display", "");
        }
    }
    if (Phone == "") {
        Phone = '0';
    }
    else {
        if (!(reg.test(Phone))) {
            bValid = false;
            $("#lbl_txtPhone").html("* Phone no must be numeric.");
            $("#lbl_txtPhone").css("display", "");
        }
    }

    if (bValid == true) {
        $.ajax({
            type: "POST",
            url: "DashBoardHandler.asmx/UpdateDetails",
            data: '{"Mobile":"' + Mobile + '","Phone":"' + Phone + '","City":"' + city + '","Address":"' + Address + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    alert("Customer details updated successfully")
                }
                if (result.retCode == 0) {
                    alert("Something went wrong!")
                }
            },
            error: function () {
                alert("An error occured while updating Customer details");
            }
        });
    }


}

function ChangePassword() {
    debugger;
    $("#lbl_txtOldPassword").css("display", "none");
    $("#lbl_txtNewPassword").css("display", "none");
    $("#lbl_txtConfirmNewPassword").css("display", "none");

    $("#lbl_txtOldPassword").html("* This field is required.");
    $("#lbl_txtNewPassword").html("* This field is required.");
    $("#lbl_txtConfirmNewPassword").html("* This field is required.");


    var Old = $('#txtOldPassword').val()
    var New = $('#txtNewPassword').val()
    var Confirm = $('#txtConfirmNewPassword').val()


    var bValid = true;

    if (Old == "") {

        bValid = false;
        $("#lbl_txtOldPassword").css("display", "");

        //alert("Please Enter old password");
        //return false
    }

    if (New == "") {

        bValid = false;
        $("#lbl_txtNewPassword").css("display", "");

        //alert("Please Enter new password");
        //return false
    }
    if (Confirm == "") {

        bValid = false;
        $("#lbl_txtConfirmNewPassword").html("* This field is required.");
        $("#lbl_txtConfirmNewPassword").css("display", "");

        //alert("Please Enter confirm password");
        //return false
    }
    else if (New != Confirm) {

        bValid = false;
        $("#lbl_txtConfirmNewPassword").html("Confirm Password did not matched!");
        $("#lbl_txtConfirmNewPassword").css("display", "");

        //alert("Password Does not match!")
        //return false
    }
    if (bValid == true) {
        var data = { Old: Old, NewPassword: New }

        $.ajax({
            type: "POST",
            url: "DashBoardHandler.asmx/ChangePassword",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var obj = JSON.parse(response.d);
                if (obj.Retcode == 1) {
                    $('#txtOldPassword').val('')
                    $('#txtNewPassword').val('')
                    $('#txtConfirmNewPassword').val('')
                    alert("Password change Successfully.")
                    //window.location.href = "../User/User.aspx";
                }

                if (obj.Retcode == 2) {
                    alert("Old Password is wrong!")
                }

                if (obj.Retcode == 0) {
                    alert("Something Went Wrong.")
                }
            }
        });
    }
}

function GetHotelPrintInvoice(ReservationID, Uid, Status, Type) {
    debugger;
    var win = window.open('ViewInvoice.aspx?ReservationId=' + ReservationID + '&Uid=' + Uid + '&Status=' + Status + '&Supplier=' + Type, '_blank');
}

function BookingList() {
    debugger;
    SearchBit = false;
    $.ajax({
        type: "POST",
        url: "DashBoardHandler.asmx/BookingList",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var arrAgentList = result.ReservationDetails;
                var PageCount = result.Count;
                if (arrAgentList) {
                    //  if (arrAgentList != 0) {
                    $('#lblStatus').css("display", "none");
                    var tRow = '';
                    var Supp = '';
                    for (var i = 0; i < arrAgentList.length; i++) {
                        UID = arrAgentList[i].Uid;
                        var strCin = arrAgentList[i].CheckIn;
                        var ret1 = strCin.split(" ");
                        var CheckIn = ret1[0];

                        var strCout = arrAgentList[i].CheckOut;
                        var ret2 = strCout.split(" ");
                        var CheOut = ret2[0];

                        var Reservation = arrAgentList[i].Date;
                        var ret3 = Reservation.split(" ");
                        var ReservationDate = ret3[0];

                        var DeadlineDate = arrAgentList[i].DeadLine;
                        var DD = DeadlineDate.split(" ");

                        var sts = arrAgentList[i].Status;
                        sts.replace("Booking", "On Hold")
                        var Type = result.Type;
                        if (Type == "HotelBeds")
                            Type = "A"

                        if (Type == "MGH")
                            Type = "B"

                        if (Type == "DoTW")
                            Type = "C"

                        tRow += '<tr>';
                        tRow += '<td align="center">' + (i + 1) + '</td>';
                        //tRow += '<td align="center" style="width:10%">' + arrAgentList[i].VoucherId + '</td>';
                        tRow += '<td align="center">' + ReservationDate + '</td>';
                        tRow += '<td align="center">' + arrAgentList[i].HotelName + "/" + arrAgentList[i].City + '</td>';
                        tRow += '<td align="center">' + arrAgentList[i].Passenger + '</td>';
                        tRow += '<td align="center">' + CheckIn + '</td>';
                        tRow += '<td align="center">' + CheOut + '</td>';
                        tRow += '<td align="center">' + sts.replace("Booking", "On Hold") + '</td>';
                        tRow += '<td>' + DD[0] + '</td>';
                        tRow += '<td align="center">' + arrAgentList[i].TotalFare + '</td>';
                        tRow += '<td align="center" style="width:8%;"><a style="cursor:pointer" title="Invoice" onclick="GetHotelPrintInvoice(\'' + arrAgentList[i].ReservationId + '\',\'' + arrAgentList[i].B2C_Id + '\',\'' + arrAgentList[i].Status + '\',\'' + Type + '\')">View</a></td>';
                        tRow += '<td align="center" style="width:8%;"><a style="cursor:pointer" title="Voucher" onclick="ViewVoucher(\'' + arrAgentList[i].ReservationId + '\',\'' + arrAgentList[i].B2C_Id + '\',\'' + arrAgentList[i].Status + '\',\'' + Type + '\')">View</a></td>';
                        tRow += '</tr>';
                    }
                    $("#tbl_Invoice tbody").empty();

                    $("#tbl_Invoice tbody").append(tRow);


                    debugger;
                    $('#Upagination').empty();
                    var paginate = PageCount;
                    var count = paginate / 10;

                    var Num = count.toString();

                    var Arr;
                    Arr = Num.split('.');

                    var Pagination = '';
                    if (parseInt(Arr[1]) > 0) {
                        Pagination = (parseInt(Arr[0]) + 1);
                    }

                    else {
                        Pagination = parseInt(Arr[0]);
                    }
                    var ul = '';
                    var skip = 0;
                    var take = 10;
                    ul += ''

                    ul += '<nav>';
                    ul += '<ul class="pagination" style="cursor: pointer">';
                    ul += '<li onclick=Previous()><a  aria-label="Previous"><span aria-hidden="true">&laquo;</span> </a></li>';


                    for (var p = 0; p < Pagination; p++) {
                        if (p != 0) {
                            skip = (skip + 10);
                            take = (take + 10);
                            Limit = skip;
                        }
                        ul += '<li onclick="PaginationAll(' + skip + ',' + take + ')"><a>' + (p + 1) + '</a></li>';
                    }
                    ul += '<li onclick=Next()><a aria-label="Next"><span aria-hidden="true">&raquo;</span> </a></li>';
                    ul += '</ul>';
                    ul += '</nav>';

                    $('#Upagination').append(ul);
                }

                else {
                    $('#Upagination').empty();
                    $("#tbl_Invoice tbody").empty();
                    tRow += '<tr >'
                    tRow += '<td  colspan="13" align="center"> No Record Found'
                    tRow += '</td>'
                    tRow += '</tr>'
                    $("#tbl_Invoice tbody").append(tRow);

                }
            }

            if (result.retCode == 0) {
                $('#SpnMessege').text('Something Went Wrong');
                $('#ModelMessege').modal('show')
                // alert('Something Went Wrong');
            }

            if (result.retCode == 2) {
                $('#Upagination').empty();
                $("#tbl_Invoice tbody").empty();
                tRow += '<tr>'
                tRow += '<td align="center" colspan="12"> No Record Found'
                tRow += '</td>'
                tRow += '</tr>'
                $("#tbl_Invoice tbody").append(tRow);

            }
        },

        error: function () {
        }
    });
}

function PackageBookingList() {


    $.ajax({
        type: "POST",
        url: "DashBoardHandler.asmx/PackageBooking",
        data: '',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0 && result.retCode == 0) {
                alert("Your session has been expired!")
                window.location.href = "Default.aspx";
            }
            else if (result.retCode == 1) {
                var PackageBooking = result.Arr;
                var tRow = '';
                for (var i = 0; i < PackageBooking.length; i++) {
                    tRow += '<tr>';
                    tRow += '<td align="center">' + (i + 1) + '</td>';
                    tRow += '<td align="center">' + PackageBooking[i].TravelDate + '</td>';
                    tRow += '<td align="center">' + PackageBooking[i].PaxName + '</td>';
                    tRow += '<td align="center">' + PackageBooking[i].PackageName + '</td>';
                    tRow += '<td align="center">' + PackageBooking[i].CatID + '</td>';
                    tRow += '<td align="center">' + PackageBooking[i].Location + '</td>';
                    tRow += '<td align="center">' + PackageBooking[i].StartDate + '</td>';
                    tRow += '<td align="center">' + PackageBooking[i].EndDate + '</td>';
                    tRow += '</tr>';
                }
                $('#tbl_Package tbody').append(tRow);
            }
        },
        error: function () {
            alert("An error occured while loading agent details")
        }
    });
}

function GetProfile() {
    $.ajax({
        type: "POST",
        url: "DashBoardHandler.asmx/GetProfile",
        data: '',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0 && result.retCode == 0) {
                alert("Your session has been expired!")
                window.location.href = "../Default.aspx";
            }
            else if (result.retCode == 1) {
                var B2C_Id = result.B2C_Id;
                var UserName = result.UserName;
                var Email = result.Email;
                var Mobile = result.Mobile;
                var Phone = result.Phone;
                var City = result.City;
                var Address = result.Address;
                $('#txtUserName').val(UserName);
                $('#txtB2C_Id').val(B2C_Id);
                $('#txtMobileNumber').val(Mobile);
                $('#txtPhone').val(Phone);
                $('#txtAddress').val(Address);

                setTimeout(function () {
                    debugger;
                    $("#selCity option").filter(function () {
                        return $(this).val() == City;
                    }).prop("selected", true);
                }, 1500);
            }
        },
        error: function () {
            alert("An error occured while loading agent details")
        }
    });
}

function ViewVoucher(ReservationID, Uid, Status) {
    debugger;
    if (Status == 'Vouchered' || Status == 'Cancelled') {



        //if (Type == "HotelBeds") {
        //    Type = "A"
        //}

        //if (Type == "MGH") {
        //    Type = "B"
        //}

        //if (Type == "DoTW") {
        //    Type = "C"
        //}



        //var win = window.open('ViewInvoice.aspx?ReservationId=' + ReservationID + '&Uid=' + Uid + '&Status=' + Status, '_blank');
        var win = window.open('ViewVoucher.aspx?ReservationId=' + ReservationID + '&Uid=' + Uid + '&Status=' + Status);
        //window.open('../Agent/Invoice.html?ReservationId=' + ReservationID + '&Status=' + Status + '&Uid=' + Uid + '&Type=' + Type, 'tester', 'left=50000,top=50000,width=800,height=600');
    }
    else {
        $('#SpnMessege').text('Please Confirm Your Booking!')
        $('#ModelMessege').modal('show')
        //  alert('Please Confirm Your Booking!')
    }
}

//$(function () {
//    GetFlightsBookings()
//});
var arrFlightsName = new Array();
arrFlightsName.push({ ID: "SG", Value: "Spice Jet", Type: ["N", "L"] },
                { ID: "6E", Value: "Indigo", Type: ["N", "L"] },
                { ID: "G8", Value: "Go Air", Type: ["N", "L"] },
                { ID: "G9", Value: "Air Arabia", Type: ["N"] },
                { ID: "FZ", Value: "Fly Dubai", Type: ["N"] },
                { ID: "IX", Value: "Air India Express", Type: ["N"] },
                { ID: "AK", Value: "Air Asia", Type: ["N"] },
                { ID: "LB", Value: "Air Costa", Type: ["N"] },
                { ID: "UK", Value: "Air Vistara", Type: ["N", "G"] },
                { ID: "AI", Value: "Air India", Type: ["G"] },
                { ID: "9W", Value: "Jet Airways", Type: ["G"] },
                { ID: "S2", Value: "JetLite", Type: ["G"] }
                    );
function GetFlightsBookings() {
    $.ajax({
        type: "POST",
        url: "../handler/FlightHandler.asmx/GetBookingList",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrAirTicket = result.arrAirTicket;
                GenrateHtml();
            }
        },
        error: function () {
            $('#SpnMessege').text("An error occured while loading countries")
            $('#ModelMessege').modal('show')
        }
    });
}

function CancelBooking(BookingID, Type) {
    var data = {
        BookingID: BookingID,
        Type: Type
    }
    $.ajax({
        type: "POST",
        url: "../handler/FlightHandler.asmx/CancelBooking",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {

            }
        },
        error: function () {
            $('#SpnMessege').text("An error occured while cancel booking")
            $('#ModelMessege').modal('show')
        }
    });
}

function GetPrintInvoice(ReservationID, Uid, Status) {
    debugger;
    if (Status == 'Tiketed' || Status == 'Cancelled') {
        //var win = window.open('ViewInvoice.aspx?ReservationId=' + ReservationID + '&Uid=' + Uid + '&Status=' + Status, '_blank');
        var win = window.open('FlightInvoice.aspx?ReservationId=' + ReservationID + '&Uid=' + Uid + '&Status=' + Status, '_blank');
        //window.open('../Agent/Invoice.html?ReservationId=' + ReservationID + '&Status=' + Status + '&Uid=' + Uid + '&Type=' + Type, 'tester', 'left=50000,top=50000,width=800,height=600');
    }
    else {
        $('#SpnMessege').text('Please Confirm Your Booking to get Invoice !!')
        $('#ModelMessege').modal('show')
        //  alert('Please Confirm Your Booking!')
    }
}

function SearchFlights() {
    var data = {
        BooingStatus: $("#Sel_Type").val(),
        sArrival: $("#datepicker3").val(),
        sDeparture: $("#datepicker4").val(),
        Airlines: $("#sel_Airlines").val(),
        Agency: 0,
        InvoiceDate: $("#datepicker5").val()
    };
    $.ajax({
        type: "POST",
        url: "../handler/FlightHandler.asmx/SearchBookingList",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrAirTicket = result.arrAirTicket;
                GenrateHtml();
            }
        },
        error: function () {
            $('#SpnMessege').text("An error occured while Searching")
            $('#ModelMessege').modal('show')
        }
    });
}

function GenrateHtml() {
    $("#tbl_AirTicket tbody tr").remove();
    var html = '';
    for (var i = 0; i < arrAirTicket.length; i++) {
        if (arrAirTicket[i].TicketStatus == "1")
            arrAirTicket[i].TicketStatus = "Tiketed"
        else if (arrAirTicket[i].TicketStatus == "")
            arrAirTicket[i].TicketStatus = "Hold"
        var FlightName = $.grep(arrFlightsName, function (p) { return p.ID == arrAirTicket[i].AirlineName; })
                         .map(function (p) { return p.Value; });
        html += '<tr><td align="center">' + parseInt(i + 1) + '</td>';
        html += '<td align="center">' + arrAirTicket[i].InvoiceNo + '</td>';
        html += '<td align="center">' + FlightName[0] + '</td>';
        html += '<td align="center">' + arrAirTicket[i].LeadingPaxName + '</td>';
        html += '<td align="center">' + arrAirTicket[i].ArrivalDate + '</td>';
        html += '<td align="center">' + arrAirTicket[i].DepartureDate + '</td>';
        html += '<td align="center">' + arrAirTicket[i].TicketStatus + '</td>';
        html += '<td align="center">' + arrAirTicket[i].InvoiceAmount + '</td>';
        html += '<td align="center"><a style="cursor:pointer" onclick="GetPrintInvoice(\'' + arrAirTicket[i].BookingID + '\',\'' + arrAirTicket[i].uid + '\',\'' + arrAirTicket[i].TicketStatus + '\')">View</a></td>';
        if (arrAirTicket[i].TicketStatus == "Tiketed") {
            html += '<td><a style="cursor:pointer" onclick="CancelBooking(\'' + arrAirTicket[i].BookingID + '\',\'' + arrAirTicket[i].TicketStatus + '\')" ><span class="glyphicon glyphicon-remove-sign"></span></a></td></tr>';
        }
        else if (arrAirTicket[i].TicketStatus == "Hold") {
            html += '<td><a style="cursor:pointer" onclick="CancelBooking(\'' + arrAirTicket[i].BookingID + '\',\'' + arrAirTicket[i].TicketStatus + '\')" ><span class="glyphicon glyphicon-remove-sign"></span></a></td></tr>';
        }
    }
    $("#tbl_AirTicket").append(html)
}