﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="ConfirmCardBooking.aspx.cs" Inherits="CUTUK.ConfirmCardBooking" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="https://cdn.worldpay.com/v1/worldpay.js"></script>
   
    <script type='text/javascript'>
        window.onload = function () {
            Worldpay.useTemplateForm({
                'clientKey': 'your-test-client-key',
                'form': 'paymentForm',
                'paymentSection': 'paymentSection',
                'display': 'inline',
                'callback': function (obj) {
                    if (obj && obj.token) {
                        var _el = document.createElement('input');
                        _el.value = obj.token;
                        _el.type = 'hidden';
                        _el.name = 'token';
                        document.getElementById('paymentForm').appendChild(_el);
                        document.getElementById('paymentForm').submit();
                    }
                }
            });
        }
    </script>
    <script src="scripts/ConfirmCardBooking.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <br />
    <br />
    <br />
    <div class="container">
        <div>
            <a class="homebtn left" href="#"></a>
            <div class="left">
                <ul class="bcrumbs">
                    <li>/</li>
                    <li id="aBack"></li>
                </ul>
            </div>
            <a class="backbtn right" href="#"></a>
        </div>
        <div class="clearfix"></div>
        <div class="brlines"></div>
    </div>
    <!-- CONTENT -->
    <div class="container">
        <div class="container mt25 offset-0">
            <div class="col-md-8 pagecontainer2 offset-0">
                <div class="padding30 grey">
                    <label>Are you sure to proceed?</label>
                    <%--                    <form action="https://secure-test.worldpay.com/wcc/purchase" method="post">

                        <!--Makes it test mode-->
                        <input type="hidden" name="testMode" value="100" />

                        <!-- This next line contains a mandatory parameter. Put your Installation ID inside the quotes after value= -->
                        <input type="hidden" id="instId" name="instId" />

                        <!-- Another mandatory parameter. Put your own reference identifier for the item purchased inside the quotes after value= -->
                        <input type="hidden" name="cartId" value="100" />

                        <!-- Another mandatory parameter. Put the total cost of the item inside the quotes after value= -->
                        <input type="hidden" id="amount" name="amount" />

                        <!-- Another mandatory parameter. Put the code for the purchase currency inside the quotes after value= -->
                        <input type="hidden" name="currency" value="GBP" />

                        <!-- This creates the button. When it is selected in the browser, the form submits the purchase details to us. -->

                        <input type="submit" class="btn-search4 margtop20" value="Buy This" />
                    </form>--%>


                    <form action="/complete" id="paymentForm" method="post">
                        <!-- all other fields you want to collect, e.g. name and shipping address -->
                        <div id='paymentSection'></div>
                        <div>
                            <input type="submit" value="Place Order" onclick="Worldpay.submitTemplateForm()" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <br />
    <br />
    <br />
    <br />
</asp:Content>
