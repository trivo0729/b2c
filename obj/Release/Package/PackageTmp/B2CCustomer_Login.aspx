﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="B2CCustomer_Login.aspx.cs" Inherits="CUTUK.B2CCustomer_Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   <script src="scripts/CountryCityCode.js"></script>
     <script src="scripts/B2CCustomerLogin.js"></script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">

      <div class="container breadcrub" style="margin-top:120px">
        <div>
            <a class="homebtn left" href="Default.aspx"></a>
            <div class="left">
                <ul class="bcrumbs">
                    <%--  <li>/</li>
                  <li><a href="Default.aspx">Home</a></li>
                    <li>/</li>--%>
                    <li><a href="B2CCustomer_Login.aspx">Customer Login</a></li>
                </ul>
            </div>
            <a class="backbtn right" href="#"></a>
        </div>
        <div class="clearfix">
        </div>
        <div class="brlines">
        </div>
    </div>

      <div class="container">
        <div class="container mt25 offset-0">
            <!-- CONTENT -->
            <div class="col-md-12 pagecontainer2 offset-0">
                <div class="hpadding50c">
                    <div class="tab-pane padding10 active" id="profile">

                        <!-- Admin top -->

                        <div class="col-md-4">
                        </div>

                        <!-- End of Admin top -->

                        <div class="clearfix"></div>

                        <div class="col-md-12 offset-0">
                            <div class="col-md-12">
                        <form id="formAgent" runat="server" enctype="multipart/form-data" method="post">
                            <%--<span class="opensans size24 slim">Register Agent</span><br />--%>
                            <div class="tab-pane padding10 active" id="profile">
                                <div class="col-md-12 offset-0">
                                    <div class="clearfix"></div>

                                    <span class="size16 bold">Personal details</span>
                                    <div class="line2"></div>

                                    <div class="row">
                                        <br />
                                          <div class="col-md-4">
                                            <span class="size13 dark">B2C_Id : </span>
                                            <input type="text" id="txt_B2C_Id" data-trigger="focus" data-placement="top" placeholder="" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your B2C_Id" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_B2C_Id">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-4">
                                            <span class="size13 dark">User Name : </span>
                                            <input type="text" id="txt_UserName" data-trigger="focus" data-placement="top" placeholder="User Name" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your User Name" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_UserName">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-4">
                                            <span class="size13 dark">Password: </span>
                                            <input type="text" id="txt_Password" data-trigger="focus" data-placement="top" placeholder="Password" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your Password" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_Password">
                                                <b>* This field is required</b></label>
                                        </div>
                                         <div class="col-md-4">
                                            <br />
                                            <span class="size13 dark">Email : </span>
                                            <input type="text" id="txt_Email" data-trigger="focus" data-toggle="popover" data-placement="top" placeholder="Email" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your Email" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_Email">
                                                <b>* This field is required</b></label>
                                        </div>
                                      
                                        <div class="col-md-4">
                                            <br />
                                            <span class="size13 dark">Mobile : </span>
                                            <input type="text" id="txt_Mobile" data-trigger="focus" data-placement="top" placeholder="Mobile" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your mobile number" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_Mobile">
                                                <b>* This field is required</b></label>
                                        </div>
                                         <div class="col-md-4">
                                            <br />
                                            <span class="size13 dark">Phone : </span>
                                            <input type="text" id="txt_Phone" data-trigger="focus" data-toggle="popover" data-placement="top" placeholder="Phone" class="form-control" data-content="This field is mandatory" data-original-title="Here you can create your Phone" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_Phone">
                                                <b>* This field is required</b></label>
                                        </div>
                                       
                                    </div>
                                    <br />
                                                                                                
                                   
                                <%--    <div class="row">
                                        <br />
                                        <div class="col-md-4">
                                            <span class="size13 dark">Agency Name : </span>
                                            <input type="text" id="txt_AgencyName" data-trigger="focus" data-placement="top" placeholder="Agency Name" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit agency name" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_AgencyName">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-4">
                                            <span class="size13 dark">Agency Mail: </span>
                                            <input type="text" id="txt_Email" data-trigger="focus" data-placement="top" placeholder="E-mail" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit agency mail" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_Email">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-4">
                                            <span class="size13 dark">Phone : </span>
                                            <input type="text" id="txt_Phone" data-trigger="focus" data-placement="top" placeholder="Phone" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit phone number" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_Phone">
                                                <b>* This field is required</b></label>
                                        </div>
                                    </div>--%>
                                  <%--  <div class="row">
                                        <div class="col-md-4">
                                            <br />
                                            <span class="size13 dark">Fax : </span>
                                            <input type="text" id="txt_Fax" data-trigger="focus" data-placement="top" placeholder="Fax" class="form-control" data-content="This field is optional" data-original-title="Here you can edit agency fax number" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_Fax">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-8">
                                            <br />
                                            <span class="size13 dark">Website URL: </span>
                                            <input type="text" id="txt_Website" data-trigger="focus" data-placement="top" placeholder="Website" class="form-control" data-content="This field is optional" data-original-title="Here you provide agency's website url" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_Website">
                                                <b>* This field is required</b></label>
                                        </div>

                                    </div>--%>
                                    <br />
                                    <br />
                                    <span class="size16 bold">Address Details</span>
                                    <div class="line2"></div>

                                    <div class="row">
                                        <br />
                                        <div class="col-md-12">
                                            <span class="size13 dark">Address : </span>
                                            <input type="text" id="txt_Address" data-trigger="focus" data-placement="top" placeholder="Address" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit address" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_Address">
                                                <b>* This field is required</b></label>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <br />
<%--                                        <div class="col-md-4">
                                            <span class="size13 dark">Country : </span>
                                            <select id="selCountry" class="form-control">
                                                <option value="-" selected="selected">Select Any Country</option>
                                            </select>
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_Country">
                                                <b>* This field is required</b></label>
                                        </div>--%>
                                        <div class="col-md-4">
                                            <span class="size13 dark">City : </span>
                                            <select id="selCity" class="form-control">
                                                <option selected="selected" value="-">Select Any City</option>
                                            </select>
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_City">
                                                <b>* This field is required</b></label>
                                        </div>
                                       <%-- <div class="col-md-4">
                                            <span class="size13 dark">Pin Code : </span>
                                            <input type="text" id="txt_PinCode" data-trigger="focus" data-placement="top" placeholder="Pin Code" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit pin code" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_PinCode">
                                                <b>* This field is required</b></label>
                                        </div>--%>
                                    </div>
                                   <%-- <br />
                                    <br />
                                    <span class="size16 bold">Other Details</span>
                                    <div class="line2"></div>--%>

                                <%--    <div class="row">
                                        <br />
                                        <div class="col-md-4">
                                            <span class="size13 dark">IATA STATUS : </span>
                                            <select id="Select_IATA" class="form-control">
                                                <option selected="selected" value="0">Select Status</option>
                                                <option value="1">Approved</option>
                                                <option value="2">UnApproved</option>

                                            </select>
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_Select_IATA">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-4">
                                            IATA No :
                                       
                                            <input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_IATA" placeholder="IATA No" disabled="disabled" class="form-control" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_IATA">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-4">
                                            <span class="size13 dark">Preferred Currency :</span>
                                            <select id="SelCurrency" class="form-control">
                                                <option selected="selected" value="0">Select Currency</option>
                                                <option value="AED">AED</option>
                                                <option value="INR">INR</option>
                                                <option value="USD">USD</option>
                                                <option value="PKR">PKR</option>
                                            </select>
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_Currency">
                                                <b>* This field is required</b></label>
                                        </div>

                                    </div>--%>
                                   <%-- <div class="row">
                                        <br />
                                        <div class="col-md-4">
                                            <span class="size13 dark">PAN No : </span>
                                            <input type="text" style="cursor: text" id="txt_PAN" data-trigger="focus" data-placement="top" placeholder="PAN No" class="form-control" data-content="This field is optional" data-original-title="Here you can edit pan number" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_PAN">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-4">
                                            <span class="size13 dark">Service Tax Number : </span>
                                            <input type="text" style="cursor: text" id="txt_ReferenceNumber" data-trigger="focus" data-placement="top" placeholder="Reference Number" class="form-control" data-content="This field is optional" data-original-title="Here you can edit service text number" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_ReferenceNumber">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-4">
                                            <span class="size13 dark">How did you hear about us?</span>
                                            <select id="SelReferral" class="form-control">
                                                <option selected="selected" value="0">Select Referral</option>
                                                <option value="e_mail_marketing">E-mail Marketing</option>
                                                <option value="business_to_consumer">Trade Show</option>
                                                <option value="search_engine">Search Engine</option>
                                                <option value="advertisement">Advertisement</option>
                                                <option value="business_associate_referral">Business Associate Referral</option>
                                            </select>
                                        </div>

                                    </div>--%>
                                  <%--  <div class="row">
                                        <div class="col-md-12">
                                            <br />
                                            <span class="size13 dark">Remarks : </span>
                                            <input type="text" id="txt_Remarks" data-trigger="focus" data-placement="top" placeholder="Remarks" class="form-control" data-content="This field is optional" data-original-title="Here you can enter your comments" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_Remarks">
                                                <b>* This field is required</b></label>
                                        </div>

                                    </div>--%>
                                     <%--  <br />
                                    <br />
                                                                     <span class="size16 bold">Review and read important information!</span>
                                    <div class="line2"></div>
                                    <br />
                                    <div class="alert alert-info">
                                        Attention! Please read important flight information!<br>
                                        <p class="size12">
                                            • You have chosen the version offered by foreign partners. In case of visa issue refusal, disease, etc. the 
refund without penalty provisions is impossible! The ticket will refunded according to the airline rules.
                                        </p>
                                    </div>--%>
                                   <%-- <div style="color: #999">
                                        <input type="checkbox" id="chkTermsAndConditions" style="margin-top: 1.5px;" />
                                        By selecting to complete this registration I acknowledge that I have read and accept the --%>
                                        <%--<a href="#" data-toggle="modal" data-target="#TermsModal" class="clblue">terms &amp; conditions</a>--%>
                                      <%--  <a href="TermsAndConditions.aspx" target="_blank" class="clblue">terms &amp; conditions</a>
                                    </div>--%>
                                    <br />
                                    <br />
                                    <table align="right">
                                        <tr>
                                            <td>
                                                <input type="button" id="btn_RegiterAgent" value="Register" class="btn-search margtop-2" onclick="return ValidateLogin()" style="width: 100%;" />
                                            </td>
                                            <td>
                                                <input type="button" id="btn_Cancel" value="Back" class="btn-search margtop-2" onclick="goBack();" style="width: 100%;" />
                                            </td>
                                            <%--<td>
                                                <input type="button" id="btn_RegiterAgent" value="Register" class="btn-search margtop-2" onclick="CheckTermsCondition" style="width: 100%;" />
                                            </td>--%>
                                        </tr>
                                    </table>
                                    <br />
                                </div>
                                </div>
                        </form>
                    </div>
                        </div>
                    </div>
                </div>
                <div class="line3">
                </div>
                <!--<div class="hpadding50c">

                    <div class="line3">
                    </div>
                    <br />
                    <div class="clearfix">
                    </div>
                </div>-->
                <!--<div class="clearfix">
                </div>
                <br />
                <br />-->
            </div>

            <!-- END CONTENT -->
            </div>
        </div>
</asp:Content>
