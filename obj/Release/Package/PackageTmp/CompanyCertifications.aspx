﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="CompanyCertifications.aspx.cs" Inherits="CUTUK.CompanyCertifications" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-title-container">
        <div class="container">
            <div class="page-title pull-left">
                <h2 class="entry-title">Company Certifications</h2>
            </div>
            <ul class="breadcrumbs pull-right">
                <li><a href="#">HOME</a></li>
                <li class="active">Company Certifications</li>
            </ul>
        </div>
    </div>

    <section id="content">
        <div class="container">
            <div id="main">
                <div class="items-container isotope row image-box style9">
                    <div class="iso-item col-xs-12 col-sms-6 col-sm-6 col-md-4 filter-all filter-island filter-beach">
                        <article class="box">
                            <figure>
                                <a class="hover-effect" title="" href="#">
                                    <img width="370" height="190" alt="" src="images/iatalogo-cerificate.png"></a>
                            </figure>
                        </article>
                    </div>
                    <div class="iso-item col-xs-12 col-sms-6 col-sm-6 col-md-4 filter-all filter-island filter-beach">
                        <article class="box">
                            <figure>
                                <a class="hover-effect" title="" href="#">
                                    <img width="370" height="190" alt="" src="images/trip-advisor-certificate-logo.png"></a>
                            </figure>
                        </article>
                    </div>
                    <div class="iso-item col-xs-12 col-sms-6 col-sm-6 col-md-4 filter-all filter-beach filter-ocean">
                        <article class="box">
                            <figure>
                                <a class="hover-effect" title="" href="#">
                                    <img width="370" height="190" alt="" src="images/cce-cerification.png"></a>
                            </figure>
                        </article>
                    </div>
                    <div class="iso-item col-xs-12 col-sms-6 col-sm-6 col-md-4 filter-all filter-countries">
                        <article class="box">
                            <figure>
                                <a class="hover-effect" title="" href="#">
                                    <img width="370" height="190" alt="" src="images/ca-certificate-logo.png"></a>
                            </figure>
                        </article>
                    </div>
                    <div class="iso-item col-xs-12 col-sms-6 col-sm-6 col-md-4 filter-all filter-beach filter-island">
                        <article class="box">
                            <figure>
                                <a class="hover-effect" title="" href="#">
                                    <img width="370" height="190" alt="" src="images/indian-govt.jpg"></a>
                            </figure>
                        </article>
                    </div>
                    <div class="iso-item col-xs-12 col-sms-6 col-sm-6 col-md-4 filter-all filter-adventure filter-ocean">
                        <article class="box">
                            <figure>
                                <a class="hover-effect" title="" href="#">
                                    <img width="370" height="190" alt="" src="images/trip-advisor-certificate-logo-16.jpg"></a>
                            </figure>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
