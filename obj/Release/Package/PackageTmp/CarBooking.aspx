﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="CarBooking.aspx.cs" Inherits="CUTUK.CarBooking" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-title-container">
        <div class="container">
            <div class="page-title pull-left">
                <h2 class="entry-title">Car Booking</h2>
            </div>
            <ul class="breadcrumbs pull-right">
                <li><a href="#">HOME</a></li>
                <li class="active">Car Booking</li>
            </ul>
        </div>
    </div>

    <section id="content" class="gray-area">
        <div class="container">
            <div id="main">
                <div class="row">
                    <div class="col-sm-8 col-md-12">
                        <div class="car-list">
                            <div class="row image-box car listing-style1">
                                <div class="col-sms-6 col-sm-6 col-md-4">
                                    <article class="box">
                                        <figure>
                                            <a title="" href="#">
                                                <img alt="" src="images/Tata-Indigo-eCS-LS.jpg"></a>
                                        </figure>
                                        <div class="details">
                                            <h4 class="box-title text-center">Tata Indigo<small>Rate of Car</small></h4>
                                            <p class="mile"><span class="skin-color">4hrs &amp; 40kms: </span>1500 INR</p>
                                            <p class="mile"><span class="skin-color">8Hrs &amp; 80Kms: </span>1800 INR</p>
                                            <p class="mile"><span class="skin-color">Extra KM :</span>14 INR</p>
                                            <p class="mile"><span class="skin-color">Extra HOUR :</span>125 INR</p>
                                            <p class="mile"><span class="skin-color">Driver Allowance:</span>250 INR</p>
                                            <p class="mile"><span class="skin-color">Overnight charge:</span>250 INR</p>
                                            <div class="action">
                                                <a class="button btn-small full-width" href="#">SELECT NOW</a>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                                <div class="col-sms-6 col-sm-6 col-md-4">
                                    <article class="box">
                                        <figure>
                                            <a title="" href="#">
                                                <img alt="" src="images/Maruti-Suzuki-Dzire-Toyota-Ethios-Tata-Manza.jpg"></a>
                                        </figure>
                                        <div class="details">
                                            <h4 class="box-title text-center">Maruti Suzuki Dzire/ Toyota Ethios/ Tata Manza<small>Rate of Car</small></h4>
                                            <p class="mile"><span class="skin-color">4hrs &amp; 40kms: </span>1300 INR</p>
                                            <p class="mile"><span class="skin-color">8Hrs &amp; 80Kms: </span>1700 INR</p>
                                            <p class="mile"><span class="skin-color">Extra KM :</span>15 INR</p>
                                            <p class="mile"><span class="skin-color">Extra HOUR :</span>150 INR</p>
                                            <p class="mile"><span class="skin-color">Driver Allowance:</span>250 INR</p>
                                            <p class="mile"><span class="skin-color">Overnight charge:</span>250 INR</p>
                                            <div class="action">
                                                <a class="button btn-small full-width" href="#">SELECT NOW</a>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                                <div class="col-sms-6 col-sm-6 col-md-4">
                                    <article class="box">
                                        <figure>
                                            <a title="" href="#">
                                                <img alt="" src="images/toyota-innova-b.jpg"></a>
                                        </figure>
                                        <div class="details">
                                            <h4 class="box-title text-center">Toyota Innova<small>Rate of Car</small></h4>
                                            <p class="mile"><span class="skin-color">4hrs &amp; 40kms: </span>1900 INR</p>
                                            <p class="mile"><span class="skin-color">8Hrs &amp; 80Kms: </span>2300 INR</p>
                                            <p class="mile"><span class="skin-color">Extra KM :</span>16 INR</p>
                                            <p class="mile"><span class="skin-color">Extra HOUR :</span>175 INR</p>
                                            <p class="mile"><span class="skin-color">Driver Allowance:</span>300 INR</p>
                                            <p class="mile"><span class="skin-color">Overnight charge:</span>300 INR</p>
                                            <div class="action">
                                                <a class="button btn-small full-width" href="#">SELECT NOW</a>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8 col-md-12">
                        <div class="car-list">
                            <div class="row image-box car listing-style1">
                                <div class="col-sms-6 col-sm-6 col-md-4">
                                    <article class="box">
                                        <figure>
                                            <a title="" href="#">
                                                <img alt="" src="images/honda-city.jpg"></a>
                                        </figure>
                                        <div class="details">
                                            <h4 class="box-title text-center">Honda City<small>Rate of Car</small></h4>
                                            <p class="mile"><span class="skin-color">4hrs &amp; 40kms: </span>2100 INR</p>
                                            <p class="mile"><span class="skin-color">8Hrs &amp; 80Kms: </span>2600 INR</p>
                                            <p class="mile"><span class="skin-color">Extra KM :</span>25 INR</p>
                                            <p class="mile"><span class="skin-color">Extra HOUR :</span>175 INR</p>
                                            <p class="mile"><span class="skin-color">Driver Allowance:</span>300 INR</p>
                                            <p class="mile"><span class="skin-color">Overnight charge:</span>300 INR</p>
                                            <div class="action">
                                                <a class="button btn-small full-width" href="#">SELECT NOW</a>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                                <div class="col-sms-6 col-sm-6 col-md-4">
                                    <article class="box">
                                        <figure>
                                            <a title="" href="#">
                                                <img alt="" src="images/c_h_1.jpg"></a>
                                        </figure>
                                        <div class="details">
                                            <h4 class="box-title text-center">Toyota Corolla Altis<small>Rate of Car</small></h4>
                                            <p class="mile"><span class="skin-color">4hrs &amp; 40kms: </span>2300 INR</p>
                                            <p class="mile"><span class="skin-color">8Hrs &amp; 80Kms: </span>3000 INR</p>
                                            <p class="mile"><span class="skin-color">Extra KM :</span>28 INR</p>
                                            <p class="mile"><span class="skin-color">Extra HOUR :</span>200 INR</p>
                                            <p class="mile"><span class="skin-color">Driver Allowance:</span>300 INR</p>
                                            <p class="mile"><span class="skin-color">Overnight charge:</span>300 INR</p>
                                            <div class="action">
                                                <a class="button btn-small full-width" href="#">SELECT NOW</a>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                                <div class="col-sms-6 col-sm-6 col-md-4">
                                    <article class="box">
                                        <figure>
                                            <a title="" href="#">
                                                <img alt="" src="images/Camry-Hybrid-Toyota.jpg"></a>
                                        </figure>
                                        <div class="details">
                                            <h4 class="box-title text-center">Toyota Camry<small>Rate of Car</small></h4>
                                            <p class="mile"><span class="skin-color">4hrs &amp; 40kms: </span>3800 INR</p>
                                            <p class="mile"><span class="skin-color">8Hrs &amp; 80Kms: </span>4850 INR</p>
                                            <p class="mile"><span class="skin-color">Extra KM :</span>48 INR</p>
                                            <p class="mile"><span class="skin-color">Extra HOUR :</span>500 INR</p>
                                            <p class="mile"><span class="skin-color">Driver Allowance:</span>400 INR</p>
                                            <p class="mile"><span class="skin-color">Overnight charge:</span>400 INR</p>
                                            <div class="action">
                                                <a class="button btn-small full-width" href="#">SELECT NOW</a>
                                            </div>
                                        </div>
                                    </article>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
